//
//  HUDAdder.h
//  Dea Sharing
//
//  Created by bijinhkarim on 14/10/13.
//  Modified by Jojin Johnson on 14/10/13.
//

#import <Foundation/Foundation.h>

@interface HUDManager : NSObject

+ (void)addHUDWithLabel:(NSString *)text;
+ (void)addHUDWithLabel:(NSString *)text dimBackground:(BOOL)status;
+ (void)hideHUDFromWindow;
+ (void)hideHUDFromWindowAfterDelay:(NSInteger)seconds;
+ (void)showHUDPercentage:(NSString *)value;

@end
