//
//  HUDAdder.m
//  Dea Sharing
//
//  Created by bijinhkarim on 14/10/13.
//  Modified by Jojin Johnson on 14/10/13.
//

#import "HUDManager.h"

#import "IBGlobal.h"

@implementation HUDManager

+ (void)addHUDWithLabel:(NSString *)text
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[SharedAppDelegate window] animated:YES];
    hud.labelText = text;
    [hud setColor:[UIColor blackColor]];
    //NSLog(@"The mbprogress is Appear");

}

+ (void)addHUDWithLabel:(NSString *)text dimBackground:(BOOL)status
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[SharedAppDelegate window] animated:YES];
    hud.labelText = text;
    [hud setColor:[UIColor blackColor]];
    hud.dimBackground = status;
    //NSLog(@"The mbprogress with label is Appear");

}

+ (void)hideHUDFromWindow
{
    for(UIView *view in [SharedAppDelegate window].subviews)
    {
        if([view isKindOfClass:[MBProgressHUD class]])
        {
            [MBProgressHUD hideHUDForView:[SharedAppDelegate window] animated:YES];
        }
    }
}

+ (void)hideHUDFromWindowAfterDelay:(NSInteger)seconds
{
    for(UIView *view in [SharedAppDelegate window].subviews)
    {
        if([view isKindOfClass:[MBProgressHUD class]])
        {
            MBProgressHUD *hud = (MBProgressHUD *)view;
            [hud hide:YES afterDelay:seconds]; 
        }
    }
}

+ (void)showHUDPercentage:(NSString *)value
{
    MBProgressHUD *hud = nil;
    for(UIView *view in [SharedAppDelegate window].subviews)
    {
        if([view isKindOfClass:[MBProgressHUD class]])
        {
            hud = (MBProgressHUD *)view;
            //NSLog(@"The mbprogress is here");
        }
    }
    
    if(hud) {
        hud.detailsLabelText = value;
    }
}

@end
