//
//  IAPHelper.h
//  In App Rage
//
//  Created by Ray Wenderlich on 9/5/12.
//  Copyright (c) 2012 Razeware LLC. All rights reserved.
//

#import <StoreKit/StoreKit.h>
#import "IBTypes.h"


UIKIT_EXTERN NSString *const IAPHelperProductPurchasedNotification;
UIKIT_EXTERN NSString *const IAPHelperRestoreFinishedNotification;
extern NSString *const IBInAppPurchaseIdentifierIsoleBella; 
extern NSString *const IBInAppPurchaseIdentifierIsoleMadre;

typedef void (^RequestProductsCompletionHandler)(BOOL success, NSArray * products);

@interface IAPHelper : NSObject 

@property (nonatomic, strong, readonly) NSArray <SKProduct *> *products;
//
- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers;
- (void)requestProductsWithCompletionHandler:(RequestProductsCompletionHandler)completionHandler;
- (void)buyProduct:(SKProduct *)product;
- (BOOL)productPurchased:(NSString *)productIdentifier;
- (void)restoreCompletedTransactions;

@end

@interface IAPHelper (LocalPrice)

- (NSString *)localPriceWithProduct:(SKProduct *)product;
+(void)setPurchaseRestored:(BOOL)flag for:(IBType)type;
+(BOOL)isPurchaseRestoredForType:(IBType)type;

@end
