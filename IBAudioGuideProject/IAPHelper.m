//
//  IAPHelper.m
//  In App Rage
//
//  Created by Ray Wenderlich on 9/5/12.
//  Copyright (c) 2012 Razeware LLC. All rights reserved.
//

// 1
#import "IAPHelper.h"
#import "IBGlobal.h"
#import "HUDManager.h"

NSString *const IAPHelperProductPurchasedNotification = @"IAPHelperProductPurchasedNotification";
NSString *const IAPHelperRestoreFinishedNotification = @"IAPHelperRestoreFinishedNotification";
NSString *const KIBInappPurchaseRestored = @"__inappPurchaseRestored__";

NSString *const IBInAppPurchaseIdentifierIsoleBella = @"com.isoleborromee.isolemadre";
NSString *const IBInAppPurchaseIdentifierIsoleMadre = @"com.isoleborromee.isolemadre2017";

// 2
@interface IAPHelper () <SKProductsRequestDelegate, SKPaymentTransactionObserver>

@property (nonatomic, strong, readwrite) NSArray <SKProduct *>* products;

@end

// 3
@implementation IAPHelper {
    SKProductsRequest * _productsRequest;
    RequestProductsCompletionHandler _completionHandler;
    
    NSSet * _productIdentifiers;
    NSMutableSet * _purchasedProductIdentifiers;
}

- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers {
    //
    if ((self = [super init])) {
        // Store product identifiers
        _productIdentifiers = productIdentifiers;
        _purchasedProductIdentifiers = [NSMutableSet set];
        //
        for (NSString * productIdentifier in _productIdentifiers) {
            //
            BOOL productPurchased = [[NSUserDefaults standardUserDefaults] boolForKey:productIdentifier];
            if (productPurchased) {
                //
                [_purchasedProductIdentifiers addObject:productIdentifier];
                NSInteger locationNo = [SharedAppDelegate locationNo];
                 [[SharedAppDelegate database] updatePurchaseOfLocationNo:locationNo withStatus:YES];
                 NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                 [defaults setBool:YES forKey:kComboPurchaseKey];
                 [defaults synchronize];
            } else {
                NSLog(@"Not purchased: %@", productIdentifier);
            }
        }
        //
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    }
    //
    return self;
}

- (void)requestProductsWithCompletionHandler:(RequestProductsCompletionHandler)completionHandler {
    // 1
    _completionHandler = [completionHandler copy];
    NSSet *productIds = _productIdentifiers;
 //   productIds = [NSSet setWithArray:@[@"com.isoleborromee.isolemadre"]];
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    // 2
    NSLog(@"_productIdentifiers = %@", productIds);
    _productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:productIds];
    _productsRequest.delegate = self;
    [_productsRequest start];
}

- (BOOL)productPurchased:(NSString *)productIdentifier {
    //
    return [_purchasedProductIdentifiers containsObject:productIdentifier];
}

- (void)buyProduct:(SKProduct *)product {
    //
    if (product) {
        SKPayment * payment = [SKPayment paymentWithProduct:product];
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }
}

#pragma mark - SKProductsRequestDelegate

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    //
    [HUDManager addHUDWithLabel:nil]; 
    _productsRequest = nil;
    //
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"InAppProducts" withExtension:@"plist"];
    NSArray *arrayIdentifiers = [NSArray arrayWithContentsOfURL:url];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:nil ascending:YES];
    NSArray *arraySortedIdentifiers = [arrayIdentifiers sortedArrayUsingDescriptors:@[sortDescriptor]];
    NSArray * skProducts = response.products;
    self.products = skProducts;
    //
    for (SKProduct * skProduct in skProducts) {
        //
        NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
        [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
        [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [numberFormatter setLocale:skProduct.priceLocale];
        NSString *formattedString = [numberFormatter stringFromNumber:skProduct.price];
        //
        if([arraySortedIdentifiers[0] isEqualToString:skProduct.productIdentifier]) [SharedAppDelegate setPgComboPrizeString:formattedString];
        else if ([arraySortedIdentifiers[1] isEqualToString:skProduct.productIdentifier]) [SharedAppDelegate setPgLeastPrizeString:formattedString];
        else [SharedAppDelegate setPgDefaultPrizeString:formattedString];
    }
    //
    _completionHandler(YES, skProducts);
    _completionHandler = nil;
    //
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    });
    
    [HUDManager hideHUDFromWindow];
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {
    //
     [HUDManager hideHUDFromWindow];
    //
    _productsRequest = nil;
    _completionHandler(NO, nil);
    _completionHandler = nil;
    //
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        //
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    });
}

#pragma mark SKPaymentTransactionOBserver

-(void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue {
    //
    [[NSUserDefaults standardUserDefaults] setInteger:Once_restored forKey:kFirstRestoreKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperRestoreFinishedNotification object:[_purchasedProductIdentifiers allObjects] userInfo:nil];
    //
    for (SKPaymentTransaction *transaction in queue.transactions) {
        //
        NSString *productID = transaction.payment.productIdentifier;
    }
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions {
    //
    //NSLog(@"SKPaymentTransaction updatedTransactions...");
    for (SKPaymentTransaction * transaction in transactions) {
        //
        switch (transaction.transactionState) {
                //
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
            case SKPaymentTransactionStatePurchasing:
                //NSLog(@"SKPaymentTransactionStatePurchasing");
            default:
                break;
        }
    }
}

- (void)completeTransaction:(SKPaymentTransaction *)transaction {
    //
    //NSLog(@"SKPaymentTransaction completeTransaction");
    
    [self provideContentForProductIdentifier:transaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void)restoreTransaction:(SKPaymentTransaction *)transaction {
    //NSLog(@"SKPaymentTransaction restoreTransaction");
    
    [self provideContentForProductIdentifier:transaction.originalTransaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void)failedTransaction:(SKPaymentTransaction *)transaction {
    
    //NSLog(@"SKPaymentTransaction failedTransaction");
    if (transaction.error.code != SKErrorPaymentCancelled)
    {
        //NSLog(@"SKPaymentTransaction Transaction error: %@", transaction.error.localizedDescription);
    }
    
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void)provideContentForProductIdentifier:(NSString *)productIdentifier {
    //
    [_purchasedProductIdentifiers addObject:productIdentifier];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:productIdentifier];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductPurchasedNotification object:productIdentifier userInfo:nil];
}

- (void)restoreCompletedTransactions {
    //
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error {
    //
    //NSLog(@"SKPaymentTransaction restoreCompletedTransactionsFailedWithError");
}

@end

@implementation  IAPHelper (LocalPrice)

- (NSString *)localPriceWithProduct:(SKProduct *)product {
    //
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [formatter setLocale:[product priceLocale]];
    
    return [formatter stringFromNumber:[product price]];
}

+(void)setPurchaseRestored:(BOOL)flag for:(IBType)type {
    //
    id key = restoreKeyForType(type);
    [[NSUserDefaults standardUserDefaults] setBool:flag forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(BOOL)isPurchaseRestoredForType:(IBType)type {
    //
    id key = restoreKeyForType(type);
    return [[NSUserDefaults standardUserDefaults] boolForKey:key];
}

static inline NSString *restoreKeyForType(IBType type) {
    return [NSString stringWithFormat:@"%@_%lu", KIBInappPurchaseRestored, (unsigned long)type];
} 


@end
