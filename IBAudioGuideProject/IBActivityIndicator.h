//
//  IBActivityIndicator.h
//  IBActivityIndicator
//
//  Created by Jojin Johnson on 29/05/14.
//  Copyright (c) 2013 Jojin Johnson. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
	IBActivityIndicationStyleGradient,
	IBActivityIndicationStyleSegment,
	IBActivityIndicationStyleSegmentLarge
} IBActivityIndicationStyle;

typedef enum {
	IBActivityIndicatorViewStyleGray,
	IBActivityIndicatorViewStyleWhite,
	IBActivityIndicatorViewStyleWhiteLarge
} IBActivityIndicatorViewStyle;

@interface IBActivityIndicator : UIView

@property (nonatomic) BOOL hidesWhenStopped;
@property (nonatomic) IBActivityIndicationStyle indicationStyle;
@property (nonatomic) IBActivityIndicatorViewStyle indicatorViewStyle;

- (void)startAnimating;
- (void)stopAnimating;

@end