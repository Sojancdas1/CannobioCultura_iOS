//
//  IBActivityIndicator.m
//  IBActivityIndicator
//
//  Created by Jojin Johnson on 29/05/14.
//  Copyright (c) 2013 Jojin Johnson. All rights reserved.
//

#import "IBActivityIndicator.h"

@interface IBActivityIndicator ()

@property (nonatomic) BOOL animating;
@property (nonatomic) CGFloat angle;
@property (strong, nonatomic) UIImageView *activityImageView;

@end

@implementation IBActivityIndicator

- (id)init {
	return [self initWithFrame:CGRectZero];
}

- (id)initWithFrame:(CGRect)frame {
	CGRect fixedFrame = {frame.origin, {21, 21}};
	self = [super initWithFrame:fixedFrame];
	if (self) {
		[self setupView];
	}
	
	return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
	self = [super initWithCoder:aDecoder];
	if (self) {
		[self setupView];
	}
	
	return self;
}

- (void)startAnimating {
	self.animating = YES;
	self.hidden = NO;
}

- (void)stopAnimating {
	self.animating = NO;
	
	self.hidden = self.hidesWhenStopped;
	
	// reset to default position
	self.angle = 0.0f;
	CGAffineTransform transform = CGAffineTransformMakeRotation(0.0f);
	self.activityImageView.transform = transform;
}

- (void)setIndicatorStyle:(IBActivityIndicationStyle)indicationStyle {
	_indicationStyle = indicationStyle;
	
	NSMutableString *imageName = [NSMutableString stringWithString:@"IBActivityIndicator.bundle/"];
	
	switch (indicationStyle) {
		case IBActivityIndicationStyleGradient:
			[imageName appendString:@"activity_gradient"];
			break;
            
		case IBActivityIndicationStyleSegment:
			[imageName appendString:@"activity_segment"];
			break;
			
		case IBActivityIndicationStyleSegmentLarge:
			[imageName appendString:@"activity_segment_full"];
			break;
            
		default:
			break;
	}
	
	// Set the style conforming native UIActivityIndicatorView constants.
	switch (self.indicatorViewStyle) {
		case IBActivityIndicatorViewStyleGray:
			[imageName appendString:@"_gray"];
			break;
			
		case IBActivityIndicatorViewStyleWhite:
			[imageName appendString:@"_white"];
			break;
			
		case IBActivityIndicatorViewStyleWhiteLarge:
			// TODO: Create large white images
			[imageName appendString:@"_white"];
			break;
			
		default:
			break;
	}
	
	UIImage *indicatorImage = [UIImage imageNamed:imageName];
	
	if (!self.activityImageView) {
		self.activityImageView = [[UIImageView alloc] initWithImage:indicatorImage];
	}
	
	[self.activityImageView setImage:indicatorImage];
}

- (void)setIndicatorViewStyle:(IBActivityIndicatorViewStyle)indicatorViewStyle
{
    _indicatorViewStyle = indicatorViewStyle;
    
    [self setIndicatorStyle:self.indicationStyle];
}

#pragma mark - Private Methods

- (void)setupView {
	// Default Value is to start animated and to hide when stopped
	self.animating = YES;
	self.hidesWhenStopped = YES;
	self.indicatorViewStyle = IBActivityIndicatorViewStyleGray;
	
	// Configure the parent view
	[self setBackgroundColor:[UIColor clearColor]];
	
	[self setIndicationStyle:IBActivityIndicationStyleGradient];
	
	self.angle = 0.0f;
	
	NSTimer *timer = [NSTimer timerWithTimeInterval:0.02 target:self selector:@selector(handleTimer:) userInfo:nil repeats:YES];
	[[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    
	[self addSubview:self.activityImageView];
}

- (void)handleTimer:(NSTimer *)timer {
	if (self.animating)
		self.angle += 0.13f;
	
	if (self.angle > 6.283)
		self.angle = 0.0f;
    
	CGAffineTransform transform = CGAffineTransformMakeRotation(self.angle);
	self.activityImageView.transform = transform;
}

@end
