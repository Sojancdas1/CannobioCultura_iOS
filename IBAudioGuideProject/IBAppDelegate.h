//
//  IBAppDelegate.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 07/05/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IBAudioGuideInfo.h"
#import "IBDataManager.h"
#import "IBAudioPlayerView.h"
#import "IBAudioPlayerViewTablet.h"
#import "IAPHelper.h"
#import "IBRestoreDownloader.h"

#import "IBMasterSplitViewController.h"
#import "IBLocationViewControllerTablet.h"
#import "IBCatogoryListViewControllerTablet.h"
#import "IBCatogoryContentViewControllerTablet.h"
#import "IBDirectDownloadViewController.h"
#import "IBInAppComboPurchaseViewController.h"
#import "IBInAppSinglePurchaseViewController.h"
#import "IBCollectionViewController.h"
#import "IBAudioGuideViewControllerTablet.h"
#import "IBChildSplitViewController.h"
#import "IBGuideDetailViewControllerTablet.h"
#import "IBGeoMapViewControllerTablet.h"
#import "IBGuideDetailViewController.h"
#import "audioSlider.h"

#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import <Crashlytics/Crashlytics.h>

#import "TestFairy.h"


/*  http://www.e-zenitsviluppo.it/ios/borromee-mobile/isole-bella/map.xml
 http://www.e-zenitsviluppo.it/ios/borromee-mobile/isole-bella/map.zip
 http://www.e-zenitsviluppo.it/ios/borromee-mobile/isole-bella/photo-gallery.zip
 http://www.e-zenitsviluppo.it/ios/borromee-mobile/isole-bella/en/photo-gallery.xml
 http://www.e-zenitsviluppo.it/ios/borromee-mobile/isole-bella/en/audio-guides.zip
 http://www.e-zenitsviluppo.it/ios/borromee-mobile/isole-bella/guides-gallery.zip
 http://www.e-zenitsviluppo.it/ios/borromee-mobile/isole-bella/en/audio-guides.xml
 http://www.e-zenitsviluppo.it/ios/borromee-mobile/isole-bella/en/audio-guides.xml
 //
 http://www.e-zenitsviluppo.it/ios/borromee-mobile/isole-madre/map.xml
 http://www.e-zenitsviluppo.it/ios/borromee-mobile/isole-madre/photo-gallery.zip
 http://www.e-zenitsviluppo.it/ios/borromee-mobile/isole-madre/en/photo-gallery.xml
 //
 http://www.e-zenitsviluppo.it/ios/borromee-mobile/rocca-angera/map.xml
 http://www.e-zenitsviluppo.it/ios/borromee-mobile/rocca-angera/photo-gallery.zip
 http://www.e-zenitsviluppo.it/ios/borromee-mobile/rocca-angera/en/photo-gallery.xml
 */ 

@interface IBAppDelegate : UIResponder <UIApplicationDelegate>



@property (readonly, strong, nonatomic) IBDataManager *database;
@property (readwrite, strong, nonatomic) NSString *resourcePath;
@property (readonly, strong, nonatomic) NSDictionary *langDictionary;
@property (nonatomic,readwrite)BOOL isDetailPageAppeared;
@property (strong, nonatomic) AVAudioPlayer *avAudioPlayer;

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) IBAudioPlayerView *playerView;
@property (strong, nonatomic) IBAudioPlayerViewTablet *playerViewTablet;
@property (readwrite, nonatomic) BOOL playerHidden;
@property (readwrite, nonatomic) BOOL sliderHidden;
@property (readwrite, nonatomic) BOOL wasPlayerHidden;
@property (readwrite, nonatomic) UINavigationController *currentNavigation;

@property (readwrite, strong, nonatomic)  IBAudioGuideInfo *currentGuide;
@property (readwrite, strong, nonatomic)  NSMutableArray *audioGuideInfos;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (readwrite, nonatomic) IBType locationNo;
@property (readwrite, nonatomic) NSInteger languageIndex;
@property (readwrite, nonatomic) CGFloat zoomScale;

@property (readwrite, nonatomic) NSString *pgLeastPrizeString;
@property (readwrite, nonatomic) NSString *pgDefaultPrizeString;
@property (readwrite, nonatomic) NSString *pgComboPrizeString;
@property (readwrite, nonatomic) NSString *selectedGuideTitleForGallery;

@property (readwrite, strong, nonatomic) NSString *lockMessage;
@property (nonatomic) CGFloat currentSystemVolume;
@property (nonatomic, readonly) BOOL isPlPlayerBussy;



//code written for specifically ipad
@property (readwrite, nonatomic) IBMasterSplitViewController *masterSplitController;
@property (readwrite, nonatomic) IBLocationViewControllerTablet *locationViewController;
@property (readwrite, nonatomic) IBChildSplitViewController *childSplitViewController;
@property (readwrite, nonatomic) IBCatogoryListViewControllerTablet *categoryListController;
@property (readwrite, nonatomic) IBCatogoryContentViewControllerTablet *categoryContentController;
@property (readwrite, nonatomic) IBGuideDetailViewControllerTablet *guideDetailViewController;
@property (nonatomic, readwrite) IBDirectDownloadViewController *directDownloadController;
@property (nonatomic, readwrite) IBInAppComboPurchaseViewController *comboInAppController;
@property (nonatomic, readwrite) IBInAppSinglePurchaseViewController *singleInAppController;
@property (nonatomic, readwrite) IBCollectionViewController *collectionViewController;
@property (nonatomic, readwrite) IBAudioGuideViewControllerTablet *audioGuideViewController;
@property (nonatomic, readwrite) IBGeoMapViewControllerTablet *geoMapViewController;
@property (nonatomic, readwrite) IBGuideDetailViewController *guideDetailViewControllerPhone;
@property (nonatomic, readwrite) audioSlider *slider;
@property (nonatomic, readwrite)BOOL isSubRoom;

//

-(void)stopAudioAndHidePlayer;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

- (void)loadLanguageDictionary;
- (void)animatePlayerHidden:(BOOL)status;

- (NSString *)readResourceValueForKey:(NSString *)key;
- (void)writeResourceValue:(NSString *)value forKey:(NSString *)key;

- (void)startPlayerProgressTimer;
- (void)stopPlayerProgressTimer;

- (void)lockInteractions;
- (void)unlockInteractions;

- (void)showAlertWithRestoreDownloader:(IBRestoreDownloader *)restoreDownloader;
- (void)hideAlertRestoreDownloader;

-(void)pauseAudioPlayer;
- (void)playButtonTriggered;

@end

