//
//  IBAppDelegate.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 07/05/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBAppDelegate.h"

#import "IBGlobal.h"
#import "MBProgressHUD.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import <Crashlytics/Crashlytics.h>

#import "IBInAppHelper.h"

@interface IBAppDelegate ()<IBAudioPlayerViewDelegate,IBAudioPlayerViewTabletDelegate,AVAudioPlayerDelegate,AVAudioSessionDelegate,MBProgressHUDDelegate>
{
//    NSNumberFormatter *playerDurationformatter;
    MBProgressHUD *alertHud;
}

@property (assign, nonatomic) BOOL interruptedOnPlayback;

@property (strong, nonatomic) NSTimer *playerProgressTimer;


- (void)updateGuideInfoToLockScreen;
- (NSInteger)getIndexOfCurrentObject;

- (void)handleMediaServicesReset;
- (void)handleAudioSessionInterruption:(NSNotification*)notification;

- (void)updateProgressBar:(NSTimer *)timer;

@end

@implementation IBAppDelegate

@synthesize locationNo = _locationNo;
@synthesize languageIndex = _languageIndex;
@synthesize zoomScale = _zoomScale;

@synthesize database = _database;
@synthesize resourcePath = _resourcePath;
@synthesize langDictionary = _langDictionary;

@synthesize window = _window;
@synthesize playerView = _playerView;
@synthesize playerViewTablet = _playerViewTablet;
@synthesize playerHidden = _playerHidden;
@synthesize wasPlayerHidden = _wasPlayerHidden;
@synthesize currentNavigation = _currentNavigation;

@synthesize interruptedOnPlayback = _interruptedOnPlayback;
@synthesize avAudioPlayer = _avAudioPlayer;
@synthesize playerProgressTimer = _playerProgressTimer;
@synthesize currentGuide = _currentGuide;
@synthesize audioGuideInfos = _audioGuideInfos;

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

@synthesize pgLeastPrizeString = _pgLeastPrizeString;
@synthesize pgDefaultPrizeString = _pgDefaultPrizeString;
@synthesize pgComboPrizeString = _pgComboPrizeString;
@synthesize currentSystemVolume = _currentSystemVolume;

@synthesize lockMessage = _lockMessage;

@synthesize slider = _slider;
@synthesize sliderHidden = _sliderHidden;
@synthesize isDetailPageAppeared = _isDetailPageAppeared;
@synthesize isSubRoom = _isSubRoom;

@synthesize masterSplitController;
@synthesize locationViewController;
@synthesize categoryListController;
@synthesize categoryContentController;
@synthesize guideDetailViewController;
@synthesize directDownloadController;
@synthesize comboInAppController;
@synthesize singleInAppController;
@synthesize collectionViewController;
@synthesize audioGuideViewController;
@synthesize childSplitViewController;
@synthesize geoMapViewController, selectedGuideTitleForGallery, guideDetailViewControllerPhone;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    if (![self _isUpdatedCoreData]) {
        [self _deleteOldCoreData];
    }
    _isSubRoom = NO;
 
    _isDetailPageAppeared = NO;
//    [Crashlytics startWithAPIKey:@"43960652841bbc13ad7334828b0097530f31638a"];
    [TestFairy begin:@"8e946f4428ef03b2885b06a68c21bb479fd0d0c3"];
 
    _resourcePath = nil;
    _langDictionary = nil;
    _playerProgressTimer = nil;
    
    _locationNo = -1;
    _languageIndex = -1;
    
    _database = [IBDataManager sharedInstance];
    
    [_database makeLanguageIndexesIfNotExists];
    [_database makeLocationIndexesIfNotExists];
    
    
    NSArray *arrayLanguages = [_database fetchLanguageIndexes];
    //
    NSLog(@"arrayLanguages = %@", arrayLanguages);
    
    if([[[[NSUserDefaults standardUserDefaults] valueForKey:@"LanguagePreferred"] stringValue] isEqualToString:@"1"]) {
        
        NSString *langString = [[NSUserDefaults standardUserDefaults] valueForKey:@"LanguageString"];
        if ([langString isEqualToString:ENGLISH ]) {
            //
            [self setLanguageIndex:0];
            [self setResourcePath:[NSString stringWithFormat:@"%@/%@",[IBGlobal createDocumentsDirectoryByAppendingPathComponent:nil],[[arrayLanguages objectAtIndex:0] valueForKey:@"resourcePath"]]];
        } else if ([langString isEqualToString:ITALIAN])  {
            //
            [self setLanguageIndex:1];
            [self setResourcePath:[NSString stringWithFormat:@"%@/%@",[IBGlobal createDocumentsDirectoryByAppendingPathComponent:nil],[[arrayLanguages objectAtIndex:1] valueForKey:@"resourcePath"]]];
        } else if ([langString isEqualToString:FRENCH])  {
            //
            [self setLanguageIndex:2];
            [self setResourcePath:[NSString stringWithFormat:@"%@/%@",[IBGlobal createDocumentsDirectoryByAppendingPathComponent:nil],[[arrayLanguages objectAtIndex:2] valueForKey:@"resourcePath"]]];
        }else if ([langString isEqualToString: DEUTSCH ])  {
            //
            [self setLanguageIndex:3];
            [self setResourcePath:[NSString stringWithFormat:@"%@/%@",[IBGlobal createDocumentsDirectoryByAppendingPathComponent:nil],[[arrayLanguages objectAtIndex:3] valueForKey:@"resourcePath"]]];
        }
    } else {
        //
        [self setLanguageIndex:0];
        [self setResourcePath:[NSString stringWithFormat:@"%@/%@",[IBGlobal createDocumentsDirectoryByAppendingPathComponent:nil],[[arrayLanguages objectAtIndex:0] valueForKey:@"resourcePath"]]];
    }

    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackTranslucent];
    
    _pgDefaultPrizeString = @"-1";
    [[IBInAppHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        //
        if (success) {
            //
            SKProduct *product = nil;
            for (SKProduct *obj in products) {
                //
                product = obj;
                break;
            }
            //
            if (product) {
                //
                NSString *price = [[IBInAppHelper sharedInstance] localPriceWithProduct:product];
                _pgDefaultPrizeString = [NSString stringWithFormat:@"   %@   ", price];
            }
        }
    }];
    
    _playerHidden = YES;
    _currentNavigation = (UINavigationController *)[[self window] rootViewController];
    
    if(IS_IPHONE) {
        _playerView = [[IBAudioPlayerView alloc] initWithFrame:CGRectMake(0, DEVICE_HEIGHT, DEVICE_WIDTH, 45)];
        _playerView.delegate = self;
        _playerView.hidden = YES;
        [_currentNavigation.view addSubview:_playerView];
    } else {
        //
        CGRect frame = CGRectMake(0, DEVICE_HEIGHT, 930, 65);
        if (IOS_OLDER_THAN(8.0)) frame = CGRectMake(0, DEVICE_WIDTH, 930, 65);
        //
        _playerViewTablet = [[IBAudioPlayerViewTablet alloc] initWithFrame:frame];
        _playerViewTablet.delegate = self;
        [self.window addSubview:_playerViewTablet];
        
        AudioSessionInitialize(NULL, NULL, NULL, NULL);
        AudioSessionSetActive(YES);
        AudioSessionAddPropertyListener(kAudioSessionProperty_CurrentHardwareOutputVolume, volumeListenerCallback, (__bridge void *)(self));
    }

    _interruptedOnPlayback = NO;
    _wasPlayerHidden = YES;
    _avAudioPlayer = nil;
    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleAudioSessionInterruption:) name:AVAudioSessionInterruptionNotification object:[AVAudioSession sharedInstance]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleMediaServicesReset) name:AVAudioSessionMediaServicesWereResetNotification object:[AVAudioSession sharedInstance]];
    
    
    
//    playerDurationformatter= [[NSNumberFormatter alloc] init];
//    [playerDurationformatter setMinimumIntegerDigits:2];
//    [playerDurationformatter setMinimumFractionDigits:2];
//    [playerDurationformatter setDecimalSeparator:@":"];
    
    
//    [[SharedAppDelegate database] updatePurchaseOfLocationNo:1 withStatus:YES];
    
    return YES;
}

-(BOOL)_isUpdatedCoreData {
    //
    NSString *value = [[NSUserDefaults standardUserDefaults] valueForKey:@"__cd__up__flag__"];
    //
    return ([value isEqualToString:@"__YES__"]);
}

-(void)_setAsCoreDataUpdated {
    //
    [[NSUserDefaults standardUserDefaults] setValue:@"__YES__" forKey:@"__cd__up__flag__"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)_deleteOldCoreData {
    //
    NSURL *url = [self _storeURL];
    NSError *error = nil;
    //
    if ([[NSFileManager defaultManager] removeItemAtURL:url error:&error]) {
        //
        [self _setAsCoreDataUpdated];
    } else {
        // abort();
    }
}

void volumeListenerCallback (void *inClientData, AudioSessionPropertyID inID, UInt32 inDataSize, const void *inData) {
    //
    const float *volumePointer = inData;
    float volume = *volumePointer;
    
    if( volume > 0 ) {
        [(__bridge IBAppDelegate *)inClientData playerViewTablet].speakerOn = YES;
    } else {
       [(__bridge IBAppDelegate *)inClientData playerViewTablet].speakerOn = NO;
    }
}
//- (void)volumeChanged:(NSNotification *)notification
//{
//    _currentSystemVolume =
//    [[[notification userInfo]
//      objectForKey:@"AVSystemController_AudioVolumeNotificationParameter"]
//     floatValue];
//   // [delegateForSoundChange physicallySoundChanged:volume];
//    
//    // Do stuff with volume
//}
- (void)startPlayerProgressTimer
{
    if(self.playerProgressTimer)    {
        [self.playerProgressTimer invalidate];
        self.playerProgressTimer = nil;
    }
    
    self.playerProgressTimer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateProgressBar:) userInfo:nil repeats:YES];
    
    if([[AVAudioSession sharedInstance] outputVolume]==0)    {
        [self.playerViewTablet setSpeakerOn:NO];
    }
    else    {
        [self.playerViewTablet setSpeakerOn:YES];
    }
}

- (void)stopPlayerProgressTimer
{
    [self.playerProgressTimer invalidate];
    self.playerProgressTimer = nil;
}

- (void)updateProgressBar:(NSTimer *)timer {
    
    if([_playerViewTablet playing]) {
        //
        NSTimeInterval playTime = [self.avAudioPlayer currentTime];
        NSTimeInterval duration = [self.avAudioPlayer duration];
        float progress = playTime/duration;
        
        [self.playerViewTablet setProgress:progress];
        [self.playerViewTablet.durationLabel setText:[self timeFormatted:duration]];
        [self.playerViewTablet.timeElapsedLabel setText:[self timeFormatted:playTime]];
    }
}

- (NSString *)timeFormatted:(int)totalSeconds {
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    return [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
}

- (void)updateGuideInfoToLockScreen
{
    UIImage *albumArtImg = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[IBGlobal createDocumentsDirectoryByAppendingPathComponent:nil],[_currentGuide thumbnailPath]]];
    MPMediaItemArtwork *albumArt = [[MPMediaItemArtwork alloc] initWithImage:albumArtImg];
    
    NSString *museumName = nil;
    MUSEUMNAME_STR(_locationNo, museumName);
    
    NSDictionary *info;
    if(IS_IPHONE)   {
        info = @{ MPMediaItemPropertyArtwork:albumArt,
                                MPMediaItemPropertyAlbumTitle: museumName,
                                MPMediaItemPropertyTitle:[_playerView titleLabel].text,
                                MPMediaItemPropertyPlaybackDuration: [NSNumber numberWithDouble:_avAudioPlayer.duration],
                                MPNowPlayingInfoPropertyElapsedPlaybackTime: [NSNumber numberWithDouble:_avAudioPlayer.currentTime],
                                MPNowPlayingInfoPropertyPlaybackRate: @1.0};
    } else {
        info = @{ MPMediaItemPropertyArtwork:albumArt,
                  MPMediaItemPropertyAlbumTitle:museumName,
                  MPMediaItemPropertyTitle:[_playerViewTablet titleLabel].text,
                  MPMediaItemPropertyPlaybackDuration: [NSNumber numberWithDouble:_avAudioPlayer.duration],
                  MPNowPlayingInfoPropertyElapsedPlaybackTime: [NSNumber numberWithDouble:_avAudioPlayer.currentTime],
                  MPNowPlayingInfoPropertyPlaybackRate: @1.0};
    }
    
    [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo = info;
}

- (NSInteger)getIndexOfCurrentObject
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"referenceNo == %@", [_currentGuide referenceNo]];
    NSArray *filteredArray = [_audioGuideInfos filteredArrayUsingPredicate:predicate];
    IBAudioGuideInfo *guideInfo = nil;
    if ([filteredArray count] > 0) {
        guideInfo = [filteredArray objectAtIndex:0];
    }else{
        return 0;
    }
    return [_audioGuideInfos indexOfObject:guideInfo];
}

- (void)handleAudioSessionInterruption:(NSNotification*)notification
{
    NSNumber *interruptionType = [[notification userInfo] objectForKey:AVAudioSessionInterruptionTypeKey];
    NSNumber *interruptionOption = [[notification userInfo] objectForKey:AVAudioSessionInterruptionOptionKey];
    
    if(IS_IPHONE)   {
        switch (interruptionType.unsignedIntegerValue) {
            case AVAudioSessionInterruptionTypeBegan:{
                // • Audio has stopped, already inactive
                // • Change state of UI, etc., to reflect non-playing state
                if([_playerView playing])   {
                    [_playerView setPlaying:NO];
                    _interruptedOnPlayback = YES;
                }
            } break;
            case AVAudioSessionInterruptionTypeEnded:{
                // • Make session active
                // • Update user interface
                // • AVAudioSessionInterruptionOptionShouldResume option
                if (interruptionOption.unsignedIntegerValue == AVAudioSessionInterruptionOptionShouldResume) {
                    // Here you should continue playback.
                    if (_interruptedOnPlayback) {
                        [_avAudioPlayer prepareToPlay];
                        [_avAudioPlayer play];
                        [_playerView setPlaying:YES];
                        _interruptedOnPlayback = NO;
                    }
                }
            } break;
            default:
                break;
        }
    }
    else    {
        switch (interruptionType.unsignedIntegerValue) {
            case AVAudioSessionInterruptionTypeBegan:{
                // • Audio has stopped, already inactive
                // • Change state of UI, etc., to reflect non-playing state
                if([_playerViewTablet playing])   {
                    [_playerViewTablet setPlaying:NO];
                    _interruptedOnPlayback = YES;
                }
            } break;
            case AVAudioSessionInterruptionTypeEnded:{
                // • Make session active
                // • Update user interface
                // • AVAudioSessionInterruptionOptionShouldResume option
                if (interruptionOption.unsignedIntegerValue == AVAudioSessionInterruptionOptionShouldResume) {
                    // Here you should continue playback.
                    if (_interruptedOnPlayback) {
                        [_avAudioPlayer prepareToPlay];
                        [_avAudioPlayer play];
                        [_playerViewTablet setPlaying:YES];
                        _interruptedOnPlayback = NO;
                    }
                }
            } break;
            default:
                break;
        }
    }
}

- (void)handleMediaServicesReset {
    // • No userInfo dictionary for this notification
    // • Audio streaming objects are invalidated (zombies)
    // • Handle this notification by fully reconfiguring audio
    
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@",[IBGlobal createDocumentsDirectoryByAppendingPathComponent:nil],[_currentGuide audioFilePath]]];
    
    if(!_avAudioPlayer) {
        _avAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
        _avAudioPlayer.delegate = self;
    }
    else    {
        [_avAudioPlayer stop];
        _avAudioPlayer = [_avAudioPlayer initWithContentsOfURL:url error:nil];
        _avAudioPlayer.delegate = self;
    }
    
    [[AVAudioSession sharedInstance] setDelegate: self];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [[AVAudioSession sharedInstance] setMode:AVAudioSessionModeDefault error:nil];
    [[AVAudioSession sharedInstance] setActive: YES error: nil];
    
    [_avAudioPlayer prepareToPlay];
    [_avAudioPlayer play];
    
    [self updateGuideInfoToLockScreen];
}
-(void)pauseAudioPlayer
{
    [self playButtonTriggered];
}
#pragma mark -
#pragma mark IBAudioPlayerViewDelegate or IBAudioPlayerViewTabletDelegate methods

- (void)playButtonTriggered
{
    if([_avAudioPlayer isPlaying]) [_avAudioPlayer pause];
    else [_avAudioPlayer play];
}

- (void)muteUnmuteTriggered
{
    
}

- (void)autoPlayStateChanged
{
    NSString *museumName = nil;
    NSInteger index = [self getIndexOfCurrentObject];
    IBAudioGuideInfo *guideInfo = [_audioGuideInfos objectAtIndex:index];
    NSArray *arrayMapRooms = [[SharedAppDelegate database] fetchRoomsByAudioGuideReferenceNo:guideInfo.referenceNo];
    NSString *roomNo = (arrayMapRooms.count > 0)?[(IBGeoMapRooms *)[arrayMapRooms objectAtIndex:0] roomNo]:@"";
    NSString *detailText = [NSString stringWithFormat:@"%@ (%@)",[[SharedAppDelegate langDictionary] valueForKey:@"LabelRoomTag"],roomNo];
    UIImage *image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[IBGlobal createDocumentsDirectoryByAppendingPathComponent:nil],guideInfo.thumbnailPath]];
                      
    MUSEUMNAME_STR(_locationNo, museumName);
    
    if(IS_IPHONE)   {
        if((NSNotFound == index) && [_playerView autoPlayEnabled]) {
            //
            _playerView.locationLabel.text = [museumName uppercaseString];
            _playerView.titleLabel.text = guideInfo.guideTitle;
            _playerView.thumbnailImageView.image = image;
            _playerView.detailLabel.text = detailText;
            _playerView.playing = YES;
            
            [self setCurrentGuide:guideInfo];
            [self updateGuideInfoToLockScreen];
        } else {
            
            NSLog(@"yyy");
        }
    } else {
        if((NSNotFound == index) && [_playerViewTablet autoPlayEnabled]) {
            //
            _playerViewTablet.locationLabel.text = [museumName uppercaseString];
            _playerViewTablet.titleLabel.text = guideInfo.guideTitle;
            _playerViewTablet.thumbnailImageView.image = image;
            _playerViewTablet.detailLabel.text = detailText;
            _playerViewTablet.playing = YES;
            
            [self setCurrentGuide:guideInfo];
            [self updateGuideInfoToLockScreen];
        } else {
            
            NSLog(@"xxx");
        }
    }
}

#pragma mark -
#pragma mark AVAudioSessionDelegate methods

- (void)beginInterruption
{
    if(IS_IPHONE)   {
        if([_playerView playing])   {
            [_playerView setPlaying:NO];
            _interruptedOnPlayback = YES;
        }
    } else {
        if([_playerViewTablet playing])   {
            [_playerViewTablet setPlaying:NO];
            _interruptedOnPlayback = YES;
        }
    }
}

- (void)endInterruption
{
    if (_interruptedOnPlayback) {
        [_avAudioPlayer prepareToPlay];
        [_avAudioPlayer play];
        if(IS_IPHONE)   {
            [_playerView setPlaying:YES];
        } else {
            [_playerViewTablet setPlaying:YES];
        }
        _interruptedOnPlayback = NO;
    }
}

#pragma mark -
#pragma mark AVAudioPlayerDelegate methods

- (void)audioPlayerBeginInterruption:(AVAudioPlayer *)player {
    if(IS_IPHONE)   {
        if([_playerView playing])   {
            [_playerView setPlaying:NO];
            _interruptedOnPlayback = YES;
        }
    } else {
        if([_playerViewTablet playing])   {
            [_playerViewTablet setPlaying:NO];
            _interruptedOnPlayback = YES;
        }
    }
}

- (void)audioPlayerEndInterruption:(AVAudioPlayer *)player withOptions:(NSUInteger)flags {
    if (_interruptedOnPlayback) {
        [player prepareToPlay];
        [player play];
        if(IS_IPHONE)   {
            [_playerView setPlaying:YES];
        } else {
            [_playerViewTablet setPlaying:YES];
        }
        _interruptedOnPlayback = NO;
    }
}

- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error
{
    //NSLog(@"audioPlayerDidFinishPlaying");
    
    NSString *museumName = nil;
    NSInteger index = [self getIndexOfCurrentObject];
    IBAudioGuideInfo *guideInfo = [_audioGuideInfos objectAtIndex:index+1];
    MUSEUMNAME_STR(_locationNo, museumName);
    NSArray *arrayMapRooms = [[SharedAppDelegate database] fetchRoomsByAudioGuideReferenceNo:guideInfo.referenceNo];
    NSString *roomNo = (arrayMapRooms.count > 0)?[(IBGeoMapRooms *)[arrayMapRooms objectAtIndex:0] roomNo]:@"";
    NSString *detailText = [NSString stringWithFormat:@"%@ (%@)",[[SharedAppDelegate langDictionary] valueForKey:@"LabelRoomTag"],roomNo];
    UIImage *image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[IBGlobal createDocumentsDirectoryByAppendingPathComponent:nil],guideInfo.thumbnailPath]];
    
    
    if(IS_IPHONE) {
        
        _playerView.playing = NO;
        
        if ([_playerView autoPlayEnabled]) {
            
            if((index+1)<[_audioGuideInfos count])   {
                
                _playerView.locationLabel.text = [museumName uppercaseString];
                _playerView.titleLabel.text = guideInfo.guideTitle;
                _playerView.detailLabel.text = detailText;
                _playerView.thumbnailImageView.image = image;
                
                [self setCurrentGuide:guideInfo];
                [self updateGuideInfoToLockScreen];
                _playerView.playing = YES;
            }
        }
    } else {
        
        _playerViewTablet.playing = NO;
        
        if ([_playerViewTablet autoPlayEnabled]) {
            
            if((index+1)<[_audioGuideInfos count]) {
                //
                _playerViewTablet.locationLabel.text = [museumName uppercaseString];
                _playerViewTablet.titleLabel.text = guideInfo.guideTitle;
                _playerViewTablet.detailLabel.text = detailText;
                _playerViewTablet.thumbnailImageView.image = image;
                
                [self setCurrentGuide:guideInfo];
                [self updateGuideInfoToLockScreen];
                _playerViewTablet.playing = YES;
            }
        }
    }
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    //NSLog(@"audioPlayerDidFinishPlaying");
    
    NSInteger index = [self getIndexOfCurrentObject];
    IBAudioGuideInfo *previousGuideInfo = [_audioGuideInfos objectAtIndex:index];
    IBAudioGuideInfo *guideInfo = nil;
    if ([previousGuideInfo.guideDescription isEqualToString:@"<Desc>-1</Desc>"]) {
        guideInfo = [_audioGuideInfos objectAtIndex:index+2];
    } else {
        guideInfo = [_audioGuideInfos objectAtIndex:index+1];
    }
    NSString *museumName = nil;
    MUSEUMNAME_STR(_locationNo, museumName);
    UIImage *image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[IBGlobal createDocumentsDirectoryByAppendingPathComponent:nil],guideInfo.thumbnailPath]];
    NSInteger roomIntValue  =  [guideInfo.referenceNo integerValue];
    NSString *detailText = [NSString stringWithFormat:@"%@ (%ld)",[[SharedAppDelegate langDictionary] valueForKey:@"LabelRoomTag"],(long)roomIntValue];
    
    if(IS_IPHONE) {
        //
        _playerView.playing = NO;
        
        if([_playerView autoPlayEnabled]) {
            
            if((index+1)<[_audioGuideInfos count]) {
                
                _playerView.locationLabel.text = [museumName uppercaseString];
                _playerView.titleLabel.text = guideInfo.guideTitle;
                _playerView.detailLabel.text = detailText;
                _playerView.thumbnailImageView.image = image;
                
                [self setCurrentGuide:guideInfo];
                [self updateGuideInfoToLockScreen];
                _playerView.playing = YES;
            }
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"audioPlayerDidFinishPlaying" object:nil];
        }
    } else {
        
        _playerViewTablet.playing = NO;
        
        if([_playerViewTablet autoPlayEnabled]) {
            
            
            if((index+1)<[_audioGuideInfos count])   { 
                
                _playerViewTablet.locationLabel.text = [museumName uppercaseString];
                _playerViewTablet.titleLabel.text = guideInfo.guideTitle;
                _playerViewTablet.detailLabel.text = detailText;
                _playerViewTablet.thumbnailImageView.image = image;
                
                [self setCurrentGuide:guideInfo];
                [self updateGuideInfoToLockScreen];
                _playerViewTablet.playing = YES;
            }
        }
    }
}

#pragma mark -

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

- (void)remoteControlReceivedWithEvent:(UIEvent*)event
{
    if (event.type == UIEventTypeRemoteControl) {
        
        if(IS_IPHONE)   {
            switch (event.subtype) {
                    
                case UIEventSubtypeRemoteControlTogglePlayPause:
                {
                    if([_playerView playing])   {
                        [_avAudioPlayer pause];
                    }
                    else    {
                        [_avAudioPlayer play];
                    }
                    
                    [_playerView setPlaying:![_playerView playing]];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"PlayPauseButtonTapped" object:nil];
                }
                    break;
                    
                case UIEventSubtypeRemoteControlPlay:
                {
                    [_avAudioPlayer play];
                    [_playerView setPlaying:YES];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"PlayPauseButtonTapped" object:nil];
                }
                    break;
                    
                case UIEventSubtypeRemoteControlPause:
                {
                    [_avAudioPlayer pause];
                    [_playerView setPlaying:NO];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"PlayPauseButtonTapped" object:nil];
                }
                    break;
                    
                case UIEventSubtypeRemoteControlPreviousTrack:
                {
                    if([_playerView autoPlayEnabled])   {
                        IBAudioGuideInfo *guideInfo = nil;
                        NSInteger index = [self getIndexOfCurrentObject];
                        
                        if((index-1)>=0)   {
                            guideInfo = [_audioGuideInfos objectAtIndex:index-1];
                        }
                        else    {
                            guideInfo = [_audioGuideInfos objectAtIndex:index];
                        }
                        
                        NSString *museumName = nil;
                        MUSEUMNAME_STR(_locationNo, museumName);
                        
                        _playerView.locationLabel.text = [museumName uppercaseString];
                        _playerView.titleLabel.text = guideInfo.guideTitle;
                        _playerView.thumbnailImageView.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[IBGlobal createDocumentsDirectoryByAppendingPathComponent:nil],guideInfo.thumbnailPath]];
                        
                        [self setCurrentGuide:guideInfo];
                        [self updateGuideInfoToLockScreen];
                    }
                    
                }
                    break;
                    
                case UIEventSubtypeRemoteControlNextTrack:
                {
                    if([_playerView autoPlayEnabled])   {
                        IBAudioGuideInfo *guideInfo = nil;
                        NSInteger index = [self getIndexOfCurrentObject];
                        
                        if((index+1)<[_audioGuideInfos count])   {
                            guideInfo = [_audioGuideInfos objectAtIndex:index+1];
                        }
                        else    {
                            guideInfo = [_audioGuideInfos objectAtIndex:index];
                        }
                        
                        NSString *museumName = nil;
                        MUSEUMNAME_STR(_locationNo, museumName);
                        
                        _playerView.locationLabel.text = [museumName uppercaseString];
                        _playerView.titleLabel.text = guideInfo.guideTitle;
                        _playerView.thumbnailImageView.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[IBGlobal createDocumentsDirectoryByAppendingPathComponent:nil],guideInfo.thumbnailPath]];
                        
                        [self setCurrentGuide:guideInfo];
                        [self updateGuideInfoToLockScreen];
                    }
                    
                }
                  break;
                    
                default:
                    break;
            }
        }
        else    {
            switch (event.subtype) {
                    
                case UIEventSubtypeRemoteControlTogglePlayPause:
                {
                    if([_playerViewTablet playing])   {
                        [_avAudioPlayer pause];
                    }
                    else    {
                        [_avAudioPlayer play];
                    }
                    
                    [_playerViewTablet setPlaying:![_playerViewTablet playing]];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"PlayPauseButtonTapped" object:nil];
                }
                    break;
                    
                case UIEventSubtypeRemoteControlPlay:
                {
                    [_avAudioPlayer play];
                    [_playerViewTablet setPlaying:YES];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"PlayPauseButtonTapped" object:nil];
                }
                    break;
                    
                case UIEventSubtypeRemoteControlPause:
                {
                    [_avAudioPlayer pause];
                    [_playerViewTablet setPlaying:NO];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"PlayPauseButtonTapped" object:nil];
                }
                    break;
                    
                case UIEventSubtypeRemoteControlPreviousTrack:
                {
                    if([_playerViewTablet autoPlayEnabled]) {
                        
                        
                        IBAudioGuideInfo *guideInfo = nil;
                        NSInteger index = [self getIndexOfCurrentObject];
                        
                        if((index-1)>=0)   {
                            guideInfo = [_audioGuideInfos objectAtIndex:index-1];
                        }  else {
                            guideInfo = [_audioGuideInfos objectAtIndex:index];
                        }
                        
                        NSString *museumName = nil;
                        MUSEUMNAME_STR(_locationNo, museumName);
                        
                        _playerViewTablet.locationLabel.text = [museumName uppercaseString];
                        _playerViewTablet.titleLabel.text = guideInfo.guideTitle;
                        
                        NSArray *arrayMapRooms = [[SharedAppDelegate database] fetchRoomsByAudioGuideReferenceNo:guideInfo.referenceNo];
                        NSString *roomNo = (arrayMapRooms.count > 0)?[(IBGeoMapRooms *)[arrayMapRooms objectAtIndex:0] roomNo]:@"";
                        NSString *detailText = [NSString stringWithFormat:@"%@ (%@)",[[SharedAppDelegate langDictionary] valueForKey:@"LabelRoomTag"],roomNo];
                        
                        _playerViewTablet.detailLabel.text = detailText;
                        _playerViewTablet.thumbnailImageView.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[IBGlobal createDocumentsDirectoryByAppendingPathComponent:nil],guideInfo.thumbnailPath]];
                        
                        [self setCurrentGuide:guideInfo];
                        [self updateGuideInfoToLockScreen];
                    }
                    
                }
                    break;
                    
                case UIEventSubtypeRemoteControlNextTrack:
                {
                    if([_playerViewTablet autoPlayEnabled])   {
                        IBAudioGuideInfo *guideInfo = nil;
                        NSInteger index = [self getIndexOfCurrentObject];
                        
                        if((index+1)<[_audioGuideInfos count])   {
                            guideInfo = [_audioGuideInfos objectAtIndex:index+1];
                        }
                        else    {
                            guideInfo = [_audioGuideInfos objectAtIndex:index];
                        }
                        
                        NSString *museumName = nil;
                        MUSEUMNAME_STR(_locationNo, museumName);
                        
                        _playerViewTablet.locationLabel.text = [museumName uppercaseString];
                        _playerViewTablet.titleLabel.text = guideInfo.guideTitle;
                        
                        NSArray *arrayMapRooms = [[SharedAppDelegate database] fetchRoomsByAudioGuideReferenceNo:guideInfo.referenceNo];
                        NSString *roomNo = (arrayMapRooms.count > 0)?[(IBGeoMapRooms *)[arrayMapRooms objectAtIndex:0] roomNo]:@"";
                        NSString *detailText = [NSString stringWithFormat:@"%@ (%@)",[[SharedAppDelegate langDictionary] valueForKey:@"LabelRoomTag"],roomNo];
                        
                        [SharedAppDelegate playerViewTablet].detailLabel.text = detailText;
                        [SharedAppDelegate playerView].detailLabel.text = detailText;

                        
                        _playerViewTablet.thumbnailImageView.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[IBGlobal createDocumentsDirectoryByAppendingPathComponent:nil],guideInfo.thumbnailPath]];
                        
                        [self setCurrentGuide:guideInfo];
                        [self updateGuideInfoToLockScreen];
                    }
                    
                }
                    break;
                    
                default:
                    break;
            }
        }
    }
}

-(BOOL)_isAudioPlayerExists {
    //
    return (_avAudioPlayer != nil);
}

-(void)stopAudioAndHidePlayer {
    
    [self _flushAudioPlayer];
    [self _animatePlayerHiddenForIphone:YES];
    self.playerHidden = YES;
        
}

-(void)_flushAudioPlayer {
    //
    if (_avAudioPlayer) [_avAudioPlayer stop];
    _avAudioPlayer = nil;
}

-(AVAudioPlayer *)_newAudioPlayerWith:(NSURL *)url {
    //
    NSError *error = nil;
    AVAudioPlayer *audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    //
    if (error) {
        //
        NSLog(@"unable to create newAudioPlayer error = %@ \n", error.localizedDescription);
        NSLog(@"url = %@", url);
        return nil;
    }
    //
    audioPlayer.delegate = self;
    //
    return audioPlayer;
}

-(NSString *)_audioFilepathFrom:(IBAudioGuideInfo *)guide {
    //
    NSString *basePath = [IBGlobal createDocumentsDirectoryByAppendingPathComponent:nil]; // /Documents
    NSString *filePath = [guide audioFilePath];
    return [NSString stringWithFormat:@"%@/%@",basePath, filePath];
}

- (void)setCurrentGuide:(IBAudioGuideInfo *)currentGuide {
    //
    NSURL *url = [NSURL fileURLWithPath:[self _audioFilepathFrom:currentGuide]];
    AVAudioPlayer *audioPlayer = nil;
    //
    if ([self _isAudioPlayerExists]) [self _flushAudioPlayer];
    //
    if ((audioPlayer = [self _newAudioPlayerWith:url])) {
        //
        [self _updateAudioSession];
        //
        if ([audioPlayer prepareToPlay]) {
            //
            if ([audioPlayer play]) NSLog(@"play");
            else NSLog(@"failed play");
        } else {
            //
            NSLog(@"failed prepareToPlay");
        }
    }
    //
    _currentGuide = currentGuide;
    _avAudioPlayer = audioPlayer;
}



-(void)_updateAudioSession {
    //
    [[AVAudioSession sharedInstance] setDelegate: self];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [[AVAudioSession sharedInstance] setMode:AVAudioSessionModeDefault error:nil];
    [[AVAudioSession sharedInstance] setActive: YES error: nil];
}

/**
- (void)setCurrentGuide:(IBAudioGuideInfo *)currentGuide {
    //
    _currentGuide = currentGuide;
    
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@",[IBGlobal createDocumentsDirectoryByAppendingPathComponent:nil],[_currentGuide audioFilePath]]];
    
    if(!_avAudioPlayer) {
        _avAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
        _avAudioPlayer.delegate = self;
    } else {
        [_avAudioPlayer stop];
        _avAudioPlayer = [_avAudioPlayer initWithContentsOfURL:url error:nil];
        _avAudioPlayer.delegate = self;
    }
    
    [[AVAudioSession sharedInstance] setDelegate: self];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [[AVAudioSession sharedInstance] setMode:AVAudioSessionModeDefault error:nil];
    [[AVAudioSession sharedInstance] setActive: YES error: nil];
    
    [_avAudioPlayer prepareToPlay];
    [_avAudioPlayer play];
}
*/

- (void)setAudioInfos:(NSMutableArray *)audioGuideInfos
{
    _audioGuideInfos = audioGuideInfos;
}

- (UIWindow *)window
{
    if (!_window) {
        _window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        _window.backgroundColor = [UIColor whiteColor];
    }
    return _window;
}

- (UIViewController *)deepestPresentedViewControllerOf:(UIViewController *)viewController
{
    if (viewController.presentedViewController) {
        return [self deepestPresentedViewControllerOf:viewController.presentedViewController];
    } else {
        return viewController;
    }
}

- (UIViewController *)topViewController
{
    UIViewController *rootViewController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    UIViewController *deepestPresentedViewController = [self deepestPresentedViewControllerOf:rootViewController];
    if ([deepestPresentedViewController isKindOfClass:[UINavigationController class]]) {
        return ((UINavigationController *)deepestPresentedViewController).topViewController;
    } else {
        return deepestPresentedViewController;
    }
}

-(BOOL)isPlPlayerBussy {
    //
    return (self.avAudioPlayer != nil);
}
-(void)setSliderHidden:(BOOL)sliderHidden
{
    if (!sliderHidden) {
        
        if(IOS_OLDER_THAN(8.0)) {
            
             _slider = [[audioSlider alloc]initWithFrame:CGRectMake(890, DEVICE_WIDTH - 263, 40, 200)];
            [[SharedAppDelegate masterSplitController].view addSubview:_slider];
        } else {
            
            _slider = [[audioSlider alloc]initWithFrame:CGRectMake(890, DEVICE_WIDTH - 520, 40, 200)];
            [[SharedAppDelegate masterSplitController].view addSubview:_slider];
        }
        
         _slider.backgroundColor=RGBCOLOR(119, 119, 119);
        _slider.layer.cornerRadius = 10;
        _slider.clipsToBounds = YES;
    } else {
        
        if(IOS_OLDER_THAN(8.0)) {
            [_slider removeFromSuperview];
        } else {
            [_slider removeFromSuperview];
        }
    }
}

- (void)setPlayerHidden:(BOOL)playerHidden
{
    
    if(IS_IPHONE)   {
        [_playerView refreshView];
        
        if(_playerHidden==playerHidden)   {
            [_currentNavigation.view addSubview:_playerView];
            return;
        }
        
        _playerHidden = playerHidden;
        
        CGRect rect = _playerView.frame;
        
        if(playerHidden == NO)   {
            rect.origin.y = DEVICE_HEIGHT-45;
        } else {
            rect.origin.y = DEVICE_HEIGHT;
        }
        
        _playerView.frame = rect;
        [_currentNavigation.view addSubview:_playerView];
    } else {
        [_playerViewTablet refreshView];
        
        if(_playerHidden==playerHidden)   {
            if(IOS_OLDER_THAN(8.0)) {
                [_currentNavigation.view addSubview:_playerViewTablet];
            } else {
                [[SharedAppDelegate masterSplitController].view addSubview:_playerView];
            }
            return;
        }
        
        _playerHidden = playerHidden;
        
        CGRect rect = _playerViewTablet.frame;
        
        CGFloat height;
        if(IOS_OLDER_THAN(8.0)) {
            height = DEVICE_WIDTH;
        } else {
            height = DEVICE_HEIGHT;
        }
        
        if(playerHidden == NO)   {
            rect.origin.y = height-65;
        } else {
            rect.origin.y = height;
        }
        
        _playerViewTablet.frame = rect;
        
        if(IOS_OLDER_THAN(8.0)) {
            [_currentNavigation.view addSubview:_playerViewTablet];
        } else {
            [[SharedAppDelegate masterSplitController].view addSubview:_playerView];
        }
    }
}

- (void)_animatePlayerHiddenForIphone:(BOOL)status {
    //
    if (!_playerView) return;
    
    [_playerView refreshView];
    
    CGRect rect;
    
   // if(_playerHidden != status) {
        //
        _playerView.hidden = NO;
        _playerHidden = status;
        rect = _playerView.frame;
        
        if (status) {
            rect.origin.y = DEVICE_HEIGHT;
        } else {
            rect.origin.y = DEVICE_HEIGHT-45;
        }
        _playerView.frame = rect;
        [_currentNavigation.view addSubview:_playerView]; 
        
        [UIView animateWithDuration:1.0 animations:^{
            //
            _playerView.frame = rect;
        }];
        //
     //   _playerHidden = status;
  //  }
    [_currentNavigation.view addSubview:_playerView]; 
}

- (void)_animatePlayerHiddenForIpad:(BOOL)status {
    //
    [_playerViewTablet refreshView];
    
    if(_playerHidden == status) {
        //
        if(IOS_OLDER_THAN(8.0)) [_currentNavigation.view addSubview:_playerViewTablet];
        else [[SharedAppDelegate masterSplitController].view addSubview:_playerView];
    } else {
        //
        _playerHidden = status;
        
        CGRect rect = _playerViewTablet.frame;
        
        CGFloat height;
        if(IOS_OLDER_THAN(8.0)) height = DEVICE_WIDTH;
        else height = DEVICE_HEIGHT;
        
        if(status == NO) rect.origin.y = height;
        else rect.origin.y = height-65;
        
        _playerViewTablet.frame = rect;
        [_currentNavigation.view addSubview:_playerViewTablet];
        
        if(status == NO) rect.origin.y = height-65;
        else rect.origin.y = height;
        
        [UIView animateWithDuration:1.0 animations:^{
            _playerViewTablet.frame = rect;
        }];
    }
}

- (void)animatePlayerHidden:(BOOL)status
{
    if(IS_IPHONE)   {
        [self _animatePlayerHiddenForIphone:status];
    } else {
         [self _animatePlayerHiddenForIpad:status];
    }
}

- (void)setLanguageIndex:(NSInteger)languageIndex
{
    _languageIndex = languageIndex;
}

- (void)setLocationNo:(NSInteger)locationNo
{
    _locationNo = locationNo;
}

- (void)loadLanguageDictionary
{
    NSString *langFilePath = [_resourcePath stringByAppendingPathComponent:@"/lang.plist"];
    _langDictionary = [[NSDictionary alloc] initWithContentsOfFile:langFilePath];
}

- (void)setResourcePath:(NSString *)resourcePath
{
    _resourcePath = resourcePath;
    [self loadLanguageDictionary];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    if(IS_IPHONE)   {
        if([_playerView playing])   {
            [self updateGuideInfoToLockScreen];
        }
    } else {
        if([_playerViewTablet playing])   {
            [self updateGuideInfoToLockScreen];
        }
        //
        AudioSessionRemovePropertyListenerWithUserData(kAudioSessionProperty_CurrentHardwareOutputVolume, volumeListenerCallback, (__bridge void *)(self));
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    if(IS_IPAD) {
        AudioSessionInitialize(NULL, NULL, NULL, NULL);
        AudioSessionSetActive(YES);
        AudioSessionAddPropertyListener(kAudioSessionProperty_CurrentHardwareOutputVolume, volumeListenerCallback, (__bridge void *)(self));
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            //NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"IBAudioGuideProject" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.

-(NSURL *)_storeURL {
    //
    return [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"IBAudioGuideProject.sqlite"];
}
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [self _storeURL];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        //NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSString *)readResourceValueForKey:(NSString *)key
{
    NSMutableDictionary *resource = [[NSMutableDictionary alloc] initWithContentsOfFile:[_resourcePath stringByAppendingPathComponent:@"/lang.plist"]];
    id value = [resource objectForKey:key];
    
    return value;
}

- (void)writeResourceValue:(NSString *)value forKey:(NSString *)key
{
    NSMutableDictionary *data = [[NSMutableDictionary alloc] initWithContentsOfFile:[_resourcePath stringByAppendingPathComponent:@"/lang.plist"]];
    [data setObject:value forKey:key];
    
    [data writeToFile:[_resourcePath stringByAppendingPathComponent:@"/lang.plist"] atomically:YES];
    [self loadLanguageDictionary];
}

- (void)lockInteractions
{
    [self.categoryListController setLocked:YES];
    [self.locationViewController setLockInteractionWithLocation:YES];
}

- (void)unlockInteractions
{
    [self.categoryListController setLocked:NO];
    [self.locationViewController setLockInteractionWithLocation:NO];
}

- (void)showAlertWithRestoreDownloader:(IBRestoreDownloader *)restoreDownloader
{
    alertHud = [[MBProgressHUD alloc] initWithView:[SharedAppDelegate window]];
    
    alertHud.color = [UIColor clearColor];
    alertHud.tintColor = RGBCOLOR(71, 117, 184);
    
    alertHud.minSize = CGSizeMake(restoreDownloader.bounds.size.width+30, restoreDownloader.bounds.size.height+30);
    alertHud.opacity = 0.5;
    [[SharedAppDelegate window] addSubview:alertHud];
    
    alertHud.delegate = self;
    alertHud.customView = restoreDownloader;
    alertHud.mode = MBProgressHUDModeCustomView;
    alertHud.dimBackground = YES;
    
    if(IS_IPHONE)   {
        alertHud.margin = 0;
    }
    
    [alertHud show:YES];
}

- (void)hideAlertRestoreDownloader
{
    [alertHud hide:YES afterDelay:1.0];
}

- (void)hudWasHidden:(MBProgressHUD *)hud
{
    [alertHud removeFromSuperview];
}

@end



