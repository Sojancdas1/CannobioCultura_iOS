//
//  IBAudioGuide.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 23/07/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class IBGuideGallery, IBMuseums;

@interface IBAudioGuide : NSManagedObject

@property (nonatomic, retain) NSString * audioFilePath;
@property (nonatomic, retain) NSString * guideDescription;
@property (nonatomic, retain) NSString * guideTitle;
@property (nonatomic, retain) NSString * referenceNo;
@property (nonatomic, retain) NSString * thumbnailPath;
@property (nonatomic, retain) NSString *isSubArray;
@property (nonatomic, retain) NSSet *audioGuideToGuideGallery;
@property (nonatomic, retain) NSString *mainExpandRow;
@property (nonatomic, retain) IBMuseums *audioGuideToMuseum;
@end

@interface IBAudioGuide (CoreDataGeneratedAccessors)

- (void)addAudioGuideToGuideGalleryObject:(IBGuideGallery *)value;
- (void)removeAudioGuideToGuideGalleryObject:(IBGuideGallery *)value;
- (void)addAudioGuideToGuideGallery:(NSSet *)values;
- (void)removeAudioGuideToGuideGallery:(NSSet *)values;

@end
