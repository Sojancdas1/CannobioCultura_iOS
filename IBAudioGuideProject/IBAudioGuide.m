//
//  IBAudioGuide.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 23/07/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBAudioGuide.h"
#import "IBGuideGallery.h"
#import "IBMuseums.h"


@implementation IBAudioGuide

@dynamic audioFilePath;
@dynamic guideDescription;
@dynamic guideTitle;
@dynamic referenceNo;
@dynamic thumbnailPath;
@dynamic audioGuideToGuideGallery;
@dynamic audioGuideToMuseum;
@synthesize isSubArray,mainExpandRow;
@end
