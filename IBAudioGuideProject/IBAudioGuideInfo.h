//
//  IBAudioGuideInfo.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 26/06/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IBAudioGuideInfo : NSObject

@property (nonatomic, retain) NSString * referenceNo;
@property (nonatomic, retain) NSString * guideTitle;
@property (nonatomic, retain) NSString * guideDescription;
@property (nonatomic, retain) NSString * audioFilePath;
@property (nonatomic, retain) NSString * thumbnailPath;

@end

@interface IBAudioGuideInfo (Equatable)

-(BOOL)isEqual:(IBAudioGuideInfo *)object;
@end
