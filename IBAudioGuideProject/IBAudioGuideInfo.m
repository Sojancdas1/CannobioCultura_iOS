//
//  IBAudioGuideInfo.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 26/06/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBAudioGuideInfo.h"

@implementation IBAudioGuideInfo

@synthesize referenceNo;
@synthesize guideTitle;
@synthesize guideDescription;
@synthesize audioFilePath;
@synthesize thumbnailPath;

@end

@implementation IBAudioGuideInfo (Equatable)

-(BOOL)isEqual:(IBAudioGuideInfo *)object {
    return [self.audioFilePath isEqualToString:object.audioFilePath];
}


@end
