//
//  IBAudioGuideTable.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 02/07/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IBAudioGuideInfo;

@protocol IBAudioGuideTableDelegate <NSObject>
- (void)audioGuideSelected:(IBAudioGuideInfo *)guideInfo;
@end

typedef enum {
	IBTagDirectionUp,
	IBTagDirectionDown,
	IBTagDirectionTopLeft,
    IBTagDirectionBottomLeft,
    IBTagDirectionTopRight,
    IBTagDirectionBottomRight
} IBTagDirection;

@interface IBAudioGuideTable : UIView
{
    NSString *_roomNo;
    NSMutableArray *_arrayAudioGuide;
    IBTagDirection _tagDirection;
    
    __unsafe_unretained id<IBAudioGuideTableDelegate> _delegate;
}
@property (nonatomic, strong) NSString *roomNo;
@property (nonatomic, strong, readonly) NSMutableArray *arrayAudioGuide;
@property (nonatomic, assign) IBTagDirection tagDirection;

@property (nonatomic, assign) id<IBAudioGuideTableDelegate> delegate;

@end
