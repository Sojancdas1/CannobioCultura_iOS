//
//  IBAudioGuideTable.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 02/07/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBAudioGuideTable.h"

#import "IBGlobal.h"
#import "IBAudioGuideTableViewCell.h"

@interface IBAudioGuideTable () <UITableViewDelegate,UITableViewDataSource>
{
    NSInteger _currentIndex;
    UITableView *_tableView;
    NSString *_stringCode, *_stringPosition;
    UIImageView *bgImageView;
}

@property (nonatomic, assign) NSInteger currentIndex;
@property (nonatomic, strong) NSString *stringCode;
@property (nonatomic, strong) NSString *stringPosition;

@property (nonatomic, strong, readonly) UITableView *tableView;

@end

@implementation IBAudioGuideTable

@synthesize roomNo = _roomNo;
@synthesize tableView = _tableView;
@synthesize currentIndex = _currentIndex;
@synthesize arrayAudioGuide = _arrayAudioGuide;
@synthesize stringCode = _stringCode;
@synthesize stringPosition = _stringPosition;
@synthesize delegate = _delegate;
@synthesize tagDirection = _tagDirection;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        bgImageView.backgroundColor = [UIColor clearColor];
        [self addSubview:bgImageView];
        
        _tableView = [[UITableView alloc] initWithFrame:self.bounds style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.bounces = NO;
        
        _tableView.userInteractionEnabled = YES;
        _tableView.exclusiveTouch = YES;
        
        _tableView.clipsToBounds = YES;
        _tableView.layer.cornerRadius = 10.0f;
        _tableView.layer.borderWidth = 1.0f;
        _tableView.layer.borderColor = [UIColor grayColor].CGColor;
       
        _tableView.layer.shadowOffset = CGSizeMake(-3, -3);
        _tableView.layer.shadowColor = [[UIColor blackColor] CGColor];
        _tableView.layer.shadowRadius = 10.0f;
        _tableView.layer.shadowOpacity = 0.80f;
        
        [self insertSubview:_tableView belowSubview:bgImageView];
        
        if([_tableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [_tableView setSeparatorInset:UIEdgeInsetsZero];
        }
        
        [_tableView setSeparatorColor:[UIColor grayColor]];
        
        NSDictionary *langDictionary = [SharedAppDelegate langDictionary];
        
        _stringCode = [langDictionary valueForKey:@"LabelCode"];
        _stringPosition = [langDictionary valueForKey:@"LabelPosition"];
        
        _arrayAudioGuide = nil;
        _currentIndex = -1;
        
        self.clipsToBounds = YES;
        self.tagDirection = IBTagDirectionDown;
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    bgImageView.frame = self.bounds;
    self.tagDirection = _tagDirection;
}

#pragma mark - Table view data source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_arrayAudioGuide count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 110;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{ 
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = (IBAudioGuideTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[IBAudioGuideTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell.imageView setHidden:NO];
    }
    IBAudioGuide *audioGuideInfo = [_arrayAudioGuide objectAtIndex:[indexPath row]];
    
   cell.imageView.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[IBGlobal createDocumentsDirectoryByAppendingPathComponent:nil],audioGuideInfo.thumbnailPath]];
    cell.textLabel.text = audioGuideInfo.guideTitle;
    cell.textLabel.font = kAppFontSemibold(30);
    
    NSString *subTitle = [NSString stringWithFormat:@"%@: %@",_stringCode,audioGuideInfo.referenceNo];
    
    NSMutableAttributedString *mutableAttributedString= [[NSMutableAttributedString alloc] initWithString:subTitle];
    [mutableAttributedString addAttribute:NSFontAttributeName value:kAppFontSemibold(22) range:NSMakeRange(0, [subTitle length])];
    [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:RGBCOLOR(124, 124, 124) range:NSMakeRange(0, [subTitle length])];
    
    cell.detailTextLabel.attributedText = mutableAttributedString;
    
    return cell;
}

#pragma mark -
#pragma mark - Table view delegates

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    // do something here
    _currentIndex = indexPath.row;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _currentIndex = indexPath.row;
    [_delegate audioGuideSelected:[_arrayAudioGuide objectAtIndex:_currentIndex]];
}

#pragma mark -

- (void)setRoomNo:(NSString *)roomNo
{
    _roomNo = roomNo;
    
//                NSString *newCutString = [[_roomNo componentsSeparatedByCharactersInSet:
//                                           [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
//                                          componentsJoinedByString:@""];
    
    //NSLog(@"%@",roomNo);
    
//    NSArray *arrayAudioGuides = [[SharedAppDelegate database] fetchAudioGuidesByRoomNo:[roomNo integerValue]];
    
    NSString *roomNoXXU = [roomNo stringByTrimmingCharactersInSet:[NSCharacterSet lowercaseLetterCharacterSet]];
    
    NSArray *arrayAudioGuides = [[SharedAppDelegate database] fetchAudioGuidesByRoomNo:roomNoXXU];

    
    [_arrayAudioGuide removeAllObjects];
    NSString *previousReferNumber;
    NSMutableArray *arrayAudioGuideInfos = [NSMutableArray array];
    for(IBAudioGuide *audioGuide in arrayAudioGuides)
    {
        if ([audioGuide.guideDescription isEqualToString:@"<Desc>-1</Desc>"])
        {
            
        }
        else
        {
            if ([previousReferNumber isEqualToString:audioGuide.referenceNo])
            {
                
            }
            else
            {
       
        IBAudioGuideInfo *audioGuideInfo = [[IBAudioGuideInfo alloc] init];
        audioGuideInfo.guideDescription = audioGuide.guideDescription;
        audioGuideInfo.audioFilePath = audioGuide.audioFilePath;
        audioGuideInfo.guideTitle = audioGuide.guideTitle;
        audioGuideInfo.referenceNo = audioGuide.referenceNo;
        audioGuideInfo.thumbnailPath = audioGuide.thumbnailPath;
        [arrayAudioGuideInfos addObject:audioGuideInfo];
        previousReferNumber = audioGuide.referenceNo;
            }
        }
    }
    
    _arrayAudioGuide = [NSMutableArray arrayWithArray:arrayAudioGuideInfos];
    
    [_tableView reloadData];
}

- (void)setTagDirection:(IBTagDirection)tagDirection
{
    _tagDirection = tagDirection;
    
    CGRect rect = self.bounds;
    if(tagDirection == IBTagDirectionDown)  {
        rect.origin.y = 0;
        rect.size.height = rect.size.height-30;
        bgImageView.frame = CGRectMake((self.bounds.size.width/2)-15, self.bounds.size.height-31, 0, 0);
        bgImageView.image = [UIImage imageNamed:@"slider-bottom-arrow.png"];
    }
    else if (tagDirection == IBTagDirectionTopLeft)    {
        rect.origin.x = 30;
        rect.size.width = rect.size.width-30;
        rect.size.height = rect.size.height-30;
        bgImageView.frame = CGRectMake(0, 10, 0, 0);
        bgImageView.image = [UIImage imageNamed:@"slider-left-arrow.png"];
    }
    else if (tagDirection == IBTagDirectionBottomLeft)  {
        rect.origin.x = 30;
        rect.size.width = rect.size.width-30;
        rect.size.height = rect.size.height-30;
        bgImageView.frame = CGRectMake(0, self.bounds.size.height-70, 0, 0);
        bgImageView.image = [UIImage imageNamed:@"slider-left-arrow.png"];
    }
    else if (tagDirection == IBTagDirectionTopRight)    {
        rect.origin.x = 0;
        rect.size.width = rect.size.width-30;
        rect.size.height = rect.size.height-30;
        bgImageView.frame = CGRectMake(self.bounds.size.width-32, 10, 0, 0);
        bgImageView.image = [UIImage imageNamed:@"slider-right-arrow.png"];
    }
    else if(tagDirection == IBTagDirectionBottomRight)  {
        rect.origin.x = 0;
        rect.size.width = rect.size.width-30;
        rect.size.height = rect.size.height-30;
        bgImageView.frame = CGRectMake(self.bounds.size.width-32, self.bounds.size.height-70, 0, 0);
        bgImageView.image = [UIImage imageNamed:@"slider-right-arrow.png"];
    }
    else if (tagDirection == IBTagDirectionUp)    {
        rect.origin.y = 30;
        rect.size.height = rect.size.height-30;
        bgImageView.frame = CGRectMake((self.bounds.size.width/2)-15, 1, 0, 0);
        bgImageView.image = [UIImage imageNamed:@"slider-top-arrow.png"];
    }
    
    _tableView.frame = rect;
}

@end
