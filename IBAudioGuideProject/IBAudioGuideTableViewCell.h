//
//  IBAudioGuideTableViewCell.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 17/06/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBTableViewCellBase.h"


@interface IBAudioGuideTableViewCell : IBTableViewCellBase
@property (nonatomic, copy) void (^additionButtonTapAction)(id sender);
@property (nonatomic) BOOL additionButtonHidden;

- (void)setupWithTitle:(NSString *)title detailText:(NSString *)detailText level:(NSInteger)level additionButtonHidden:(BOOL)additionButtonHidden;
- (void)setAdditionButtonHidden:(BOOL)additionButtonHidden animated:(BOOL)animated;


@end
