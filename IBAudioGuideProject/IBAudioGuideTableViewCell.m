//
//  IBAudioGuideTableViewCell.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 17/06/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBAudioGuideTableViewCell.h"

@implementation IBAudioGuideTableViewCell



#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark -
#pragma mark accessors

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat width = self.frame.size.width;
    CGFloat height = self.frame.size.height;
    
    [self.imageView setFrame:CGRectMake(0, 0, height, height)];
    
    [self.textLabel setFrame:CGRectMake(height+5, 10, width-height-35, (height/2)-10)];
    [self.detailTextLabel setFrame:CGRectMake(height+5, (height/2)-5, width-height-35, height/2)]; 
}

- (void)setupWithTitle:(NSString *)title detailText:(NSString *)detailText level:(NSInteger)level additionButtonHidden:(BOOL)additionButtonHidden
{
	//self.customTitleLabel.text = title;
 // self.detailedLabel.text = detailText;
	//  self.additionButtonHidden = additionButtonHidden;
	
	//  if (level == 0) {
	//    self.detailTextLabel.textColor = [UIColor blackColor];
	//  }
	
	if (level == 0) {
		self.backgroundColor = UIColorFromRGB(0xF7F7F7);
	} else if (level == 1) {
		self.backgroundColor = UIColorFromRGB(0xD1EEFC);
	} else if (level >= 2) {
		self.backgroundColor = UIColorFromRGB(0xE0F8D8);
	}
	
//	CGFloat left = 11 + 20 * level;
	
//	CGRect titleFrame = self.customTitleLabel.frame;
//	titleFrame.origin.x = left;
//	self.customTitleLabel.frame = titleFrame;
	
	//  CGRect detailsFrame = self.detailedLabel.frame;
	//  detailsFrame.origin.x = left;
	//  self.detailedLabel.frame = detailsFrame;
}


#pragma mark - Properties

- (void)setAdditionButtonHidden:(BOOL)additionButtonHidden
{
	[self setAdditionButtonHidden:additionButtonHidden animated:NO];
}

- (void)setAdditionButtonHidden:(BOOL)additionButtonHidden animated:(BOOL)animated
{
	_additionButtonHidden = additionButtonHidden;
	[UIView animateWithDuration:animated ? 0.2 : 0 animations:^{
		// self.additionButton.hidden = additionButtonHidden;
	}];
}


#pragma mark - Actions

- (IBAction)additionButtonTapped:(id)sender
{
	if (self.additionButtonTapAction) {
		self.additionButtonTapAction(sender);
	}
}

@end
