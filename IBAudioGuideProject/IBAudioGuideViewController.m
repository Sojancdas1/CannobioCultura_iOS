//
//  IBAudioGuideViewController.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 15/06/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBAudioGuideViewController.h"

#import "IBGlobal.h"
#import "HUDManager.h"

#import "IBAudioGuideTableViewCell.h"
#import "IBGuideDetailViewController.h"
#import "RATreeView.h"

#import "RADataObject.h"
#import "RATreeView+Private.h"
#import "RATableViewCelliphone.h"


#define kTitleHeight 26
#define kDetailsHeight 15
#define kTitleFont [UIFont fontWithName:@"Cambria" size:16]
#define kDetailsFont [UIFont fontWithName:@"Cambria" size:10]

@interface IBAudioGuideViewController ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,RATreeViewDelegate,RATreeViewDataSource>
{
    NSInteger currentIndex;
    UILabel *labelTitle, *labelSubTitle;
    NSArray *_arrayAudioGuide;
    NSMutableArray *_arraySearchedGuide;
    NSString *stringCode, *stringPosition;
    NSIndexPath * selectedIndex;
    NSMutableArray *arrayOfSelectedGuide;
    BOOL isFirstTym;
    NSString *currentMainTag;
    NSMutableArray *arraySubOfMainRoomTags, *arrayOfNotExpandblearray;
    BOOL iscurrentHasSubArray;
    BOOL isExpanded;
    RATableViewCelliphone *selecedCell;
    RATableViewCelliphone *tempCell;
    NSString *previousCellReferenceNo, *previousCellDescription;
    BOOL isPreviousCellSubArray;
    NSMutableArray *arrayWithoutExpandRow;
    IBAudioGuideInfo *currentGuideSelectedRow;
    NSString *previousTitleString;
      NSArray *previousVisibleCells;
    BOOL isScrolledTreeView;
    
}
@property (strong, nonatomic) NSString *stringCode;
@property (strong, nonatomic) NSString *stringPosition;

@property (weak, nonatomic) IBOutlet UISearchBar *tableSearchBar;
@property (weak, nonatomic) IBOutlet UITableView *tableViewAudioGuide;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewBottomConstraint;

@property (nonatomic, strong) NSArray *arrayAudioGuide;
@property (nonatomic, strong) NSMutableArray *arraySearchedGuide;
@property(nonatomic,retain)RATreeView *treeView;
@property (strong, nonatomic) NSArray *data;

- (void)refreshView;
- (void)buttonBackTouched;
- (void)buttonMapViewTouched;

@end

@implementation IBAudioGuideViewController

@synthesize stringCode,stringPosition,treeView,data;

@synthesize arrayAudioGuide = _arrayAudioGuide;
@synthesize arraySearchedGuide = _arraySearchedGuide;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _arrayAudioGuide = nil;
        _arraySearchedGuide = nil;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     isScrolledTreeView = NO;
    // Do any additional setup after loading the view.
    
    tempCell = nil;
    isExpanded = NO;
    iscurrentHasSubArray = NO;
    arrayOfSelectedGuide = [[NSMutableArray alloc]init];
    arraySubOfMainRoomTags = [[NSMutableArray alloc]init];
    arrayOfNotExpandblearray = [[NSMutableArray alloc]init];
    arrayWithoutExpandRow = [[NSMutableArray alloc]init];
    
    if (IOS_OLDER_THAN(6.0))    {
        [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:NO];
    }
    
    if (IOS_NEWER_OR_EQUAL_TO(7.0))    {
        self.extendedLayoutIncludesOpaqueBars=YES;
        self.automaticallyAdjustsScrollViewInsets=YES;
        self.tableViewAudioGuide.separatorInset = UIEdgeInsetsZero;
        self.searchDisplayController.searchResultsTableView.separatorInset = UIEdgeInsetsZero;
    }
    
    self.tableSearchBar.tintColor = RGBCOLOR(244, 244, 244);
    
    UIButton *buttonBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonBack setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [buttonBack setFrame:CGRectMake(0, 0, 12, 21)];
    [buttonBack addTarget:self action:@selector(buttonBackTouched) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem =
    [[UIBarButtonItem alloc] initWithCustomView:buttonBack];
    
    labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    labelTitle.backgroundColor = [UIColor clearColor];
    labelTitle.textColor = [UIColor blackColor];
    labelTitle.font = kAppFontSemibold(17);
    labelTitle.text = @"Your Title here";
    labelTitle.textAlignment = NSTextAlignmentCenter;
    [labelTitle sizeToFit];
    
    labelSubTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 22, 0, 0)];
    labelSubTitle.backgroundColor = [UIColor clearColor];
    labelSubTitle.textColor = [UIColor blackColor];
    labelSubTitle.font = kAppFontSemibold(11);
    labelSubTitle.text = @"Your subtitle here";
    labelSubTitle.textAlignment = NSTextAlignmentCenter;
    [labelSubTitle sizeToFit];
    
    UIView *twoLineTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAX(labelSubTitle.frame.size.width, labelTitle.frame.size.width), 30)];
    [twoLineTitleView addSubview:labelTitle];
    [twoLineTitleView addSubview:labelSubTitle];
    
    float widthDiff = labelSubTitle.frame.size.width - labelTitle.frame.size.width;
    
    if (widthDiff > 0) {
        CGRect frame = labelTitle.frame;
        frame.origin.x = widthDiff / 2;
        labelTitle.frame = CGRectIntegral(frame);
    } else {
        CGRect frame = labelSubTitle.frame;
        frame.origin.x = abs(widthDiff) / 2;
        labelSubTitle.frame = CGRectIntegral(frame);
    }
    
    self.navigationItem.titleView = twoLineTitleView;
    
    UIButton *buttonMap = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonMap.userInteractionEnabled = YES;
    [buttonMap setBackgroundImage:[UIImage imageNamed:@"mapNew.png"] forState:UIControlStateNormal];
    [buttonMap setFrame:CGRectMake(0, 0, 40, 27)];
    [buttonMap addTarget:self action:@selector(buttonMapViewTouched) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItem =
    [[UIBarButtonItem alloc] initWithCustomView:buttonMap];
    
//    self.tableViewAudioGuide.delegate = self;
//    self.tableViewAudioGuide.dataSource = self;
//    
//    self.tableViewAudioGuide.bounces = NO;
    
    self.tableSearchBar.delegate = self;
    self.searchDisplayController.searchResultsTableView.rowHeight = 70;
    
    
    [self loadData];
     CGRect temprect = [UIScreen mainScreen].bounds;
    if (temprect.size.width == 375)
    {
        if([[SharedAppDelegate playerView] isAppeared])
        {
             treeView = [[RATreeView alloc] initWithFrame:CGRectMake(-15, 45, temprect.size.width+15, temprect.size.height - 155)];
        }
        else
        {
       treeView = [[RATreeView alloc] initWithFrame:CGRectMake(-15, 45, temprect.size.width+15, temprect.size.height - 110)];
        }
    }
    else if (temprect.size.width == 320)
    {
        if([[SharedAppDelegate playerView] isAppeared])
        {
             treeView = [[RATreeView alloc] initWithFrame:CGRectMake(-15, 45, temprect.size.width+15, temprect.size.height - 155)];
        }
        else
        {
         treeView = [[RATreeView alloc] initWithFrame:CGRectMake(-15, 45, temprect.size.width+15, temprect.size.height - 110)];
        }
    }
    else if (temprect.size.width == 414)
    {
        if([[SharedAppDelegate playerView] isAppeared])
        {
            treeView = [[RATreeView alloc] initWithFrame:CGRectMake(-15, 45, temprect.size.width+15, temprect.size.height - 155)];
        }
        else
        {
        treeView = [[RATreeView alloc] initWithFrame:CGRectMake(-15, 45, temprect.size.width+15, temprect.size.height - 110)];
        }
    }
  
    
    //NSLog(@"%@",NSStringFromCGRect(treeView.frame));
    treeView.backgroundColor = [UIColor whiteColor];
        treeView.separatorColor = [IBGlobal colorWithHexString:@"cccccc"];
    treeView.separatorInset = UIEdgeInsetsZero;
    treeView.bounces = NO;
    treeView.rowHeight = 70;
    treeView.delegate = self;
    treeView.dataSource = self;
    [self.view addSubview:treeView];
    [treeView registerNib:[UINib nibWithNibName:NSStringFromClass([RATableViewCelliphone class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([RATableViewCelliphone class])];
    [self.view addSubview:treeView];
    [treeView reloadData];
}


#pragma mark - RATableView delegates and Datasources
- (UITableViewCell *)treeView:(RATreeView *)treeView cellForItem:(id)item {
    //
    RADataObject *dataObject = item;
    NSInteger level = [self.treeView levelForCellForItem:item];
    // 
    NSInteger numberOfChildren = [dataObject.children count];
    NSString *detailText = [NSString localizedStringWithFormat:@"Number of children %@", [@(numberOfChildren) stringValue]];
    BOOL expanded = [self.treeView isCellForItemExpanded:item];
    
    isExpanded = expanded;
    NSMutableAttributedString *desc = dataObject.desc;
    NSString *imagePath = dataObject.imagePath;
    NSMutableAttributedString *secondDesc = dataObject.secondDesc;
    
    RATableViewCelliphone *cell = [self.treeView dequeueReusableCellWithIdentifier:NSStringFromClass([RATableViewCelliphone class])];
    
    if (isScrolledTreeView) cell.isCellDequed = YES;
    else cell.isCellDequed = NO; 
    
    cell.isExpanded = expanded;
    
    UITableViewCell * cellTemp = [self.treeView cellForItem:item];
    
    if(cellTemp == [self.treeView itemForSelectedRow]) cell.isSelectedCell = YES;
    else cell.isSelectedCell = NO;
    
    if (!cell) NSLog(@"!Cell");
    //
    [cell setupWithTitle:dataObject.name detailText:detailText imagePath:imagePath descriptionText:desc secondDescription:secondDesc level:level additionButtonHidden:!expanded];
    //    [cell setupWithTitle:dataObject.name detailText:detailText imagePath:imagePath descriptionText:desc level:level additionButtonHidden:!expanded];
    
   cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    __weak typeof(self) weakSelf = self;
    cell.additionButtonTapAction = ^(id sender){
        if (![weakSelf.treeView isCellForItemExpanded:dataObject] || weakSelf.treeView.isEditing) {
            return;
        }
    };
    //
    return cell;
}

- (id)treeView:(RATreeView *)treeView child:(NSInteger)index ofItem:(id)item
{
    RADataObject *data1 = item;
    if (item == nil)
    {
        return [self.data objectAtIndex:index];
    }
    
    return data1.children[index];
}

- (NSInteger)treeView:(RATreeView *)treeView numberOfChildrenOfItem:(id)item
{
    if (item == nil) {
        return [self.data count];
    }
    
    RADataObject *data1 = item;
    return [data1.children count];
}

- (id)treeView:(RATreeView *)treeView willSelectRowForItem:(id)item;
{
    RATableViewCelliphone *cell = (RATableViewCelliphone *)[self.treeView cellForItem:item];
    
    return cell;
    
}

- (void)treeView:(RATreeView *)treeView didSelectRowForItem:(id)item
{
 //   IBGuideDetailViewController *guideDetailController = [SharedAppDelegate guideDetailViewControllerPhone];
    
    // //NSLog(@"%@",item row);
    [self.treeView deselectRowForItem:item animated:YES ];

    RADataObject *forPreviousData = item;
    
    NSIndexPath *indexPath = [self.treeView indexPathForItem:item];
    //NSLog(@"%ld",(long)indexPath.row);
    
    selecedCell = (RATableViewCelliphone *)[self.treeView cellForItem:item];
    NSString *tempStr =  selecedCell.labelDescription.text;
    NSString *titleStr = selecedCell.customTitleLabel.text;
    NSString *numberOfAuodios = selecedCell.labelSubArrayCount.text;
    
    numberOfAuodios = [numberOfAuodios stringByReplacingOccurrencesOfString:@"Nr.audio" withString:@""];

    
    NSString *tempStringcode = [NSString stringWithFormat:@"%@%@",stringCode,@". "];
    NSString *newreferenceString = [tempStr stringByReplacingOccurrencesOfString:tempStringcode withString:@""];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"referenceNo == %@ AND guideTitle == %@ ", newreferenceString,titleStr];
    NSArray *filteredArray = [_arrayAudioGuide filteredArrayUsingPredicate:predicate];
    
    IBAudioGuide *audioGuideInfo = nil;
    
    if ([filteredArray count] > 0) {
        audioGuideInfo = [filteredArray objectAtIndex:0];
    }
    
    if ([audioGuideInfo.guideDescription  isEqualToString:@"<Desc>-1</Desc>"] && [numberOfAuodios isEqualToString:@"no subRooms"])
    {
        audioGuideInfo = [filteredArray objectAtIndex:1];
    }

    
    if ([audioGuideInfo.guideDescription  isEqualToString:@"<Desc>-1</Desc>"])
    {
              // [self.treeView reloadRowsForItems:item withRowAnimation:RATreeViewRowAnimationAutomatic];
        if ([self.treeView isCellForItemExpanded:item])
        {
            
            selecedCell.additionButtonHidden = NO;
        }
        else
        {
            selecedCell.isExpanded = YES;
            selecedCell.additionButtonHidden = !selecedCell.additionButtonHidden;
        }
        
        [selecedCell setAdditionButtonHidden:selecedCell.additionButtonHidden cellInfo:selecedCell];
        // [cell setAdditionButtonHidden:YES cellInfo:cell];

    }
    else
    {
         [SharedAppDelegate setSelectedGuideTitleForGallery:selecedCell.customTitleLabel.text] ;
        for (RATableViewCelliphone *temp in self.treeView.visibleCells)
        {
            //NSLog(@"%@",temp.customTitleLabel.text);
            //NSLog(@"%@",tempCell.customTitleLabel.text);
            if ([temp.customTitleLabel.text isEqualToString:previousTitleString] && tempCell.isCellDequed)
            {
                if ([[previousCellReferenceNo stringByTrimmingCharactersInSet:[NSCharacterSet lowercaseLetterCharacterSet]] isEqualToString:previousCellReferenceNo] && !isPreviousCellSubArray)
                {
                    [tempCell UnselectedCell:YES];
                }
                else
                {
                    [tempCell UnselectedCell:NO];
                }
                
            }
            else if(!temp.isCellDequed)
            {
                if ([[previousCellReferenceNo stringByTrimmingCharactersInSet:[NSCharacterSet lowercaseLetterCharacterSet]] isEqualToString:previousCellReferenceNo] && !isPreviousCellSubArray)
                {
                    [tempCell UnselectedCell:YES];
                }
                else
                {
                    [tempCell UnselectedCell:NO];
                }
                
            }
            else
            {
                //NSLog(@"exception");
            }

        }
        
        previousVisibleCells = self.treeView.visibleCells;

        tempCell = nil;
        [selecedCell selectedCellInRows];
        
        previousTitleString = selecedCell.customTitleLabel.text;
        tempCell = selecedCell;
        previousCellReferenceNo = audioGuideInfo.referenceNo;
        previousCellDescription = audioGuideInfo.guideDescription;
        selecedCell.isSelectedCell = YES;
        
        isPreviousCellSubArray = NO;
        if (forPreviousData.isSubArray)
        {
            [SharedAppDelegate setIsSubRoom:YES];
            isPreviousCellSubArray = YES;
        }
        else
        {
            [SharedAppDelegate setIsSubRoom:NO];
        }
        
        
       currentGuideSelectedRow = [[IBAudioGuideInfo alloc] init];
        currentGuideSelectedRow.guideDescription = audioGuideInfo.guideDescription;
        currentGuideSelectedRow.audioFilePath = audioGuideInfo.audioFilePath;
        currentGuideSelectedRow.guideTitle = audioGuideInfo.guideTitle;
        currentGuideSelectedRow.referenceNo = audioGuideInfo.referenceNo;
        currentGuideSelectedRow.thumbnailPath = audioGuideInfo.thumbnailPath;
        //NSLog(@"%@",audioGuideInfo.referenceNo);
      //  guideDetailController.audioGuideInfo = currentGuideSelectedRow;
        
        [self performSegueWithIdentifier:@"segueToAGDetailedView" sender:self];
        
        
    }
    
}
- (UITableViewCellEditingStyle)treeView:(RATreeView *)treeView editingStyleForRowForItem:(id)item;
{
    return UITableViewCellEditingStyleNone;
}

- (void)loadData
{
    [self refreshView];
    
    [arrayOfSelectedGuide removeAllObjects];
    [self loadMainRoomNumbersDetails];
    
    RADataObject *expandableRow;
    NSMutableArray *rowArray = [[NSMutableArray alloc]init];
    
    
    NSString *referenceNumber;
    for (IBAudioGuide *obj in arrayOfSelectedGuide) {
        
        if ([obj.guideDescription isEqualToString:@"<Desc>-1</Desc>"]) {
            
            
            NSString *subTitle = [NSString stringWithFormat:@"%@. %@",stringCode,obj.referenceNo];
            NSMutableAttributedString *mutableAttributedString= [[NSMutableAttributedString alloc] initWithString:subTitle];
            [mutableAttributedString addAttribute:NSFontAttributeName value:kAppFontSemibold(11) range:NSMakeRange(0, [subTitle length])];
            [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:RGBCOLOR(124, 124, 124) range:NSMakeRange(0, [subTitle length])];
            
            
            NSString *secondSubTitle = [NSString stringWithFormat:@"Nr.audio"];
            NSMutableAttributedString *mutableAttributedStringsecondSubTitle = [[NSMutableAttributedString alloc] initWithString:secondSubTitle];
            [mutableAttributedStringsecondSubTitle addAttribute:NSFontAttributeName value:kAppFontSemibold(11) range:NSMakeRange(0, [secondSubTitle length])];
            [mutableAttributedStringsecondSubTitle addAttribute:NSForegroundColorAttributeName value:RGBCOLOR(124, 124, 124) range:NSMakeRange(0, [secondSubTitle length])];
            
            expandableRow = [RADataObject dataObjectWithName:obj.guideTitle detailDescription:mutableAttributedString detailSecondDesc:mutableAttributedStringsecondSubTitle ProperImage:obj.thumbnailPath children:nil];
            NSString *mainGuide = obj.guideTitle;
            
            referenceNumber = obj.referenceNo;
            for(IBAudioGuide *obj in _arrayAudioGuide) {
                
                
                if ([referenceNumber isEqualToString:[obj.referenceNo  stringByTrimmingCharactersInSet:[NSCharacterSet lowercaseLetterCharacterSet]]]&& ![obj.guideDescription isEqualToString:@"<Desc>-1</Desc>"])
                {
                    
                    NSString *codString = stringCode;
                    NSMutableAttributedString *codeAttributedString = [[NSMutableAttributedString alloc]initWithString:codString];
                    [codeAttributedString addAttribute:NSFontAttributeName value:kAppFontSemibold(11) range:NSMakeRange(0, [codeAttributedString length])];
                    [codeAttributedString addAttribute:NSForegroundColorAttributeName value:RGBCOLOR(124, 124, 124) range:NSMakeRange(0, [codString length])];
                    
                    
                    NSString *subTitle1 = [NSString stringWithFormat:@". %@",obj.referenceNo];
                    NSMutableAttributedString *mutableAttributedString= [[NSMutableAttributedString alloc] initWithString:subTitle1];
                    [mutableAttributedString addAttribute:NSFontAttributeName value:kAppFontBold(11) range:NSMakeRange(0, [subTitle1 length])];
                    [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:RGBCOLOR(124, 124, 124) range:NSMakeRange(0, [subTitle1 length])];
                    
                    
                    NSMutableAttributedString *joiningAttributeString = [[NSMutableAttributedString alloc]initWithAttributedString:codeAttributedString];
                    [joiningAttributeString appendAttributedString:mutableAttributedString];
                    
                    NSString *secondSubTitle1 = [NSString stringWithFormat:@"%@%@%@",@"(",mainGuide,@")"];
                    NSMutableAttributedString *mutableAttributedStringsecondSubTitle1 = [[NSMutableAttributedString alloc] initWithString:secondSubTitle1];
                    [mutableAttributedStringsecondSubTitle1 addAttribute:NSFontAttributeName value:kAppFontBold(11) range:NSMakeRange(0, [secondSubTitle1 length])];
                    [mutableAttributedStringsecondSubTitle1 addAttribute:NSForegroundColorAttributeName value:RGBCOLOR(124, 124, 124) range:NSMakeRange(0, [secondSubTitle1 length])];
                    
                    RADataObject *tempArray = [RADataObject dataObjectWithName:obj.guideTitle detailDescription:joiningAttributeString detailSecondDesc:mutableAttributedStringsecondSubTitle1 ProperImage:obj.thumbnailPath children:nil];
                    tempArray.isSubArray = YES;
                    [expandableRow addChild:tempArray];
                    
                    
                }
            }
        } else  {
            [obj setValue:@"YES" forKey:@"isSubArray"];
            
            
            NSString *codString = stringCode;
            NSMutableAttributedString *codeAttributedString = [[NSMutableAttributedString alloc]initWithString:codString];
            [codeAttributedString addAttribute:NSFontAttributeName value:kAppFontSemibold(11) range:NSMakeRange(0, [codeAttributedString length])];
            [codeAttributedString addAttribute:NSForegroundColorAttributeName value:RGBCOLOR(124, 124, 124) range:NSMakeRange(0, [codString length])];
            
            NSString *subTitle2 = [NSString stringWithFormat:@". %@",obj.referenceNo];
            NSMutableAttributedString *mutableAttributedString= [[NSMutableAttributedString alloc] initWithString:subTitle2];
            [mutableAttributedString addAttribute:NSFontAttributeName value:kAppFontBold(11) range:NSMakeRange(0, [subTitle2 length])];
            [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:RGBCOLOR(124, 124, 124) range:NSMakeRange(0, [subTitle2 length])];
            
            
            NSMutableAttributedString *joiningAttributeString = [[NSMutableAttributedString alloc]initWithAttributedString:codeAttributedString];
            [joiningAttributeString appendAttributedString:mutableAttributedString];
            
            NSString *secondSubTitle2 = [NSString stringWithFormat:@"Nr.audio"];
            NSMutableAttributedString *mutableAttributedStringsecondSubTitle2 = [[NSMutableAttributedString alloc] initWithString:secondSubTitle2];
            [mutableAttributedStringsecondSubTitle2 addAttribute:NSFontAttributeName value:kAppFontSemibold(11) range:NSMakeRange(0, [secondSubTitle2 length])];
            [mutableAttributedStringsecondSubTitle2 addAttribute:NSForegroundColorAttributeName value:RGBCOLOR(124, 124, 124) range:NSMakeRange(0, [secondSubTitle2 length])];
            
            //NSLog(@"%@",obj.thumbnailPath);
            
            expandableRow = [RADataObject dataObjectWithName:obj.guideTitle detailDescription:joiningAttributeString detailSecondDesc:mutableAttributedStringsecondSubTitle2 ProperImage:obj.thumbnailPath children:nil];
            
        }
        expandableRow.isSubArray = NO;
        [rowArray addObject:expandableRow];
        
    }
    
    self.data = (NSArray *) rowArray;
}

-(void)loadElementsWithoutExpandRow
{
    [arrayWithoutExpandRow removeAllObjects];
    for(NSArray *array in _arrayAudioGuide)
    {
        if([[array valueForKey:@"guideDescription"] isEqualToString:@"<Desc>-1</Desc>"])
        {
            
        }
        else
        {
            [arrayWithoutExpandRow addObject:array];
        }
        
    }
}

-(void)loadMainRoomNumbersDetails
{
    NSString *tempRefern_No;
    tempRefern_No = @"temp";
    
    for (NSArray *array in _arrayAudioGuide)
    {
        if ([[[array valueForKey:@"referenceNo"]  stringByTrimmingCharactersInSet:[NSCharacterSet lowercaseLetterCharacterSet]] isEqualToString:[array valueForKey:@"referenceNo"]] | [[array valueForKey:@"guideDescription"] isEqualToString:@"<Desc>-1</Desc>"])
        {
            if (![tempRefern_No isEqualToString:[array valueForKey:@"referenceNo"] ])
            {
                //NSLog(@"%@",[array valueForKey:@"referenceNo"]);
                //NSLog(@"%@",[array valueForKey:@"guideDescription"]);
                [arrayOfSelectedGuide addObject:array];
            }
            
            tempRefern_No = [array valueForKey:@"referenceNo"];
        }
        
        //NSLog(@"%lu",(unsigned long)[arrayOfNotExpandblearray count]);
    }
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    CGRect temprect = [UIScreen mainScreen].bounds;
    BOOL isPlPlayerBussy = [SharedAppDelegate isPlPlayerBussy];
    if(isPlPlayerBussy)
    {
        treeView.frame = CGRectMake(-15, 45, temprect.size.width+15, temprect.size.height - 155);
        [SharedAppDelegate animatePlayerHidden:NO];
    } else {
        treeView.frame = CGRectMake(-15, 45, temprect.size.width+15, temprect.size.height - 110);
        [SharedAppDelegate animatePlayerHidden:YES];
    } 
    
    NSInteger locationNo = [SharedAppDelegate locationNo];
    
    NSString *museumName = nil;
    MUSEUMNAME_STR(locationNo, museumName);
    
    NSDictionary *langDictionary = [SharedAppDelegate langDictionary];
    
    labelTitle.text = [langDictionary valueForKey:@"LabelAudioGuide"];
    labelSubTitle.text = museumName;
    
    [SharedAppDelegate setCurrentNavigation:[self navigationController]];
    [self navigationController].navigationBarHidden = NO;
    [self.view setBackgroundColor:RGBCOLOR(200, 200, 200)];
    
    for (UIView *subView in self.tableSearchBar.subviews) {
        //Find the button
        if([subView isKindOfClass:[UIButton class]])
        {
            //Change its properties
            UIButton *cancelButton = (UIButton *)subView;
            cancelButton.titleLabel.font = kAppFontRegular(15);
            if(IOS_OLDER_THAN(7.0))    {
               [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            }
        }
        else if ([subView isKindOfClass:[UITextField class]])   {
            UITextField *textField = (UITextField *)subView;
            [textField setFont:kAppFontRegular(15)];
        }
    }
    
    [self.tableViewAudioGuide setAlpha:0.f];
    
    NSString *museumDir;
    DIRLOCATION_STR(locationNo,museumDir);
    
    [HUDManager addHUDWithLabel:nil dimBackground:YES];
    [self performSelector:@selector(refreshView) withObject:nil afterDelay:0.1f];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate
{
    return YES;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    //
    if ([segue.identifier isEqualToString:@"segueToAGDetailedView"]) {
        //
        UIViewController *controller = [segue destinationViewController];
        
        IBGuideDetailViewController *guideDetailController = (IBGuideDetailViewController *)controller;
        
        IBAudioGuideInfo *audioGuideInfo = [[IBAudioGuideInfo alloc]init];
        if(sender == self.searchDisplayController.searchResultsTableView)
        {
           NSIndexPath *indexPath = [self.searchDisplayController.searchResultsTableView indexPathForSelectedRow];
          //   NSIndexPath *indexPath = [self.tableViewAudioGuide indexPathForSelectedRow];
            audioGuideInfo = [_arraySearchedGuide objectAtIndex:[indexPath row]];
             //
        } else {
            //
            audioGuideInfo.guideDescription = currentGuideSelectedRow.guideDescription;
            audioGuideInfo.audioFilePath = currentGuideSelectedRow.audioFilePath;
            audioGuideInfo.guideTitle = currentGuideSelectedRow.guideTitle;
            audioGuideInfo.referenceNo = currentGuideSelectedRow.referenceNo;
            audioGuideInfo.thumbnailPath = currentGuideSelectedRow.thumbnailPath;
        }
        NSArray *arrayGuideGallery = [[SharedAppDelegate database] fetchGuideGalleryByAudioReferenceIndex:audioGuideInfo.referenceNo];
        
        NSMutableArray *arrayImagePaths = [NSMutableArray array];
        for(IBGuideGallery *guideGalleryInfo in arrayGuideGallery)
        {
            [arrayImagePaths addObject:guideGalleryInfo.guideGalleryFilePath];
        }
        
        //NSLog(@"%@",arrayImagePaths);
        NSInteger locationNo = [SharedAppDelegate locationNo];
        NSString *tempCoverPath = [[SharedAppDelegate database] loadCoverPhotoOfCurrespondWithLocationNo:locationNo andReferenceNo:audioGuideInfo.referenceNo];
        guideDetailController.arrayImagePathsGallery = [NSArray arrayWithArray:arrayImagePaths];
        guideDetailController.arrayImagePaths = [[NSArray alloc]initWithObjects:tempCoverPath, nil];

        //NSLog(@"%@",arrayImagePaths);
     //   guideDetailController.arrayImagePaths = [NSArray arrayWithArray:arrayImagePaths];
        guideDetailController.audioGuide = audioGuideInfo;
    }
    else if ([segue.identifier isEqualToString:@"segueToMapView"])   {
        UINavigationController *controller = [segue destinationViewController];
        
        [SharedAppDelegate setCurrentNavigation:controller];
    }
    currentGuideSelectedRow = nil;
}

- (void)buttonMapViewTouched
{
    [self performSegueWithIdentifier:@"segueToMapView" sender:self];
}

- (void)buttonBackTouched
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)refreshView
{
    NSInteger locationNo = [SharedAppDelegate locationNo];
    NSUInteger languageIndex = [SharedAppDelegate languageIndex];
    
    NSString *museumDir,*languageDir,*museumName;
    
    MUSEUMNAME_STR(locationNo, museumName);
    
    labelSubTitle.text = museumName;
    
    NSDictionary *langDictionary = [SharedAppDelegate langDictionary];
    
    self.tableSearchBar.placeholder = [langDictionary valueForKey:@"LabelSearchPlaceholder"];
    labelTitle.text = [langDictionary valueForKey:@"LabelAudioGuide"];
    
    stringCode = [[langDictionary valueForKey:@"LabelCode"] substringToIndex:3];
    stringPosition = [langDictionary valueForKey:@"LabelPosition"];
    
    DIRLOCATION_STR(locationNo,museumDir);
    DIRLANGUAGE_STR(languageIndex, languageDir);
    
    if(_arrayAudioGuide) _arrayAudioGuide = nil;
    if(_arraySearchedGuide) _arraySearchedGuide = nil;
    
    _arrayAudioGuide = [[SharedAppDelegate database] loadAudioGuideWithLocationNo:locationNo andLanguageIndex:languageIndex];
    
    [self.tableViewAudioGuide setAlpha:1.f];
  //  [self.tableViewAudioGuide reloadData];
    
    NSMutableArray *arrayAudioGuideInfos = [NSMutableArray array];
    for(IBAudioGuide *audioGuide in _arrayAudioGuide)   {
        IBAudioGuideInfo *currentGuide = [[IBAudioGuideInfo alloc] init];
        currentGuide.guideDescription = audioGuide.guideDescription;
        currentGuide.audioFilePath = audioGuide.audioFilePath;
        currentGuide.guideTitle = audioGuide.guideTitle;
        currentGuide.referenceNo = audioGuide.referenceNo;
        currentGuide.thumbnailPath = audioGuide.thumbnailPath;
        
        [arrayAudioGuideInfos addObject:currentGuide];
    }
    
    [SharedAppDelegate setAudioGuideInfos:arrayAudioGuideInfos];
    
     
    [HUDManager hideHUDFromWindowAfterDelay:0.2f];
    
}

#pragma mark - Table view data source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.searchDisplayController.searchResultsTableView)
	{
        [self loadElementsWithoutExpandRow];
        return [_arraySearchedGuide count];
    }
	else
	{
        return [_arrayAudioGuide count];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{ 
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = (IBAudioGuideTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[IBAudioGuideTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell.imageView setHidden:NO];
    }
     
    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"navigate-normal.png"]];
    cell.textLabel.textColor = [UIColor blackColor];
    
    IBAudioGuide *audioGuideInfo;
    
    if (tableView == self.searchDisplayController.searchResultsTableView)
	{
        audioGuideInfo = [_arraySearchedGuide objectAtIndex:[indexPath row]];
    }
	else
	{
        audioGuideInfo = [arrayOfSelectedGuide objectAtIndex:[indexPath row]];
    }
    
    cell.imageView.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[IBGlobal createDocumentsDirectoryByAppendingPathComponent:nil],audioGuideInfo.thumbnailPath]];
    
    cell.textLabel.text = audioGuideInfo.guideTitle;
    cell.textLabel.font = kAppFontSemibold(16);
    
    NSString *subTitle = [NSString stringWithFormat:@"%@. %@",stringCode,audioGuideInfo.referenceNo];
    
    NSMutableAttributedString *mutableAttributedString= [[NSMutableAttributedString alloc] initWithString:subTitle];
    [mutableAttributedString addAttribute:NSFontAttributeName value:kAppFontSemibold(11) range:NSMakeRange(0, [subTitle length])];
    [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:RGBCOLOR(124, 124, 124) range:NSMakeRange(0, [subTitle length])];
    
    cell.detailTextLabel.attributedText = mutableAttributedString;
    
    return cell;
}

#pragma mark -
#pragma mark - Table view delegates

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    // do something here
    currentIndex = indexPath.row;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    currentIndex = indexPath.row;
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [SharedAppDelegate setSelectedGuideTitleForGallery:cell.textLabel.text ];
    [self performSegueWithIdentifier:@"segueToAGDetailedView" sender:tableView];
    [self.searchDisplayController setActive:NO animated:YES];
}

#pragma mark -
#pragma mark Content Filtering

- (void)filterContentForSearchText:(NSString *)searchText scope:(NSString *)scope {
	//
	if(_arraySearchedGuide) { [_arraySearchedGuide removeAllObjects]; _arraySearchedGuide = nil; }
    
	// Filter the array using NSPredicate
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"guideTitle contains[c] %@",searchText];
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"referenceNo contains[c] %@",searchText];
    NSPredicate *predicate = [NSCompoundPredicate orPredicateWithSubpredicates:[NSArray arrayWithObjects:predicate1, predicate2, nil]];
    
    NSArray *tempArray1 = [arrayWithoutExpandRow filteredArrayUsingPredicate:predicate];
    
    _arraySearchedGuide = [NSMutableArray arrayWithArray:tempArray1];
}


#pragma mark - UISearchDisplayController Delegate Methods

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    // Tells the table data source to reload when text changes
    [self filterContentForSearchText:searchString scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
{
    // Tells the table data source to reload when scope bar selection changes
    [self filterContentForSearchText:[self.searchDisplayController.searchBar text] scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

@end
