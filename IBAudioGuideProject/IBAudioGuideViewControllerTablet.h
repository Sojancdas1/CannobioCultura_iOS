//
//  IBAudioGuideViewControllerTablet.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 01/09/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface IBAudioGuideViewControllerTablet : UIViewController

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewBottomConstraint;
@property(nonatomic) BOOL isDetailPageAppearedOnce;
- (void)dismissIfExistedMapViewController;

@end
