//
//  IBAudioGuideViewControllerTablet.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 01/09/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBAudioGuideViewControllerTablet.h"

#import "Base64.h"
#import "IBGlobal.h"
#import "HUDManager.h"
#import "IBZipDownloadButton.h"
#import "MBProgressHUD.h"
#import "ASIHTTPRequest.h"
#import "ZipArchive.h"
#import "IBAudioGuideTableViewCell.h"
#import "IBGuideDetailViewController.h"
#import "IBGeoMapViewControllerTablet.h"
#import "UIView+Toast.h"
#import "RATreeView.h"
#import "RATableViewCell.h"
#import "RADataObject.h"
#import "RATreeView+Private.h"
#import "IBGuideDetailViewControllerTablet.h"

#define kTitleHeight 26
#define kDetailsHeight 15
#define kTitleFont [UIFont fontWithName:@"Cambria" size:16]
#define kDetailsFont [UIFont fontWithName:@"Cambria" size:10]

@interface IBAudioGuideViewControllerTablet ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,IBGeoMapViewControllerTabletDelegate,RATreeViewDelegate,RATreeViewDataSource,tableReloadDelegate>
{
    ASIHTTPRequest *asiRequest;
    long long receivedBytes,contentLength;
    
    MBProgressHUD *hud;
    
    NSInteger currentIndex, previousPressedRowIndex;
    UILabel *labelTitle, *labelSubTitle;
    NSArray *_arrayAudioGuide;
    NSMutableArray *_arraySearchedGuide;
    NSString *stringCode, *stringPosition;
    IBGeoMapViewControllerTablet *geoMapController;
    NSIndexPath * selectedIndex;
    NSMutableArray *arrayOfSelectedGuide;
    BOOL isFirstTym;
    NSString *currentMainTag;
    NSMutableArray *arraySubOfMainRoomTags, *arrayOfNotExpandblearray;
    BOOL iscurrentHasSubArray;
    BOOL isExpanded;
    RATableViewCell *selecedCell;
    RATableViewCell *tempCell;
    NSString *previousCellReferenceNo, *previousCellDescription;
    BOOL isPreviousCellSubArray;
    NSMutableArray *arrayWithoutExpandRow;
    BOOL isPlayerHidden;
    NSString *previousTitleString;
    NSArray *previousVisibleCells;
    BOOL isScrolledTreeView;
    
    
}

@property (strong, nonatomic) NSString *stringCode;
@property (strong, nonatomic) NSString *stringPosition;

@property (nonatomic,assign) Status status;
@property (nonatomic,readonly) NSString *key;
@property (nonatomic,strong) NSString *urlString;
@property (nonatomic,strong) NSString *destinationDir;

@property (weak, nonatomic) IBOutlet UIView *mapButtonContainer;
@property (weak, nonatomic) IBOutlet UISearchBar *tableSearchBar;
@property (weak, nonatomic) IBOutlet UITableView *tableViewAudioGuide;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mapButtonContainerTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mapButtonContainerHeightContraint;

@property (nonatomic, strong) NSArray *arrayAudioGuide;
@property (nonatomic, strong) NSMutableArray *arraySearchedGuide;

@property(nonatomic,retain)RATreeView *treeView;
@property (strong, nonatomic) NSArray *data;

@property (strong, nonatomic)IBAudioPlayerView *playerView;
@property (strong, nonatomic) IBOutlet UIImageView *viewShade;


- (void)refreshView;
- (void)buttonBackTouched;
- (void)buttonMapViewTouched;

@end

@implementation IBAudioGuideViewControllerTablet

@synthesize stringCode,stringPosition,treeView,data;

@synthesize arrayAudioGuide = _arrayAudioGuide;
@synthesize arraySearchedGuide = _arraySearchedGuide;
@synthesize playerView = _playerView;
@synthesize isDetailPageAppearedOnce = _isDetailPageAppearedOnce;
@synthesize viewShade;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _arrayAudioGuide = nil;
        _arraySearchedGuide = nil;
    }
    //
    return self;
}

- (void)viewDidLoad {
    //
    [super viewDidLoad];
    //
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTreeview:) name:@"tableReloadNotification" object:nil];
    //
    if (IOS_OLDER_THAN(6.0)) {
        //
        [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:NO];
    }
    //
    if (IOS_NEWER_OR_EQUAL_TO(7.0)) {
        //
        self.extendedLayoutIncludesOpaqueBars=YES;
        self.automaticallyAdjustsScrollViewInsets=YES;
        self.tableViewAudioGuide.separatorInset = UIEdgeInsetsZero;
        self.searchDisplayController.searchResultsTableView.separatorInset = UIEdgeInsetsZero;
    }
    //
    self.view.backgroundColor = [UIColor blackColor];
    self.tableSearchBar.tintColor = RGBCOLOR(102, 102, 102);
    self.tableSearchBar.backgroundColor = RGBCOLOR(211, 211, 211);
    self.mapButtonContainer.backgroundColor = RGBCOLOR(211, 211, 211);
    self.searchDisplayController.searchResultsTableView.allowsSelection = YES;
    //
    UIButton *buttonBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonBack setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [buttonBack setFrame:CGRectMake(0, 0, 12, 21)];
    [buttonBack addTarget:self action:@selector(buttonBackTouched) forControlEvents:UIControlEventTouchUpInside];
    //
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:buttonBack];
    //
    labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    labelTitle.backgroundColor = [UIColor clearColor];
    labelTitle.textColor = [UIColor blackColor];
    labelTitle.font = kAppFontSemibold(17);
    labelTitle.text = @"Your Title here";
    labelTitle.textAlignment = NSTextAlignmentCenter;
    [labelTitle sizeToFit];
    //
    labelSubTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 22, 0, 0)];
    labelSubTitle.backgroundColor = [UIColor clearColor];
    labelSubTitle.textColor = [UIColor blackColor];
    labelSubTitle.font = kAppFontSemibold(11);
    labelSubTitle.text = @"Your subtitle here";
    labelSubTitle.textAlignment = NSTextAlignmentCenter;
    [labelSubTitle sizeToFit];
    //
    UIView *twoLineTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAX(labelSubTitle.frame.size.width, labelTitle.frame.size.width), 30)];
    [twoLineTitleView addSubview:labelTitle];
    [twoLineTitleView addSubview:labelSubTitle];
    
    float widthDiff = labelSubTitle.frame.size.width - labelTitle.frame.size.width;
    
    if (widthDiff > 0) {
        //
        CGRect frame = labelTitle.frame;
        frame.origin.x = widthDiff / 2;
        labelTitle.frame = CGRectIntegral(frame);
    } else {
        //
        CGRect frame = labelSubTitle.frame;
        frame.origin.x = fabsf(widthDiff) / 2;
        labelSubTitle.frame = CGRectIntegral(frame);
    }
    //
    self.navigationItem.titleView = twoLineTitleView;
    //
    UITapGestureRecognizer *oneTouchOnMapButton = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(OneTouchOnMapButtonContainerTouched:)];
    [oneTouchOnMapButton setNumberOfTouchesRequired:1];
    [self.mapButtonContainer addGestureRecognizer:oneTouchOnMapButton];
    self.tableSearchBar.delegate = self;
    self.searchDisplayController.searchResultsTableView.rowHeight = 70;
    [[UISearchBar appearance] setBackgroundImage:[IBGlobal imageWithColor:RGBCOLOR(211, 211, 211)]];
    //
    [self _loadViewAgain];
}

- (void)viewWillAppear:(BOOL)animated {
    //
    [super viewWillAppear:animated];
    [self _reFresh];
}

-(void)viewWillDisappear:(BOOL)animated {
    //
    [super viewWillDisappear:animated];
    [self.searchDisplayController setActive:NO animated:YES];
}

-(void)_loadViewAgain {
    //
    isScrolledTreeView = NO;
    //
    _isDetailPageAppearedOnce = [SharedAppDelegate isDetailPageAppeared];
    previousPressedRowIndex = 2000;
    tempCell = nil;
    //
    isFirstTym = YES;
    isExpanded = NO;
    iscurrentHasSubArray = NO;
    arrayOfSelectedGuide = [[NSMutableArray alloc]init];
    arraySubOfMainRoomTags = [[NSMutableArray alloc]init];
    arrayOfNotExpandblearray = [[NSMutableArray alloc]init];
    arrayWithoutExpandRow = [[NSMutableArray alloc]init];
    //
    [self loadData];
    [treeView removeFromSuperview];
    //
    if ([SharedAppDelegate playerViewTablet].playing) {
        //
        treeView = [[RATreeView alloc] initWithFrame: CGRectMake(-14, 45, self.view.frame.size.width+15, self.view.frame.size.height - 110)];
    } else {
        //
        treeView = [[RATreeView alloc] initWithFrame:CGRectMake(-14, 45, self.view.frame.size.width + 15, self.view.frame.size.height - 65)];
    }
    //
    treeView.showsVerticalScrollIndicator = NO;
    treeView.separatorColor = [IBGlobal colorWithHexString:@"cccccc"];
    treeView.separatorInset = UIEdgeInsetsZero;
    treeView.bounces = NO;
    treeView.rowHeight = 70;
    treeView.delegate = self;
    treeView.dataSource = self;
    [self.view addSubview:treeView];
    [treeView registerNib:[UINib nibWithNibName:NSStringFromClass([RATableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([RATableViewCell class])];
    [self.view addSubview:treeView];
    [treeView reloadData];
     //
    UISwipeGestureRecognizer *swipeGestureRecogniser = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeAndMove:)];
    swipeGestureRecogniser.direction = UISwipeGestureRecognizerDirectionRight | UISwipeGestureRecognizerDirectionLeft;
    UISwipeGestureRecognizer *swipeGestureRecogniserOnSearchTable = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeAndMove:)];
    swipeGestureRecogniserOnSearchTable.direction = UISwipeGestureRecognizerDirectionRight | UISwipeGestureRecognizerDirectionLeft;
    //
    [self.treeView addGestureRecognizer:swipeGestureRecogniser];
    [self.searchDisplayController.searchResultsTableView addGestureRecognizer:swipeGestureRecogniserOnSearchTable];
    [self.view bringSubviewToFront:viewShade];
}

-(void)_reFresh {
    //
    [arrayOfSelectedGuide removeAllObjects];
    [arraySubOfMainRoomTags removeAllObjects];
    //
    if([[SharedAppDelegate playerViewTablet] isAppeared]) {
        //
        treeView.frame = CGRectMake(-14, 45, self.view.frame.size.width+15, self.view.frame.size.height - 110) ;
    } else {
        //
        treeView.frame = CGRectMake(-14, 45, self.view.frame.size.width+15, self.view.frame.size.height - 45);
    }
    //
    NSInteger locationNo = [SharedAppDelegate locationNo];
    NSString *museumName = nil;
    MUSEUMNAME_STR(locationNo, museumName);
    NSDictionary *langDictionary = [SharedAppDelegate langDictionary];
    self.tableSearchBar.placeholder = [langDictionary valueForKey:@"LabelSearchPlaceholder"];
    [self.tableSearchBar setImage:[UIImage imageNamed:@"search@2x.png"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    labelTitle.text = [langDictionary valueForKey:@"LabelAudioGuide"];
    labelSubTitle.text = museumName;
    
    [self navigationController].navigationBarHidden = YES;
    [self.view setBackgroundColor:RGBCOLOR(200, 200, 200)];
    
    for (UIView *subView in self.tableSearchBar.subviews) {
        //Find the button
        if([subView isKindOfClass:[UIButton class]]) {
            //Change its properties
            UIButton *cancelButton = (UIButton *)subView;
            cancelButton.titleLabel.font = kAppFontRegular(15);
            //
            if(IOS_OLDER_THAN(7.0)) {
                //[cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [cancelButton setTitleColor:RGBCOLOR(102, 102, 102) forState:UIControlStateNormal];
            }
        } else if ([subView isKindOfClass:[UITextField class]])   {
            UITextField *textField = (UITextField *)subView;
            [textField setFont:kAppFontRegular(15)];
        }
    }
    
    [self.tableViewAudioGuide setAlpha:0.f];
    [[SharedAppDelegate guideDetailViewController].view setAlpha:0.f];
    
    NSString *museumDir;
    DIRLOCATION_STR(locationNo,museumDir);
    
    _urlString = [NSString stringWithFormat:@"%@/%@/%@/%@",SYNC_URL,IBParentDirectory,museumDir,kGuidesGalleryZip];
    _destinationDir = [IBGlobal createDocumentsDirectoryByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@/",ENGLISH,museumDir]];
    
    NSLog(@"_urlString %@", _urlString);
    
    NSString *downloadStatus = [[NSUserDefaults standardUserDefaults] valueForKey:self.key];
    
    if ([downloadStatus intValue] == StatusDownloaded) {
        //
        self.status = StatusDownloaded;
        [HUDManager addHUDWithLabel:nil dimBackground:YES];
        [self performSelector:@selector(refreshView) withObject:nil afterDelay:0.1f];
    } else {
        //
        self.status = StatusDownloading;
        //
        NSURL *url = [NSURL URLWithString:_urlString];
        NSString *filename = [url lastPathComponent];
        NSString *tempDownloadPath = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"loc%ld-%@.download",(long)locationNo, filename]];
        NSString *destinationPath = [NSString stringWithFormat:@"%@%@",_destinationDir,filename];
        //
        NSLog(@"url %@", url);
        NSLog(@"filename %@", filename);
        NSLog(@"tempDownloadPath %@", tempDownloadPath);
        NSLog(@"destinationPath %@", destinationPath);
        
        asiRequest = [ASIHTTPRequest requestWithURL:url];
        [asiRequest setTimeOutSeconds:360];
        [asiRequest setDelegate:self];
        [asiRequest setDownloadProgressDelegate:self];
        [asiRequest setTemporaryFileDownloadPath:tempDownloadPath];
        [asiRequest setDownloadDestinationPath:destinationPath];
        [asiRequest setAllowResumeForFileDownloads:YES];
        
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeDeterminate;
        hud.dimBackground = YES;
        
        [SharedAppDelegate lockInteractions];
        [asiRequest startAsynchronous];
        //
        [self.view makeToast:[[SharedAppDelegate langDictionary] valueForKey:@"LabelGuideGalleryLoad"] duration:3.f position:@"bottom"];
    }
    //
    geoMapController = nil;
    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 32)];
    footer.backgroundColor = [UIColor whiteColor];
    self.treeView.treeFooterView = footer;
    [self.treeView reloadData];
} 


#pragma mark - RATableView delegates and Datasources


- (UITableViewCell *)treeView:(RATreeView *)treeView cellForItem:(id)item
{
    RADataObject *dataObject = item;
    NSInteger level = [self.treeView levelForCellForItem:item];
    //NSLog(@"%lu",(unsigned long)[dataObject.children count]);
    NSInteger numberOfChildren = [dataObject.children count];
    NSString *detailText = [NSString localizedStringWithFormat:@"Number of children %@", [@(numberOfChildren) stringValue]];
    BOOL expanded = [self.treeView isCellForItemExpanded:item];
    
    isExpanded = expanded;
    NSMutableAttributedString *desc = dataObject.desc;
    NSString *imagePath = dataObject.imagePath;
    NSMutableAttributedString *secondDesc = dataObject.secondDesc;
    
    RATableViewCell *cell = [self.treeView dequeueReusableCellWithIdentifier:NSStringFromClass([RATableViewCell class])];
    
    if (isScrolledTreeView) cell.isCellDequed = YES;
    else cell.isCellDequed = NO;
    
    cell.isExpanded = expanded;
    
    UITableViewCell * cellTemp = [self.treeView cellForItem:item];
    
    if(cellTemp == [self.treeView itemForSelectedRow]) cell.isSelectedCell = YES;
    else cell.isSelectedCell = NO;
    
    [cell setupWithTitle:dataObject.name detailText:detailText imagePath:imagePath descriptionText:desc secondDescription:secondDesc level:level additionButtonHidden:!expanded];
    //    [cell setupWithTitle:dataObject.name detailText:detailText imagePath:imagePath descriptionText:desc level:level additionButtonHidden:!expanded];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    __weak typeof(self) weakSelf = self;
    cell.additionButtonTapAction = ^(id sender){
        if (![weakSelf.treeView isCellForItemExpanded:dataObject] || weakSelf.treeView.isEditing) {
            return;
        }
    };
    
    return cell;
}

- (id)treeView:(RATreeView *)treeView child:(NSInteger)index ofItem:(id)item
{
    RADataObject *data1 = item;
    if (item == nil) {
        return [self.data objectAtIndex:index];
    }
    
    return data1.children[index];
}

- (NSInteger)treeView:(RATreeView *)treeView numberOfChildrenOfItem:(id)item
{
    if (item == nil) {
        return [self.data count];
    }
    
    RADataObject *data1 = item;
    return [data1.children count];
}

- (id)treeView:(RATreeView *)treeView willSelectRowForItem:(id)item;
{
    RATableViewCell *cell = (RATableViewCell *)[self.treeView cellForItem:item];
    
    return cell;
    
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //
    //    for (RATableViewCell *temp in self.treeView.visibleCells)
    //    {
    //        //NSLog(@"%@",temp.customTitleLabel.text);
    //        //NSLog(@"%@",tempCell.customTitleLabel.text);
    //        if ([temp.customTitleLabel.text isEqualToString:previousTitleString])
    //        {
    //            isScrolledTreeView = NO;
    //        }
    //        else
    //        {
    isScrolledTreeView = YES;
    //  }
    //  }
    
}

- (void)treeView:(RATreeView *)treeView didSelectRowForItem:(id)item
{
    IBGuideDetailViewControllerTablet *guideDetailController = [SharedAppDelegate guideDetailViewController];
    [self.treeView deselectRowForItem:item animated:YES ];
    //    isPlayerHidden = [SharedAppDelegate playerHidden];
    //
    //    if(!isPlayerHidden)    {
    //
    //        [SharedAppDelegate animatePlayerHidden:YES];
    //        isPlayerHidden = YES;
    //    }
    //    [[SharedAppDelegate playerViewTablet]setPlaying:NO];
    //   [SharedAppDelegate pauseAudioPlayer];
    
    RADataObject *forPreviousData = item;
    
    NSIndexPath *indexPath = [self.treeView indexPathForItem:item];
    //NSLog(@"%ld",(long)indexPath.row);
    selecedCell = nil;
    selecedCell = (RATableViewCell *)[self.treeView cellForItem:item];
    NSString *tempStr =  selecedCell.labelDescription.text;
    NSString *titleStr = selecedCell.customTitleLabel.text;
    NSString *numberOfAuodios = selecedCell.labelSubArrayCount.text;
    //  NSString *descr = selecedCell.labelDescription.text;
    
    numberOfAuodios = [numberOfAuodios stringByReplacingOccurrencesOfString:@"Nr.audio" withString:@""];
    
    NSString *tempStringcode = [NSString stringWithFormat:@"%@%@",stringCode,@". "];
    NSString *newreferenceString = [tempStr stringByReplacingOccurrencesOfString:tempStringcode withString:@""];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"referenceNo == %@ AND guideTitle == %@ ", newreferenceString,titleStr];
    NSArray *filteredArray = [_arrayAudioGuide filteredArrayUsingPredicate:predicate];
    
    IBAudioGuide *audioGuideInfo = nil;
    
    
    if ([filteredArray count] > 0)
    {
        audioGuideInfo = [filteredArray objectAtIndex:0];
    }
    
    
    if ([audioGuideInfo.guideDescription  isEqualToString:@"<Desc>-1</Desc>"] && [numberOfAuodios isEqualToString:@"no subRooms"])
    {
        audioGuideInfo = [filteredArray objectAtIndex:1];
    }
    
    
    //NSLog(@"%@",audioGuideInfo.guideDescription);
    //NSLog(@"%@",audioGuideInfo.guideTitle);
    //NSLog(@"%@",audioGuideInfo.referenceNo);
    //NSLog(@"%@",audioGuideInfo.isSubArray);
    
    
    if ([audioGuideInfo.guideDescription  isEqualToString:@"<Desc>-1</Desc>"])
    {
        
        //   [self.treeView deselectRowForItem:item animated:YES ];
        //  [self.treeView reloadRowsForItems:item withRowAnimation:RATreeViewRowAnimationAutomatic];
        
        if ([self.treeView isCellForItemExpanded:item])
        {
            selecedCell.additionButtonHidden = NO;
        }
        else
        {
            selecedCell.isExpanded = YES;
            selecedCell.additionButtonHidden = YES;
        }
        
        [selecedCell setAdditionButtonHidden:selecedCell.additionButtonHidden cellInfo:selecedCell];
        // [cell setAdditionButtonHidden:YES cellInfo:cell];
    }
    else
    {
        
        // [UIApplication sharedApplication]
        [SharedAppDelegate setSelectedGuideTitleForGallery:selecedCell.customTitleLabel.text ] ;
        
        
        for (RATableViewCell *temp in self.treeView.visibleCells)
        {
            //NSLog(@"%@",temp.customTitleLabel.text);
            //NSLog(@"%@",tempCell.customTitleLabel.text);
            if ([temp.customTitleLabel.text isEqualToString:previousTitleString] && tempCell.isCellDequed)
            {
                
                if ([[previousCellReferenceNo stringByTrimmingCharactersInSet:[NSCharacterSet lowercaseLetterCharacterSet]] isEqualToString:previousCellReferenceNo] && !isPreviousCellSubArray)
                {
                    [tempCell UnselectedCell:YES];
                    //NSLog(@"cell Unselected Dequed Cell");
                }
                else
                {
                    [tempCell UnselectedCell:NO];
                    //NSLog(@"cell selected Dequed Cell");
                }
                
                // isScrolledTreeView = NO;
            }
            else if(!temp.isCellDequed)
            {
                if ([[previousCellReferenceNo stringByTrimmingCharactersInSet:[NSCharacterSet lowercaseLetterCharacterSet]] isEqualToString:previousCellReferenceNo] && !isPreviousCellSubArray)
                {
                    [tempCell UnselectedCell:YES];
                    //NSLog(@"cell Unselected NonDequed Cell");
                }
                else
                {
                    [tempCell UnselectedCell:NO];
                    //NSLog(@"cell Selected NonDequed Cell");
                }
                
            }
            else
            {
                //NSLog(@"exception");
            }
        }
        
        
        
        
        
        previousVisibleCells = self.treeView.visibleCells;
        
        
        
        
        
        tempCell = nil;
        [selecedCell selectedCellInRows];
        
        previousTitleString = selecedCell.customTitleLabel.text;
        
        tempCell = selecedCell;
        previousCellReferenceNo = audioGuideInfo.referenceNo;
        previousCellDescription = audioGuideInfo.guideDescription;
        selecedCell.isSelectedCell = YES;
        
        isPreviousCellSubArray = NO;
        if (forPreviousData.isSubArray)
        {
            [SharedAppDelegate setIsSubRoom:YES];
            isPreviousCellSubArray = YES;
        }
        else
        {
            [SharedAppDelegate setIsSubRoom:NO];
        }
        
        //NSLog(@"%@",audioGuideInfo.referenceNo);
        
        NSArray *arrayGuideGallery = [[SharedAppDelegate database] fetchGuideGalleryByAudioReferenceIndex:audioGuideInfo.referenceNo];
        
        NSMutableArray *arrayImagePaths = [NSMutableArray array];
        for(IBGuideGallery *guideGalleryInfo in arrayGuideGallery)
        {
            [arrayImagePaths addObject:guideGalleryInfo.guideGalleryFilePath];
        }
        
        
        //NSLog(@"%@",audioGuideInfo.referenceNo);
        NSInteger locationNo = [SharedAppDelegate locationNo];
        NSString *tempCoverPath = [[SharedAppDelegate database] loadCoverPhotoOfCurrespondWithLocationNo:locationNo andReferenceNo:audioGuideInfo.referenceNo];
        guideDetailController.arrayImagePathsGallery = [NSArray arrayWithArray:arrayImagePaths];
        guideDetailController.arrayImagePaths = [[NSArray alloc]initWithObjects:tempCoverPath, nil];
        
        
        IBAudioGuideInfo *currentGuide = [[IBAudioGuideInfo alloc] init];
        currentGuide.guideDescription = audioGuideInfo.guideDescription;
        currentGuide.audioFilePath = audioGuideInfo.audioFilePath;
        currentGuide.guideTitle = audioGuideInfo.guideTitle;
        currentGuide.referenceNo = audioGuideInfo.referenceNo;
        currentGuide.thumbnailPath = audioGuideInfo.thumbnailPath;
        guideDetailController.audioGuideInfo = currentGuide;
        
        [UIView animateWithDuration:0.5f animations:^{
            [guideDetailController refreshView];
        }];
        
        if([[SharedAppDelegate childSplitViewController] showThirdContainer]!=YES)  {
            [[SharedAppDelegate childSplitViewController] setShowThirdContainer:YES];
            [SharedAppDelegate setIsDetailPageAppeared:YES];
            _isDetailPageAppearedOnce = [SharedAppDelegate isDetailPageAppeared];
        }
        
        
    }
    
}
- (UITableViewCellEditingStyle)treeView:(RATreeView *)treeView editingStyleForRowForItem:(id)item;
{
    return UITableViewCellEditingStyleNone;
}

- (void)loadData
{
    [self refreshView];
    
    [arrayOfSelectedGuide removeAllObjects];
    [self loadMainRoomNumbersDetails];
    
    RADataObject *expandableRow;
    NSMutableArray *rowArray = [[NSMutableArray alloc]init];
    
    
    NSString *referenceNumber;
    for (IBAudioGuide *obj in arrayOfSelectedGuide)
    {
        //NSLog(@"%@",obj.guideTitle);
        //NSLog(@"%@",obj.guideDescription);
        //NSLog(@"%@",obj.referenceNo);
        if ([obj.guideDescription isEqualToString:@"<Desc>-1</Desc>"])
        {
            NSString *subTitle = [NSString stringWithFormat:@"%@. %@",stringCode,obj.referenceNo];
            NSMutableAttributedString *mutableAttributedString= [[NSMutableAttributedString alloc] initWithString:subTitle];
            [mutableAttributedString addAttribute:NSFontAttributeName value:kAppFontSemibold(11) range:NSMakeRange(0, [subTitle length])];
            [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:RGBCOLOR(124, 124, 124) range:NSMakeRange(0, [subTitle length])];
            
            
            NSString *secondSubTitle = [NSString stringWithFormat:@"Nr.audio"];
            NSMutableAttributedString *mutableAttributedStringsecondSubTitle = [[NSMutableAttributedString alloc] initWithString:secondSubTitle];
            [mutableAttributedStringsecondSubTitle addAttribute:NSFontAttributeName value:kAppFontSemibold(11) range:NSMakeRange(0, [secondSubTitle length])];
            [mutableAttributedStringsecondSubTitle addAttribute:NSForegroundColorAttributeName value:RGBCOLOR(124, 124, 124) range:NSMakeRange(0, [secondSubTitle length])];
            
            expandableRow =   [RADataObject dataObjectWithName:obj.guideTitle detailDescription:mutableAttributedString detailSecondDesc:mutableAttributedStringsecondSubTitle ProperImage:obj.thumbnailPath children:nil];
            NSString *mainGuide = obj.guideTitle;
            
            referenceNumber = obj.referenceNo;
            for(IBAudioGuide *obj in _arrayAudioGuide)
            {
                if ([referenceNumber isEqualToString:[obj.referenceNo  stringByTrimmingCharactersInSet:[NSCharacterSet lowercaseLetterCharacterSet]]]&& ![obj.guideDescription isEqualToString:@"<Desc>-1</Desc>"])
                {
                    
                    NSString *codString = stringCode;
                    NSMutableAttributedString *codeAttributedString = [[NSMutableAttributedString alloc]initWithString:codString];
                    [codeAttributedString addAttribute:NSFontAttributeName value:kAppFontSemibold(11) range:NSMakeRange(0, [codeAttributedString length])];
                    [codeAttributedString addAttribute:NSForegroundColorAttributeName value:RGBCOLOR(124, 124, 124) range:NSMakeRange(0, [codString length])];
                    
                    
                    NSString *subTitle1 = [NSString stringWithFormat:@". %@",obj.referenceNo];
                    NSMutableAttributedString *mutableAttributedString= [[NSMutableAttributedString alloc] initWithString:subTitle1];
                    [mutableAttributedString addAttribute:NSFontAttributeName value:kAppFontBold(11) range:NSMakeRange(0, [subTitle1 length])];
                    [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:RGBCOLOR(124, 124, 124) range:NSMakeRange(0, [subTitle1 length])];
                    
                    
                    NSMutableAttributedString *joiningAttributeString = [[NSMutableAttributedString alloc]initWithAttributedString:codeAttributedString];
                    [joiningAttributeString appendAttributedString:mutableAttributedString];
                    
                    NSString *secondSubTitle1 = [NSString stringWithFormat:@"%@%@%@",@"(",mainGuide,@")"];
                    NSMutableAttributedString *mutableAttributedStringsecondSubTitle1 = [[NSMutableAttributedString alloc] initWithString:secondSubTitle1];
                    [mutableAttributedStringsecondSubTitle1 addAttribute:NSFontAttributeName value:kAppFontBold(11) range:NSMakeRange(0, [secondSubTitle1 length])];
                    [mutableAttributedStringsecondSubTitle1 addAttribute:NSForegroundColorAttributeName value:RGBCOLOR(124, 124, 124) range:NSMakeRange(0, [secondSubTitle1 length])];
                    
                    RADataObject *tempArray = [RADataObject dataObjectWithName:obj.guideTitle detailDescription:joiningAttributeString detailSecondDesc:mutableAttributedStringsecondSubTitle1 ProperImage:obj.thumbnailPath children:nil];
                    tempArray.isSubArray = YES;
                    [expandableRow addChild:tempArray];
                    
                    
                } else {
                    
                    
                    
                }
            }
        } else {
            [obj setValue:@"YES" forKey:@"isSubArray"];
            
            NSString *codString = stringCode;
            NSMutableAttributedString *codeAttributedString = [[NSMutableAttributedString alloc]initWithString:codString];
            [codeAttributedString addAttribute:NSFontAttributeName value:kAppFontSemibold(11) range:NSMakeRange(0, [codeAttributedString length])];
            [codeAttributedString addAttribute:NSForegroundColorAttributeName value:RGBCOLOR(124, 124, 124) range:NSMakeRange(0, [codString length])];
            
            NSString *subTitle2 = [NSString stringWithFormat:@". %@",obj.referenceNo];
            NSMutableAttributedString *mutableAttributedString= [[NSMutableAttributedString alloc] initWithString:subTitle2];
            [mutableAttributedString addAttribute:NSFontAttributeName value:kAppFontBold(11) range:NSMakeRange(0, [subTitle2 length])];
            [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:RGBCOLOR(124, 124, 124) range:NSMakeRange(0, [subTitle2 length])];
            
            NSMutableAttributedString *joiningAttributeString = [[NSMutableAttributedString alloc]initWithAttributedString:codeAttributedString];
            [joiningAttributeString appendAttributedString:mutableAttributedString];
            
            NSString *secondSubTitle2 = [NSString stringWithFormat:@"Nr.audio"];
            NSMutableAttributedString *mutableAttributedStringsecondSubTitle2 = [[NSMutableAttributedString alloc] initWithString:secondSubTitle2];
            [mutableAttributedStringsecondSubTitle2 addAttribute:NSFontAttributeName value:kAppFontSemibold(11) range:NSMakeRange(0, [secondSubTitle2 length])];
            [mutableAttributedStringsecondSubTitle2 addAttribute:NSForegroundColorAttributeName value:RGBCOLOR(124, 124, 124) range:NSMakeRange(0, [secondSubTitle2 length])];
            
            expandableRow =   [RADataObject dataObjectWithName:obj.guideTitle detailDescription:joiningAttributeString detailSecondDesc:mutableAttributedStringsecondSubTitle2 ProperImage:obj.thumbnailPath children:nil];
        }
        //
        expandableRow.isSubArray = NO;
        [rowArray addObject:expandableRow];
    }
    
    //
    //	RADataObject *N1 = [RADataObject dataObjectWithName:@"N 1" children:nil];
    //	RADataObject *N2 = [RADataObject dataObjectWithName:@"N 2" children:nil];
    //
    //
    //	RADataObject *Nsome = [RADataObject dataObjectWithName:@"else"
    //											  children:[NSArray arrayWithObjects:N1, N2, nil]];
    
    
    self.data = (NSArray *) rowArray;
    
}



-(void)handleSwipeAndMove:(UISwipeGestureRecognizer *)swipeGesture
{
    if (_isDetailPageAppearedOnce) {
        //
        [[NSNotificationCenter defaultCenter] postNotificationName:@"swipeTouchOnTableNotification" object:self];
    } else {
        //
        
    }
}

-(void)setStatus:(Status)status {
    //
    _status = status;
    //
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:[NSString stringWithFormat:@"%d",_status] forKey:self.key];
    [defaults synchronize];
}

- (void)requestStarted:(ASIHTTPRequest *)request {
    //
    self.status = StatusDownloading;
}

- (void)requestFinished:(ASIHTTPRequest *)request {
    //
    NSError *error = nil;
    NSString *filename = [request.url lastPathComponent];
    NSString *unZipTo = _destinationDir;
    NSString *zipFilePath = [NSString stringWithFormat:@"%@%@",_destinationDir, filename];
    //
    NSLog(@"filename %@", filename);
    NSLog(@"zipFilePath %@", zipFilePath);
    NSLog(@"unZipTo %@", unZipTo);
    //
    if ([self _unZipFile:zipFilePath to:unZipTo]) {
        //
        [[NSFileManager defaultManager] removeItemAtPath:zipFilePath error:&error];
    }
    
    //
    self.status = StatusDownloaded;
    __weak __typeof(self) __weakSelf = self;
    //
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        //
        [hud setProgress:1.0f];
        [hud hide:YES afterDelay:0.2];
        
        [__weakSelf _loadViewAgain];
        [__weakSelf _reFresh];
        //
        
    });
}

- (void)requestFailed:(ASIHTTPRequest *)request {
    //
    NSLog(@"requestFailedrequestFailed = %@", request.error);
    self.status = StatusDownloaded;
    [hud hide:YES afterDelay:0.2];
    
    [SharedAppDelegate unlockInteractions];
}

- (void)request:(ASIHTTPRequest *)request didReceiveBytes:(long long)bytes {
    //
    if(bytes > 0) {
        //
        receivedBytes = receivedBytes+bytes;
        float percentage = (float)receivedBytes/(float)contentLength;
        [hud setProgress:percentage];
    }
}

- (void)request:(ASIHTTPRequest *)request incrementDownloadSizeBy:(long long)newLength
{
    if (newLength > 0) contentLength = newLength;
    
    [self.treeView reloadData];
}

-(BOOL)_unZipFile:(NSString *)zipFilePath to:(NSString *)unZipTo {
    //
    BOOL flag = NO;
    ZipArchive *zipArchive = [[ZipArchive alloc] init];
    [zipArchive UnzipOpenFile:zipFilePath];
    flag = [zipArchive UnzipFileTo:unZipTo overWrite:YES];
    [zipArchive UnzipCloseFile];
    //
    return flag;
}

#pragma mark -
#pragma mark - IBGeoMapViewControllerTabletDelegate methods

- (void)wasOnDisplayController
{
    self.view.alpha = 0.f;
    [[SharedAppDelegate guideDetailViewController].view setAlpha:0.f];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self performSegueWithIdentifier:@"segueToMapView" sender:self];
        [UIView animateWithDuration:0.8f animations:^{
            self.view.alpha = 1.f;
            [[SharedAppDelegate guideDetailViewController].view setAlpha:1.f];
        }];
    });
}

#pragma mark -
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"segueToMapView"])   {
        geoMapController = [segue destinationViewController];
        geoMapController.delegate = self;
        [SharedAppDelegate setGeoMapViewController:geoMapController];
    }
}

- (void)OneTouchOnMapButtonContainerTouched:(UITapGestureRecognizer *)tapGeusture
{
    [self.searchDisplayController setActive:NO animated:YES];
    [self performSegueWithIdentifier:@"segueToMapView" sender:self];
}

- (void)buttonBackTouched
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)refreshView
{
    NSInteger locationNo = [SharedAppDelegate locationNo];
    NSUInteger languageIndex = [SharedAppDelegate languageIndex];
    
    NSString *museumDir,*languageDir,*museumName;
    
    MUSEUMNAME_STR(locationNo, museumName);
    
    labelSubTitle.text = museumName;
    
    NSDictionary *langDictionary = [SharedAppDelegate langDictionary];
    
    self.tableSearchBar.placeholder = [langDictionary valueForKey:@"LabelSearchPlaceholder"];
    
    [self.tableSearchBar setImage:[UIImage imageNamed:@"search@2x.png"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    labelTitle.text = [langDictionary valueForKey:@"LabelAudioGuide"];
    
    stringCode = [[langDictionary valueForKey:@"LabelCode"] substringToIndex:3];
    stringPosition = [langDictionary valueForKey:@"LabelPosition"];
    
    DIRLOCATION_STR(locationNo,museumDir);
    DIRLANGUAGE_STR(languageIndex, languageDir);
    
    if(_arrayAudioGuide) { _arrayAudioGuide = nil; }
    if(_arraySearchedGuide) { [_arraySearchedGuide removeAllObjects]; _arraySearchedGuide = nil; }
    
    _arrayAudioGuide = [[SharedAppDelegate database] loadAudioGuideWithLocationNo:locationNo andLanguageIndex:languageIndex];
    
    [self.tableViewAudioGuide setAlpha:1.f];
    [[SharedAppDelegate guideDetailViewController].view setAlpha:1.f];
    //   [self.tableViewAudioGuide reloadData];
    
    NSMutableArray *arrayAudioGuideInfos = [NSMutableArray array];
    for(IBAudioGuide *audioGuide in _arrayAudioGuide)   {
        IBAudioGuideInfo *currentGuide = [[IBAudioGuideInfo alloc] init];
        currentGuide.guideDescription = audioGuide.guideDescription;
        currentGuide.audioFilePath = audioGuide.audioFilePath;
        currentGuide.guideTitle = audioGuide.guideTitle;
        currentGuide.referenceNo = audioGuide.referenceNo;
        currentGuide.thumbnailPath = audioGuide.thumbnailPath;
        
        [arrayAudioGuideInfos addObject:currentGuide];
    }
    
    [SharedAppDelegate setAudioGuideInfos:arrayAudioGuideInfos];
    
    if([SharedAppDelegate playerHidden] == NO)  {
        self.tableViewBottomConstraint.constant = 65;
        
        if ([SharedAppDelegate playerHidden] != NO) {
            [SharedAppDelegate animatePlayerHidden:NO];
        } 
    }
    
    [SharedAppDelegate unlockInteractions];
    //NSLog(@"audioguideviewcontroller Hud");
    [HUDManager hideHUDFromWindowAfterDelay:0.2f];
    if(hud) [hud hide:YES afterDelay:0.2f];
    //
    [self.tableViewAudioGuide reloadData];
}


-(void)loadElementsWithoutExpandRow
{
    [arrayWithoutExpandRow removeAllObjects];
    for(NSArray *array in _arrayAudioGuide)
    {
        if([[array valueForKey:@"guideDescription"] isEqualToString:@"<Desc>-1</Desc>"])
        {
            
        }
        else
        {
            [arrayWithoutExpandRow addObject:array];
        }
        
    }
}

-(void)loadMainRoomNumbersDetails
{
    NSString *tempRefern_No;
    tempRefern_No = @"temp";
    
    for (NSArray *array in _arrayAudioGuide)
    {
        //NSLog(@"%@",[array valueForKey:@"referenceNo"]);
        //NSLog(@"%@",[array valueForKey:@"guideDescription"]);
        
        
        if ([[[array valueForKey:@"referenceNo"]  stringByTrimmingCharactersInSet:[NSCharacterSet lowercaseLetterCharacterSet]] isEqualToString:[array valueForKey:@"referenceNo"]] | [[array valueForKey:@"guideDescription"] isEqualToString:@"<Desc>-1</Desc>"])
        {
            if (![tempRefern_No isEqualToString:[array valueForKey:@"referenceNo"] ])
            {
                //NSLog(@"%@",[array valueForKey:@"referenceNo"]);
                //NSLog(@"%@",[array valueForKey:@"guideDescription"]);
                [arrayOfSelectedGuide addObject:array];
            }
            
            tempRefern_No = [array valueForKey:@"referenceNo"];
        }
    }
}
#pragma mark - Table view data source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        //	[arrayWithoutExpandRow removeAllObjects];
        [self loadElementsWithoutExpandRow];
        return [_arraySearchedGuide count];
    }
    else
    {
        if(isFirstTym)
        {
            [arrayOfSelectedGuide removeAllObjects];
            [self loadMainRoomNumbersDetails];
        }
        //NSLog(@"%lu", (unsigned long)[arrayOfSelectedGuide count]);
        return [arrayOfSelectedGuide count];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = (IBAudioGuideTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    IBAudioGuide *audioGuideInfo;
    
    if (tableView == self.searchDisplayController.searchResultsTableView) audioGuideInfo = [_arraySearchedGuide objectAtIndex:[indexPath row]];
    else audioGuideInfo = [arrayOfSelectedGuide objectAtIndex:[indexPath row]];
    
    if (isFirstTym) {
        if (cell == nil) {
            cell = [[IBAudioGuideTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleGray;
            cell.exclusiveTouch = YES;
            
            [cell.imageView setHidden:NO];
        }
        
        //NSLog(@"%@/%@",audioGuideInfo.thumbnailPath,[NSString stringWithFormat:@"%@/%@",[IBGlobal createDocumentsDirectoryByAppendingPathComponent:nil],audioGuideInfo.thumbnailPath]);
        
        //	cell.imageView.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[IBGlobal createDocumentsDirectoryByAppendingPathComponent:nil],audioGuideInfo.thumbnailPath]];
        cell.textLabel.text = audioGuideInfo.guideTitle;
        cell.textLabel.font = kAppFontSemibold(16);
        //cell.backgroundColor = [UIColor clearColor];
        
        
    } else {
        if (cell == nil) {
            //
            cell = [[IBAudioGuideTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleGray;
            cell.exclusiveTouch = YES;
            [cell.imageView setHidden:NO];
        }
        cell.imageView.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[IBGlobal createDocumentsDirectoryByAppendingPathComponent:nil],audioGuideInfo.thumbnailPath]];
        cell.textLabel.text = audioGuideInfo.guideTitle;
        cell.textLabel.font = kAppFontSemibold(16);
    }
    //
    NSString *subTitle = [NSString stringWithFormat:@"%@. %@",stringCode,audioGuideInfo.referenceNo];
    NSMutableAttributedString *mutableAttributedString= [[NSMutableAttributedString alloc] initWithString:subTitle];
    [mutableAttributedString addAttribute:NSFontAttributeName value:kAppFontSemibold(11) range:NSMakeRange(0, [subTitle length])];
    [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:RGBCOLOR(124, 124, 124) range:NSMakeRange(0, [subTitle length])];
    //
    if (indexPath.row == previousPressedRowIndex) {
        //
        cell.accessoryView.backgroundColor = RGBCOLOR(117, 143, 150);
        [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, mutableAttributedString.length)];
        cell.detailTextLabel.attributedText = mutableAttributedString;
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"notExpanded@2x.png"]];
        cell.backgroundColor = RGBCOLOR(117, 143, 150);
        
    } else if ([[audioGuideInfo valueForKey:@"guideDescription"]  isEqualToString:@"<Desc>-1</Desc>"]) {
        //
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.attributedText = mutableAttributedString;
        cell.backgroundColor = [UIColor clearColor];
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"isExpanded@2x.png"]];
        cell.accessoryView.backgroundColor = [UIColor clearColor];
        
    } else if ([[audioGuideInfo valueForKey:@"isSubArray"] isEqualToString:@"YES"]) {
        //
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.attributedText = mutableAttributedString;
        cell.backgroundColor = [UIColor yellowColor];
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"navigate-normal.png"]];
        cell.accessoryView.backgroundColor = [UIColor yellowColor];
    } else {
        //
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.attributedText = mutableAttributedString;
        cell.backgroundColor = [UIColor clearColor];
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"navigate-normal.png"]];
        cell.accessoryView.backgroundColor = [UIColor clearColor];
    }
    //NSLog(@"%@",audioGuideInfo.description);
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

#pragma mark -
#pragma mark - Table view delegates

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    // do something here
    currentIndex = indexPath.row;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    currentIndex = indexPath.row;
    // selectedIndex
    //     isPlayerHidden = [SharedAppDelegate playerHidden];
    //    if(!isPlayerHidden)    {
    //        [SharedAppDelegate animatePlayerHidden:YES];
    //        isPlayerHidden = YES;
    //    }
    //    [[SharedAppDelegate playerViewTablet]setPlaying:NO];
    //    [SharedAppDelegate pauseAudioPlayer];
    
    IBGuideDetailViewControllerTablet *guideDetailController = [SharedAppDelegate guideDetailViewController];
    
    IBAudioGuide *audioGuideInfo;
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        NSIndexPath *indexPath = [self.searchDisplayController.searchResultsTableView indexPathForSelectedRow];
        audioGuideInfo = [_arraySearchedGuide objectAtIndex:[indexPath row]];
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        [SharedAppDelegate setSelectedGuideTitleForGallery:cell.textLabel.text ];
    }
    else
    {
        NSIndexPath *indexPath = [self.tableViewAudioGuide indexPathForSelectedRow];
        audioGuideInfo = [arrayOfSelectedGuide objectAtIndex:[indexPath row]];
    }
    
    NSArray *arrayGuideGallery = [[SharedAppDelegate database] fetchGuideGalleryByAudioReferenceIndex:audioGuideInfo.referenceNo];
    
    NSMutableArray *arrayImagePaths = [NSMutableArray array];
    for(IBGuideGallery *guideGalleryInfo in arrayGuideGallery)
    {
        [arrayImagePaths addObject:guideGalleryInfo.guideGalleryFilePath];
    }
    
    //NSLog(@"%@",arrayImagePaths);
    //  guideDetailController.arrayImagePaths = [NSArray arrayWithArray:arrayImagePaths];
    
    IBAudioGuideInfo *currentGuide = [[IBAudioGuideInfo alloc] init];
    currentGuide.guideDescription = audioGuideInfo.guideDescription;
    currentGuide.audioFilePath = audioGuideInfo.audioFilePath;
    currentGuide.guideTitle = audioGuideInfo.guideTitle;
    currentGuide.referenceNo = audioGuideInfo.referenceNo;
    currentGuide.thumbnailPath = audioGuideInfo.thumbnailPath;
    //NSLog(@"%@",audioGuideInfo.referenceNo);
    
    
    //  NSArray *arrayGuideGallery = [[SharedAppDelegate database] fetchGuideGalleryByAudioReferenceIndex:audioGuideInfo.referenceNo];
    
    //   NSMutableArray *arrayImagePaths = [NSMutableArray array];
    //    for(IBGuideGallery *guideGalleryInfo in arrayGuideGallery)
    //    {
    //        [arrayImagePaths addObject:guideGalleryInfo.guideGalleryFilePath];
    //    }
    
    //NSLog(@"%@",arrayImagePaths);
    NSInteger locationNo = [SharedAppDelegate locationNo];
    NSString *tempCoverPath = [[SharedAppDelegate database] loadCoverPhotoOfCurrespondWithLocationNo:locationNo andReferenceNo:audioGuideInfo.referenceNo];
    guideDetailController.arrayImagePathsGallery = [NSArray arrayWithArray:arrayImagePaths];
    guideDetailController.arrayImagePaths = [[NSArray alloc]initWithObjects:tempCoverPath, nil];
    
    
    guideDetailController.audioGuideInfo = currentGuide;
    [UIView animateWithDuration:0.5f animations:^{
        [guideDetailController refreshView];
    }];
    
    if([[SharedAppDelegate childSplitViewController] showThirdContainer]!=YES) {
        //
        [[SharedAppDelegate childSplitViewController] setShowThirdContainer:YES];
        [SharedAppDelegate setIsDetailPageAppeared:YES];
        _isDetailPageAppearedOnce = [SharedAppDelegate isDetailPageAppeared];
        
    }
    //[self.tableViewAudioGuide reloadData];
}


-(void)removePreviousInsertedData
{
    for (NSArray *objArray in arraySubOfMainRoomTags) {
        //
        [arrayOfSelectedGuide removeObject:objArray];
    } 
}
-(void)addObjectsInBetWeenArray
{
    
    for (NSArray *objArray in arraySubOfMainRoomTags) {
        
        [arrayOfSelectedGuide insertObject:objArray atIndex:currentIndex+1];
        currentIndex ++;
    }
    //NSLog(@"%lu",(unsigned long)[arrayOfSelectedGuide count]);
}
#pragma mark -
#pragma mark Content Filtering

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    // Update the filtered array based on the search text and scope.
    
    // Remove all objects from the filtered search array
    if(_arraySearchedGuide) { [_arraySearchedGuide removeAllObjects]; _arraySearchedGuide = nil; }
    
    // Filter the array using NSPredicate
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"guideTitle contains[c] %@",searchText];
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"referenceNo contains[c] %@",searchText];
    NSPredicate *predicate = [NSCompoundPredicate orPredicateWithSubpredicates:[NSArray arrayWithObjects:predicate1, predicate2, nil]];
    
    NSArray *tempArray1 = [arrayWithoutExpandRow filteredArrayUsingPredicate:predicate];
    
    _arraySearchedGuide = [NSMutableArray arrayWithArray:tempArray1];
}


#pragma mark - UISearchDisplayController Delegate Methods

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    // Tells the table data source to reload when text changes
    [self filterContentForSearchText:searchString scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
{
    // Tells the table data source to reload when scope bar selection changes
    [self filterContentForSearchText:[self.searchDisplayController.searchBar text] scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    self.tableSearchBar.placeholder = @"";
    
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    self.tableSearchBar.placeholder = [[SharedAppDelegate langDictionary] valueForKey:@"LabelSearchPlaceholder"];
    
    return YES;
}

- (void)dismissIfExistedMapViewController
{
    if(geoMapController) [geoMapController dismissViewControllerAnimated:NO completion:NULL];
}

#pragma mark -

- (NSString *)key {
    //
    NSString *keyString = [[[_urlString stringByReplacingOccurrencesOfString:SYNC_URL withString:@""] base64EncodedString] uppercaseString];
    NSMutableString *newString = [NSMutableString string];
    //
    for (int i = 0; i < [keyString length]; i++) {
        //
        int ascii = [keyString characterAtIndex:i];
        if (((ascii>64) && (ascii<91)) || ((ascii>47) && (ascii<58))) {
            //
            [newString appendFormat:@"%c",ascii];
        }
    }
    //
    return [NSString stringWithString:newString];
}

// custom Dewlegate For Relaod Ttable

-(void)reloadTreeview:(NSNotificationCenter *)notification
{
    if (treeView.frame.size.height ==  self.view.frame.size.height - 110) {
        
    } else {
        [UIView animateWithDuration:1.0f animations:^{
            treeView.frame = CGRectMake(-14, 45, self.view.frame.size.width+15, self.view.frame.size.height - 110) ;
            
        }]; 
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate {
    //
    return NO;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations {
    //
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    //
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
}

@end
