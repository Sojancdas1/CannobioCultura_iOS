//
//  IBAudioGuideXMLInfo.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 26/05/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBAudioGuideXMLInfo.h"

@implementation IBAudioGuideXMLInfo

@synthesize referenceNo;
@synthesize guideTitle;
@synthesize guideDescription;

@end
