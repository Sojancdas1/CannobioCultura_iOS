//
//  IBAudioPlayer.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 25/06/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IBAudioPlayerViewDelegate
@required
- (void)playButtonTriggered;
- (void)autoPlayStateChanged;
@end

@interface IBAudioPlayerView : UIView
{
    UIImageView *_thumbnailImageView; 
    
    UIButton *_playPauseButton;
    UIButton *_autoPlayStateButton;
    
    UILabel *_autoPlayTitleLabel;
    UILabel *_autoPlayStatusLabel;
    UILabel *_titleLabel;
    UILabel *_locationLabel; 
}

@property (nonatomic, assign) id<IBAudioPlayerViewDelegate> delegate;

@property (nonatomic, strong, readonly) UIImageView *thumbnailImageView;
@property (nonatomic, strong, readonly) UILabel *locationLabel;
@property (nonatomic, strong, readonly) UILabel *titleLabel;
@property (nonatomic, strong, readonly) UILabel *detailLabel;

@property (nonatomic, assign) BOOL playing;
@property (nonatomic, assign) BOOL isAppeared;
@property (nonatomic, assign) BOOL autoPlayEnabled;

- (void)refreshView;
- (void)playPauseButtonTouched;
- (void)autoPlayStateButtonTouched;
@end
