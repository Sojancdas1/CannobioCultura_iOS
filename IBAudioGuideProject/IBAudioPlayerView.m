//
//  IBAudioPlayer.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 25/06/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBAudioPlayerView.h"

#import "IBGlobal.h"

#define kPlayerWidth [UIScreen mainScreen].bounds.size.width
#define kPlayerHeight 45

@interface IBAudioPlayerViewInternal :UITableViewCell

@property (nonatomic, strong) IBOutlet UIImageView *thumb;
@property (nonatomic, strong) IBOutlet UILabel *locationLabel;
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UILabel *autoPlayTitleLabel;
@property (nonatomic, strong) IBOutlet UILabel *autoPlayStatusLabel;
@property (nonatomic, strong) IBOutlet UIButton *playPauseButton;
@property (nonatomic, strong) IBOutlet UIButton *autoPlayStateButton;
@end

@implementation IBAudioPlayerViewInternal

@end

@interface IBAudioPlayerView ()

@property (nonatomic, strong, readonly) UIButton *playPauseButton;
@property (nonatomic, strong, readonly) UIButton *autoPlayStateButton;
@property (nonatomic, strong, readonly) UIImageView *playPauseIconView;
@property (nonatomic, strong, readonly) UIView *autoplayBackground;
@property (nonatomic, strong, readonly) UILabel *autoPlayTitleLabel;
@property (nonatomic, strong, readonly) UILabel *autoPlayStatusLabel;

@property (nonatomic, strong) IBAudioPlayerViewInternal *xib;



@end

@implementation IBAudioPlayerView

@synthesize delegate = _delegate;
@synthesize thumbnailImageView = _thumbnailImageView;
@synthesize locationLabel = _locationLabel;
@synthesize titleLabel = _titleLabel;
@synthesize playPauseButton = _playPauseButton;
@synthesize autoPlayStateButton = _autoPlayStateButton;
@synthesize autoPlayTitleLabel = _autoPlayTitleLabel;
@synthesize autoPlayStatusLabel = _autoPlayStatusLabel;
@synthesize playing = _playing;
@synthesize detailLabel = _detailLabel;
@synthesize autoPlayEnabled = _autoPlayEnabled;
@synthesize isAppeared = _isAppeared;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, kPlayerWidth, kPlayerHeight)];
    if (self) {
        //
        IBAudioPlayerViewInternal *xib = [self xib];
        xib.frame = self.bounds;
        //
        [self addSubview:xib.contentView];
        [self _setUp];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_audioPlayerDidFinishPlaying:) name:@"audioPlayerDidFinishPlaying" object:nil];
    }
    //
    return self;
}

-(void)_audioPlayerDidFinishPlaying:(NSNotification *)notification {
    [self _pause];
}

-(void)_pause {
    //
    _playing = NO;
    _playPauseButton.selected = _playing;
}

-(void)_play {
    //
    _playing = YES;
    _playPauseButton.selected = _playing;
}

-(void)_setUp {
    //
    self.backgroundColor = RGBCOLOR(117, 141, 150);
    //
    UIImageView *thumbnailImageView = self.thumbnailImageView;
    UILabel *locationLabel = self.locationLabel;
    UILabel *titleLabel = self.titleLabel;
    UIButton *playPauseButton = self.playPauseButton;
    UILabel *autoPlayTitleLabel = self.autoPlayTitleLabel;
    UILabel *autoPlayStatusLabel = self.autoPlayStatusLabel;
    UIButton *autoPlayStateButton = self.autoPlayStateButton;
    //
    
    thumbnailImageView.image = [UIImage imageNamed:@"logo.png"];
    //
    [autoPlayStateButton addTarget:self action:@selector(autoPlayStateButtonTouched) forControlEvents:UIControlEventTouchUpInside];
    //
    autoPlayTitleLabel.backgroundColor = [UIColor clearColor];
    autoPlayTitleLabel.textColor = [UIColor whiteColor];
    autoPlayTitleLabel.font = kAppFontSemibold(11);
    autoPlayTitleLabel.text = @"Automatic";
    autoPlayTitleLabel.adjustsFontSizeToFitWidth = YES;
    autoPlayTitleLabel.textAlignment = NSTextAlignmentCenter;
    
    autoPlayStatusLabel.backgroundColor = [UIColor clearColor];
    autoPlayStatusLabel.textColor = [UIColor whiteColor];
    autoPlayStatusLabel.font = kAppFontSemibold(11);
    autoPlayStatusLabel.text = @"Play: OFF";
    autoPlayStatusLabel.adjustsFontSizeToFitWidth = YES;
    
    [playPauseButton addTarget:self action:@selector(playPauseButtonTouched) forControlEvents:UIControlEventTouchUpInside];
    
    locationLabel.backgroundColor = [UIColor clearColor];
    locationLabel.textColor = RGBCOLOR(190, 201, 204);
    locationLabel.font = kAppFontSemibold(9);
    locationLabel.text = @"LOCATION";
    
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = kAppFontSemibold(14);
    titleLabel.text = @"Audio Title";
    
    _playing = NO;
    _autoPlayEnabled = NO;
    _isAppeared = NO;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

- (void)setPlaying:(BOOL)playing {
    //
    if (playing) {
        [self _play];
    } else {
        [self _pause];
    }
}

- (void)setAutoPlayEnabled:(BOOL)autoPlayEnabled
{
    _autoPlayEnabled = autoPlayEnabled;
    
    NSDictionary *langDictionary = [SharedAppDelegate langDictionary];
    
    NSString *labelString;
    NSMutableAttributedString *mutableAttributedString;
    if(!_autoPlayEnabled) {
        labelString = [NSString stringWithFormat:@"%@: OFF",[langDictionary valueForKey:@"LabelPlay"]];
        NSRange offRange = [labelString rangeOfString:@"OFF"];
        mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:labelString];
        [mutableAttributedString addAttribute:NSFontAttributeName value:kAppFontSemibold(11) range:NSMakeRange(0, [labelString length])];
        [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, [labelString length])];
        [mutableAttributedString addAttribute:NSFontAttributeName value:kAppFontBold(11) range:offRange];
    } else {
        labelString = [NSString stringWithFormat:@"%@: ON",[langDictionary valueForKey:@"LabelPlay"]];
        NSRange onRange = [labelString rangeOfString:@"ON"];
        mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:labelString];
        [mutableAttributedString addAttribute:NSFontAttributeName value:kAppFontSemibold(11) range:NSMakeRange(0, [labelString length])];
        [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, [labelString length])];
        [mutableAttributedString addAttribute:NSFontAttributeName value:kAppFontBold(11) range:onRange];
    }
    //
    _autoPlayStatusLabel.attributedText = mutableAttributedString;
}

- (void)playPauseButtonTouched
{
    [self setPlaying:!_playing];
    [_delegate playButtonTriggered];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"PlayPauseButtonTapped" object:nil];
}

- (void)autoPlayStateButtonTouched
{
    [self setAutoPlayEnabled:!_autoPlayEnabled];
    [_delegate autoPlayStateChanged];
}

- (void)refreshView
{
    NSDictionary *langDictionary = [SharedAppDelegate langDictionary];
    
    _autoPlayTitleLabel.text = [langDictionary valueForKey:@"LabelAutomatic"];
    
    [self setPlaying:_playing];
    [self setAutoPlayEnabled:_autoPlayEnabled];
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    self.xib.contentView.frame = CGRectMake(0, self.frame.size.height-kPlayerHeight, width, kPlayerHeight);
}

-(IBAudioPlayerViewInternal *)xib {
    //
    if (_xib) return _xib;
    //
    IBAudioPlayerViewInternal *xib = [[NSBundle mainBundle] loadNibNamed:@"IBAudioPlayerView" owner:self options:nil][0];
    //
    _xib = xib;
    return _xib;
}

-(UIImageView *)thumbnailImageView {
    //
    if (_thumbnailImageView) return _thumbnailImageView;
    //
    _thumbnailImageView = self.xib.thumb;
    //
    return _thumbnailImageView;
}

-(UILabel *)locationLabel {
    
    if (_locationLabel) return _locationLabel;
    //
    _locationLabel = self.xib.locationLabel;
    //
    return _locationLabel;
}

-(UILabel *)titleLabel {
    
    if (_titleLabel) return _titleLabel;
    //
    _titleLabel = self.xib.titleLabel;
    //
    return _titleLabel;
    
    return self.xib.titleLabel;
}

-(UIButton *)playPauseButton {
    //
    if (_playPauseButton) return _playPauseButton;
    //
    _playPauseButton = self.xib.playPauseButton;
    //
    return _playPauseButton;
}

-(UIButton *)autoPlayStateButton {
    //
    if (_autoPlayStateButton) return _autoPlayStateButton;
    //
    _autoPlayStateButton = self.xib.autoPlayStateButton;
    //
    return _autoPlayStateButton;
}

-(UILabel *)autoPlayTitleLabel {
    //
    if (_autoPlayTitleLabel) return _autoPlayTitleLabel;
    //
    _autoPlayTitleLabel = self.xib.autoPlayTitleLabel;
    //
    return _autoPlayTitleLabel;
}

-(UILabel *)autoPlayStatusLabel {
    //
    if (_autoPlayStatusLabel) return _autoPlayStatusLabel;
    //
    _autoPlayStatusLabel = self.xib.autoPlayStatusLabel;
    //
    return _autoPlayStatusLabel;
}


@end
