//
//  IBAudioPlayerViewTablet.h
//  BITourGuideComponents
//
//  Created by Mobility 2014 on 15/08/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IBAudioPlayerViewTabletDelegate
@required
- (void)playButtonTriggered;
- (void)muteUnmuteTriggered;
- (void)autoPlayStateChanged;
@end

@interface IBAudioPlayerViewTablet : UIView
{
    UILabel *_locationLabel;
    UILabel *_titleLabel;
    UILabel *_detailLabel;
    UIImageView *_thumbnailImageView;
}

@property (nonatomic, assign) id<IBAudioPlayerViewTabletDelegate> delegate;

@property (nonatomic, strong, readonly) UILabel *titleLabel;
@property (nonatomic, strong, readonly) UILabel *detailLabel;
@property (nonatomic, strong, readonly) UILabel *locationLabel;
@property (nonatomic, strong, readonly) UIImageView *thumbnailImageView;

@property (nonatomic, strong, readonly) UILabel *durationLabel;
@property (nonatomic, strong, readonly) UILabel *timeElapsedLabel;

@property (nonatomic, assign) BOOL playing;
@property (nonatomic, assign) BOOL autoPlayEnabled;
@property (nonatomic, assign) BOOL speakerOn;
@property (nonatomic, assign) BOOL isAppeared;

- (void)refreshView;
- (void)setProgress:(float)progress;

@end
