//
//  IBAudioPlayerViewTablet.m
//  BITourGuideComponents
//
//  Created by Mobility 2014 on 15/08/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBAudioPlayerViewTablet.h"

#import "IBGlobal.h"
#import "TYMProgressBarView.h"
#import "VerticalSlider.h"
#import "UIButton+Extensions.h"

#define kPlayerWidth 930
#define kPlayerHeight 80

@interface IBAudioPlayerViewTablet ()
{
    UILabel *_autoPlayTitleLabel;
    UILabel *_autoPlayStatusLabel;
    UIView *_autoplayBackgroundView;
    UIButton *_autoPlayStateButton;
    UIImageView *_playPauseImageView;
    UIButton *_playPauseButton;
    UIButton *_muteUnmuteImageView;
    UIButton *_muteUnmuteButton;
    
    UILabel *_durationLabel;
    UILabel *_timeElapsedLabel;
    TYMProgressBarView *_progressView;
    VerticalSlider *_verticalSlider;
    BOOL isSliderHidden;
}

@property (nonatomic, strong, readonly) UILabel *autoPlayTitleLabel;
@property (nonatomic, strong, readonly) UILabel *autoPlayStatusLabel;
@property (nonatomic, strong, readonly) UIView *autoplayBackgroundView;
@property (nonatomic, strong, readonly) UIButton *autoPlayStateButton;
@property (nonatomic, strong, readonly) UIImageView *playPauseImageView;
@property (nonatomic, strong, readonly) UIButton *playPauseButton;
@property (nonatomic, strong, readonly) UIButton *muteUnmuteImageView;
@property (nonatomic, strong, readonly) UIButton *muteUnmuteButton;
@property (nonatomic, strong, readonly) UISlider *volumeSlider;

@property (nonatomic, strong, readonly) TYMProgressBarView *progressView;
@property (nonatomic, strong, readonly) VerticalSlider *verticalSlider;

- (void)setPlaying:(BOOL)playing;
- (void)setSpeakerOn:(BOOL)speakerOn;
- (void)playPauseButtonTouched;
- (void)muteUnmuteButtonTouched;
- (void)autoPlayStateButtonTouched;
- (void)setAutoPlayEnabled:(BOOL)autoPlayEnabled;

@end

@implementation IBAudioPlayerViewTablet

@synthesize delegate = _delegate;
@synthesize thumbnailImageView = _thumbnailImageView;
@synthesize locationLabel = _locationLabel;
@synthesize titleLabel = _titleLabel;
@synthesize detailLabel = _detailLabel;

@synthesize playing = _playing;
@synthesize autoPlayEnabled = _autoPlayEnabled;
@synthesize speakerOn = _speakerOn;

@synthesize autoPlayTitleLabel = _autoPlayTitleLabel;
@synthesize autoPlayStatusLabel = _autoPlayStatusLabel;
@synthesize autoplayBackgroundView = _autoplayBackgroundView;
@synthesize autoPlayStateButton = _autoPlayStateButton;
@synthesize playPauseImageView = _playPauseImageView;
@synthesize playPauseButton = _playPauseButton;
@synthesize muteUnmuteImageView = _muteUnmuteImageView;
@synthesize muteUnmuteButton = _muteUnmuteButton;

@synthesize durationLabel = _durationLabel;
@synthesize timeElapsedLabel = _timeElapsedLabel;
@synthesize progressView = _progressView;
@synthesize verticalSlider = _verticalSlider;
@synthesize isAppeared = _isAppeared;
@synthesize volumeSlider = _volumeSlider;
- (id)initWithFrame:(CGRect)frame
{
    isSliderHidden = YES;
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, kPlayerWidth, kPlayerHeight)];
    if (self) {
        // Initialization code
        self.backgroundColor = RGBCOLOR(119, 119, 119);
        
        _autoplayBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(kPlayerWidth-85, 0, 85, kPlayerHeight)];
        _autoplayBackgroundView.backgroundColor = RGBCOLOR(105, 144, 152);
        
        _autoPlayStateButton = [[UIButton alloc] initWithFrame:_autoplayBackgroundView.bounds];
        [_autoPlayStateButton addTarget:self action:@selector(autoPlayStateButtonTouched) forControlEvents:UIControlEventTouchUpInside];
        
        _autoPlayTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 13, _autoplayBackgroundView.frame.size.width, (kPlayerHeight-26)/2)];
        _autoPlayStatusLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 5+_autoPlayTitleLabel.frame.size.height, _autoPlayTitleLabel.frame.size.width, _autoPlayTitleLabel.frame.size.height)];
        
        _autoPlayTitleLabel.backgroundColor = [UIColor clearColor];
        _autoPlayTitleLabel.textColor = [UIColor whiteColor];
        _autoPlayTitleLabel.font = kAppFontSemibold(12);
        _autoPlayTitleLabel.text = @"Automatic";
        _autoPlayTitleLabel.adjustsFontSizeToFitWidth = YES;
        _autoPlayTitleLabel.textAlignment = NSTextAlignmentCenter;
        
        _autoPlayStatusLabel.backgroundColor = [UIColor clearColor];
        _autoPlayStatusLabel.textColor = [UIColor whiteColor];
        _autoPlayStatusLabel.font = kAppFontSemibold(12);
        _autoPlayStatusLabel.text = @"Play: OFF";
        _autoPlayStatusLabel.adjustsFontSizeToFitWidth = YES;
         _autoPlayStatusLabel.textAlignment = NSTextAlignmentCenter;
        
        [_autoplayBackgroundView addSubview:_autoPlayTitleLabel];
        [_autoplayBackgroundView addSubview:_autoPlayStatusLabel];
        
        [_autoplayBackgroundView addSubview:_autoPlayStateButton];
        
        [self addSubview:_autoplayBackgroundView];
        
        _thumbnailImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kPlayerHeight, kPlayerHeight)];
        _thumbnailImageView.backgroundColor = [UIColor whiteColor];
        _thumbnailImageView.image = [UIImage imageNamed:@"logo.png"];
        
        [self addSubview:_thumbnailImageView];
        
        _locationLabel = [[UILabel alloc] initWithFrame:CGRectMake(_thumbnailImageView.frame.origin.x+_thumbnailImageView.frame.size.width+7, 7, 265, 12)];
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(_locationLabel.frame.origin.x, _locationLabel.frame.origin.y+14, _locationLabel.frame.size.width, 20)];
        _detailLabel = [[UILabel alloc] initWithFrame:CGRectMake(_locationLabel.frame.origin.x, _titleLabel.frame.origin.y+22, _locationLabel.frame.size.width, 12)];
        
        _locationLabel.backgroundColor = [UIColor clearColor];
        _locationLabel.textColor = RGBCOLOR(190, 201, 204);
        _locationLabel.font = kAppFontSemibold(9);
        _locationLabel.text = @"LOCATION";
        
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.font = kAppFontSemibold(16);
        _titleLabel.text = @"Audio Title";
        
        _detailLabel.backgroundColor = [UIColor clearColor];
        _detailLabel.textColor = [UIColor whiteColor];
        _detailLabel.font = kAppFontSemibold(10);
        _detailLabel.text = @"Detail";
        
        [self addSubview:_locationLabel];
        [self addSubview:_titleLabel];
        [self addSubview:_detailLabel];
        
        _timeElapsedLabel = [[UILabel alloc] initWithFrame:CGRectMake(_locationLabel.frame.origin.x+_locationLabel.frame.size.width+7, 25, 35, 15)];
        _timeElapsedLabel.textAlignment = NSTextAlignmentCenter;
        _timeElapsedLabel.backgroundColor = [UIColor clearColor];
        _timeElapsedLabel.textColor = [UIColor whiteColor];
        _timeElapsedLabel.font = kAppFontSemibold(10);
        _timeElapsedLabel.text = @"00:45";
        
        [self addSubview:_timeElapsedLabel];
        
        _progressView = [[TYMProgressBarView alloc] initWithFrame:CGRectMake(_timeElapsedLabel.frame.origin.x+_timeElapsedLabel.frame.size.width+5, 28.5, 285, 8)];
        _progressView.barBorderWidth = 0.0f;
        _progressView.barInnerPadding = 0.0f;
        _progressView.barBackgroundColor = RGBCOLOR(164, 175, 176);
        _progressView.barFillColor = [UIColor whiteColor];
        _progressView.progress = 0.15f;
        
        [self addSubview:_progressView];
        
        _durationLabel = [[UILabel alloc] initWithFrame:CGRectMake(_progressView.frame.origin.x+_progressView.frame.size.width+5, 25, 35, 15)];
        _durationLabel.textAlignment = NSTextAlignmentCenter;
        _durationLabel.backgroundColor = [UIColor clearColor];
        _durationLabel.textColor = [UIColor whiteColor];
        _durationLabel.font = kAppFontSemibold(10);
        _durationLabel.text = @"03:58";
        
        [self addSubview:_durationLabel];
        
        _playPauseImageView = [[UIImageView alloc] initWithFrame:CGRectMake(_durationLabel.frame.origin.x+_durationLabel.frame.size.width+30, 22.5, 20, 20)];
        _playPauseImageView.backgroundColor = [UIColor clearColor];
        _playPauseImageView.image = [UIImage imageNamed:@"playbutton.png"];
        
        _playPauseButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 65)];
        _playPauseButton.backgroundColor = [UIColor clearColor];
        [_playPauseButton addTarget:self action:@selector(playPauseButtonTouched) forControlEvents:UIControlEventTouchUpInside];
        _playPauseButton.center = _playPauseImageView.center;
        
        [self addSubview:_playPauseImageView];
        [self addSubview:_playPauseButton];
        
        _muteUnmuteImageView = [[UIButton alloc] initWithFrame:CGRectMake(_playPauseImageView.frame.origin.x+_playPauseImageView.frame.size.width+30, 22.5, 20, 20)];
        
        [_muteUnmuteImageView setHitTestEdgeInsets:UIEdgeInsetsMake(-20, -20, -20, -20)];
        
        [_muteUnmuteImageView addTarget:self action:@selector(muteUnmuteImageViewTouched) forControlEvents:UIControlEventTouchUpInside];
        _muteUnmuteImageView.userInteractionEnabled = YES;
        _muteUnmuteImageView.backgroundColor = [UIColor clearColor];
        
        [_muteUnmuteImageView setImage:[UIImage imageNamed:@"speaker-on.png"] forState:UIControlStateNormal];
     //   _muteUnmuteImageView.image = [UIImage imageNamed:@"speaker-on.png"];
        
       // _muteUnmuteButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 65)];
        _muteUnmuteButton.backgroundColor = [UIColor clearColor];
        [_muteUnmuteButton addTarget:self action:@selector(muteUnmuteButtonTouched) forControlEvents:UIControlEventTouchUpInside];
        _muteUnmuteButton.center = _muteUnmuteImageView.center;
        
        [self addSubview:_muteUnmuteImageView];
        [self addSubview:_muteUnmuteButton];
        
        
//        _volumeSlider = [[UISlider alloc]initWithFrame:CGRectMake(_playPauseImageView.frame.origin.x+_playPauseImageView.frame.size.width - 55, -125, 200, 50)];
//        _volumeSlider.userInteractionEnabled = YES;
//        
//        CGAffineTransform trans = CGAffineTransformMakeRotation(M_PI_2);
//        _volumeSlider.transform = trans;
//        _volumeSlider.backgroundColor = [UIColor grayColor];
//        
//        [self addSubview:_volumeSlider];
//        [self bringSubviewToFront:_volumeSlider];
        
        _playing = NO;
        _speakerOn = YES;
        _autoPlayEnabled = NO;
        _isAppeared = NO;
    }
    return self;
}
//- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
//{
//    CGFloat radius = 100.0;
//    CGRect frame = CGRectMake(-radius, -radius,
//                              self.frame.size.width + radius,
//                              self.frame.size.height + radius);
//    
//    if (CGRectContainsPoint(frame, point)) {
//        return self;
//    }
//    return nil;
//}
////- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
////{
////    if (CGRectContainsPoint(self.view.bounds, point) ||
////        CGRectContainsPoint(button.view.frame, point))
////    {
////        return YES;
////    }
////    return NO;
////}
//
//-(BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
//{
//    if ( CGRectContainsPoint(_muteUnmuteImageView.frame, point) )
//        return YES;
//    
//    return [super pointInside:point withEvent:event];
//}

-(UIImageView *)thumbnailImageView {
    //
    
    return _thumbnailImageView;
}
-(void)muteUnmuteImageViewTouched
{
    if (isSliderHidden)
    {
         [SharedAppDelegate setSliderHidden:NO];
        isSliderHidden = !isSliderHidden;
    }
    else
    {
        [SharedAppDelegate setSliderHidden:YES];
        isSliderHidden = !isSliderHidden;

    }
   
   
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)refreshView
{
    _progressView.progress = 0.0f;
    
    NSDictionary *langDictionary = [SharedAppDelegate langDictionary];
    
    _autoPlayTitleLabel.text = [langDictionary valueForKey:@"LabelAutomatic"];
    
    [self setPlaying:_playing];
    [self setAutoPlayEnabled:_autoPlayEnabled];
}

- (void)setProgress:(float)progress
{
    _progressView.progress = progress;
}

- (void)playPauseButtonTouched
{
    [self setPlaying:!_playing];
    [_delegate playButtonTriggered];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"PlayPauseButtonTapped" object:nil];
}

- (void)muteUnmuteButtonTouched
{
    [_delegate muteUnmuteTriggered];
}

- (void)autoPlayStateButtonTouched
{
    [self setAutoPlayEnabled:!_autoPlayEnabled];
    [_delegate autoPlayStateChanged];
}

- (void)setPlaying:(BOOL)playing
{
    _playing = playing;
    
    if(!_playing)    {
        _playPauseImageView.image = [UIImage imageNamed:@"playbutton.png"];
        [SharedAppDelegate stopPlayerProgressTimer];
    }
    else    {
        _playPauseImageView.image = [UIImage imageNamed:@"pause.png"];
        [SharedAppDelegate startPlayerProgressTimer];
    }
}

- (void)setSpeakerOn:(BOOL)speakerOn
{
    _speakerOn = speakerOn;
    
    if(!_speakerOn)    {
        
        [_muteUnmuteImageView setImage:[UIImage imageNamed:@"speaker-off.png"] forState:UIControlStateNormal];
       // _muteUnmuteImageView.image = [UIImage imageNamed:@"speaker-off.png"];
    }
    else    {
        [_muteUnmuteImageView setImage:[UIImage imageNamed:@"speaker-on.png"] forState:UIControlStateNormal];

      //  _muteUnmuteImageView.image = [UIImage imageNamed:@"speaker-on.png"];
    }
}

- (void)setAutoPlayEnabled:(BOOL)autoPlayEnabled
{
    _autoPlayEnabled = autoPlayEnabled;
    
    NSDictionary *langDictionary = [SharedAppDelegate langDictionary];
    
    NSString *labelString;
    NSMutableAttributedString *mutableAttributedString;
    if(!_autoPlayEnabled)    {
        labelString = [NSString stringWithFormat:@"%@: OFF",[langDictionary valueForKey:@"LabelPlay"]];
        NSRange offRange = [labelString rangeOfString:@"OFF"];
        mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:labelString];
        [mutableAttributedString addAttribute:NSFontAttributeName value:kAppFontSemibold(11) range:NSMakeRange(0, [labelString length])];
        [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, [labelString length])];
        [mutableAttributedString addAttribute:NSFontAttributeName value:kAppFontBold(11) range:offRange];
    }
    else    {
        labelString = [NSString stringWithFormat:@"%@: ON",[langDictionary valueForKey:@"LabelPlay"]];
        NSRange onRange = [labelString rangeOfString:@"ON"];
        mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:labelString];
        [mutableAttributedString addAttribute:NSFontAttributeName value:kAppFontSemibold(11) range:NSMakeRange(0, [labelString length])];
        [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, [labelString length])];
        [mutableAttributedString addAttribute:NSFontAttributeName value:kAppFontBold(11) range:onRange];
    }
    
    _autoPlayStatusLabel.attributedText = mutableAttributedString;
}

@end
