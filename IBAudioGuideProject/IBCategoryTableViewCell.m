//
//  IBCategoryTableViewCell.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 07/08/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBCategoryTableViewCell.h"

@implementation IBCategoryTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.textLabel.frame = CGRectMake(60, 0, self.frame.size.width-120, self.frame.size.height);
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    if(selected) {
        self.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"navigate-highlighted.png"]];
    }
    else    {
        self.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"navigate-normal.png"]];
    }
}

@end
