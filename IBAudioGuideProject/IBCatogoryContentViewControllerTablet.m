//
//  IBCatogoryContentViewControllerTablet.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 27/08/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBCatogoryContentViewControllerTablet.h"

#import "Base64.h"
#import "IBGlobal.h"
#import "IBDirectDownloadViewController.h"
#import "IBInAppComboPurchaseViewController.h"
#import "IBInAppSinglePurchaseViewController.h"
#import "IBCollectionViewController.h"
#import "IBAudioGuideViewControllerTablet.h"
#import "UIView+Toast.h"
#import "HUDManager.h"

#import "IBInAppHelper.h"
#import <StoreKit/StoreKit.h>

@interface IBCatogoryContentViewControllerTablet ()<IBDirectDownloadViewControllerDelegate,IBInAppComboPurchaseViewControllerDelegate,IBInAppSinglePurchaseViewControllerDelegate>
{
    NSInteger categoryIndex;
    NSString *_urlString;
    NSString *_destinationDir;
    NSArray  *_products;
}

@property (nonatomic,strong) NSString *urlString;
@property (nonatomic,strong) NSString *destinationDir;

- (void)refreshView;

@end

@implementation IBCatogoryContentViewControllerTablet

@synthesize urlString = _urlString;
@synthesize destinationDir = _destinationDir;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGRect frameSelf = self.view.frame;
    frameSelf.size.height = frameSelf.size.height - 110;
    self.view.frame = frameSelf;
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:RGBCOLOR(211, 211, 211)];
    
    [SharedAppDelegate setZoomScale:0.5];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:nil];
    
    [HUDManager addHUDWithLabel:nil];
    [[IBInAppHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        //
        if (success) _products = products;
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self performSelector:@selector(refreshView) withObject:nil afterDelay:.1f];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    //NSLog(@"categorycontnet Hud.Identifier = %@", segue.identifier);
    [HUDManager hideHUDFromWindow];
    if ([segue.identifier isEqualToString:@"segueToDirectDownload"])
    {
        IBDirectDownloadViewController *directDownloader = [segue destinationViewController];
        directDownloader.delegate = self;
        [SharedAppDelegate setDirectDownloadController:directDownloader];
        
        if(categoryIndex==0)    {
            
            NSDictionary *langDictionary = [SharedAppDelegate langDictionary];
            
            directDownloader.heading = [langDictionary valueForKey:@"LabelPhotoGallery"];
            directDownloader.description = [langDictionary valueForKey:@"LabelGalleryMessage"] ;
            directDownloader.urlString = _urlString;
            directDownloader.destinationDir = _destinationDir;
            directDownloader.item = FreeGallery;
        } else {
            NSDictionary *langDictionary = [SharedAppDelegate langDictionary];
            
            directDownloader.heading = [langDictionary valueForKey:@"LabelAudioGuide"];
            directDownloader.description = [langDictionary valueForKey:@"LabelGalleryMessage"];
            directDownloader.description = [langDictionary valueForKey:@"LabelDownloadGuide"];
            directDownloader.urlString = _urlString;
            directDownloader.destinationDir = _destinationDir;
            directDownloader.item = AudioGuide;
        }
    }
    else if ([segue.identifier isEqualToString:@"segueToInAppCombo"])
    {
        IBInAppComboPurchaseViewController *comboInAppDownloader = [segue destinationViewController];
        comboInAppDownloader.delegate = self;
        
        [SharedAppDelegate setComboInAppController:comboInAppDownloader];
        
        NSDictionary *langDictionary = [SharedAppDelegate langDictionary];
        
        comboInAppDownloader.heading = [langDictionary valueForKey:@"LabelSpecialOffer"];
        comboInAppDownloader.description = [langDictionary valueForKey:@"LabelOfferMessage"];
    }
    else if ([segue.identifier isEqualToString:@"segueToInAppSingle"])
    {
        IBInAppSinglePurchaseViewController *singleInAppDownloader = [segue destinationViewController];
        singleInAppDownloader.delegate = self;
        
        [SharedAppDelegate setSingleInAppController:singleInAppDownloader];
        
        NSDictionary *langDictionary = [SharedAppDelegate langDictionary];
        
        singleInAppDownloader.heading = [langDictionary valueForKey:@"LabelSpecialOffer"];
        singleInAppDownloader.description = [langDictionary valueForKey:@"LabelPurchaseGuide"];
    }
    else if ([segue.identifier isEqualToString:@"segueToGalleryCollection"])
    {
        IBCollectionViewController *collectionViewController = [segue destinationViewController];
        [SharedAppDelegate setCollectionViewController:collectionViewController];
    }
    else if ([segue.identifier isEqualToString:@"segueToAudioList"])
    {
        IBAudioGuideViewControllerTablet *audioGuideViewController = [segue destinationViewController];
        [SharedAppDelegate setAudioGuideViewController:audioGuideViewController];
    }
    //NSLog(@"categorycontent1 Hud");
    [HUDManager hideHUDFromWindow];
}

/*
 - (void)refreshView
 {
 NSInteger locationNo = [SharedAppDelegate locationNo];
 NSUInteger languageIndex = [SharedAppDelegate languageIndex];
 
 NSString *museumDir,*languageDir;
 
 DIRLOCATION_STR(locationNo,museumDir);
 DIRLANGUAGE_STR(languageIndex, languageDir);
 
 NSString *audioGuideURL = [NSString stringWithFormat:@"%@/%@/%@/%@/%@",SYNC_URL,IBParentDirectory,museumDir,languageDir,kAudioGuideZip];
 NSString *photoGalleryURL = [NSString stringWithFormat:@"%@/%@/%@/%@",SYNC_URL,IBParentDirectory,museumDir,kGalleryZip];
 
 NSString *absoluteDestination = [IBGlobal createDocumentsDirectoryByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@/",languageDir,museumDir]];
 NSString *defaultDestination = [IBGlobal createDocumentsDirectoryByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@/",ENGLISH,museumDir]];
 
 if(categoryIndex == 0) {
 //
 _urlString = photoGalleryURL;
 _destinationDir = defaultDestination;
 } else if(categoryIndex == 1) {
 //
 _urlString = audioGuideURL;
 _destinationDir = absoluteDestination;
 }
 
 NSString *downloadStatus = [[NSUserDefaults standardUserDefaults] valueForKey:self.key];
 
 NSLog(@"self.key = %@", self.key);
 NSLog(@"downloadStatus = %@", downloadStatus);
 NSLog(@"categoryIndex = %ld", (long)categoryIndex);
 NSString *segueID = nil; //IBInAppPurchaseIdentifier
 //
 if([IBInAppHelper isPurchaseRestored]) {
 //
 if (!downloadStatus) {
 //
 segueID = @"segueToDirectDownload";
 } else {
 if(categoryIndex == 0) {
 segueID = @"segueToGalleryCollection";
 }
 else {
 segueID = @"segueToAudioList";
 }
 }
 } else {
 //
 if(categoryIndex == 0) {
 //
 segueID = @"segueToDirectDownload";
 } else {
 //
 if ([[IBInAppHelper sharedInstance] productPurchased:IBInAppPurchaseIdentifier]) {
 segueID = @"segueToDirectDownload";
 } else {
 segueID = @"segueToInAppSingle";
 }
 }
 }
 //
 NSLog(@"segueIDsegueID = %@", segueID);
 [self performSegueWithIdentifier:segueID sender:self];
 }
 */

//- 2 (void)refreshView
//{
//    NSInteger locationNo = [SharedAppDelegate locationNo];
//    NSUInteger languageIndex = [SharedAppDelegate languageIndex];
//
//    NSString *museumDir,*languageDir;
//
//    DIRLOCATION_STR(locationNo,museumDir);
//    DIRLANGUAGE_STR(languageIndex, languageDir);
//
//    NSString *audioGuideURL = [NSString stringWithFormat:@"%@/%@/%@/%@/%@",SYNC_URL,IBParentDirectory,museumDir,languageDir,kAudioGuideZip];
//    NSString *photoGalleryURL = [NSString stringWithFormat:@"%@/%@/%@/%@",SYNC_URL,IBParentDirectory,museumDir,kGalleryZip];
//
//    NSString *absoluteDestination = [IBGlobal createDocumentsDirectoryByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@/",languageDir,museumDir]];
//    NSString *defaultDestination = [IBGlobal createDocumentsDirectoryByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@/",ENGLISH,museumDir]];
//
//    if (categoryIndex == 0) {
//        //
//        _urlString = photoGalleryURL;
//        _destinationDir = defaultDestination;
//    } else if (categoryIndex == 1) {
//        //
//        _urlString = audioGuideURL;
//        _destinationDir = absoluteDestination;
//    }
//
//    NSString *downloadStatus = [[NSUserDefaults standardUserDefaults] valueForKey:self.key];
//    NSString *segueID = nil;
//    //
//    if([downloadStatus intValue] == Downloaded) {
//        //
//        if(categoryIndex == 0) { segueID = @"segueToGalleryCollection"; NSLog(@"xxxxxxxyyyyy 1"); }
//        else { segueID = @"segueToAudioList"; NSLog(@"xxxxxxxyyyyy 2"); }
//    } else {
//        //
//        if(categoryIndex == 0) {
//            //
//            if ([IBInAppHelper isPurchaseRestored]) { segueID = @"segueToAudioList"; NSLog(@"xxxxxxxyyyyy 3"); }
//            else { segueID = @"segueToDirectDownload";  NSLog(@"xxxxxxxyyyyy 4"); }
//        } else {
//            //
//            BOOL flag = ([[IBInAppHelper sharedInstance] productPurchased:IBInAppPurchaseIdentifier] || [IBInAppHelper isPurchaseRestored]);
//            if (flag) { segueID = @"segueToDirectDownload"; NSLog(@"xxxxxxxyyyyy 5");}
//            else { segueID = @"segueToInAppSingle"; NSLog(@"xxxxxxxyyyyy 6");}
//        }
//    }
//    //
//    [self performSegueWithIdentifier:segueID sender:self];
//}



- (void)refreshView {
    //
    if (iSInAppPurchaseEnabled == YES) {
        //
        [self _refreshViewWithInAppPurchase];
    } else {
        [self _refreshViewWithoutInAppPurchase];
    }
}

-(void)_setUpPaths {
    //
    NSInteger locationNo = [SharedAppDelegate locationNo];
    NSUInteger languageIndex = [SharedAppDelegate languageIndex];
    
    NSString *musiemName,*languade;
    NSString *audioGuideSource,*photoGallerySource;
    
    DIRLOCATION_STR(locationNo,musiemName);
    DIRLANGUAGE_STR(languageIndex, languade);
    
    audioGuideSource = [NSString stringWithFormat:@"%@/%@/%@/%@/%@",SYNC_URL,IBParentDirectory,musiemName,languade,kAudioGuideZip];
    photoGallerySource = [NSString stringWithFormat:@"%@/%@/%@/%@",SYNC_URL,IBParentDirectory,musiemName,kGalleryZip];
    
    audioGuideSource = [NSString stringWithFormat:@"%@/%@/%@/%@/%@",SYNC_URL,IBParentDirectory,musiemName, languade, kAudioGuideZip];
    NSString *audioGuideDestinationPath = [IBGlobal createDocumentsDirectoryByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@/",languade,musiemName]];
    //
    NSString *photoGalleryDestinationPath = [IBGlobal createDocumentsDirectoryByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@/",ENGLISH,musiemName]];
    //
    if(categoryIndex == 0) {
        //
        _urlString = photoGallerySource;
        _destinationDir = photoGalleryDestinationPath;
    } else if(categoryIndex == 1) {
        //
        _urlString = audioGuideSource;
        _destinationDir = audioGuideDestinationPath;
    }
}


-(void)_refreshViewWithoutInAppPurchase {
    //
    [self _setUpPaths];
    
    NSString *downloadStatus = [[NSUserDefaults standardUserDefaults] valueForKey:self.key];
    
    if (categoryIndex == 0) {
        //
        if ([downloadStatus intValue] == StatusDownloaded) {
            //
            [self _showDownloadedCollection];
        } else {
            [self _showDownloadPage];
        }
    } else if (categoryIndex == 1) {
        //
        if ([downloadStatus intValue] == StatusDownloaded)   {
            //
            [self _showAudioList];
        } else {
            [self _showDownloadPage];
        }
    }
}

-(void)_refreshViewWithInAppPurchase {
    //
    [self _setUpPaths];
    
    NSString *downloadStatus = [[NSUserDefaults standardUserDefaults] valueForKey:self.key];
    IBType type = [SharedAppDelegate locationNo];
    //
    if (type == IBTypeIsoleBella) {
        //
        [self _handleForIsoleBella:downloadStatus atSubIndex:categoryIndex];
    } else if (type == IBTypeIsoleMadre) {
        //
        [self _handleForIsoleMadre:downloadStatus atSubIndex:categoryIndex];
    } else if (type == IBTypeRoccaDiAngera) {
        //
        [self _handleForRoccaDiAngera:downloadStatus];
    }
}

-(void)_handleForIsoleBella:(NSString *)downloadStatus atSubIndex:(NSInteger)subIndex {
    //
    if (subIndex == 0) {
        //
        if ([downloadStatus intValue] == StatusDownloaded) {
            //
            [self _showDownloadedCollection];
        } else {
            [self _showDownloadPage];
        }
        
    } else if (subIndex == 1) {
        //
        BOOL isPurchashed = [[IBInAppHelper sharedInstance] productPurchased:IBInAppPurchaseIdentifierIsoleBella];
        BOOL isRestored = [IBInAppHelper isPurchaseRestoredForType:IBTypeIsoleBella];
        //
        if ([downloadStatus intValue] == StatusDownloaded) {
            //
            [self _showAudioList];
        } else if (isPurchashed || isRestored) {
            //
            [self _showDownloadPage];
        } else {
            //
            [self _showInAppPurchasePage];
        }
    }
}

-(void)_handleForIsoleMadre:(NSString *)downloadStatus atSubIndex:(NSInteger)subIndex {
    //
    if (subIndex == 0) {
        //
        if ([downloadStatus intValue] == StatusDownloaded) {
            //
            [self _showDownloadedCollection];
        } else {
            [self _showDownloadPage];
        }
        
    } else if (subIndex == 1) {
        //
        BOOL isPurchashed = [[IBInAppHelper sharedInstance] productPurchased:IBInAppPurchaseIdentifierIsoleMadre];
        BOOL isRestored = [IBInAppHelper isPurchaseRestoredForType:IBTypeIsoleMadre];
        //
        if ([downloadStatus intValue] == StatusDownloaded) {
            //
            [self _showAudioList];
        } else if (isPurchashed || isRestored) {
            //
            [self _showDownloadPage];
        } else {
            //
            [self _showInAppPurchasePage];
        }
    }
}

-(void)_handleForRoccaDiAngera:(NSString *)downloadStatus {
    //
    if ([downloadStatus intValue] == StatusDownloaded) {
        //
        [self _showDownloadedCollection];
    } else {
        [self _showDownloadPage];
    }
}

-(void)_showDownloadedCollection {
    //
    [self performSegueWithIdentifier:@"segueToGalleryCollection" sender:self];
}

-(void)_showDownloadPage {
    //
    [self performSegueWithIdentifier:@"segueToDirectDownload" sender:self];
}

-(void)_showAudioList {
    //
    [self performSegueWithIdentifier:@"segueToAudioList" sender:self];
}

-(void)_showInAppPurchasePage {
    //
    [self performSegueWithIdentifier:@"segueToInAppSingle" sender:self];
}

//-(void)_refreshViewWithInAppPurchase {
//    //
//    [self _setUpPaths];
//
//    NSString *downloadStatus = [[NSUserDefaults standardUserDefaults] valueForKey:self.key];
//
//    if (categoryIndex == 0) {
//        //
//        if ([downloadStatus intValue] == StatusDownloaded)   {
//            [self performSegueWithIdentifier:@"segueToGalleryCollection" sender:self];
//        } else {
//            [self performSegueWithIdentifier:@"segueToDirectDownload" sender:self];
//        }
//    } else if (categoryIndex == 1) {
//        //
//        if ([downloadStatus intValue] == StatusDownloaded)   {
//            [self performSegueWithIdentifier:@"segueToAudioList" sender:self];
//        } else if ([[IBInAppHelper sharedInstance] productPurchased:IBInAppPurchaseIdentifier] || [IBInAppHelper isPurchaseRestored]) {
//            [self performSegueWithIdentifier:@"segueToDirectDownload" sender:self];
//        } else {
//            [self performSegueWithIdentifier:@"segueToInAppSingle" sender:self];
//        }
//    }
//}

- (void)setDownloadIndex:(NSInteger)downloadIndex
{
    categoryIndex = downloadIndex;
    
    if([SharedAppDelegate directDownloadController])    {
        [[SharedAppDelegate directDownloadController] dismissViewControllerAnimated:NO completion:^{
            //NSLog(@"download controller dismissed!!!");
            [SharedAppDelegate setDirectDownloadController:nil];
        }];
    }
    else if ([SharedAppDelegate comboInAppController])    {
        [[SharedAppDelegate comboInAppController] dismissViewControllerAnimated:NO completion:^{
            //NSLog(@"comboInAppController dismissed!!!");
            [SharedAppDelegate setComboInAppController:nil];
        }];
    }
    else if ([SharedAppDelegate singleInAppController])    {
        [[SharedAppDelegate singleInAppController] dismissViewControllerAnimated:NO completion:^{
            //NSLog(@"singleInAppController dismissed!!!");
            [SharedAppDelegate setSingleInAppController:nil];
        }];
    }
    else if ([SharedAppDelegate collectionViewController])  {
        [self.navigationController popToRootViewControllerAnimated:YES];
        //NSLog(@"collectionViewController dismissed!!!");
        [SharedAppDelegate setCollectionViewController:nil];
    }
    else if ([SharedAppDelegate audioGuideViewController])  {
        [[SharedAppDelegate audioGuideViewController] dismissIfExistedMapViewController];
        [self.navigationController popToRootViewControllerAnimated:YES];
        //NSLog(@"collectionViewController dismissed!!!");
        [SharedAppDelegate setAudioGuideViewController:nil];
    }
}

#pragma mark-
#pragma IBInAppDelegate methods

- (void)productPurchased:(NSNotification *)notification {
    //
    NSString *productIdentifier = notification.object;
    //
    [_products enumerateObjectsUsingBlock:^(SKProduct * product, NSUInteger idx, BOOL *stop) {
        //
        if ([product.productIdentifier isEqualToString:productIdentifier]) {
            //
            NSInteger locationNo = [SharedAppDelegate locationNo];
            [[SharedAppDelegate database] updatePurchaseOfLocationNo:1 withStatus:YES];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setBool:YES forKey:productIdentifier];
            [defaults synchronize];
            
            [self setDownloadIndex:categoryIndex];
            
            *stop = YES;
        }
    }];
}


//- (void)productPurchased:(NSNotification *)notification {
//    //
//    NSURL *url = [[NSBundle mainBundle] URLForResource:@"InAppProducts" withExtension:@"plist"];
//    NSArray *arrayIdentifiers = [NSArray arrayWithContentsOfURL:url];
//    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:nil ascending:YES];
//    NSArray *arraySortedIdentifiers = [arrayIdentifiers sortedArrayUsingDescriptors:@[sortDescriptor]];
//
//    NSString * productIdentifier = notification.object;
//    [_products enumerateObjectsUsingBlock:^(SKProduct * product, NSUInteger idx, BOOL *stop) {
//        if ([product.productIdentifier isEqualToString:productIdentifier]) {
//            if([arraySortedIdentifiers[1] isEqualToString:productIdentifier]) {
//                [[SharedAppDelegate database] updatePurchaseOfLocationNo:1 withStatus:YES];
//            }
//            else if ([arraySortedIdentifiers[2] isEqualToString:productIdentifier])   {
//                [[SharedAppDelegate database] updatePurchaseOfLocationNo:2 withStatus:YES];
//            }
//            else if ([arraySortedIdentifiers[3] isEqualToString:productIdentifier])    {
//                [[SharedAppDelegate database] updatePurchaseOfLocationNo:3 withStatus:YES];
//            }
//            else    {
//                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//                [defaults setBool:YES forKey:kComboPurchaseKey];
//                [defaults synchronize];
//            }
//
//            [self setDownloadIndex:categoryIndex];
//
//            *stop = YES;
//        }
//    }];
//}

#pragma mark-
#pragma IBInAppComboPurchaseViewControllerDelegate methods

- (void)comboInAppPurchaseTriggered
{
    if([[NSUserDefaults standardUserDefaults] integerForKey:kFirstRestoreKey]==first_restore)  {
        [HUDManager addHUDWithLabel:[[SharedAppDelegate langDictionary] valueForKey:@"LabelFirstRestore"]];
        [[IBInAppHelper restoreInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
            [[IBInAppHelper restoreInstance] restoreCompletedTransactions];
            //NSLog(@"categorycontent2 Hud");
            [HUDManager hideHUDFromWindowAfterDelay:0.2f];
        }];
    }
    else    {
        if([_products count]>0) {
            [[IBInAppHelper sharedInstance] buyProduct:[_products objectAtIndex:0]];
        }
    }
}

#pragma mark-
#pragma IBInAppSinglePurchaseViewControllerDelegate methods

- (void)singleInAppPurchaseTriggered {
    //
    return;
}

-(void)singleInAppPurchaseTriggeredFor:(IBType)type {
    //
    if (_products.count < 2) {
        //
        [self _showAlertForNoInternetConnectionWithLangDictionary:[SharedAppDelegate langDictionary]];
    } else {
        //
        if (type == IBTypeIsoleBella) {
            [[IBInAppHelper sharedInstance] buyProduct:_products[0]];
        } else if (type == IBTypeIsoleMadre) {
            [[IBInAppHelper sharedInstance] buyProduct:_products[1]];
        }
    }
}

-(void)_showAlertForNoInternetConnectionWithLangDictionary:(NSDictionary *)langDictionary {
    //
    NSString *error = [langDictionary valueForKey:@"Error"];
    NSString *internetNotAvailable = [langDictionary valueForKey:@"CheckInternetConnection"];
    //
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:error message:internetNotAvailable delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}


//- (void)singleInAppPurchaseTriggered {
//    //
//    NSLog(@"singleInAppPurchaseTriggered 1 = %@", _products);
//    if([[NSUserDefaults standardUserDefaults] integerForKey:kFirstRestoreKey]==first_restore) {
//        //
//        NSLog(@"singleInAppPurchaseTriggered 1");
//        [HUDManager addHUDWithLabel:[[SharedAppDelegate langDictionary] valueForKey:@"LabelFirstRestore"]];
//        [[IBInAppHelper restoreInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
//            //
//            [[IBInAppHelper restoreInstance] restoreCompletedTransactions];
//
//            [HUDManager hideHUDFromWindowAfterDelay:0.2f];
//        }];
//    } else {
//        NSInteger locationNo = [SharedAppDelegate locationNo];
//        NSLog(@"singleInAppPurchaseTriggered 2 = %ld", (long)locationNo);
//        //
//        if([_products count] > locationNo) {
//            //
//            NSLog(@"singleInAppPurchaseTriggered 3 = %@", [_products objectAtIndex:locationNo]);
//            [[IBInAppHelper sharedInstance] buyProduct:[_products objectAtIndex:locationNo]];
//        }
//
//        NSLog(@"singleInAppPurchaseTriggered 4");
//    }
//}

#pragma mark-
#pragma IBDownloadButtonViewTabletDelegate methods

- (void)downloadStarted
{
    NSLog(@"download started");
}

- (void)downloadFinished
{
    if([SharedAppDelegate directDownloadController].item == AudioGuide) {
        
        NSInteger locationNo = [SharedAppDelegate locationNo];
        
        NSString *museumDir;
        DIRLOCATION_STR(locationNo,museumDir);
        
        NSString *guidesGalleryURLString = [NSString stringWithFormat:@"%@/%@/%@/%@",SYNC_URL,IBParentDirectory,museumDir,kGuidesGalleryZip];
        NSString *downloadStatus = [[NSUserDefaults standardUserDefaults] valueForKey:[IBGlobal keyForUrlString:guidesGalleryURLString]];
        
        if([downloadStatus intValue]==StatusDownloaded)   {
            [self setDownloadIndex:categoryIndex];
        }
        else  {
            IBDirectDownloadViewController *directDownloader = [SharedAppDelegate directDownloadController];
            
            NSString *guidesGalleryDestinationDir = [IBGlobal createDocumentsDirectoryByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@/",ENGLISH,museumDir]];
            
            directDownloader.urlString = guidesGalleryURLString;
            directDownloader.destinationDir = guidesGalleryDestinationDir;
            [directDownloader refreshView];
            [directDownloader forceDownload];
            [SharedAppDelegate setLockMessage:[[SharedAppDelegate langDictionary] valueForKey:@"LabelGuideGalleryLoad"]];
            [directDownloader.view makeToast:[SharedAppDelegate lockMessage] duration:3.f position:@"bottom"];
        }
    } else {
        [self setDownloadIndex:categoryIndex];
    }
    
    [HUDManager addHUDWithLabel:nil];
    NSLog(@"download finished");
}

- (void)downloadCancelled
{
    NSLog(@"download cancelled");
}

- (void)downloadFailed
{
    NSLog(@"download fialed");
}

#pragma mark-

- (NSString *)key {
    //
    NSString *keyString = [[[_urlString stringByReplacingOccurrencesOfString:SYNC_URL withString:@""] base64EncodedString] uppercaseString];
    NSMutableString *newString = [NSMutableString string];
    //
    for (int i = 0; i < [keyString length]; i++) {
        //
        int ascii = [keyString characterAtIndex:i];
        //
        if (((ascii>64) && (ascii<91)) || ((ascii>47) && (ascii<58))) [newString appendFormat:@"%c",ascii];
    }
    //
    return [NSString stringWithString:newString];
}

@end
