//
//  IBCatogoryViewControllerTablet.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 20/08/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IBCatogoryListViewControllerTabletDelegate
@required
- (void)itemSelectedForDownloadAtIndex:(NSInteger)index;
@end

@interface IBCatogoryListViewControllerTablet : UIViewController{
    BOOL _locked;
    NSInteger _currentIndex;
}
@property (nonatomic, assign) id <IBCatogoryListViewControllerTabletDelegate> delegate;
@property (nonatomic,assign) BOOL locked;
@property (assign, nonatomic) NSInteger currentIndex;

- (void)refreshView;
- (void)setCurrentIndex:(NSInteger)currentIndex;

- (void)updateLanguage;

@end