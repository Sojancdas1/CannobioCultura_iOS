//
//  IBCatogoryViewControllerTablet.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 20/08/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBCatogoryListViewControllerTablet.h"

#import "IBGlobal.h"
#import "HPGrowingTextView.h"
#import "IBCategoryTableViewCell.h"
#import "HUDManager.h"
#import "IBSetLangViewControllerTablet.h"

#define kDownloadItemHeight 50
#define kDownloadItemCount 2

#define kDownloadItemCountForExceptIsoleBella 1



@interface IBCatogoryListViewControllerTablet ()<UITableViewDelegate,UITableViewDataSource>
{
    CGRect calcRect;
    
    UIScrollView *_scrollViewMaster;
    
    UIView *_containerView;
    UIView *_downloaderBackground;
    UITableView *_tableViewDownloader;
    UIImageView *_imageViewLocation;
    HPGrowingTextView *_museumDescriptionView;
    
    NSMutableArray *arrayDownloadLabels;
    NSArray *arrayNormalIcons, *arrayHighlightedIcons;
}

@property (nonatomic, strong, readonly) UIScrollView *scrollViewMaster;

@property (strong, nonatomic, readwrite) UIView *containerView;
@property (strong, nonatomic, readwrite) UIView *downloaderBackground;
@property (strong, nonatomic, readwrite) UITableView *tableViewDownloader;
@property (strong, nonatomic, readwrite) UIImageView *imageViewLocation;
@property (strong, nonatomic, readwrite) HPGrowingTextView *museumDescriptionView;
@property (strong, nonatomic) IBOutlet UIImageView *imageViewShade;

- (void)setDefaultIndexSelected;

@end

@implementation IBCatogoryListViewControllerTablet

@synthesize delegate = _delegate;

@synthesize locked = _locked;
@synthesize currentIndex = _currentIndex;
@synthesize scrollViewMaster = _scrollViewMaster;

@synthesize containerView = _containerView;
@synthesize downloaderBackground = _downloaderBackground;
@synthesize tableViewDownloader = _tableViewDownloader;
@synthesize imageViewLocation = _imageViewLocation;
@synthesize museumDescriptionView = _museumDescriptionView;
@synthesize imageViewShade;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reloadDataWhileChangingData)
                                                 name:@"reloadViewWhileChangingLanguage"
                                               object:nil];

 
    
//    _scrollViewMaster = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    _scrollViewMaster = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 465, self.view.frame.size.height)];
    [self.view addSubview:_scrollViewMaster];
    
    _scrollViewMaster.bounces = NO;
    _scrollViewMaster.showsHorizontalScrollIndicator = NO;
    _scrollViewMaster.showsVerticalScrollIndicator = NO;
    _scrollViewMaster.userInteractionEnabled = YES;
    _scrollViewMaster.backgroundColor = RGBCOLOR(211, 211, 211);
    
    calcRect = CGRectMake(_scrollViewMaster.frame.origin.x, _scrollViewMaster.frame.origin.y, _scrollViewMaster.frame.size.width, 98);
    
    _containerView = [[UIView alloc] initWithFrame:calcRect];
    _containerView.backgroundColor = RGBCOLOR(218.f, 218.f, 218.f);
    
    _downloaderBackground = [[UIView alloc] initWithFrame:calcRect];
    
    [_scrollViewMaster addSubview:_containerView];
    [_containerView addSubview:_downloaderBackground];
    
    _downloaderBackground.backgroundColor = RGBCOLOR(218, 218, 218);
    NSInteger locationNo = [SharedAppDelegate locationNo];
    if (locationNo == 3)
    {
        calcRect.size.height = kDownloadItemHeight*kDownloadItemCountForExceptIsoleBella;
    }
    else
    {
        calcRect.size.height = kDownloadItemHeight*kDownloadItemCount;
    }
    
    _tableViewDownloader = [[UITableView alloc] initWithFrame:calcRect];
    _tableViewDownloader.delegate = self;
    _tableViewDownloader.dataSource = self;
    
    if(IOS_NEWER_OR_EQUAL_TO(7.0))  {
        [_tableViewDownloader setSeparatorInset:UIEdgeInsetsZero];
    }
    
    _tableViewDownloader.separatorColor = RGBCOLOR(218.f, 218.f, 218.f);
    
    [_tableViewDownloader setBounces:NO];
    [_tableViewDownloader setScrollEnabled:NO];
    
    _museumDescriptionView = [[HPGrowingTextView alloc] initWithFrame:calcRect];
      //
    _museumDescriptionView.editable = NO;
    _museumDescriptionView.isScrollable = NO;
    _museumDescriptionView.userInteractionEnabled = NO;
    _museumDescriptionView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
    _museumDescriptionView.minNumberOfLines = 1;
    _museumDescriptionView.maxNumberOfLines = 30;
    _museumDescriptionView.font = kAppFontRegular(15);
    _museumDescriptionView.textColor = RGBCOLOR(110, 110, 110);
    _museumDescriptionView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 0);
    _museumDescriptionView.backgroundColor = RGBCOLOR(211, 211, 211);
    
    calcRect.size.height = 230.f;
    _imageViewLocation = [[UIImageView alloc] initWithFrame:calcRect];
    _imageViewLocation.contentMode = UIViewContentModeScaleAspectFit;
    [_scrollViewMaster insertSubview:_imageViewLocation aboveSubview:_containerView];
    [_scrollViewMaster insertSubview:_tableViewDownloader aboveSubview:_containerView];
    [_scrollViewMaster insertSubview:_museumDescriptionView aboveSubview:_containerView];
    
    arrayDownloadLabels = [NSMutableArray array];
    arrayNormalIcons = [NSArray arrayWithObjects:[UIImage imageNamed:@"gallery.png"],[UIImage imageNamed:@"audio-normal.png"], nil];
    arrayHighlightedIcons = [NSArray arrayWithObjects:[UIImage imageNamed:@"gallery-highlighted.png"],[UIImage imageNamed:@"audio-highlighted.png"], nil];
    
    [self.view bringSubviewToFront:imageViewShade];
    [self refreshView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Table view data source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger locationNo = [SharedAppDelegate locationNo];
    
    
//    if (locationNo == 2 | locationNo == 3)
//    {
//        calcRect.size.height = kDownloadItemHeight*kDownloadItemCountForExceptIsoleBella;
//        _tableViewDownloader.frame = calcRect;
//    }
//    else
//    {
//        calcRect.size.height = kDownloadItemHeight*kDownloadItemCount;
//        _tableViewDownloader.frame = calcRect;
//    }
    

    
    if (locationNo == 3)
    {
       return  kDownloadItemCountForExceptIsoleBella;
    }
    else
    {
        return kDownloadItemCount;
    }  
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)TableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
//{
//     NSInteger locationNo = [SharedAppDelegate locationNo];
//    CGFloat tempHeight;
//    
//    if (locationNo == 2 | locationNo == 3)
//    {
//        if (indexPath.row == 1)
//        {
//            tempHeight = 0;
//        }
//    }
//    else
//    {
//      tempHeight = 150;
//    }
//    return tempHeight;
//    
//}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{ 
    static NSString *CellIdentifier = @"Cell";
    IBCategoryTableViewCell *cell = (IBCategoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[IBCategoryTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
        cell.textLabel.numberOfLines = 4;
        cell.textLabel.font = kAppFontSemibold(15);
        cell.textLabel.highlightedTextColor = RGBCOLOR(104, 144, 152);
        cell.textLabel.textColor = RGBCOLOR(110, 110, 110);
        
        cell.backgroundColor = RGBCOLOR(211, 211, 211);
        
        UIView *selectedBackgroundView = [[UIView alloc] initWithFrame:cell.frame];
        selectedBackgroundView.backgroundColor = [UIColor whiteColor];
        
        cell.selectedBackgroundView = selectedBackgroundView;
    }
    
    NSInteger locationNo = [SharedAppDelegate locationNo];
    
    if (locationNo == 3) {
        if (indexPath.row ==1)  cell.userInteractionEnabled = NO;
        else cell.userInteractionEnabled = YES;
    } else {
        cell.userInteractionEnabled = YES;
    } 
    
    cell.textLabel.text = arrayDownloadLabels[indexPath.row];
    cell.imageView.image = arrayNormalIcons[indexPath.row];
    cell.imageView.highlightedImage = arrayHighlightedIcons[indexPath.row];
    
    return cell;
}

#pragma mark -
#pragma mark - Table view delegates

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.locked) {
        return nil;
    } else {
        return indexPath;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
 
   // HUDManager *hud = [[HUDManager alloc]init];
    
    NSInteger locationNo = [SharedAppDelegate locationNo];
    if (locationNo == 3 )
    {
        
    }
    else if (locationNo == 1 && indexPath.row == 0) [HUDManager addHUDWithLabel:nil];
    else [HUDManager addHUDWithLabel:nil];
    
    
    if(_currentIndex!=indexPath.row)
        
    {
        
        _currentIndex = indexPath.row;
        [_delegate itemSelectedForDownloadAtIndex:_currentIndex];
        [SharedAppDelegate lockInteractions];
    }
    else
    {
         //NSLog(@"categorylist Hud");
        [HUDManager hideHUDFromWindow];

    }
    
   }

#pragma mark -

- (void)refreshView
{
    _currentIndex = -1;
    [self updateLanguage];
    [self performSelector:@selector(setDefaultIndexSelected) withObject:nil afterDelay:0.1f];
}

-(NSString *)_parseDescription:(NSDictionary *)obj {
    //
    NSString *description = @" ";
    //
    for (NSString *key in obj) {
        //
        if ([key isEqualToString:@"museumDescription"]) {
            //
            description = obj[key];
            break;
        }
    }
    //
    return description;
}

- (void)updateLanguage
{
    NSInteger locationNo = [SharedAppDelegate locationNo];
    NSUInteger languageIndex = [SharedAppDelegate languageIndex];
    
    [arrayDownloadLabels removeAllObjects];
    
    NSArray *museumInfoArray = [[SharedAppDelegate database] loadLocationDetailsWithLocationNo:[SharedAppDelegate locationNo] andLanguageIndex:languageIndex];
    
    NSDictionary *langDictionary = [SharedAppDelegate langDictionary];
    NSString *museumDescription = @"";
    
    [arrayDownloadLabels addObject:[[langDictionary valueForKey:@"LabelPhotoGallery"] stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"]];
    
    if(locationNo == 1) {
        //
        _imageViewLocation.image = [UIImage imageNamed:@"isola_bella_main1.jpg"];
        [arrayDownloadLabels addObject:[NSString stringWithFormat:@"%@ \n%@",[[langDictionary valueForKey:@"LabelPayedGuide"] stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"],[langDictionary valueForKey:@"LabelGuideBella"]]];
        museumDescription = langDictionary[@"LabelIsolaBellaMusceumDecription"];
    } else if(locationNo == 2) {
        //
        _imageViewLocation.image = [UIImage imageNamed:@"isola_madre_main1.jpg"];
        [arrayDownloadLabels addObject:[NSString stringWithFormat:@"%@ \n%@",[[langDictionary valueForKey:@"LabelPayedGuide"] stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"],[langDictionary valueForKey:@"LabelGuideMadre"]]];
        museumDescription = langDictionary[@"LabelIsolaMadreMusceumDecription"];
    } else if(locationNo == 3) {
        //
        _imageViewLocation.image = [UIImage imageNamed:@"rocca_angera_main1.jpg"];
        [arrayDownloadLabels addObject:[NSString stringWithFormat:@"%@ \n%@",[[langDictionary valueForKey:@"LabelPayedGuide"] stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"],[langDictionary valueForKey:@"LabelGuideAngera"]]];
        museumDescription = langDictionary[@"LabelRoccaMusceumDecription"];
    }
    
   // NSDictionary *obj = museumInfoArray[0];
  //  NSString *museumDescription = [self _parseDescription:obj];
    museumDescription = [museumDescription stringByAppendingString:@" \n\n"];
    self.museumDescriptionView.text = museumDescription;
    CGRect descriptionRect = self.museumDescriptionView.frame; 
    // Apply some inline CSS
    NSString *styledHtml = [IBGlobal styledAppHtmlWithString:museumDescription andTextSize:15];
    
    // Generate an attributed string from the HTML
    NSAttributedString *attributedText = [IBGlobal attributedStringWithHTML:styledHtml];
    
    //NSLog(@"%@",NSStringFromCGRect(_museumDescriptionView.frame));
    self.museumDescriptionView.internalTextView.attributedText = attributedText;
    self.museumDescriptionView.frame = descriptionRect;
    
    calcRect.origin.x = self.scrollViewMaster.frame.origin.x;
    calcRect.origin.y = _imageViewLocation.frame.origin.y+_imageViewLocation.frame.size.height;
    //
    //Sojan Edit remove marada
    if (locationNo == 3) calcRect.size.height = kDownloadItemHeight*kDownloadItemCountForExceptIsoleBella;
    else calcRect.size.height = kDownloadItemHeight*kDownloadItemCount;
    //
    calcRect.size.width = self.scrollViewMaster.frame.size.width;
    _tableViewDownloader.frame = calcRect;
    //
    calcRect.origin.x = self.scrollViewMaster.frame.origin.x+6;
    calcRect.origin.y = _tableViewDownloader.frame.origin.y+_tableViewDownloader.frame.size.height;
    calcRect.size.height = _museumDescriptionView.frame.size.height;
    
    if(IOS_NEWER_OR_EQUAL_TO(7.0)) calcRect.size.width = self.scrollViewMaster.frame.size.width-12;
    
    _museumDescriptionView.frame = calcRect;
    //
    CGFloat contentHeight;
    
    if([[SharedAppDelegate playerView] playing] || ![SharedAppDelegate playerHidden] || ![SharedAppDelegate wasPlayerHidden])    {
        contentHeight = self.museumDescriptionView.frame.origin.y+self.museumDescriptionView.frame.size.height+45;
    } else {
        contentHeight = self.museumDescriptionView.frame.origin.y+self.museumDescriptionView.frame.size.height;
    }
    
    self.scrollViewMaster.contentSize = CGSizeMake(self.scrollViewMaster.frame.size.width, contentHeight+10);
    
    calcRect.origin.x = self.scrollViewMaster.frame.origin.x;
    calcRect.origin.y = _imageViewLocation.frame.origin.y+_imageViewLocation.frame.size.height;
    calcRect.size.width = self.scrollViewMaster.frame.size.width;
    calcRect.size.height = _tableViewDownloader.frame.size.height+4;
    _containerView.frame = calcRect;
    
    [_containerView setBackgroundColor:RGBCOLOR(211, 211, 211)];
    
    CGFloat downloaderHeight = _tableViewDownloader.frame.size.height+2;
    
    _downloaderBackground.frame = CGRectMake(0, 0, _containerView.frame.size.width, downloaderHeight);
    
    [_tableViewDownloader reloadData];
    
    if(_currentIndex != -1)   {
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:_currentIndex inSection:0];
        [self.tableViewDownloader selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionTop];
    }
}

- (CGRect)updateHeight:(UITextView *)textView
{
    textView.scrollEnabled = NO;
    CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    textView.frame = newFrame;
    return  newFrame;
}


- (void)setDefaultIndexSelected
{
    [self setCurrentIndex:0];
    
}

-(void)reloadDataWhileChangingData
{
    [self setCurrentIndex:0];
    [[SharedAppDelegate childSplitViewController]setShowThirdContainer:NO];
}
- (void)setCurrentIndex:(NSInteger)currentIndex
{
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:currentIndex inSection:0];
    [self.tableViewDownloader selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionTop];
    [self tableView:self.tableViewDownloader didSelectRowAtIndexPath:indexPath];
}

@end
