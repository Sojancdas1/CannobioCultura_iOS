//
//  IBChildSplitViewController.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 27/08/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IBChildSplitViewController : UIViewController

@property (nonatomic,assign) BOOL showThirdContainer;

@end
