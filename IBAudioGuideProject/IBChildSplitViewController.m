//
//  IBChildSplitViewController.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 27/08/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBChildSplitViewController.h"

#import "IBGlobal.h"
#import "IBCatogoryListViewControllerTablet.h"
#import "IBCatogoryContentViewControllerTablet.h"
#import "IBGuideDetailViewControllerTablet.h"
#import "IBAudioGuideViewControllerTablet.h"

@interface IBChildSplitViewController ()<IBCatogoryListViewControllerTabletDelegate>

@property (strong, nonatomic) UIView *gradientView;

@property (weak, nonatomic) IBOutlet UIView *containerView0;
@property (weak, nonatomic) IBOutlet UIView *containerView1;
@property (weak, nonatomic) IBOutlet UIView *containerView2;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerView1LeadingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerView1TrailingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerView2WidthConstraint;

@property (strong, nonatomic, readwrite) IBCatogoryContentViewControllerTablet *contentController;

- (void)handleSingleTap:(UIGestureRecognizer *)gestureRecognizer;

@end

@implementation IBChildSplitViewController

@synthesize contentController;
@synthesize showThirdContainer = _showThirdContainer;
@synthesize gradientView = _gradientView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
	
		
    self.containerView1LeadingConstraint.constant = 464;
    self.containerView1TrailingConstraint.constant = 0;
    
    _gradientView = [[UIView alloc] initWithFrame:self.containerView0.bounds];
    _gradientView.backgroundColor = [UIColor blackColor];
    _gradientView.alpha = 0.3;
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [singleTap setNumberOfTapsRequired:1];
    [singleTap setNumberOfTouchesRequired:1];
    [_gradientView addGestureRecognizer:singleTap];
	
	UISwipeGestureRecognizer *swipeGestureRecogniser;
	swipeGestureRecogniser = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeAndMove:)];
	//There is a direction property on UISwipeGestureRecognizer. You can set that to both right and left swipes
	swipeGestureRecogniser.direction = UISwipeGestureRecognizerDirectionRight | UISwipeGestureRecognizerDirectionLeft;
	[_gradientView addGestureRecognizer:swipeGestureRecogniser];
	
    
    //drop shadow
    [self.containerView1.layer setShadowColor:[UIColor grayColor].CGColor];
    [self.containerView1.layer setShadowOpacity:0.3];
    [self.containerView1.layer setShadowRadius:8.0];
    [self.containerView1.layer setShadowOffset:CGSizeMake(5, 5)];
    
    self.containerView0.backgroundColor = RGBCOLOR(211, 211, 211);
    self.containerView1.backgroundColor = RGBCOLOR(211, 211, 211);
    self.containerView2.backgroundColor = RGBCOLOR(211, 211, 211);
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(recieveSwipeTouchOnTableNotification:)
                                                 name:@"swipeTouchOnTableNotification"
                                               object:nil];

    
}
- (void) recieveSwipeTouchOnTableNotification:(NSNotification *) notification
{
    if (_showThirdContainer)
    {
        [self setShowThirdContainer:NO];

    }
    else
    {
    [self setShowThirdContainer:YES];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.identifier isEqualToString:@"segueToCategoryList"]) {
        UINavigationController *navigationController = [segue destinationViewController];
        IBCatogoryListViewControllerTablet *categoryController = [navigationController.viewControllers objectAtIndex:0];
        categoryController.delegate = self;
        [categoryController refreshView];
        [categoryController setCurrentIndex:1];
        [SharedAppDelegate setCategoryListController:categoryController];
    } else if ([segue.identifier isEqualToString:@"segueToCategoryContent"]) {
        UINavigationController *navigationController = [segue destinationViewController];
        self.contentController = [navigationController.viewControllers objectAtIndex:0];
        [self.contentController setDownloadIndex:0];
        [SharedAppDelegate setCategoryContentController:contentController];
    } else if ([segue.identifier isEqualToString:@"segueToCategoryDetail"])  {
        UINavigationController *navigationController = [segue destinationViewController];
        IBGuideDetailViewControllerTablet *guideDetailViewController = [navigationController.viewControllers objectAtIndex:0];
        [SharedAppDelegate setGuideDetailViewController:guideDetailViewController];
    }
}

#pragma mark -
#pragma mark - IBCatogoryListViewControllerTabletDelegate methods

- (void)itemSelectedForDownloadAtIndex:(NSInteger)index
{
    [self.contentController setDownloadIndex:index];
}

#pragma mark -

- (void)handleSingleTap:(UIGestureRecognizer *)gestureRecognizer
{
    if(([self showThirdContainer]==YES) && ([SharedAppDelegate geoMapViewController]))  {
        [[SharedAppDelegate geoMapViewController] dismissViewControllerAnimated:NO completion:NULL];
        [SharedAppDelegate audioGuideViewController].view.alpha = 0.f;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [[SharedAppDelegate audioGuideViewController] performSegueWithIdentifier:@"segueToMapView" sender:self];
            [UIView animateWithDuration:1.0f animations:^{
                [SharedAppDelegate audioGuideViewController].view.alpha = 1.f;
            }];
        });

    }
    
    [self setShowThirdContainer:NO];
}

-(void)catchGestureFromTheSwipe
{
	[self setShowThirdContainer:NO];
}

-(void)handleSwipeAndMove:(UISwipeGestureRecognizer *)swipeGesture
{
	 [self setShowThirdContainer:NO];
}

- (void)setShowThirdContainer:(BOOL)showThirdContainer
{
    _showThirdContainer = showThirdContainer;
	
    if(showThirdContainer)  {
        self.containerView1LeadingConstraint.constant = 1;
        self.containerView1TrailingConstraint.constant = 465;
        
        [self.containerView0 addSubview:_gradientView];
    }
    else    {
        self.containerView1LeadingConstraint.constant = 464;
        self.containerView1TrailingConstraint.constant = 0;
        
        [_gradientView removeFromSuperview];
    }

    [self.containerView1 setNeedsUpdateConstraints];
	
    [UIView animateWithDuration:0.3 animations:^{
        
        [self.view layoutIfNeeded];
        
    } completion:^(BOOL isFinished){
    }];
}

@end
