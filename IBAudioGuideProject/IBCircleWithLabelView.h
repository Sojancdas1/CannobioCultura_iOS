//
//  IBCircleWithLabelView.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 26/01/15.
//  Copyright (c) 2015 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IBCircleWithLabelView : UIView
@property(nonatomic,retain)UILabel *roundLabel;
- (void)drawRect:(CGRect)rect number :(NSInteger)value;
@end
