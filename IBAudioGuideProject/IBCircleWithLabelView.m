//
//  IBCircleWithLabelView.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 26/01/15.
//  Copyright (c) 2015 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBCircleWithLabelView.h"
#import "IBGlobal.h"
@implementation IBCircleWithLabelView


@synthesize roundLabel;
- (instancetype)initWithFrame:(CGRect)frame number : (NSInteger )value
{
    self = [super initWithFrame:frame];
    if (self) {
        
        
        
               
        UIView *roundView = [[UIView alloc]initWithFrame:frame];
        roundView.layer.cornerRadius = 15;
        roundView.layer.masksToBounds = YES;
        roundView.backgroundColor = [UIColor yellowColor];
        
        roundLabel = [[UILabel alloc]initWithFrame:frame];
        roundLabel.layer.cornerRadius = 5;
        
        roundLabel.text = @"9";
        
             
        roundLabel.font = kAppFontBold(24);
        roundLabel.backgroundColor = [UIColor greenColor];
        roundLabel.textColor = [UIColor blueColor];
        
        [self addSubview:roundView];
        [self addSubview:roundLabel];
        

    }
    return self;
}


 //Only override drawRect: if you perform custom drawing.
 //An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect number :(NSInteger)value
{
    roundLabel.text = [NSString stringWithFormat:@"%ld",(long)value];
}


@end
