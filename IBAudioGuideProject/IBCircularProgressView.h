//
//  IBCircularProgressView.h
//  IBCircularProgressView
//
//  Created by Jojin Johnson on 28/05/14.
//  Copyright (c) 2013 Jojin Johnson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IBCircularProgressView : UIControl

@property (nonatomic) float progress;
@property (nonatomic, strong) UIColor *tintColor;
@property (nonatomic, assign) BOOL showProgressCentre;

- (void)setProgress:(float)progress animated:(BOOL)animated;

@end
