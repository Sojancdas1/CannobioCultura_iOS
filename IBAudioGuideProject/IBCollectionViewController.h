//
//  IBCollectionViewController.h
//  IBourGuideComponents
//
//  Created by Mobility 2014 on 13/08/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IBCollectionViewController : UICollectionViewController

@property (strong, nonatomic) NSArray *galleryInfos;

@end
