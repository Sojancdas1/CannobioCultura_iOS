//
//  IBCollectionViewController.m
//  IBourGuideComponents
//
//  Created by Mobility 2014 on 13/08/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBCollectionViewController.h"

#import "IBGlobal.h"
#import "IBGalleryImageInfo.h"
#import "IBGalleryDescriptionReusableView.h"
#import "RBCollectionViewInfoFolderLayout.h"
#import "IBPhotoGalleryViewControllerTablet.h"
#import "HUDManager.h"

#import "IBGalleryCell.h"

#define Width 465
#define Height 748
#define kPlayerHeight 65
@interface IBCollectionViewController ()<RBCollectionViewInfoFolderLayoutDelegate>
{
    NSInteger currentIndex;
}

@property (nonatomic, strong) NSArray * dataKeys;
@property (nonatomic, strong) NSDictionary * data;
@property (nonatomic, strong) NSCache * imageCache;

- (void)refreshView;
- (void)handleLongPressGesture:(UIGestureRecognizer *)gestureRecognizer;

@end

@implementation IBCollectionViewController

@synthesize galleryInfos = _galleryInfos;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
     CGRect frameAcroll = scrollView.frame;
   // scrollView.frame = self.collectionView.frame;
    if ([SharedAppDelegate playerViewTablet].isAppeared)
    {
       
        
        if (scrollView.frame.size.height == self.view.frame.size.height - kPlayerHeight)
        {
            //NSLog(@"ogt it");
        }
        else
        {
            frameAcroll.size.height = frameAcroll.size.height - kPlayerHeight;
            scrollView.frame = frameAcroll;
            
            //NSLog(@"%@",NSStringFromCGRect(scrollView.frame));
            [self.collectionView reloadData];
            
        }
     
    }
    else
    {
        
        if (scrollView.frame.size.height == self.view.frame.size.height - kPlayerHeight)
        {
            frameAcroll.size.height = frameAcroll.size.height + kPlayerHeight;
            scrollView.frame = frameAcroll;
            
            //NSLog(@"%@",NSStringFromCGRect(scrollView.frame));
            [self.collectionView reloadData];

        }
        else
        {
            
        }

    }
}

- (void)viewDidLoad {
    //
    [super viewDidLoad];
    
    
//    CGRect frameSelf = self.view.frame;
//    
//    frameSelf.size.height = frameSelf.size.height - 90;
//    self.view.frame = frameSelf;
    // Do any additional setup after loading the view.
    
    self.imageCache = [[NSCache alloc] init];
    
	RBCollectionViewInfoFolderLayout *layout = (id)self.collectionView.collectionViewLayout;
	layout.cellSize = CGSizeMake(154.8, 154.8);
	layout.interItemSpacingY = 0;
    layout.interItemSpacingX = 0;
	layout.stickyHeaders = NO;
    
	[self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:RBCollectionViewInfoFolderHeaderKind withReuseIdentifier:@"header"];
	[self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:RBCollectionViewInfoFolderFooterKind withReuseIdentifier:@"footer"];
	[self.collectionView registerClass:[IBGalleryDescriptionReusableView class] forSupplementaryViewOfKind:RBCollectionViewInfoFolderFolderKind withReuseIdentifier:@"folder"];
	[self.collectionView registerClass:[RBCollectionViewInfoFolderDimple class] forSupplementaryViewOfKind:RBCollectionViewInfoFolderDimpleKind withReuseIdentifier:@"dimple"];
    
//	self.collectionView.backgroundColor = [UIColor whiteColor];
    self.collectionView.backgroundColor = RGBCOLOR(211, 211, 211);
//    self.collectionView.backgroundColor = [UIColor blackColor];
    
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGesture:)];
    [longPressGesture setNumberOfTouchesRequired:1];
    [self.view addGestureRecognizer:longPressGesture];
    
    self.collectionView.bounces = NO;
    
    currentIndex = -1;
    
    if(IOS_NEWER_OR_EQUAL_TO(7.0))  {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.collectionView.alpha = 0.f;
    [HUDManager addHUDWithLabel:nil];
    [self performSelectorOnMainThread:@selector(refreshView) withObject:nil waitUntilDone:YES];
}

- (void)refreshView
{
    NSInteger locationNo = [SharedAppDelegate locationNo];
    NSUInteger languageIndex = [SharedAppDelegate languageIndex];
    
    NSString *museumDir,*languageDir;
    
    DIRLOCATION_STR(locationNo,museumDir);
    DIRLANGUAGE_STR(languageIndex, languageDir);
    
    if(_galleryInfos) { _galleryInfos = nil; }
    
    NSArray *arrayGalleryImages = [[SharedAppDelegate database] loadPhotoGalleryWithLocationNo:locationNo andLanguageIndex:languageIndex];
    
    NSMutableArray *array = [NSMutableArray array];
    
    IBGalleryImageInfo *info;
    for (IBGalleryImages *galleryImage in arrayGalleryImages) {
        info = [[IBGalleryImageInfo alloc] init];
        info.referenceNo = galleryImage.referenceNo;
        info.photoPath = galleryImage.photoPath;
        info.photoTitle = galleryImage.photoTitle;
        info.photoDescription = galleryImage.photoDescription;
        [array addObject:info];
    }

    self.galleryInfos = [NSArray arrayWithArray:array];
    
    [HUDManager hideHUDFromWindowAfterDelay:0.2f];
    
    [UIView animateWithDuration:1.f animations:^{
        self.collectionView.alpha = 1.f;
        [SharedAppDelegate unlockInteractions];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"segueToPhotoGallery"])
    {
        IBPhotoGalleryViewControllerTablet *photoGallery = [segue destinationViewController];
        photoGallery.currentIndex = currentIndex;
    }
}

- (void)handleLongPressGesture:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded)
    {
        CGPoint point = [gestureRecognizer locationInView:self.collectionView];
        NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:point];
        if (indexPath)
        {
            RBCollectionViewInfoFolderLayout * layout = (id)self.collectionView.collectionViewLayout;
            [layout toggleFolderViewForIndexPath:indexPath];
        }
    }
}

- (void)setGalleryInfos:(NSArray *)galleryInfos
{
    _galleryInfos = galleryInfos;
    [self.collectionView reloadData];
}

#pragma mark - UICollectionViewDelegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
	return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
	return [_galleryInfos count];
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
	UICollectionReusableView * reuseView;
    
	if (kind == RBCollectionViewInfoFolderHeaderKind)
	{
		reuseView = [self.collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"header" forIndexPath:indexPath];
        
//		reuseView.backgroundColor = [UIColor whiteColor];
        reuseView.backgroundColor = RGBCOLOR(211, 211, 211);
//        reuseView.backgroundColor = [UIColor blackColor];
//		UILabel * label = (id)[reuseView viewWithTag:1];
//		if (label == nil)
//		{
//			label = [[UILabel alloc] init];
//			label.tag = 1;
//			label.frame = CGRectMake(0, 0, Width, 30);
//			label.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//			label.textAlignment = NSTextAlignmentCenter;
//            label.font = kAppFontRegular(18);
//            label.textColor = RGBCOLOR(110, 110, 110);
//			[reuseView addSubview:label];
//		}
//        
//        NSString *museumName = nil;
//        MUSEUMNAME_STR([SharedAppDelegate locationNo], museumName);
//        
//        label.text = museumName;
	}
    
	if (kind == RBCollectionViewInfoFolderFooterKind)
	{
		reuseView = [self.collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"footer" forIndexPath:indexPath];
        
		reuseView.backgroundColor = [UIColor clearColor];
        
//		UILabel * label = (id)[reuseView viewWithTag:1];
//		if (label == nil)
//		{
//			label = [[UILabel alloc] init];
//			label.tag = 1;
//			label.frame = CGRectMake(0, 0, Width, 30);
//			label.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//			label.textAlignment = NSTextAlignmentCenter;
//            label.font = kAppFontRegular(15);
//            label.textColor = RGBCOLOR(110, 110, 110);
//			[reuseView addSubview:label];
//		}
//        
//		label.text = @"Galleria Fotografica dell’Isola Borromee";
	}
    
	if (kind == RBCollectionViewInfoFolderFolderKind)
	{
		IBGalleryDescriptionReusableView *galleryDescriptionView = (id)[self.collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"folder" forIndexPath:indexPath];
        
		galleryDescriptionView.backgroundColor = [UIColor whiteColor];
        
		UILabel * label = (id)[reuseView viewWithTag:1];
		if (label == nil)
		{
			label = [[UILabel alloc] init];
			label.tag = 1;
			label.frame = CGRectMake(0, 0, Width, 30);
			label.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
			label.textAlignment = NSTextAlignmentCenter;
            label.font = kAppFontRegular(15);
            label.textColor = RGBCOLOR(104, 144, 152);
			[reuseView addSubview:label];
		}
        
        IBGalleryImageInfo *info = [_galleryInfos objectAtIndex:indexPath.row];
		galleryDescriptionView.title.text = [info photoTitle];
		galleryDescriptionView.desc.text = [info photoDescription];
        
		reuseView = galleryDescriptionView;
	}
    
	if (kind == RBCollectionViewInfoFolderDimpleKind)
	{
		RBCollectionViewInfoFolderDimple * dimple = (id)[self.collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"dimple" forIndexPath:indexPath];
        
		dimple.color = [UIColor whiteColor];
        
		reuseView = dimple;
	}
    
	return reuseView;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    //
	IBGalleryCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath]; 
	IBGalleryImageInfo *info = [_galleryInfos objectAtIndex:indexPath.row];
    //
	NSString *imagePath = info.photoPath;
    NSString *fileName = [imagePath lastPathComponent];
    NSString *filePath = [imagePath stringByDeletingLastPathComponent];
    NSString *firstPart = [IBGlobal createDocumentsDirectoryByAppendingPathComponent:nil];
    NSString *thumbnailPath = [NSString stringWithFormat:@"%@/%@/T_%@",firstPart,filePath,fileName];
    //
    NSLog(@"imagePath = %@", imagePath);
    NSLog(@"thumbnailPath = %@", thumbnailPath);
    if(![[NSFileManager defaultManager] fileExistsAtPath:thumbnailPath]) {
        //
        thumbnailPath = [thumbnailPath stringByReplacingOccurrencesOfString:@"JPG" withString:kImageExtension];
    }
    //
	if ([self.imageCache objectForKey:thumbnailPath] == nil) {
        //
		__weak typeof(self) weakSelf = self;
        //
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul), ^{
            //
            @autoreleasepool {
                //
                UIImage *image = [UIImage imageWithContentsOfFile:thumbnailPath];
                //
                dispatch_sync(dispatch_get_main_queue(), ^{
                    //
                    if (image) [weakSelf.imageCache setObject:image forKey:thumbnailPath];
                    
                    UIImageView *imageView = (id)[cell viewWithTag:1];
                    [imageView setImage:image];
                    [cell setNeedsLayout];
                });
            }
		});
	} else {
        //
		UIImageView * imageView = (id)[cell viewWithTag:1];
		imageView.image = [self.imageCache objectForKey:thumbnailPath];
	}
    //
	cell.layer.masksToBounds = NO;
	cell.layer.shadowOpacity = 0.4f;
	cell.layer.shadowRadius = 2.0f;
	cell.layer.shadowOffset = CGSizeMake(0, 1);
	cell.layer.shadowPath = [UIBezierPath bezierPathWithRect:cell.bounds].CGPath;
    //
	return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    currentIndex = indexPath.row;
    
	RBCollectionViewInfoFolderLayout * layout = (id)self.collectionView.collectionViewLayout;
    
    if([layout isAnyFolderOpen])    {
        [layout closeAllOpenFolders];
    }
    else    {
        [self performSegueWithIdentifier:@"segueToPhotoGallery" sender:self];
    }
}

#pragma mark - RBCollectionViewInfoFolderLayoutDelegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(RBCollectionViewInfoFolderLayout *)collectionViewLayout sizeForHeaderInSection:(NSInteger)section
{
//	return CGSizeMake(Width, 30);
    return CGSizeMake(Width, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(RBCollectionViewInfoFolderLayout *)collectionViewLayout heightForFolderAtIndexPath:(NSIndexPath *)indexPath
{
    IBGalleryImageInfo *info = [_galleryInfos objectAtIndex:indexPath.row];
    
	NSString * title = [info photoTitle];
	NSString * desc = [info photoDescription];
	
	CGSize constrainedSize = CGSizeMake(Width, CGFLOAT_MAX);
    
	return [IBGalleryDescriptionReusableView heightOfViewWithTitle:title description:desc constrainedToSize:constrainedSize];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(RBCollectionViewInfoFolderLayout *)collectionViewLayout sizeForFooterInSection:(NSInteger)section
{
//	return CGSizeMake(Width, 25);
   return CGSizeMake(Width, 0);
}
//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
//    return UIEdgeInsetsMake(0, 0, 100, 0);
//}
//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
//{
//    return UIEdgeInsetsMake(60, 10, 50, 10);
//}
//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
//{
//    return UIEdgeInsetsMake(0, 0, 200, 0);
//}

@end
