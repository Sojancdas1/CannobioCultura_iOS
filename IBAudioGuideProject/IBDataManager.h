//
//  IBDataManager.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 07/05/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IBDataManager : NSObject

+ (IBDataManager *)sharedInstance;

- (void)makeLanguageIndexesIfNotExists;
- (void)makeLocationIndexesIfNotExists;

- (NSArray *)loadLocationDetailsWithLocationNo:(NSInteger)locationNo andLanguageIndex:(NSInteger)languageIndex;
- (NSArray *)loadLocationDetailsWithLocationNo:(NSInteger)locationNo andLanguageIndex:(NSInteger)languageIndex forceDownload:(BOOL)status;

- (NSArray *)loadPhotoGalleryWithLocationNo:(NSInteger)locationNo andLanguageIndex:(NSInteger)languageIndex;
- (NSArray *)loadPhotoGalleryWithLocationNo:(NSInteger)locationNo andLanguageIndex:(NSInteger)languageIndex forceDownload:(BOOL)status;

- (NSArray *)loadAudioGuideWithLocationNo:(NSInteger)locationNo andLanguageIndex:(NSInteger)languageIndex;
- (NSArray *)loadAudioGuideWithLocationNo:(NSInteger)locationNo andLanguageIndex:(NSInteger)languageIndex forceDownload:(BOOL)status;

- (NSInteger)countPurchase;
- (NSInteger)countNoOfRooms;
- (NSInteger)countNoOfMapSections;

- (NSArray *)fetchLanguageIndexes;
- (NSArray *)fetchLocationIndexes;

-(NSArray *)fetchWholeCircleCoordinatesWithIndex:(NSString *)index;
- (NSArray *)fetchGuideGalleryByAudioReferenceIndex:(NSString *)referenceNo;
- (NSArray *)fetchGeoMapRoomsBySessionIndex:(NSInteger)index;
- (NSArray *)fetchAudioGuidesByRoomNo:(NSString *)roomNo;
- (NSArray *)fetchRoomsByAudioGuideReferenceNo:(NSString *)referenceNo;
- (NSArray *)fetchAudioGuideByReferenceNo:(NSString *)referenceNo;
- (void)setLanguageSelectedByDisplayName:(NSString *)displayName;
- (BOOL)updatePurchaseOfLocationNo:(NSInteger)locationNo withStatus:(BOOL)status;
-(NSString *)loadCoverPhotoOfCurrespondWithLocationNo:(NSInteger)locationNo andReferenceNo:(NSString *)referenceNo;
- (BOOL)isPurchasedForLocationNo:(NSInteger)locationNo;

@end
