//
//  IBDataManager.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 07/05/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBDataManager.h"

#import "Base64.h"
#import "IBXMLParser.h"
#import "UIImage+Resize.h"
#import "IBGlobal.h"

@interface IBDataManager ()
{
    IBXMLParser *parser;
}

- (NSString *)keyWithURLString:(NSString *)urlString;

@end

@implementation IBDataManager

+ (IBDataManager *)sharedInstance
{
    static IBDataManager *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [[IBDataManager alloc] init];
    });
    
    return _sharedInstance;
}

- (id) init {
    self = [super init];
    if (self != nil) {
        // initializations go here.
        parser = [[IBXMLParser alloc] init];
    }
    return self;
}

- (void)makeLanguageIndexesIfNotExists
{
    NSManagedObjectContext *context = [SharedAppDelegate managedObjectContext];
    
    NSError *error = nil;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"IBLanguages" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSUInteger count = [context countForFetchRequest:fetchRequest error:&error];
    
    if(!error) {
        if(count == 0) {
            //
            IBLanguages *languageInfo1 = [NSEntityDescription insertNewObjectForEntityForName:@"IBLanguages" inManagedObjectContext:context];
            languageInfo1.displayName = @"English";
            languageInfo1.order = @(1);
            languageInfo1.resourcePath = [NSString stringWithFormat:@"%@/",ENGLISH];
            languageInfo1.selected = [NSNumber numberWithBool:NO];
            
            IBLanguages *languageInfo4 = [NSEntityDescription insertNewObjectForEntityForName:@"IBLanguages" inManagedObjectContext:context];
            languageInfo4.displayName = @"Italiano";
            languageInfo4.order = @(2);
            languageInfo4.resourcePath = [NSString stringWithFormat:@"%@/",ITALIAN];
            languageInfo4.selected = [NSNumber numberWithBool:NO];
            
            IBLanguages *languageInfo3 = [NSEntityDescription insertNewObjectForEntityForName:@"IBLanguages" inManagedObjectContext:context];
            languageInfo3.displayName = @"Français";
            languageInfo3.order = @(3);
            languageInfo3.resourcePath = [NSString stringWithFormat:@"%@/",FRENCH];
            languageInfo3.selected = [NSNumber numberWithBool:NO];
            
            IBLanguages *languageInfo2 = [NSEntityDescription insertNewObjectForEntityForName:@"IBLanguages" inManagedObjectContext:context];
            languageInfo2.displayName = @"Deutsch";
            languageInfo2.order = @(4);
            languageInfo2.resourcePath = [NSString stringWithFormat:@"%@/",DEUTSCH];
            languageInfo2.selected = [NSNumber numberWithBool:NO];
            
            if (![context save:&error]) {
                //NSLog(@"Failed to save - error: %@", [error localizedDescription]);
            }
        }
    }
    
    [IBGlobal copyResorceFile:@"Deutsch" withExtention:@"plist" ToDocumentsDirectoryByAppendingPathComponent:[NSString stringWithFormat:@"%@/",DEUTSCH] andDestinationFileName:@"lang"];
    
    [IBGlobal copyResorceFile:@"English" withExtention:@"plist" ToDocumentsDirectoryByAppendingPathComponent:[NSString stringWithFormat:@"%@/",ENGLISH] andDestinationFileName:@"lang"];
    
    [IBGlobal copyResorceFile:@"Français" withExtention:@"plist" ToDocumentsDirectoryByAppendingPathComponent:[NSString stringWithFormat:@"%@/",FRENCH] andDestinationFileName:@"lang"];
    
    [IBGlobal copyResorceFile:@"Italiano" withExtention:@"plist" ToDocumentsDirectoryByAppendingPathComponent:[NSString stringWithFormat:@"%@/",ITALIAN] andDestinationFileName:@"lang"];
}
-(NSString *)loadCoverPhotoOfCurrespondWithLocationNo:(NSInteger)locationNo andReferenceNo:(NSString *)referenceNo
{
    NSString *museumDir;
    DIRLOCATION_STR(locationNo,museumDir);
    NSString *destinationDir = [IBGlobal createDocumentsDirectoryByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@/%@",ENGLISH,museumDir,@"cover"]];
    NSString *coverPath = [NSString stringWithFormat:@"%@/%@.%@",destinationDir,referenceNo,@"jpg"];
    return coverPath;
    
}

- (void)makeLocationIndexesIfNotExists
{
    NSManagedObjectContext *context = [SharedAppDelegate managedObjectContext];
    
    NSError *error = nil;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"IBMuseums"
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSUInteger count = [context countForFetchRequest:fetchRequest
                                               error:&error];
    if(!error)  {
        if(count==0) {
            IBMuseums *museumInfo1 = [NSEntityDescription
                                      insertNewObjectForEntityForName:@"IBMuseums"
                                      inManagedObjectContext:context];
            museumInfo1.museumName = @"Isola Bella";
            museumInfo1.museumDescription = @"Isola Bella (lit. 'beautiful island') is one of the Borromean Islands of Lago Maggiore in north Italy. The island is situated in the Borromean Gulf 400 metres from the lakeside town of Stresa. Isola Bella is 320 metres long by 400 metres wide and is divided between the Palace, its Italianate garden, and a small fishing village.";
            museumInfo1.guidePurchased = [NSNumber numberWithBool:NO];
            
            IBMuseums *museumInfo2 = [NSEntityDescription
                                      insertNewObjectForEntityForName:@"IBMuseums"
                                      inManagedObjectContext:context];
            museumInfo2.museumName = @"Isola Madre";
            museumInfo2.museumDescription = @"Isola Madre, at 220 m wide and 330 m long, is the largest island of the Isole Borromee archipelago which falls within the Italian part of the AlpineLake Maggiore, in the Province of Verbano Cusio Ossola, Piedmont. The island is occupied by a number of buildings and architectural structures and is especially well known for its gardens. In the past it was known as Isola di San Vittore and later as Isola Maggiore.";
            museumInfo2.guidePurchased = [NSNumber numberWithBool:NO];
            
            IBMuseums *museumInfo3 = [NSEntityDescription
                                      insertNewObjectForEntityForName:@"IBMuseums"
                                      inManagedObjectContext:context];
            museumInfo3.museumName = @"Rocca d’Angera";
            museumInfo3.museumDescription = @"The Rocca Borromeo di Angera, or Rocca d'Angera, also called Borromeo Castle, is a castle that stands on a lakeside hilltop in the limits of the town of Angera in the Province of Varese on the Southern shores of Lago Maggiore. It is visible from across the lake from Arona, where originally stood another castle formerly owned by the Borromeo family.";
            museumInfo3.guidePurchased = [NSNumber numberWithBool:NO];
            
            if (![context save:&error]) {
                //NSLog(@"Failed to save - error: %@", [error localizedDescription]);
            }
        }
    }
}

- (NSArray *)loadLocationDetailsWithLocationNo:(NSInteger)locationNo andLanguageIndex:(NSInteger)languageIndex
{
    NSString *urlString,*museumDir;
    
    DIRLOCATION_STR(locationNo,museumDir);
    
    urlString = [NSString stringWithFormat:@"%@/%@/%@/%@",SYNC_URL,IBParentDirectory,museumDir,kMapXml];
    NSString *key = [self keyWithURLString:urlString];
    
    BOOL downloadStatus = [[[NSUserDefaults standardUserDefaults] valueForKey:key] intValue];
    
    NSDictionary *locationDictionary = [parser loadLocationXMLWithLocationNo:locationNo andLanguageIndex:languageIndex forceDownload:!downloadStatus];
    
    NSDictionary *descDictionary = [locationDictionary valueForKey:@"Desc"];
    
    NSString *museumName = nil;
    if(locationNo==1){museumName=@"Isola Bella";}else if(locationNo==2){museumName=@"Isola Madre";}else{museumName=@"Rocca d’Angera";}
    
    NSString *keyName = nil;
    DIRLANGUAGE_STR(languageIndex, keyName);
    
    NSManagedObjectContext *context = [SharedAppDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity1 = [NSEntityDescription entityForName:@"IBMuseums" inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity1];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        //NSLog(@"Failed to load - error: %@", [error localizedDescription]);
    }
    else    {
        for(IBMuseums *museum in fetchedObjects)    {
            if([museum.museumName isEqualToString:museumName]) {
                museum.museumDescription = [descDictionary valueForKey:[keyName uppercaseString]];
            }
        }
    }
    
    if (![context save:&error]) {
        //NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
    
    NSEntityDescription *entity2 = [NSEntityDescription entityForName:@"IBMuseums" inManagedObjectContext:context];
    [fetchRequest setEntity:entity2];
    
    fetchRequest.resultType = NSDictionaryResultType;
    
    [fetchRequest setPropertiesToFetch:[NSArray arrayWithObjects:@"museumName", @"museumDescription", nil]];
    
    NSPredicate *predicateID = [NSPredicate predicateWithFormat:@"museumName == %@",museumName];
    [fetchRequest setPredicate:predicateID];
    
    error = nil;
    fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        //NSLog(@"Failed to load - error: %@", [error localizedDescription]);
    }
    
    //NSLog(@"%@",fetchedObjects);
    
    NSFetchRequest *fetchRequest3 = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity3 = [NSEntityDescription entityForName:@"IBGeoMapRooms" inManagedObjectContext:context];
    [fetchRequest3 setEntity:entity3];
    
    error = nil;
    NSArray *existedGeoMaps = [context executeFetchRequest:fetchRequest3 error:&error];
    if (existedGeoMaps != nil) {
        for (IBGeoMapRooms *existedMapInfo in existedGeoMaps) {
            [context deleteObject:existedMapInfo];
        }
        
        if (![context save:&error]) {
            //NSLog(@"Failed to save - error: %@", [error localizedDescription]);
        }
    }
    
    NSArray *arrayMuseumRooms = [locationDictionary valueForKey:@"Section"];
    
    for(NSDictionary *sectionDictionary in arrayMuseumRooms)
    {
        NSNumber *sectionNo = [NSNumber numberWithInteger:[[sectionDictionary valueForKey:@"index"] integerValue]];
        NSArray *rooms = [sectionDictionary valueForKey:@"rooms"];
        
        IBGeoMapRooms *mapRoomToSave;
        for(IBMuseumRoomInfo *roomInfo in rooms)
        {
            mapRoomToSave = [NSEntityDescription
                             insertNewObjectForEntityForName:@"IBGeoMapRooms"
                             inManagedObjectContext:context];
            
            if ([roomInfo.roomNo isKindOfClass:[NSString class]]) {
                mapRoomToSave.roomNo = roomInfo.roomNo;
            }else{
                mapRoomToSave.roomNo = @"00";
            }

            mapRoomToSave.coordinates = roomInfo.coordinates;
            mapRoomToSave.sectionNo = sectionNo;
            mapRoomToSave.circleCoordinates = roomInfo.circleCoordinates;
        }
    }
    
    if (![context save:&error]) {
        //NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:@"1" forKey:key];
    [defaults synchronize];
    
    return fetchedObjects;
}




- (NSArray *)loadLocationDetailsWithLocationNo:(NSInteger)locationNo andLanguageIndex:(NSInteger)languageIndex forceDownload:(BOOL)status
{
    if(status == YES)   {
        NSString *urlString,*museumDir;
        
        DIRLOCATION_STR(locationNo,museumDir);
        
        urlString = [NSString stringWithFormat:@"%@/%@/%@/%@",SYNC_URL,IBParentDirectory,museumDir,kMapXml];
        NSString *key = [self keyWithURLString:urlString];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:@"0" forKey:key];
        [defaults synchronize];
    }
    
    return [self loadLocationDetailsWithLocationNo:locationNo andLanguageIndex:languageIndex];
}

- (NSArray *)loadPhotoGalleryWithLocationNo:(NSInteger)locationNo andLanguageIndex:(NSInteger)languageIndex {
    //
    NSError *error = nil;
    NSString *museumName = nil;
    NSString *museumDir,*languageDir;
    DIRLOCATION_STR(locationNo,museumDir);
    DIRLANGUAGE_STR(languageIndex,languageDir);
    
    NSString *urlString = [NSString stringWithFormat:@"%@/%@/%@/%@/%@",SYNC_URL,IBParentDirectory,museumDir,languageDir,kGalleryXml];
    NSString *key = [self keyWithURLString:urlString];
    BOOL downloadStatus = [[[NSUserDefaults standardUserDefaults] valueForKey:key] intValue];
    NSArray *galleryArray = [parser loadPhotoGalleryXMLWithLocationNo:locationNo andLanguageIndex:languageIndex forceDownload:!downloadStatus];
    if (locationNo==1) {museumName=@"Isola Bella";}else if(locationNo==2){museumName=@"Isola Madre";}else{museumName=@"Rocca d’Angera";}
    
    NSManagedObjectContext *context = [SharedAppDelegate managedObjectContext]; 
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"IBMuseums" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSPredicate *predicateID = [NSPredicate predicateWithFormat:@"museumName == %@",museumName];
    [fetchRequest setPredicate:predicateID];
    
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) return  nil;
    //
    entity = [NSEntityDescription entityForName:@"IBGalleryImages" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    predicateID = [NSPredicate predicateWithFormat:@"imageGalleryToMuseum == %@",fetchedObjects[0]];
    [fetchRequest setPredicate:predicateID];
    //
    NSArray *existedGallery = [context executeFetchRequest:fetchRequest error:&error];
    //
    if (existedGallery != nil) {
        //
        for (IBGalleryImages *existedImageInfo in existedGallery) {
            //
            [context deleteObject:existedImageInfo];
        }
    }
    
    NSString *destinationDir = [IBGlobal createDocumentsDirectoryByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@/%@",ENGLISH,museumDir,kGallery]];
    //
    IBGalleryImages *galleryDataToSave = nil;
    
    for(IBGalleryImageXMLInfo *info in galleryArray) {
        //
        galleryDataToSave = [NSEntityDescription insertNewObjectForEntityForName:@"IBGalleryImages" inManagedObjectContext:context];
        
        galleryDataToSave.referenceNo = info.referenceNo;
        galleryDataToSave.photoTitle = info.heading;
        galleryDataToSave.photoDescription = info.description;
        //
        
        NSString *destinationPath = [NSString stringWithFormat:@"%@/%@",destinationDir,info.imageName];
        NSString *mainImagePath = [NSString stringWithFormat:@"%@/%@/%@/%@",ENGLISH,museumDir,kGallery,info.imageName];
        //
        //
        if(![[NSFileManager defaultManager] fileExistsAtPath:destinationPath]) {
            //
            mainImagePath = [mainImagePath stringByReplacingOccurrencesOfString:kImageExtension withString:@"JPG"];
        }
        //
        galleryDataToSave.photoPath = mainImagePath;
        
        
        
        //NSLog(@"%@/%@/%@/%@",ENGLISH,museumDir,kGallery,info.imageName);
        //NSLog(@"%@/%@",destinationDir,info.imageName);
        
        
        if(IS_IPAD) {
            
            //NSLog(@"%@",info.imageName);
            
            NSString *thumbnailPath = [NSString stringWithFormat:@"%@/T_%@",destinationDir, info.imageName];
            
            //Thumbnail generation if not exists
            if(![[NSFileManager defaultManager] fileExistsAtPath:thumbnailPath]) {
                //
                @autoreleasepool {
                    //
                    UIImage *thumbnailParentImage = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",destinationDir,info.imageName]];
                    if (thumbnailParentImage == nil) {
                        thumbnailParentImage = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",destinationDir,[info.imageName stringByReplacingOccurrencesOfString:kImageExtension withString:@"JPG"]]];
                    }
                    //
                    UIImage *thumbImage = [thumbnailParentImage thumbnailImage:200.f transparentBorder:0.f cornerRadius:0.f interpolationQuality:kCGInterpolationMedium];
                    NSData *imageData = UIImageJPEGRepresentation(thumbImage,0.8);
                    [imageData writeToFile:thumbnailPath atomically:YES];
                }
            }
        }
        
        galleryDataToSave.imageGalleryToMuseum = fetchedObjects[0];
    }
    
    error = nil;
    if (![context save:&error]) {
        //NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"referenceNo" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    
    error = nil;
    fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        //NSLog(@"Failed to load - error: %@", [error localizedDescription]);
    }
    
    //NSLog(@"%@",fetchedObjects);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:@"1" forKey:key];
    [defaults synchronize];
    
    return fetchedObjects;
}

- (NSArray *)loadPhotoGalleryWithLocationNo:(NSInteger)locationNo andLanguageIndex:(NSInteger)languageIndex forceDownload:(BOOL)status
{
    if(status == YES)   {
        NSString *urlString,*museumDir,*languageDir;
        
        DIRLOCATION_STR(locationNo,museumDir);
        DIRLANGUAGE_STR(languageIndex,languageDir);
        
        urlString = [NSString stringWithFormat:@"%@/%@/%@/%@/%@",SYNC_URL,IBParentDirectory,museumDir,languageDir,kGalleryXml];
        NSString *key = [self keyWithURLString:urlString];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:@"0" forKey:key];
        [defaults synchronize];
    }
    
    return [self loadPhotoGalleryWithLocationNo:locationNo andLanguageIndex:languageIndex forceDownload:status];
}

- (NSArray *)loadAudioGuideWithLocationNo:(NSInteger)locationNo andLanguageIndex:(NSInteger)languageIndex
{
    NSString *urlString,*museumDir,*languageDir;
    DIRLOCATION_STR(locationNo,museumDir);
    DIRLANGUAGE_STR(languageIndex,languageDir);
    
    urlString = [NSString stringWithFormat:@"%@/%@/%@/%@/%@",SYNC_URL,IBParentDirectory,museumDir,languageDir,kAudioGuideXml];
    NSString *key = [self keyWithURLString:urlString];
    
    BOOL downloadStatus = [[[NSUserDefaults standardUserDefaults] valueForKey:key] intValue];
    
    NSArray *audioGuideArray = [parser loadAudioGuideXMLWithLocationNo:locationNo andLanguageIndex:languageIndex forceDownload:!downloadStatus];
    
    NSManagedObjectContext *context = [SharedAppDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"IBAudioGuide" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"referenceNo" ascending:YES];
    NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"guideDescription" ascending:YES];
    
    [fetchRequest setSortDescriptors:[NSArray arrayWithObjects:sortDescriptor, sortDescriptor2, nil]];
    
    
    NSError *error = nil;
    NSArray *existedAudioGuides = [context executeFetchRequest:fetchRequest error:&error];
    if (existedAudioGuides != nil)
    {
        for (IBAudioGuide *existedAudioInfo in existedAudioGuides)
        {
            [context deleteObject:existedAudioInfo];
        }
        if (![context save:&error])
        {
            //NSLog(@"Failed to save - error: %@", [error localizedDescription]);
        }
    }
    
    //NSLog(@"The data is %@",existedAudioGuides);
    
    NSFetchRequest *museumFetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *museumEntity = [NSEntityDescription entityForName:@"IBMuseums" inManagedObjectContext:context];
    [museumFetchRequest setEntity: museumEntity];
    
    NSString *museumName = nil;
    if(locationNo==1)
    {
        museumName=@"Isola Bella";
    }
    else if(locationNo==2)
    {
        museumName=@"Isola Madre";
    }
    else
    {
        museumName=@"Rocca d’Angera";
    }
    
    NSPredicate * museumNamePredicate = [NSPredicate predicateWithFormat:@"museumName == %@",museumName];
    [museumFetchRequest setPredicate:museumNamePredicate];
    
    error = nil;
    NSArray *fetchedMuseumObjects = [context executeFetchRequest: museumFetchRequest error:&error];
    
    IBAudioGuide *audioDataToSave = nil;
    NSString *guidesGalleryDir = [IBGlobal createDocumentsDirectoryByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@/%@",ENGLISH,museumDir,kGuidesGallery]];
    NSString *audioGuideDir = [IBGlobal createDocumentsDirectoryByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@/%@",languageDir,museumDir,kAudioGuide]];
    
    //NSLog(@"%@",audioGuideDir);
    
    for(IBAudioGuideXMLInfo *audioGuideXMLInfo in audioGuideArray)
    {
        audioDataToSave = [NSEntityDescription insertNewObjectForEntityForName:@"IBAudioGuide" inManagedObjectContext:context];
        //NSLog(@"referenceNumbers are : %@",audioGuideXMLInfo.referenceNo);
        
        audioDataToSave.referenceNo = audioGuideXMLInfo.referenceNo;
        audioDataToSave.guideTitle = audioGuideXMLInfo.guideTitle;
        audioDataToSave.guideDescription = audioGuideXMLInfo.guideDescription;
        audioDataToSave.audioFilePath = [NSString stringWithFormat:@"%@/%@/%@/%@",languageDir,museumDir,kAudioGuide,[NSString stringWithFormat:@"%@.%@",audioGuideXMLInfo.referenceNo,kAudioExtension]];
        
        NSString *thumbnailPath = [NSString stringWithFormat:@"%@/%@",guidesGalleryDir,[NSString stringWithFormat:@"T_%@.%@",audioGuideXMLInfo.referenceNo,kThumbExtension]];
        
        //Thumbnail generation if not exists
        if(![[NSFileManager defaultManager] fileExistsAtPath:thumbnailPath])
        {
            NSString *thumbnailParent = [NSString stringWithFormat:@"%@/%@",guidesGalleryDir,[NSString stringWithFormat:@"%@-01.%@",audioGuideXMLInfo.referenceNo,kImageExtension]];
            if([[NSFileManager defaultManager] fileExistsAtPath:thumbnailParent])
            {
                @autoreleasepool {
                    UIImage *thumbnailParentImage = [UIImage imageWithContentsOfFile:thumbnailParent];
                    UIImage *thumbImage = [thumbnailParentImage thumbnailImage:100.f transparentBorder:0.f cornerRadius:1.5f interpolationQuality:kCGInterpolationLow];
                    NSData *imageData = UIImageJPEGRepresentation(thumbImage,0.8);
                    [imageData writeToFile:thumbnailPath atomically:YES];
                    audioDataToSave.thumbnailPath = [NSString stringWithFormat:@"%@/%@/%@/%@",ENGLISH,museumDir,kGuidesGallery,[NSString stringWithFormat:@"T_%@.%@",audioGuideXMLInfo.referenceNo,kThumbExtension]];
                }
            }
            else if([[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"%@/%@",guidesGalleryDir,[NSString stringWithFormat:@"%@-01.%@",audioGuideXMLInfo.referenceNo,@"JPG"]]])
            {
                @autoreleasepool {
                    UIImage *thumbnailParentImage = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",guidesGalleryDir,[NSString stringWithFormat:@"%@-01.%@",audioGuideXMLInfo.referenceNo,@"JPG"]]];
                    UIImage *thumbImage = [thumbnailParentImage thumbnailImage:100.f transparentBorder:0.f cornerRadius:1.5f interpolationQuality:kCGInterpolationLow];
                    NSData *imageData = UIImageJPEGRepresentation(thumbImage,0.8);
                    [imageData writeToFile:[NSString stringWithFormat:@"%@/%@",guidesGalleryDir,[NSString stringWithFormat:@"T_%@.%@",audioGuideXMLInfo.referenceNo,@"JPG"]] atomically:YES];
                    audioDataToSave.thumbnailPath = [NSString stringWithFormat:@"%@/%@/%@/%@",ENGLISH,museumDir,kGuidesGallery,[NSString stringWithFormat:@"T_%@.%@",audioGuideXMLInfo.referenceNo,@"JPG"]];
                }
            }
            else
            {
                audioDataToSave.thumbnailPath = [[NSBundle mainBundle] pathForResource:@"no-thumb-available" ofType:@"jpg"];
            }
        }
        else    {
            audioDataToSave.thumbnailPath = [NSString stringWithFormat:@"%@/%@/%@/%@",ENGLISH,museumDir,kGuidesGallery,[NSString stringWithFormat:@"T_%@.%@",audioGuideXMLInfo.referenceNo,kThumbExtension]];
        }
        
        audioDataToSave.audioGuideToMuseum = fetchedMuseumObjects[0];
    }
    //}
    error = nil;
    if (![context save:&error]) {
        //NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
    
    NSFetchRequest *guideGalleryFetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *guideGalleryEntity = [NSEntityDescription entityForName:@"IBGuideGallery" inManagedObjectContext:context];
    [guideGalleryFetchRequest setEntity:guideGalleryEntity];
    
    error = nil;
    NSArray *existedGuideGallery = [context executeFetchRequest:guideGalleryFetchRequest error:&error];
    if (existedGuideGallery != nil) {
        for (IBGuideGallery *existedGuideGalleryInfo in existedGuideGallery) {
            [context deleteObject:existedGuideGalleryInfo];
        }
        
        if (![context save:&error]) {
            //NSLog(@"Failed to save - error: %@", [error localizedDescription]);
        }
    }
    
    NSArray *filesArray = [IBGlobal loadFilesAtDirectory:guidesGalleryDir];
    NSMutableArray *roomsArray = [NSMutableArray array];
    
    IBGuideGallery *guidesGalleryToSave = nil;
    existedAudioGuides = [context executeFetchRequest:fetchRequest error:&error];
    
    // P and G based Sortting - Sojan 27-06-2017
    if(locationNo==2)
    {
        NSMutableArray * g_AudioGuideArray = [[NSMutableArray alloc]init];
        NSMutableArray * p_AudioGuideArray = [[NSMutableArray alloc]init];
        NSMutableArray * otherAudioGuideArray = [[NSMutableArray alloc]init];
        
        for(IBAudioGuide *audioGuide in existedAudioGuides)
        {
            NSString *RN_Character = [audioGuide.referenceNo stringByTrimmingCharactersInSet:[[NSCharacterSet uppercaseLetterCharacterSet]invertedSet]];
            if ([RN_Character isEqualToString:@"G"]) {
                [g_AudioGuideArray addObject:audioGuide];
            }else if ([RN_Character isEqualToString:@"P"]) {
                [p_AudioGuideArray addObject:audioGuide];
            }else{
                [otherAudioGuideArray addObject:audioGuide];
            }
        }
        [otherAudioGuideArray addObjectsFromArray:p_AudioGuideArray];
        [otherAudioGuideArray addObjectsFromArray:g_AudioGuideArray];
        existedAudioGuides = otherAudioGuideArray;
    }
    
    for(IBAudioGuide *audioGuide in existedAudioGuides)
    {
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF beginswith[c] %@",[NSString stringWithFormat:@"%@-",audioGuide.referenceNo]];
        NSArray *filteredArray = [filesArray filteredArrayUsingPredicate:predicate];
        
        for(NSString *stringName in filteredArray)  {
            guidesGalleryToSave = [NSEntityDescription
                                   insertNewObjectForEntityForName:@"IBGuideGallery"
                                   inManagedObjectContext:context];
            guidesGalleryToSave.guideGalleryFilePath = [NSString stringWithFormat:@"%@/%@/%@/%@",ENGLISH,museumDir,kGuidesGallery,stringName];
            guidesGalleryToSave.guideGalleryToAudioGuide = audioGuide;
        }
        
        if([filteredArray count]>0) {
            
            
            [roomsArray addObject:audioGuide.referenceNo];
        }
    }
    
    error = nil;
    if (![context save:&error]) {
        //NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
    
    NSFetchRequest *roomAudioGuidesRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *roomAudioGuidesEntity = [NSEntityDescription entityForName:@"IBRoomAudioGuides" inManagedObjectContext:context];
    [roomAudioGuidesRequest setEntity:roomAudioGuidesEntity];
    
    error = nil;
    NSArray *existedRoomAudioGuides = [context executeFetchRequest:roomAudioGuidesRequest error:&error];
    if (existedRoomAudioGuides != nil) {
        for (IBRoomAudioGuides *existedRoomAudioGuideInfo in existedRoomAudioGuides) {
            [context deleteObject:existedRoomAudioGuideInfo];
        }
        
        if (![context save:&error]) {
            //NSLog(@"Failed to save - error: %@", [error localizedDescription]);
        }
    }
    
    IBRoomAudioGuides *roomAudioGuides = nil;
    NSInteger roomCount = [self countNoOfRooms];
    for(int i=1; i<=roomCount; i++) {
        NSString *refString = [NSString stringWithFormat:@"(^(%03d)[A-Z]-.*)|(^(%03d)[A-Z][a-z]-.*)|(^(%03d)[a-z]-.*)|(^(%03d-).*)",i,i,i,i];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",refString];
        NSArray *filteredArray = [filesArray filteredArrayUsingPredicate:predicate];
        NSMutableArray *arrayReferenceNo = [NSMutableArray array];
        for(NSString *stringName in filteredArray) {
            NSRange splitRange = [stringName rangeOfString:@"-"];
            NSString *referenceNo = [stringName substringToIndex:splitRange.location];
            if(![arrayReferenceNo containsObject:referenceNo]) { roomAudioGuides = [NSEntityDescription insertNewObjectForEntityForName:@"IBRoomAudioGuides" inManagedObjectContext:context];
                
                // roomAudioGuides.mapRoomNo = [NSNumber numberWithInt:i];
                
                NSString *referenceNoCharacter = [referenceNo stringByTrimmingCharactersInSet:[[NSCharacterSet uppercaseLetterCharacterSet]invertedSet]];
                
                roomAudioGuides.mapRoomNo = [NSString stringWithFormat:@"%d%@",[referenceNo intValue],referenceNoCharacter];
                roomAudioGuides.audioGuideReferenceNo = referenceNo;
                
                //NSLog(@"%@%@",roomAudioGuides.mapRoomNo,roomAudioGuides.audioGuideReferenceNo);
                [arrayReferenceNo addObject:referenceNo];
            }
        }
    }
    
    error = nil;
    if (![context save:&error]) {
        //NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
    
    return existedAudioGuides;
}

- (NSArray *)loadAudioGuideWithLocationNo:(NSInteger)locationNo andLanguageIndex:(NSInteger)languageIndex forceDownload:(BOOL)status
{
    if(status == YES)   {
        NSString *urlString,*museumDir,*languageDir;
        
        DIRLOCATION_STR(locationNo,museumDir);
        DIRLANGUAGE_STR(languageIndex,languageDir);
        
        urlString = [NSString stringWithFormat:@"%@/%@/%@/%@/%@",SYNC_URL,IBParentDirectory,museumDir,languageDir,kAudioGuideXml];
        NSString *key = [self keyWithURLString:urlString];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setValue:@"0" forKey:key];
        [defaults synchronize];
    }
    
    return [self loadAudioGuideWithLocationNo:locationNo andLanguageIndex:languageIndex forceDownload:status];
}

- (NSInteger)countNoOfRooms
{
    NSManagedObjectContext *context = [SharedAppDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"IBGeoMapRooms" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setPropertiesToFetch:[NSArray arrayWithObjects:@"roomNo", nil]];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"roomNo" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    
    fetchRequest.resultType = NSDictionaryResultType;
    
    NSError *error = nil;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        //NSLog(@"Failed to load - error: %@", [error localizedDescription]);
    }
    
    return [fetchedObjects count];
}

- (NSInteger)countNoOfMapSections
{
    NSManagedObjectContext *context = [SharedAppDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"IBGeoMapRooms" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setPropertiesToFetch:[NSArray arrayWithObjects:@"sectionNo", nil]];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sectionNo" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    
    fetchRequest.resultType = NSDictionaryResultType;
    
    NSError *error = nil;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        //NSLog(@"Failed to load - error: %@", [error localizedDescription]);
    }
    
    NSMutableArray *numArray = [NSMutableArray array];
    for(NSDictionary *dictionary in fetchedObjects) {
        NSNumber *number = [NSNumber numberWithInteger:[[dictionary valueForKey:@"sectionNo"] integerValue]];
        
        if(![numArray containsObject:number])   {
            [numArray addObject:number];
        }
    }
    
    return [numArray count];
}

- (NSInteger)countPurchase
{
    NSManagedObjectContext *context = [SharedAppDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"IBMuseums" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setPropertiesToFetch:[NSArray arrayWithObjects:@"guidePurchased", nil]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"guidePurchased == 1"];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        //NSLog(@"Failed to load - error: %@", [error localizedDescription]);
    }
    
    return [fetchedObjects count];
}

- (NSArray *)fetchLanguageIndexes
{
    NSManagedObjectContext *context = [SharedAppDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"IBLanguages" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    fetchRequest.resultType = NSDictionaryResultType;
    
    [fetchRequest setPropertiesToFetch:[NSArray arrayWithObjects:@"displayName", @"resourcePath", @"selected", nil]];
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"order" ascending:YES];
    [fetchRequest setSortDescriptors:@[sort]];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        //NSLog(@"Failed to load - error: %@", [error localizedDescription]);
    }
    
    return fetchedObjects;
}

- (NSArray *)fetchLocationIndexes
{
    NSManagedObjectContext *context = [SharedAppDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"IBMuseums" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    fetchRequest.resultType = NSDictionaryResultType;
    
    [fetchRequest setPropertiesToFetch:[NSArray arrayWithObjects:@"museumName", @"museumDescription", nil]];
    
    NSSortDescriptor *sortDisplayName = [[NSSortDescriptor alloc] initWithKey:@"museumName" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDisplayName]];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        //NSLog(@"Failed to load - error: %@", [error localizedDescription]);
    }
    
    return fetchedObjects;
}

- (NSArray *)fetchGuideGalleryByAudioReferenceIndex:(NSString *)referenceNo
{
    NSManagedObjectContext *context = [SharedAppDelegate managedObjectContext];
    
    NSFetchRequest *fetchAudioGuideRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *audioGuideEntity = [NSEntityDescription entityForName:@"IBAudioGuide" inManagedObjectContext:context];
    [fetchAudioGuideRequest setEntity:audioGuideEntity];
    
    NSPredicate *roomAudioGuidePredicate = [NSPredicate predicateWithFormat:@"referenceNo = %@",referenceNo];
    [fetchAudioGuideRequest setPredicate:roomAudioGuidePredicate];
    
    NSError *error = nil;
    NSArray *fetchedRoomAudioGuideObjects = [context executeFetchRequest:fetchAudioGuideRequest error:&error];
    if (fetchedRoomAudioGuideObjects == nil) {
        //NSLog(@"Failed to load - error: %@", [error localizedDescription]);
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"IBGuideGallery" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setPropertiesToFetch:[NSArray arrayWithObjects:@"guideGalleryFilePath", nil]];
    
    NSSortDescriptor *sortDisplayName = [[NSSortDescriptor alloc] initWithKey:@"guideGalleryFilePath" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDisplayName]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"guideGalleryToAudioGuide = %@",fetchedRoomAudioGuideObjects[0]];
    [fetchRequest setPredicate:predicate];
    
    error = nil;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        //NSLog(@"Failed to load - error: %@", [error localizedDescription]);
    }
    
    return fetchedObjects;
}
//-(NSArray *)fetchWholeCircleCoordinatesWithIndex:(NSString *)index
//{
//      NSManagedObjectContext *context = [SharedAppDelegate managedObjectContext];
//    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
//    NSEntityDescription *entity = [NSEntityDescription entityForName:@"IBGeoMapRooms" inManagedObjectContext:context];
//    [fetchRequest setEntity:entity];
//    [fetchRequest setPropertiesToFetch:[NSArray arrayWithObjects:@"roomNo", @"circleCoordinates", nil]];
//
//
//
//}
- (NSArray *)fetchGeoMapRoomsBySessionIndex:(NSInteger)index
{
    NSManagedObjectContext *context = [SharedAppDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"IBGeoMapRooms" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setPropertiesToFetch:[NSArray arrayWithObjects:@"roomNo", @"coordinates", nil]];
    
    NSSortDescriptor *sortDisplayName = [[NSSortDescriptor alloc] initWithKey:@"roomNo" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDisplayName]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sectionNo == %@",[NSNumber numberWithInteger:index]];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        //NSLog(@"Failed to load - error: %@", [error localizedDescription]);
    }
    
    return fetchedObjects;
}

- (NSArray *)fetchAudioGuideByReferenceNo:(NSString *)referenceNo
{
    NSManagedObjectContext *context = [SharedAppDelegate managedObjectContext];
    NSFetchRequest *fetchRoomAudioGuideRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *roomAudioGuideEntity = [NSEntityDescription entityForName:@"IBAudioGuide" inManagedObjectContext:context];
    [fetchRoomAudioGuideRequest setEntity:roomAudioGuideEntity];
    NSPredicate *roomAudioGuidePredicate = [NSPredicate predicateWithFormat:@"referenceNo LIKE[c] %@",referenceNo];
    [fetchRoomAudioGuideRequest setPredicate:roomAudioGuidePredicate];
    NSError *error = nil;
    NSArray *fetchedAudioGuideDetails = [context executeFetchRequest:fetchRoomAudioGuideRequest error:&error];
    
    if (fetchedAudioGuideDetails == nil) {
        //NSLog(@"Failed to load - error: %@", [error localizedDescription]);
    }
    
    if([fetchedAudioGuideDetails count]>0) {
        NSMutableArray *arrayAudioGuideReferenceIndices = [NSMutableArray array];
        for(IBAudioGuide *roomAudioGuides in fetchedAudioGuideDetails) {
            if(![arrayAudioGuideReferenceIndices containsObject:roomAudioGuides.guideTitle])  {
                [arrayAudioGuideReferenceIndices addObject:roomAudioGuides.referenceNo];
            }
        }
    }
    
    return fetchedAudioGuideDetails;
}

- (NSArray *)fetchAudioGuidesByRoomNo:(NSString *)roomNo
{
    NSManagedObjectContext *context = [SharedAppDelegate managedObjectContext];
    
    NSFetchRequest *fetchRoomAudioGuideRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *roomAudioGuideEntity = [NSEntityDescription entityForName:@"IBRoomAudioGuides" inManagedObjectContext:context];
    [fetchRoomAudioGuideRequest setEntity:roomAudioGuideEntity];
    
    [fetchRoomAudioGuideRequest setPropertiesToFetch:[NSArray arrayWithObjects:@"audioGuideReferenceNo", nil]];
    
    NSPredicate *roomAudioGuidePredicate = [NSPredicate predicateWithFormat:@"mapRoomNo = %@",roomNo];
    [fetchRoomAudioGuideRequest setPredicate:roomAudioGuidePredicate];
    
    NSError *error = nil;
    NSArray *fetchedRoomAudioGuideObjects = [context executeFetchRequest:fetchRoomAudioGuideRequest error:&error];
    if (fetchedRoomAudioGuideObjects == nil) {
        //NSLog(@"Failed to load - error: %@", [error localizedDescription]);
    }
    
    if([fetchedRoomAudioGuideObjects count]>0) {
        NSMutableArray *arrayAudioGuideReferenceIndices = [NSMutableArray array];
        for(IBRoomAudioGuides *roomAudioGuides in fetchedRoomAudioGuideObjects) {
            if(![arrayAudioGuideReferenceIndices containsObject:roomAudioGuides.audioGuideReferenceNo])  {
                [arrayAudioGuideReferenceIndices addObject:roomAudioGuides.audioGuideReferenceNo];
            }
        }
        
        NSFetchRequest *fetchAudioGuideRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *audioGuideEntity = [NSEntityDescription entityForName:@"IBAudioGuide" inManagedObjectContext:context];
        [fetchAudioGuideRequest setEntity:audioGuideEntity];
        
        NSSortDescriptor *sortAudioGuide = [[NSSortDescriptor alloc] initWithKey:@"referenceNo" ascending:YES];
        [fetchAudioGuideRequest setSortDescriptors:@[sortAudioGuide]];
        
        NSPredicate *audioGuidePredicate;
        if([arrayAudioGuideReferenceIndices count]>1)   {
            audioGuidePredicate = [NSPredicate predicateWithFormat:@"referenceNo IN %@",arrayAudioGuideReferenceIndices];
        }
        else    {
            audioGuidePredicate = [NSPredicate predicateWithFormat:@"referenceNo = %@",arrayAudioGuideReferenceIndices[0]];
        }
        
        [fetchAudioGuideRequest setPredicate:audioGuidePredicate];
        
        error = nil;
        NSArray *fetchedAudioGuideObjects = [context executeFetchRequest:fetchAudioGuideRequest error:&error];
        if (fetchedAudioGuideObjects == nil) {
            //NSLog(@"Failed to load - error: %@", [error localizedDescription]);
        }
        
        return fetchedAudioGuideObjects;
    }
    else    {
        return [NSArray array];
    }
}


- (NSArray *)fetchRoomsByAudioGuideReferenceNo:(NSString *)referenceNo
{
    NSManagedObjectContext *context = [SharedAppDelegate managedObjectContext];
    NSFetchRequest *fetchRoomAudioGuideRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *roomAudioGuideEntity = [NSEntityDescription entityForName:@"IBRoomAudioGuides" inManagedObjectContext:context];
    [fetchRoomAudioGuideRequest setEntity:roomAudioGuideEntity];
    
    [fetchRoomAudioGuideRequest setPropertiesToFetch:[NSArray arrayWithObjects:@"mapRoomNo", nil]];
    
    NSPredicate *roomAudioGuidePredicate = [NSPredicate predicateWithFormat:@"audioGuideReferenceNo == %@",referenceNo];
    [fetchRoomAudioGuideRequest setPredicate:roomAudioGuidePredicate];
    
    NSError *error = nil;
    NSArray *fetchedRoomAudioGuideObjects = [context executeFetchRequest:fetchRoomAudioGuideRequest error:&error];
    if (fetchedRoomAudioGuideObjects == nil) {
        //NSLog(@"Failed to load - error: %@", [error localizedDescription]);
    }
    
    if([fetchedRoomAudioGuideObjects count]>0)
    {
        NSMutableArray *arrayMapRoomReferenceIndices = [NSMutableArray array];
        for(IBRoomAudioGuides *roomAudioGuide in fetchedRoomAudioGuideObjects)  {
            if(![arrayMapRoomReferenceIndices containsObject:roomAudioGuide.mapRoomNo])  {
                //NSLog(@"%@",roomAudioGuide.mapRoomNo);
                [arrayMapRoomReferenceIndices addObject:roomAudioGuide.mapRoomNo];
            }
        }
        
        NSFetchRequest *geoMapRoomFetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *geoMapRoomEntity = [NSEntityDescription entityForName:@"IBGeoMapRooms" inManagedObjectContext:context];
        [geoMapRoomFetchRequest setEntity:geoMapRoomEntity];
        
        NSPredicate *geoMapRoomPredicate;
        if([arrayMapRoomReferenceIndices count]>1)   {
            geoMapRoomPredicate = [NSPredicate predicateWithFormat:@"roomNo IN %@",arrayMapRoomReferenceIndices];
        }
        else    {
            geoMapRoomPredicate = [NSPredicate predicateWithFormat:@"roomNo = %@",arrayMapRoomReferenceIndices[0]];
        }
        
        [geoMapRoomFetchRequest setPredicate:geoMapRoomPredicate];
        
        error = nil;
        NSArray *fetchedGeoMapRoomObjects = [context executeFetchRequest:geoMapRoomFetchRequest error:&error];
        if (fetchedGeoMapRoomObjects == nil) {
            //NSLog(@"Failed to load - error: %@", [error localizedDescription]);
        }
        
        return fetchedGeoMapRoomObjects;
    }
    else    {
        return [NSArray array ];
    }
}

- (void)setLanguageSelectedByDisplayName:(NSString *)displayName
{
    NSManagedObjectContext *context = [SharedAppDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"IBLanguages" inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        //NSLog(@"Failed to load - error: %@", [error localizedDescription]);
    }
    else    {
        for(IBLanguages *language in fetchedObjects)    {
            if([language.displayName isEqualToString:displayName])  {
                language.selected = [NSNumber numberWithBool:YES];
            }
            else    {
                language.selected = [NSNumber numberWithBool:NO];
            }
        }
    }
    
    if (![context save:&error]) {
        //NSLog(@"Failed to save - error: %@", [error localizedDescription]);
    }
}

- (BOOL)updatePurchaseOfLocationNo:(NSInteger)locationNo withStatus:(BOOL)status
{
    NSString *museumName = nil;
    if(locationNo==1){museumName=@"Isola Bella";}else if(locationNo==2){museumName=@"Isola Madre";}else{museumName=@"Rocca d’Angera";}
    
    NSManagedObjectContext *context = [SharedAppDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"IBMuseums" inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"museumName == %@",museumName];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        //NSLog(@"Failed to load - error: %@", [error localizedDescription]);
    }
    else    {
        for(IBMuseums *museum in fetchedObjects)    {
            [museum setGuidePurchased:[NSNumber numberWithBool:status]];
        }
    }
    
    if (![context save:&error]) {
        //NSLog(@"Failed to save - error: %@", [error localizedDescription]);
        
        return NO;
    }
    
    return YES;
}

- (BOOL)isPurchasedForLocationNo:(NSInteger)locationNo
{
    NSString *museumName = nil;
    if(locationNo==1){museumName=@"Isola Bella";}else if(locationNo==2){museumName=@"Isola Madre";}else{museumName=@"Rocca d’Angera";}
    
    NSManagedObjectContext *context = [SharedAppDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"IBMuseums" inManagedObjectContext:context];
    
    [fetchRequest setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"museumName == %@",museumName];
    [fetchRequest setPredicate:predicate];
    
    NSError *error = nil;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    if (fetchedObjects == nil) {
        //NSLog(@"Failed to load - error: %@", [error localizedDescription]);
    }
    else    {
        BOOL isPurchased = NO;
        for(IBMuseums *museum in fetchedObjects)    {
            isPurchased = [[museum guidePurchased] boolValue];
            break;
        }
        
        return isPurchased;
    }
    
    return NO;
}

- (NSString *)keyWithURLString:(NSString *)urlString
{
    NSString *keyString = [[[urlString stringByReplacingOccurrencesOfString:SYNC_URL withString:@""] base64EncodedString] uppercaseString];
    
    NSMutableString *newString = [NSMutableString string];
    for (int i = 0; i < [keyString length]; i++)
    {
        int ascii = [keyString characterAtIndex:i];
        if (((ascii>64) && (ascii<91)) || ((ascii>47) && (ascii<58)))
        {
            [newString appendFormat:@"%c",ascii];
        }
    }
    
    return [NSString stringWithString:newString];
}

@end
