//
//  IBDetailViewController.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 06/08/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IBDetailViewControllerDelegate
@required
- (void)downloadOnProgressSignal;
- (void)detailsLoadedSuccess;
- (void)downloadingStated;
- (void)downloadingStopped;
@end

@interface IBDetailViewController : UIViewController

@property (nonatomic, assign) id <IBDetailViewControllerDelegate> delegate;

@end
