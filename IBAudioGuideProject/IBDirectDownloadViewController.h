//
//  IBDirectDownloadViewController.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 27/08/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IBDownloadButtonViewTablet.h"

typedef enum {
    AudioGuide,
    FreeGallery,
    GardenGuide,
}DownloadItem;

@protocol IBDirectDownloadViewControllerDelegate
@required
- (void)downloadStarted;
- (void)downloadFinished;
- (void)downloadCancelled;
- (void)downloadFailed;
@end

@interface IBDirectDownloadViewController : UIViewController
{
    Status _status;
    NSString *_heading;
    NSString *_description;
    NSString *_urlString;
    NSString *_destinationDir;
}
@property (nonatomic, assign) id <IBDirectDownloadViewControllerDelegate> delegate;

@property (nonatomic, assign) Status status;
@property (nonatomic,assign) DownloadItem item;
@property (strong, nonatomic) NSString *heading;
@property (strong, nonatomic) NSString *description;
@property (nonatomic,strong) NSString *urlString;
@property (nonatomic,strong) NSString *destinationDir;

- (void)refreshView;
- (void)forceDownload;

@end
