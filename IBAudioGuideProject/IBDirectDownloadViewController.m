//
//  IBDirectDownloadViewController.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 27/08/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBDirectDownloadViewController.h"

#import "Base64.h"
#import "IBGlobal.h"
#import "ZipArchive.h"
#import "ASIHTTPRequest.h"
#import "IBActivityIndicator.h"
#import "IBCircularProgressView.h"
#import "NSString+SSToolkitAdditions.h"

@interface IBDirectDownloadViewController ()<IBDownloadButtonViewTabletDelegate>

@property (weak, nonatomic) IBOutlet UILabel *labelHeader;
@property (weak, nonatomic) IBOutlet UILabel *labelDescription;
@property (weak, nonatomic) IBOutlet UIView *descriptionContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descriptionContainerHeightConstraint;
@property (weak, nonatomic) IBOutlet IBDownloadButtonViewTablet *downloadButton;

@end

@implementation IBDirectDownloadViewController

@synthesize delegate = _delegate;
@synthesize heading = _heading;
@synthesize description = _description;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.labelHeader.font = kAppFontSemibold(30);
    self.labelHeader.textColor = RGBCOLOR(104, 144, 152);
    self.labelHeader.backgroundColor = [UIColor whiteColor];
    
    [self.labelHeader sizeToFit];
    
    self.labelDescription.font = kAppFontRegular(15);
    self.labelDescription.textColor = RGBCOLOR(141, 141, 141);
    self.labelDescription.backgroundColor = [UIColor whiteColor];
    self.labelDescription.numberOfLines = 0;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self refreshView];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [SharedAppDelegate unlockInteractions];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if(self.status == StatusDownloading)  {
        [self.downloadButton cancelDownload];
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)setHeading:(NSString *)heading
{
    _heading = heading;
}

- (void)setDescription:(NSString *)description
{
    _description = description;
}

- (void)setUrlString:(NSString *)urlString
{
    _urlString = urlString;
}

- (void)setDestinationDir:(NSString *)destinationDir
{
    BOOL isDir = YES;
    if (![[NSFileManager defaultManager] fileExistsAtPath:destinationDir isDirectory:&isDir])  {
        [[NSFileManager defaultManager] createDirectoryAtPath:destinationDir withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    if(![destinationDir hasSuffix:@"/"])   {
        _destinationDir = [NSString stringWithFormat:@"%@/",destinationDir];
    }
    else    {
        _destinationDir = [NSString stringWithString:destinationDir];
    }
}

- (Status)status
{
    return self.downloadButton.status;
}

- (void)refreshView
{
    self.labelHeader.text = self.heading;
    self.labelDescription.text = self.description;
    
    CGFloat height = [IBGlobal heightFromString:self.description withFont:self.labelDescription.font constraintToWidth:self.descriptionContainer.frame.size.width];
    self.descriptionContainerHeightConstraint.constant = height;
    
    self.downloadButton.delegate = self;
    
    self.downloadButton.urlString = _urlString;
    self.downloadButton.destinationDir = _destinationDir;
   
    if(_item==FreeGallery)    {
        [self.downloadButton.labelTitle setText:[[[SharedAppDelegate langDictionary] valueForKey:@"LabelPhotoGallery"] stringByReplacingOccurrencesOfString:@" " withString:@""]];
    }
    else if (_item==AudioGuide) {
        [self.downloadButton.labelTitle setText:[[[SharedAppDelegate langDictionary] valueForKey:@"LabelAudioGuide"] stringByReplacingOccurrencesOfString:@" " withString:@""]];
    }
    else    {
        
    }
}

- (void)forceDownload
{
    [self.downloadButton startDownloadWithProgress];
}

#pragma mark-
#pragma IBDownloadButtonViewTabletDelegate methods

- (void)downloadStarted
{
    NSLog(@"_delegate_delegate = %@", _delegate);
    [_delegate downloadCancelled];
}

- (void)downloadFinished
{
    [_delegate downloadFinished];
}

- (void)downloadCancelled
{
    [_delegate downloadCancelled];
}

- (void)downloadFailed
{
    [_delegate downloadFailed];
}

#pragma mark-

@end
