//
//  IBDownloadButton.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 28/08/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IBZipDownloadButton.h"

@protocol IBDownloadButtonViewTabletDelegate
@required
- (void)downloadStarted;
- (void)downloadFinished;
- (void)downloadCancelled;
- (void)downloadFailed;
@end

@interface IBDownloadButtonViewTablet : UIView
{
    BOOL _locked;
    Status _status;
    NSString *_urlString;
    NSString *_destinationDir;
}

@property (nonatomic, assign) id <IBDownloadButtonViewTabletDelegate> delegate;

@property (nonatomic,assign) float progress;
@property (nonatomic,readonly) Status status;
@property (nonatomic,assign) BOOL locked;
@property (nonatomic,strong) NSString *urlString;
@property (nonatomic,strong) NSString *destinationDir;
@property (nonatomic,strong,readonly) UILabel *labelTitle;

- (void)setProgress:(float)progress animated:(BOOL)animated;

- (void)startDownloadWithProgress;
- (void)cancelDownload;

@end
