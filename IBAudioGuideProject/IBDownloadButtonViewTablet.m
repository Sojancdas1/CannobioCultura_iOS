//
//  IBDownloadButton.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 28/08/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBDownloadButtonViewTablet.h"

#import "Base64.h"
#import "IBGlobal.h"
#import "ZipArchive.h"
#import "ASIHTTPRequest.h"
#import "IBCircularProgressView.h"
#import "NSString+SSToolkitAdditions.h"
#import "UIView+Toast.h"
#import "HUDManager.h"
#import "IBTypes.h"

#define kButtonHeight 35
#define kPaddingSpace 30



@interface IBDownloadButtonViewTablet ()
{
    BOOL _isRunning;
    UIButton *_buttonZipDownload;
    UILabel *_labelProgress, *_labelStatus, *_labelTitle;
    IBCircularProgressView *_progressView;
    
    ASIHTTPRequest *_asiRequest;
    long long receivedBytes,contentLength;
}

@property (nonatomic,assign) BOOL isRunning;
@property (nonatomic,strong) UIButton *buttonZipDownload;
@property (nonatomic,strong) UILabel *labelProgress;
@property (nonatomic,strong) UILabel *labelStatus;
@property (nonatomic,strong) IBCircularProgressView *progressView;

- (void)initialize;
- (void)downloadButtonTouched;
- (void)setButtonExpanded:(BOOL)status;
- (void)updateProgressLabel;
- (NSString *)key;
- (void)startDownload;

@end

@implementation IBDownloadButtonViewTablet

@synthesize delegate = _delegate;

@synthesize progress = _progress;
@synthesize status = _status;
@synthesize urlString = _urlString;
@synthesize destinationDir = _destinationDir;

@synthesize locked = _locked;
@synthesize isRunning = _isRunning;
@synthesize buttonZipDownload = _buttonZipDownload;
@synthesize labelProgress = _labelProgress;
@synthesize labelStatus = _labelStatus;
@synthesize labelTitle = _labelTitle;
@synthesize progressView = _progressView;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code
        [self initialize];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder {
    if (self = [super initWithCoder:coder]) {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    self.clipsToBounds = NO;
    self.backgroundColor = [UIColor whiteColor];
    
    _buttonZipDownload = [[UIButton alloc] initWithFrame:CGRectMake(0.f, 0.f, self.frame.size.width, kButtonHeight)];
    [_buttonZipDownload setTitleColor:RGBCOLOR(104, 144, 152) forState:UIControlStateNormal];
    [_buttonZipDownload.titleLabel setFont:kAppFontSemibold(15)];
    _buttonZipDownload.layer.backgroundColor = [UIColor whiteColor].CGColor;
    _buttonZipDownload.layer.borderColor = RGBCOLOR(104, 144, 152).CGColor;
    _buttonZipDownload.layer.borderWidth = 2.f;
    _buttonZipDownload.layer.cornerRadius = 5.f;
    
    [_buttonZipDownload setTitle:[[SharedAppDelegate langDictionary] valueForKey:@"LabelDownload"] forState:UIControlStateNormal];
    [self addSubview:_buttonZipDownload];
    
    [_buttonZipDownload addTarget:self action:@selector(downloadButtonTouched) forControlEvents:UIControlEventTouchUpInside];
    
    _progressView = [[IBCircularProgressView alloc] initWithFrame:CGRectMake(0.f, 0.f, self.frame.size.width, self.frame.size.width)];
    [self addSubview:_progressView];
    
    [_progressView setShowProgressCentre:NO];
    
    _labelProgress = [[UILabel alloc] initWithFrame:CGRectMake(17.5f, 40.f, self.frame.size.width-35.f, 75.f)];
    _labelProgress.backgroundColor = [UIColor clearColor];
    _labelProgress.textAlignment = NSTextAlignmentCenter;
    
    _progress = 0.0;
    [self updateProgressLabel];
    
    _labelStatus = [[UILabel alloc] initWithFrame:CGRectMake(27.5f, _labelProgress.frame.origin.y+_labelProgress.frame.size.height-4, self.frame.size.width-55.f, 25.f)];
    _labelStatus.font = kAppFontSemibold(18);
    _labelStatus.textColor = RGBCOLOR(141, 141, 141);
    _labelStatus.backgroundColor = [UIColor clearColor];
    _labelStatus.textAlignment = NSTextAlignmentCenter;
    _labelStatus.text = [[SharedAppDelegate langDictionary] valueForKey:@"LabelWaiting"];
    
    _labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(27.5f, _labelStatus.frame.origin.y+_labelStatus.frame.size.height-8, self.frame.size.width-55.f, 25.f)];
    _labelTitle.font = kAppFontSemibold(10);
    _labelTitle.textColor = RGBCOLOR(141, 141, 141);
    _labelTitle.backgroundColor = [UIColor clearColor];
    _labelTitle.textAlignment = NSTextAlignmentCenter;
    _labelTitle.text = [[SharedAppDelegate langDictionary] valueForKey:@"LabelAudioguide"];
    
    [_progressView addSubview:_labelProgress];
    [_progressView addSubview:_labelStatus];
    [_progressView addSubview:_labelTitle];
    
    _isRunning = NO;
    _progressView.alpha = 0.f;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)startDownloadWithProgress
{
    [self setButtonExpanded:YES];
    _isRunning = YES;
}

- (void)startDownload
{
    _status = StatusDownloading;
    [_delegate downloadStarted];
    
    if(_asiRequest) {
        _asiRequest.delegate = nil;
        _asiRequest.downloadProgressDelegate = nil;
        _asiRequest = nil;
    }
    
    NSURL *url = [NSURL URLWithString:_urlString];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setTimeOutSeconds:360];
    [request setDelegate:self];
    [request setDownloadProgressDelegate:self];
    [request setDownloadDestinationPath:[NSString stringWithFormat:@"%@%@",_destinationDir,[_urlString lastPathComponent]]];
    [request setTemporaryFileDownloadPath:[NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"loc%ld-%@.download",(long)[SharedAppDelegate locationNo],[_urlString lastPathComponent]]]];
    [request setAllowResumeForFileDownloads:YES];
    [request startAsynchronous];
    
    _labelStatus.text = [[SharedAppDelegate langDictionary] valueForKey:@"LabelWaiting"];
    _asiRequest = request;
}

- (void)cancelDownload
{
    if(_status == StatusDownloading) {
        //
        [_asiRequest cancel];
        [_asiRequest setDelegate:nil];
        [_asiRequest setDownloadProgressDelegate:nil];
        //
        receivedBytes = 0;
        contentLength = 0;
        [self setProgress:0.f];
        //
        _labelStatus.text = [[SharedAppDelegate langDictionary] valueForKey:@"LabelWaiting"];
        [self updateProgressLabel];
        [_delegate downloadCancelled];
    }
}

#pragma mark-
#pragma ASIHTTPRequest methods

- (void)requestStarted:(ASIHTTPRequest *)request {
    //
    _progress = 0.0;
    [self setProgress:0.f];
    _status = StatusDownloading;
    _labelStatus.text = [[SharedAppDelegate langDictionary] valueForKey:@"LabelDownloading"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:[NSString stringWithFormat:@"%d",self.status] forKey:self.key];
    [defaults synchronize];
}

- (void)requestFinished:(ASIHTTPRequest *)request {
    //
    NSLog(@"ewrewrererererer = %@", [NSString stringWithFormat:@"%@%@",_destinationDir, [_urlString lastPathComponent]]);
    NSError *error = nil;
    //
    [self setProgress:1.0f animated:YES];
    
    ZipArchive *zipArchive = [[ZipArchive alloc] init];
    [zipArchive UnzipOpenFile:[NSString stringWithFormat:@"%@%@",_destinationDir, [_urlString lastPathComponent]]];
    if([zipArchive UnzipFileTo:_destinationDir overWrite:YES]) _status = StatusDownloaded;
    [zipArchive UnzipCloseFile];
    //
    [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@%@",_destinationDir,[_urlString lastPathComponent]] error:&error];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:[NSString stringWithFormat:@"%d",self.status] forKey:self.key];
    [defaults synchronize];
    //
    if(self.status == StatusDownloaded) {
        //
        [_delegate downloadFinished];
        _labelStatus.text = [[SharedAppDelegate langDictionary] valueForKey:@"LabelDownloaded"];
        [_buttonZipDownload setTitle:[[SharedAppDelegate langDictionary] valueForKey:@"LabelFinish"] forState:UIControlStateNormal];
        [self setLocked:NO];
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request {
    //
    //NSLog(@"request.failed.reason = %@", request.error);
    _progress = 0.0;
    [self setProgress:0.f];
    _status = StatusFailed;
    
    _labelStatus.text = [[SharedAppDelegate langDictionary] valueForKey:@"LabelWaiting"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:[NSString stringWithFormat:@"%d",self.status] forKey:self.key];
    [defaults synchronize];
    
    [self setProgress:0.f];
    [self updateProgressLabel];
    
    [_delegate downloadFailed];
    
    [self setLocked:NO];
    [self setButtonExpanded:NO];
}

- (void)request:(ASIHTTPRequest *)request didReceiveBytes:(long long)bytes {
    //
    [HUDManager hideHUDFromWindow];
    if(bytes > 0) {
        receivedBytes = receivedBytes+bytes;
        float percentage = (float)receivedBytes/(float)contentLength;
        [self setProgress:percentage animated:YES];
    }
}

- (void)request:(ASIHTTPRequest *)request incrementDownloadSizeBy:(long long)newLength
{
    if(newLength > 0) {
        //
        _progress = 0.0;
        [self setProgress:0.f];
        contentLength = newLength;
    }
    //
    _labelStatus.text = [[SharedAppDelegate langDictionary] valueForKey:@"LabelDownloading"]; 
}

#pragma mark-

- (void)downloadButtonTouched
{
    if(self.locked) {
        [[SharedAppDelegate directDownloadController].view makeToast:[SharedAppDelegate lockMessage] duration:3.f position:@"bottom"];
        return;
    }
    if (_isRunning) {
        //
        [self setButtonExpanded:NO];
        _isRunning = NO;
    } else {
        //
        [self setButtonExpanded:YES];
        _isRunning = YES;
    }
}

- (void)setButtonExpanded:(BOOL)status {
    //
    if(status == YES) {
        //
        [SharedAppDelegate lockInteractions];
        const CGRect frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, kButtonHeight+kPaddingSpace+_progressView.frame.size.height);
        [super setFrame:frame];
        
        [UIView animateWithDuration:.5f animations:^{
            //
            _buttonZipDownload.frame = CGRectMake(0, _progressView.frame.size.height+kPaddingSpace, self.frame.size.width, kButtonHeight);
            _buttonZipDownload.layer.borderColor = RGBCOLOR(141, 141, 141).CGColor;
            [_buttonZipDownload setTitleColor:RGBCOLOR(141, 141, 141) forState:UIControlStateNormal];
            [_buttonZipDownload setTitle:[[SharedAppDelegate langDictionary] valueForKey:@"LabelPause"] forState:UIControlStateNormal];
        } completion:^(BOOL finished) {
            //
            [UIView animateWithDuration:1.f animations:^{
                //
                _progressView.alpha = 1.f;
            } completion:^(BOOL finished) {
                //
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self startDownload];
                }); 
            }];
        }];
    } else {
        //
        [self cancelDownload];
        _progressView.alpha = 0.f;
        //
        [UIView animateWithDuration:.5f animations:^{
            //
            _buttonZipDownload.frame = CGRectMake(0.f, 0.f, self.frame.size.width, kButtonHeight);
            _buttonZipDownload.layer.borderColor = RGBCOLOR(104, 144, 152).CGColor;
            [_buttonZipDownload setTitleColor:RGBCOLOR(104, 144, 152) forState:UIControlStateNormal];
            [_buttonZipDownload setTitle:[[SharedAppDelegate langDictionary] valueForKey:@"LabelDownload"] forState:UIControlStateNormal];
        } completion:^(BOOL finished) {
            //
            [super setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, kButtonHeight)];
            [SharedAppDelegate unlockInteractions];
        }];
    }
}

- (void)updateProgressLabel {
    //
    int percentage = (int)(_progress*100);
    if(percentage < 10) {
        //
        [_labelProgress setTextAlignment:NSTextAlignmentCenter];
        [_labelProgress setFrame:CGRectMake(40.f, 40.f, self.frame.size.width-80.f, 75.f)];
    } else {
        //
        [_labelProgress setTextAlignment:NSTextAlignmentRight];
        [_labelProgress setFrame:CGRectMake(17.5f, 40.f, self.frame.size.width-35.f, 75.f)];
    }
    NSString *string = [NSString stringWithFormat:@"%d%%",percentage];
    NSMutableAttributedString * mutableString = [[NSMutableAttributedString alloc] initWithString:string];
    [mutableString addAttribute:NSForegroundColorAttributeName value:RGBCOLOR(141, 141, 141) range:NSMakeRange(0,[string length])];
    [mutableString addAttribute:NSFontAttributeName value:kAppFontBold(65) range:NSMakeRange(0,[string length])];
    [mutableString addAttribute:NSFontAttributeName value:kAppFontSemibold(30) range:NSMakeRange([string length]-1,1)];
    _labelProgress.attributedText = mutableString;
}

- (void)setProgress:(float)progress {
    NSLog(@"progress1 = %f", progress);
   if (progress < _progress) return;
    _progress = progress;
    [_progressView setProgress:progress animated:YES];
    [self updateProgressLabel];
}

- (void)setProgress:(float)progress animated:(BOOL)animated {
  //  NSLog(@"progress2 = %f", progress);
  //  if (progress < _progress) return;
  //  _progress = progress;
    
  //  [_progressView setProgress:progress animated:animated];
  //  [self updateProgressLabel];
}

#pragma mark-

- (NSString *)key {
    //
    NSString *keyString = [[[_urlString stringByReplacingOccurrencesOfString:SYNC_URL withString:@""] base64EncodedString] uppercaseString]; 
    NSMutableString *newString = [NSMutableString string];
    //
    for (int i = 0; i < [keyString length]; i++) {
        //
        int ascii = [keyString characterAtIndex:i];
        if (((ascii>64) && (ascii<91)) || ((ascii>47) && (ascii<58))) {
            [newString appendFormat:@"%c",ascii];
        }
    }
    
    return [NSString stringWithString:newString];
}

@end
 

