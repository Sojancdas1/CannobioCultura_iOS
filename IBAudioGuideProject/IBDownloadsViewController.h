//
//  IBDownloadsViewController.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 20/05/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    IBDownloadsViewControllerUIModeNone,
    IBDownloadsViewControllerUIModeIsolaBella,
    IBDownloadsViewControllerUIModeIsolaMadre,
    IBDownloadsViewControllerUIModeRoccaAngera
    
} IBDownloadsViewControllerUIMode;

@interface IBDownloadsViewController : UIViewController

@property (nonatomic, assign) BOOL wantsInAppPurchaseButton;
@property (nonatomic, assign) IBDownloadsViewControllerUIMode uiMode;

@end
