//
//  IBDownloadsViewController.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 20/05/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBDownloadsViewController.h"

#import "IBGlobal.h"
#import "HUDManager.h"
#import "HPGrowingTextView.h"
#import "IBZipDownloadItemView.h"
#import "FPPopoverController.h"
#import "IBInAppTableViewController.h"

#import "IBInAppHelper.h"
#import <StoreKit/StoreKit.h>

#import "ZipArchive.h"
#import "ASIHTTPRequest.h"
#import "IBZipDownloadButton.h"

#import "NSString+SSToolkitAdditions.h"
#import "NSString+NumberChecking.h"

#import "UIView+Toast.h"

#define kAudioGuideTag 1
#define kPhotoGalleryTag 2

@interface IBDownloadsViewController ()<IBZipDownloadItemViewDelegate,IBInAppTableViewControllerDelegate> {
    //
    CGRect calcRect;
    UILabel *labelTitle;
    NSArray *_products;
    long long receivedBytes,contentLength;
    
    ASIHTTPRequest *asiRequest;
    FPPopoverController *popover;
}

@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewMaster;

@property (nonatomic,assign) Status status;
@property (nonatomic,readonly) NSString *key;
@property (nonatomic,strong) NSString *urlString;
@property (nonatomic,strong) NSString *destinationDir;

@property (strong, nonatomic, readwrite) UIView *containerView;
@property (strong, nonatomic, readwrite) UIView *downloaderBackground;

@property (strong, nonatomic, readwrite) UIImageView *imageViewLocation;
@property (strong, nonatomic, readwrite) IBZipDownloadItemView *audioGuideZipDownloadItemView;
@property (strong, nonatomic, readwrite) IBZipDownloadItemView *photoGalleryZipDownloadItemView;
@property (strong, nonatomic, readwrite) HPGrowingTextView *museumDescriptionView;

- (void)refreshView;
- (void)buttonBackTouched;

@end

@implementation IBDownloadsViewController

@synthesize containerView;
@synthesize downloaderBackground;

@synthesize imageViewLocation;
@synthesize audioGuideZipDownloadItemView;
@synthesize photoGalleryZipDownloadItemView;
@synthesize museumDescriptionView;

@synthesize key = _key;
@synthesize status = _status;
@synthesize urlString = _urlString;
@synthesize destinationDir = _destinationDir;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self updateViewConstraints];
    if (IOS_OLDER_THAN(6.0)) [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:NO];
    
    if (IOS_NEWER_OR_EQUAL_TO(7.0)) self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    else self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    UIButton *buttonBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonBack setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [buttonBack setFrame:CGRectMake(0, 0, 12, 21)];
    [buttonBack addTarget:self action:@selector(buttonBackTouched) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem =
    [[UIBarButtonItem alloc] initWithCustomView:buttonBack];
    
    labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    labelTitle.backgroundColor = [UIColor clearColor];
    labelTitle.textColor = [UIColor blackColor];
    labelTitle.font = kAppFontSemibold(15);
    labelTitle.text = @"Your Title Header Add Here";
    labelTitle.textAlignment = NSTextAlignmentCenter;
    [labelTitle sizeToFit];
    
    self.navigationItem.titleView = labelTitle;
    
    self.scrollViewMaster.bounces = NO;
    self.scrollViewMaster.showsHorizontalScrollIndicator = NO;
    self.scrollViewMaster.showsVerticalScrollIndicator = NO;
    self.scrollViewMaster.userInteractionEnabled = YES;
    self.scrollViewMaster.backgroundColor = RGBCOLOR(211, 211, 211);
    
     CGRect temprect = [UIScreen mainScreen].bounds;
    calcRect = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, temprect.size.width, 90);
    
    //NSLog(@"Cgrect Size :%@",NSStringFromCGRect(calcRect));
     //NSLog(@"Frame Size :%@",NSStringFromCGRect(self.view.frame));
    
    containerView = [[UIView alloc] initWithFrame:calcRect];
    containerView.backgroundColor = RGBCOLOR(218.f, 218.f, 218.f);
    
    downloaderBackground = [[UIView alloc] initWithFrame:calcRect];
    
    [self.scrollViewMaster addSubview:containerView];
    [self.containerView addSubview:downloaderBackground];
    
    downloaderBackground.backgroundColor = RGBCOLOR(218, 218, 218);
    //
    [self _loadView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    labelTitle.text = @"";
    [self navigationController].navigationBarHidden = NO;
    [self.view setBackgroundColor:RGBCOLOR(200, 200, 200)];
    [self.scrollViewMaster setAlpha:0.f];
    
    NSString *downloadStatus = [[NSUserDefaults standardUserDefaults] valueForKey:self.key];
    
    if(([downloadStatus intValue] == StatusNotDownloaded) || ([[NSUserDefaults standardUserDefaults] valueForKey:self.key] == nil))   {
        [HUDManager addHUDWithLabel:nil dimBackground:YES];
    }
    
    [SharedAppDelegate setZoomScale:0.5];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:nil];
    
    _products = nil;
    
    [[IBInAppHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        if (success) {
            _products = products;
        }
        
        [self performSelector:@selector(refreshView) withObject:nil afterDelay:0.1f];
    }];
    
    if ([SharedAppDelegate isPlPlayerBussy]) {
        [SharedAppDelegate animatePlayerHidden:NO];
    } else {
        [SharedAppDelegate animatePlayerHidden:YES];
    } 
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [SharedAppDelegate animatePlayerHidden:YES];
}

-(NSString *)_audioGuideSourcePathForMusiamName:(NSString *)museumName andLanguage:(NSString *)language {
    //
    NSString *fileName = kAudioGuideZip;
    //
    NSString *path = [NSString stringWithFormat:@"%@/%@/%@/%@/%@",SYNC_URL,IBParentDirectory,museumName,language,fileName];
    //
    return path;
}

-(NSString *)_photoGallerySourcePathForMusiamName:(NSString *)museumName andLanguage:(NSString *)language {
    //
    NSString *path = [NSString stringWithFormat:@"%@/%@/%@/%@",SYNC_URL,IBParentDirectory,museumName,kGalleryZip];
    //
    return path;
}

-(NSString *)_audioGuideDestinationPathForMusiamName:(NSString *)museumName andLanguage:(NSString *)language {
    //
    return  [IBGlobal createDocumentsDirectoryByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@/",language,museumName]];
}

-(NSString *)_photoGalleryDestinationPathForMusiamName:(NSString *)museumName andLanguage:(NSString *)language {
    //
    return [IBGlobal createDocumentsDirectoryByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@/",ENGLISH,museumName]];
}

- (void)refreshView {
    //
    NSInteger locationNo = [SharedAppDelegate locationNo];
    NSUInteger languageIndex = [SharedAppDelegate languageIndex];
    NSDictionary *langDictionary = [SharedAppDelegate langDictionary];
    //
    NSString *language;
    NSString *musiemName;
    NSString *museumDescription = @"";
    //
    DIRLANGUAGE_STR(languageIndex, language)
    DIRLOCATION_STR(locationNo, musiemName);
    //
    NSString *audioGuideSource = [self _audioGuideSourcePathForMusiamName:musiemName andLanguage:language];
    NSString *photoGallerySource = [self _photoGallerySourcePathForMusiamName:musiemName andLanguage:language];
    NSString *audioGuideDestination = [self _audioGuideDestinationPathForMusiamName:musiemName andLanguage:language];
    NSString *photoGalleryDestination = [self _photoGalleryDestinationPathForMusiamName:musiemName andLanguage:language];
    [[SharedAppDelegate database] loadLocationDetailsWithLocationNo:[SharedAppDelegate locationNo] andLanguageIndex:languageIndex];
    //
    NSLog(@"self.key  %@,  self.status = %d ", self.key, self.status);
    if(locationNo == 1)  {
        //
        labelTitle.text = [[SharedAppDelegate langDictionary] valueForKey:@"LabelBella"];
        self.imageViewLocation.image = [UIImage imageNamed:@"isola_bella_main1.jpg"];
        self.audioGuideZipDownloadItemView.text = [NSString stringWithFormat:@"%@ \n%@",[[langDictionary valueForKey:@"LabelPayedGuide"] stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"],[langDictionary valueForKey:@"LabelGuideBella"]];
        self.audioGuideZipDownloadItemView.downloadButton.promoLocation = [[SharedAppDelegate langDictionary] valueForKey:@"LabelBella"];
        
        NSLog(@"destinationDir = %@", self.audioGuideZipDownloadItemView.downloadButton.destinationDir);
        //
        self.audioGuideZipDownloadItemView.downloadButton.urlString = audioGuideSource;
        self.audioGuideZipDownloadItemView.downloadButton.destinationDir = audioGuideDestination;
        
        Status audioDownloadedStatus = [[[NSUserDefaults standardUserDefaults] valueForKey:self.audioGuideZipDownloadItemView.downloadButton.key] intValue];
        //
        if (iSInAppPurchaseEnabled == YES) {
            //
            if([[SharedAppDelegate database] isPurchasedForLocationNo:locationNo]) {
                // 
                if (audioDownloadedStatus == StatusDownloaded) {
                    //
                    self.audioGuideZipDownloadItemView.downloadButton.status = StatusDownloaded;
                } else {
                    self.audioGuideZipDownloadItemView.downloadButton.status = StatusNotDownloaded;
                }
            } else {
                self.audioGuideZipDownloadItemView.downloadButton.status = StatusInAppPurchase;
            }
        } else {
             if (audioDownloadedStatus == StatusDownloaded) {
                //
                self.audioGuideZipDownloadItemView.downloadButton.status = StatusDownloaded;
            } else {
                self.audioGuideZipDownloadItemView.downloadButton.status = StatusNotDownloaded;
            }
        }
        
        museumDescription = langDictionary[@"LabelIsolaBellaMusceumDecription"];
    } else if(locationNo == 2) {
        //
        labelTitle.text = [[SharedAppDelegate langDictionary] valueForKey:@"LabelMadre"];
        self.imageViewLocation.image = [UIImage imageNamed:@"isola_madre_main1.jpg"];
        
        self.audioGuideZipDownloadItemView.text = [NSString stringWithFormat:@"%@ \n%@",[[langDictionary valueForKey:@"LabelPayedGuide"] stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"],[langDictionary valueForKey:@"LabelGuideBella"]];
        self.audioGuideZipDownloadItemView.downloadButton.promoLocation = [[SharedAppDelegate langDictionary] valueForKey:@"LabelMadre"];
        
        NSLog(@"destinationDir = %@", self.audioGuideZipDownloadItemView.downloadButton.destinationDir);
        //
        self.audioGuideZipDownloadItemView.downloadButton.urlString = audioGuideSource;
        self.audioGuideZipDownloadItemView.downloadButton.destinationDir = audioGuideDestination;
        
        Status audioDownloadedStatus = [[[NSUserDefaults standardUserDefaults] valueForKey:self.audioGuideZipDownloadItemView.downloadButton.key] intValue];
        //
        if (iSInAppPurchaseEnabled == YES) {
            //
            if([[SharedAppDelegate database] isPurchasedForLocationNo:locationNo]) {
                //
                if (audioDownloadedStatus == StatusDownloaded) {
                    //
                    self.audioGuideZipDownloadItemView.downloadButton.status = StatusDownloaded;
                } else {
                    self.audioGuideZipDownloadItemView.downloadButton.status = StatusNotDownloaded;
                }
            } else {
                self.audioGuideZipDownloadItemView.downloadButton.status = StatusInAppPurchase;
            }
        } else {
            if (audioDownloadedStatus == StatusDownloaded) {
                //
                self.audioGuideZipDownloadItemView.downloadButton.status = StatusDownloaded;
            } else {
                self.audioGuideZipDownloadItemView.downloadButton.status = StatusNotDownloaded;
            }
        }
        
        museumDescription = langDictionary[@"LabelIsolaMadreMusceumDecription"];
        
        
    } else if(locationNo == 3) {
        //
        labelTitle.text = [[SharedAppDelegate langDictionary] valueForKey:@"LabelAngera"];
        self.imageViewLocation.image = [UIImage imageNamed:@"rocca_angera_main1.jpg"];
        museumDescription = langDictionary[@"LabelRoccaMusceumDecription"];
    }
    //
    museumDescription = [museumDescription stringByAppendingString:@" \n\n"];
    
    self.photoGalleryZipDownloadItemView.downloadButton.urlString = photoGallerySource;
    self.photoGalleryZipDownloadItemView.downloadButton.destinationDir = photoGalleryDestination;
    
    Status photoDownloadedStatus = [[[NSUserDefaults standardUserDefaults] valueForKey:self.photoGalleryZipDownloadItemView.downloadButton.key] intValue];
    
    if (photoDownloadedStatus == StatusDownloaded) {
        self.photoGalleryZipDownloadItemView.downloadButton.status = StatusDownloaded;
    } else {
        self.photoGalleryZipDownloadItemView.downloadButton.status = StatusNotDownloaded;
    } 
    
    self.photoGalleryZipDownloadItemView.text = [[langDictionary valueForKey:@"LabelPhotoGallery"] stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"]; 
    self.audioGuideZipDownloadItemView.normalImage = [UIImage imageNamed:@"audio-normal.png"];
    self.audioGuideZipDownloadItemView.highlightedImage = [UIImage imageNamed:@"audio-highlighted.png"];
    self.audioGuideZipDownloadItemView.normalColor = RGBCOLOR(110, 110, 110);
    self.audioGuideZipDownloadItemView.highlightedColor = RGBCOLOR(104, 144, 152);
    self.audioGuideZipDownloadItemView.normalBackgoundColor = RGBCOLOR(211, 211, 211);
    self.audioGuideZipDownloadItemView.highlightedBackgoundColor = RGBCOLOR(255, 255, 255);
    self.audioGuideZipDownloadItemView.currentSelection = NO;
    
    self.photoGalleryZipDownloadItemView.normalImage = [UIImage imageNamed:@"gallery.png"];
    self.photoGalleryZipDownloadItemView.highlightedImage = [UIImage imageNamed:@"gallery-highlighted.png"];
    self.photoGalleryZipDownloadItemView.normalColor = RGBCOLOR(110, 110, 110);
    self.photoGalleryZipDownloadItemView.highlightedColor = RGBCOLOR(104, 144, 152);
    self.photoGalleryZipDownloadItemView.normalBackgoundColor = RGBCOLOR(211, 211, 211);
    self.photoGalleryZipDownloadItemView.highlightedBackgoundColor = RGBCOLOR(255, 255, 255);
    self.photoGalleryZipDownloadItemView.currentSelection = NO;
    
    //  NSString *museumDescription = [museumInfoArray[0] valueForKey:@"museumDescription"];
    self.museumDescriptionView.text = museumDescription;
    CGRect descriptionRect = self.museumDescriptionView.frame;
    self.museumDescriptionView.text = @"";
    // Apply some inline CSS
    NSString *styledHtml = [IBGlobal styledAppHtmlWithString:museumDescription andTextSize:15];
    // Generate an attributed string from the HTML
    NSAttributedString *attributedText = [IBGlobal attributedStringWithHTML:styledHtml];
    
    
    self.museumDescriptionView.internalTextView.attributedText = attributedText;
    
    //  descriptionRect.size = [self sizeForWidth:self.museumDescriptionView.frame.size.width attributtedtext:attributedText];
    descriptionRect.size.height = [self updateHeight:self.museumDescriptionView.internalTextView].size.height + self.audioGuideZipDownloadItemView.frame.size.height;// +self.photoGalleryZipDownloadItemView.frame.size.height;
    self.museumDescriptionView.frame = descriptionRect;
    
    calcRect.origin.x = self.scrollViewMaster.frame.origin.x;
    calcRect.origin.y = self.imageViewLocation.frame.origin.y+self.imageViewLocation.frame.size.height;
    calcRect.size.height = photoGalleryZipDownloadItemView.frame.size.height;
    calcRect.size.width = self.scrollViewMaster.frame.size.width;
    photoGalleryZipDownloadItemView.frame = calcRect;
    //
    if (self.uiMode == IBDownloadsViewControllerUIModeIsolaBella) [self _configureUIForIsolaBella];
    else if (self.uiMode == IBDownloadsViewControllerUIModeIsolaMadre) [self _configureUIForIsolaMadre];
    else if (self.uiMode == IBDownloadsViewControllerUIModeRoccaAngera) [self _configureUIForRoccaAngera];
    //
    calcRect.origin.x = self.scrollViewMaster.frame.origin.x;
    calcRect.origin.y = self.imageViewLocation.frame.origin.y+self.imageViewLocation.frame.size.height;
    calcRect.size.width = self.scrollViewMaster.frame.size.width;
    calcRect.size.height = audioGuideZipDownloadItemView.frame.size.height+photoGalleryZipDownloadItemView.frame.size.height+museumDescriptionView.frame.size.height+4;
    containerView.frame = calcRect;
    
    NSLog(@"calcRect = %@", NSStringFromCGRect(calcRect));
    [containerView setBackgroundColor:RGBCOLOR(211, 211, 211)];
    
    CGFloat downloaderHeight = audioGuideZipDownloadItemView.frame.size.height+photoGalleryZipDownloadItemView.frame.size.height+2;
    downloaderBackground.frame = CGRectMake(0, 0, containerView.frame.size.width, downloaderHeight);
    
    [UIView animateWithDuration:1.f animations:^{ [self.scrollViewMaster setAlpha:1.0f]; }];
    
    if (![SharedAppDelegate playerHidden]) {
        if([[SharedAppDelegate playerView] playing] || ![SharedAppDelegate wasPlayerHidden])  {
            [SharedAppDelegate animatePlayerHidden:NO];
        }
    }
    
    
    NSString *downloadStatus = [[NSUserDefaults standardUserDefaults] valueForKey:self.key];
    
    if(([downloadStatus intValue] == StatusNotDownloaded) || (downloadStatus == nil)) [HUDManager hideHUDFromWindowAfterDelay:0.2f];
}


-(UIImage *)imageForLocation:(NSInteger)locationNo {
    //
    if (locationNo == 1) {
        return [UIImage imageNamed:@"isola_bella_main1.jpg"];
    } else if (locationNo == 2) {
        return [UIImage imageNamed:@"isola_madre_main1.jpg"];
    } else if (locationNo == 3) {
        return [UIImage imageNamed:@"rocca_angera_main1.jpg"];
    }
    //
    return nil;
}

-(CGFloat)heightForImage:(UIImage *)image scaledToWidth:(CGFloat)width {
    //
    CGFloat oldWidth = image.size.width;
    CGFloat scaleFactor = width / oldWidth;
    
    return image.size.height * scaleFactor;
}

- (void)_loadView {
    //
    NSInteger locationNo = [SharedAppDelegate locationNo];
    audioGuideZipDownloadItemView = [[IBZipDownloadItemView alloc] initWithFrame:calcRect];
    photoGalleryZipDownloadItemView = [[IBZipDownloadItemView alloc] initWithFrame:calcRect];
    
    [audioGuideZipDownloadItemView setTag:kAudioGuideTag];
    [photoGalleryZipDownloadItemView setTag:kPhotoGalleryTag];
    
    [audioGuideZipDownloadItemView setDelegate:self];
    [photoGalleryZipDownloadItemView setDelegate:self];
    
    museumDescriptionView = [[HPGrowingTextView alloc] initWithFrame:calcRect];
    
    museumDescriptionView.editable = NO;
    museumDescriptionView.isScrollable = NO;
    museumDescriptionView.userInteractionEnabled = NO;
    museumDescriptionView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
    museumDescriptionView.minNumberOfLines = 1;
    museumDescriptionView.maxNumberOfLines = 200;
    museumDescriptionView.font = kAppFontRegular(15);
    museumDescriptionView.textColor = RGBCOLOR(110, 110, 110);
    museumDescriptionView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 0);
    museumDescriptionView.backgroundColor = RGBCOLOR(211, 211, 211);
    
    UIImage *image = [self imageForLocation:locationNo];
    calcRect.size.height = [self heightForImage:image scaledToWidth:calcRect.size.width];
    NSLog(@"calcRect 1 = %@", NSStringFromCGRect(calcRect));
    imageViewLocation = [[UIImageView alloc] initWithFrame:calcRect];
    imageViewLocation.backgroundColor = [UIColor redColor];
    imageViewLocation.contentMode = UIViewContentModeScaleAspectFit;
    
    [self.scrollViewMaster insertSubview:imageViewLocation aboveSubview:containerView];
    [self.scrollViewMaster insertSubview:photoGalleryZipDownloadItemView aboveSubview:containerView];
    [self.scrollViewMaster insertSubview:audioGuideZipDownloadItemView aboveSubview:containerView];
    [self.scrollViewMaster insertSubview:museumDescriptionView aboveSubview:containerView];
    
    NSString *museumDir;
    DIRLOCATION_STR(locationNo,museumDir);
    
    _urlString = [NSString stringWithFormat:@"%@/%@/%@/%@",SYNC_URL,IBParentDirectory,museumDir,kGuidesGalleryZip];
    
    _destinationDir = [IBGlobal createDocumentsDirectoryByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@/",ENGLISH,museumDir]];
}

- (void)productPurchased:(NSNotification *)notification {
    //
    NSString *productIdentifier = notification.object;
    //
    [_products enumerateObjectsUsingBlock:^(SKProduct * product, NSUInteger idx, BOOL *stop) {
        //
        if ([product.productIdentifier isEqualToString:productIdentifier]) {
            //
            NSInteger locationNo = [SharedAppDelegate locationNo];
            [[SharedAppDelegate database] updatePurchaseOfLocationNo:locationNo withStatus:YES];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setBool:YES forKey:productIdentifier];
            [defaults synchronize];
            
            [self setDownloadIndex:idx];
            
            *stop = YES;
        }
    }];
}

-(void)setDownloadIndex:(NSInteger)categoryIndex {
    //
    IBType type = [SharedAppDelegate locationNo];
    if(type == IBTypeIsoleBella || type == IBTypeIsoleMadre)   {
        self.audioGuideZipDownloadItemView.downloadButton.status = StatusNotDownloaded;
    }
}

#pragma -
#pragma IBInAppTableViewControllerDelegate delegate methods

- (void)comboInAppPurchaseTriggered
{
    [[IBInAppHelper sharedInstance] buyProduct:_products[0]];
    [popover dismissPopoverAnimated:YES];
}

- (void)singleInAppPurchaseTriggered
{
    IBType type = [SharedAppDelegate locationNo];
    if (_products.count < 2) {
        //
        [self _showAlertForNoInternetConnectionWithLangDictionary:[SharedAppDelegate langDictionary]];
    } else {
        //
        if (type == IBTypeIsoleBella) {
            [[IBInAppHelper sharedInstance] buyProduct:_products[0]];
        } else if (type == IBTypeIsoleMadre) {
            [[IBInAppHelper sharedInstance] buyProduct:_products[1]];
        }
    }
    
    [popover dismissPopoverAnimated:YES];
}

-(void)_showAlertForNoInternetConnectionWithLangDictionary:(NSDictionary *)langDictionary {
    //
    NSString *error = [langDictionary valueForKey:@"Error"];
    NSString *internetNotAvailable = [langDictionary valueForKey:@"CheckInternetConnection"];
    //
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:error message:internetNotAvailable delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma -
#pragma IBZipDownloadItemView delegate methods

- (void)inAppActionRequired {
    //
    [self singleInAppPurchaseTriggered];
}

- (void)zipDownloadedForItem:(IBZipDownloadItemView *)zipDownloadItemView
{
    NSInteger tag = zipDownloadItemView.tag;
    
    if(tag == kAudioGuideTag) {
        //
        NSString *downloadStatus = [[NSUserDefaults standardUserDefaults] valueForKey:self.key];
        
        if([downloadStatus intValue] == StatusNotDownloaded) {
            //
            self.status = StatusNotDownloaded;
        } else {
            //
            self.status = StatusDownloading;
            
            NSURL *url = [NSURL URLWithString:_urlString];
            asiRequest = [ASIHTTPRequest requestWithURL:url];
            [asiRequest setTimeOutSeconds:360];
            [asiRequest setDelegate:self];
            [asiRequest setDownloadProgressDelegate:self];
            [asiRequest setDownloadDestinationPath:[NSString stringWithFormat:@"%@%@",_destinationDir,[_urlString lastPathComponent]]];
            [asiRequest setTemporaryFileDownloadPath:[NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"loc%ld-%@.download",(long)[SharedAppDelegate locationNo],[_urlString lastPathComponent]]]];
            [asiRequest setAllowResumeForFileDownloads:YES];
            
            [audioGuideZipDownloadItemView.downloadButton startCustomProgress];
            
            [asiRequest startAsynchronous];
            
            [self.view makeToast:[[SharedAppDelegate langDictionary] valueForKey:@"LabelGuideGalleryLoad"] duration:3.f position:@"bottom"];
        }
    }
}

- (void)actionRequiredOnZipDownloadItemView:(IBZipDownloadItemView *)zipDownloadItemView {
    //
    NSInteger tag = zipDownloadItemView.tag;
    
    switch (tag) {
            //
            [HUDManager addHUDWithLabel:nil dimBackground:YES];
        case kAudioGuideTag:
            if(audioGuideZipDownloadItemView.downloadButton.status == StatusDownloaded) {
                [self performSegueWithIdentifier:@"segueToAudioGuide" sender:self];
            } else {
                
            }
            [audioGuideZipDownloadItemView setCurrentSelection:YES];
            [photoGalleryZipDownloadItemView setCurrentSelection:NO];
            break;
            
        case kPhotoGalleryTag:
            if(photoGalleryZipDownloadItemView.downloadButton.status == StatusDownloaded) {
                [self performSegueWithIdentifier:@"segueToAlbum" sender:self];
            }
            [photoGalleryZipDownloadItemView setCurrentSelection:YES];
            [audioGuideZipDownloadItemView setCurrentSelection:NO];
            break;
            
        default:
            break;
    }
}

#pragma -

- (void)buttonBackTouched
{
    //NSLog(@"%d",self.status);
    if(self.status == StatusDownloading) return;
    
    if(audioGuideZipDownloadItemView.downloadButton.status == StatusDownloading) return;
    if(photoGalleryZipDownloadItemView.downloadButton.status == StatusDownloading) return;
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)_isInternetconnectionExists {
    //
    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"https://www.wikipedia.org/"]];
    //
    return (data.length != 0);
}

-(BOOL)_canPurchase {
    //
    return (kPGDefaultPrize != nil || kPGDefaultPrize.length > 0 || ![kPGDefaultPrize isEqualToString:@"-1"]);
}

- (CGRect)updateHeight:(UITextView *)textView
{
    textView.scrollEnabled = NO;
    CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    textView.frame = newFrame;
    return  newFrame;
}

-(void)_configureUIForIsolaBella {
    //
    calcRect.origin.y = self.photoGalleryZipDownloadItemView.frame.origin.y+self.photoGalleryZipDownloadItemView.frame.size.height+1;
    calcRect.size.height = audioGuideZipDownloadItemView.frame.size.height;
    //
    // calcRect.origin.y = calcRect.origin.y-photoGalleryZipDownloadItemView.frame.size.height;
    audioGuideZipDownloadItemView.frame = calcRect;
    
    calcRect.origin.x = self.scrollViewMaster.frame.origin.x+6;
    calcRect.origin.y = self.audioGuideZipDownloadItemView.frame.origin.y+self.audioGuideZipDownloadItemView.frame.size.height+10;
    calcRect.size.height = museumDescriptionView.frame.size.height;
    
    if(IOS_NEWER_OR_EQUAL_TO(7.0)) calcRect.size.width = self.scrollViewMaster.frame.size.width-12;
    NSLog(@"calcRectcalcRect = %@", NSStringFromCGRect(calcRect));
    //calcRect.origin.y = audioGuideZipDownloadItemView.frame.origin.y;
    museumDescriptionView.frame = calcRect;
    
    CGFloat contentHeight;
    
    if([[SharedAppDelegate playerView] playing] || ![SharedAppDelegate playerHidden] || ![SharedAppDelegate wasPlayerHidden])    {
        contentHeight = self.museumDescriptionView.frame.origin.y+self.museumDescriptionView.frame.size.height+45;
    } else {
        contentHeight = self.museumDescriptionView.frame.origin.y+self.museumDescriptionView.frame.size.height;
    }
    //
    self.scrollViewMaster.contentSize = CGSizeMake(self.scrollViewMaster.frame.size.width, contentHeight+10);
}

-(void)_configureUIForIsolaMadre {
    //
//    [self _doAlignAndHideAudioGuideDownloadButton];
    //
    calcRect.origin.y = self.photoGalleryZipDownloadItemView.frame.origin.y+self.photoGalleryZipDownloadItemView.frame.size.height+1;
    calcRect.size.height = audioGuideZipDownloadItemView.frame.size.height;
    //
    // calcRect.origin.y = calcRect.origin.y-photoGalleryZipDownloadItemView.frame.size.height;
    audioGuideZipDownloadItemView.frame = calcRect;
    
    calcRect.origin.x = self.scrollViewMaster.frame.origin.x+6;
    calcRect.origin.y = self.audioGuideZipDownloadItemView.frame.origin.y+self.audioGuideZipDownloadItemView.frame.size.height+10;
    calcRect.size.height = museumDescriptionView.frame.size.height;
    
    if(IOS_NEWER_OR_EQUAL_TO(7.0)) calcRect.size.width = self.scrollViewMaster.frame.size.width-12;
    NSLog(@"calcRectcalcRect = %@", NSStringFromCGRect(calcRect));
    //calcRect.origin.y = audioGuideZipDownloadItemView.frame.origin.y;
    museumDescriptionView.frame = calcRect;
    
    CGFloat contentHeight;
    
    if([[SharedAppDelegate playerView] playing] || ![SharedAppDelegate playerHidden] || ![SharedAppDelegate wasPlayerHidden])    {
        contentHeight = self.museumDescriptionView.frame.origin.y+self.museumDescriptionView.frame.size.height+45;
    } else {
        contentHeight = self.museumDescriptionView.frame.origin.y+self.museumDescriptionView.frame.size.height;
    }
    //
    self.scrollViewMaster.contentSize = CGSizeMake(self.scrollViewMaster.frame.size.width, contentHeight+10);
}
-(void)_configureUIForRoccaAngera {
    //
    [self _doAlignAndHideAudioGuideDownloadButton];
}

-(void)_doAlignAndHideAudioGuideDownloadButton {
    //
    calcRect.origin.y = self.photoGalleryZipDownloadItemView.frame.origin.y+self.photoGalleryZipDownloadItemView.frame.size.height+1;
    calcRect.size.height = audioGuideZipDownloadItemView.frame.size.height;
    //
    // calcRect.origin.y = calcRect.origin.y-photoGalleryZipDownloadItemView.frame.size.height;
    audioGuideZipDownloadItemView.frame = calcRect;
    
    calcRect.origin.x = self.scrollViewMaster.frame.origin.x+6;
    calcRect.origin.y = self.audioGuideZipDownloadItemView.frame.origin.y+self.audioGuideZipDownloadItemView.frame.size.height+10;
    calcRect.size.height = museumDescriptionView.frame.size.height;
    
    if(IOS_NEWER_OR_EQUAL_TO(7.0)) calcRect.size.width = self.scrollViewMaster.frame.size.width-12;
    NSLog(@"calcRectcalcRect = %@", NSStringFromCGRect(calcRect));
    calcRect.origin.y = audioGuideZipDownloadItemView.frame.origin.y;
    museumDescriptionView.frame = calcRect;
    
    CGFloat contentHeight;
    
    if([[SharedAppDelegate playerView] playing] || ![SharedAppDelegate playerHidden] || ![SharedAppDelegate wasPlayerHidden])    {
        contentHeight = self.museumDescriptionView.frame.origin.y+self.museumDescriptionView.frame.size.height+45;
    } else {
        contentHeight = self.museumDescriptionView.frame.origin.y+self.museumDescriptionView.frame.size.height;
    }
    //
    self.scrollViewMaster.contentSize = CGSizeMake(self.scrollViewMaster.frame.size.width, contentHeight+10);
    //
    audioGuideZipDownloadItemView.hidden = YES;
}

#pragma mark -

- (void)requestStarted:(ASIHTTPRequest *)request
{
    _status = StatusDownloading;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:[NSString stringWithFormat:@"%d",self.status] forKey:self.key];
    [defaults synchronize];
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [audioGuideZipDownloadItemView.downloadButton setCustomProgress:1.f];
    
    ZipArchive *zipArchive = [[ZipArchive alloc] init];
    [zipArchive UnzipOpenFile:[NSString stringWithFormat:@"%@%@",_destinationDir,[_urlString lastPathComponent]]];
    
    if([zipArchive UnzipFileTo:_destinationDir overWrite:YES])  {
         _status = StatusDownloaded;
        [audioGuideZipDownloadItemView.downloadButton endCustomProgress];
    }
    
    [zipArchive UnzipCloseFile];
    
    NSError *error = nil;
    [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@%@",_destinationDir,[_urlString lastPathComponent]] error:&error];
    
    //NSLog(@"Total length received %lld",receivedBytes);
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:[NSString stringWithFormat:@"%d",self.status] forKey:self.key];
    [defaults synchronize];
}

- (void)requestFailed:(ASIHTTPRequest *)request {
    //
    _status = StatusFailed;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:[NSString stringWithFormat:@"%d",self.status] forKey:self.key];
    [defaults synchronize];
}

- (void)request:(ASIHTTPRequest *)request didReceiveBytes:(long long)bytes
{
    if(bytes>0) {
        receivedBytes = receivedBytes+bytes;
        float percentage = (float)receivedBytes/(float)contentLength;
        [audioGuideZipDownloadItemView.downloadButton setCustomProgress:percentage];
    }
}

- (void)request:(ASIHTTPRequest *)request incrementDownloadSizeBy:(long long)newLength
{
    if(newLength>0) {
        contentLength = newLength;
        [audioGuideZipDownloadItemView.downloadButton setCustomProgress:0.f];
    }
}

#pragma mark -

- (NSString *)key {
    //
    NSString *keyString = [[[_urlString stringByReplacingOccurrencesOfString:SYNC_URL withString:@""] base64EncodedString] uppercaseString];
    NSMutableString *newString = [NSMutableString string];
    //
    for (int i = 0; i < [keyString length]; i++) {
        int ascii = [keyString characterAtIndex:i];
        if (((ascii>64) && (ascii<91)) || ((ascii>47) && (ascii<58))) {
            //
            [newString appendFormat:@"%c",ascii]; 
        }
    }
    //
    return [NSString stringWithString:newString];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate {
    return YES;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

@end
