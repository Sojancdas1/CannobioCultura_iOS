//
//  IBGalleryDescriptionView.h
//  IBourGuideComponents
//
//  Created by Mobility 2014 on 13/08/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IBGalleryDescriptionReusableView : UICollectionReusableView

@property (strong, nonatomic) UILabel * title;
@property (strong, nonatomic) UILabel * desc;

+ (CGFloat)heightOfViewWithTitle:(NSString *)title description:(NSString *)desc constrainedToSize:(CGSize)constrainedSize;

@end