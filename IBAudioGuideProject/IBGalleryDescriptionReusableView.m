//
//  IBGalleryDescriptionView.m
//  IBourGuideComponents
//
//  Created by Mobility 2014 on 13/08/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBGalleryDescriptionReusableView.h"

#import "IBGlobal.h"

@implementation IBGalleryDescriptionReusableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
		self.clipsToBounds = YES;
        
		self.title = [[UILabel alloc] init];
		self.title.translatesAutoresizingMaskIntoConstraints = NO;
		self.title.font = kAppFontSemibold(17);
		self.title.textColor = RGBCOLOR(110, 110, 110);
		self.title.backgroundColor = [UIColor clearColor];
        
		self.desc = [[UILabel alloc] init];
		self.desc.numberOfLines = 0;
		self.desc.translatesAutoresizingMaskIntoConstraints = NO;
		self.desc.font = kAppFontRegular(15);
		self.desc.textColor = RGBCOLOR(110, 110, 110);
        self.desc.backgroundColor = [UIColor clearColor];
        
		[self addSubview:self.title];
		[self addSubview:self.desc];
        
		NSMutableArray * constraintsArray = [NSMutableArray array];
        
		// title
		[constraintsArray addObject: [NSLayoutConstraint constraintWithItem:self.title attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0 constant:20.0f]];
        
		[constraintsArray addObject: [NSLayoutConstraint constraintWithItem:self.title attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1.0 constant:10.0f]];
        
        [constraintsArray addObject: [NSLayoutConstraint constraintWithItem:self.title attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeRight multiplier:1.0 constant:-10.0f]];
        
		// description
		[constraintsArray addObject: [NSLayoutConstraint constraintWithItem:self.desc attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.title attribute:NSLayoutAttributeLeading multiplier:1.0 constant:0.0f]];
        
		[constraintsArray addObject: [NSLayoutConstraint constraintWithItem:self.desc attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.title attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:0.0f]];
        
		[constraintsArray addObject: [NSLayoutConstraint constraintWithItem:self.desc attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.title attribute:NSLayoutAttributeBottom	multiplier:1.0 constant:5.0f]];
        
		[self addConstraints:constraintsArray];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

+ (CGFloat)heightOfViewWithTitle:(NSString *)title description:(NSString *)desc constrainedToSize:(CGSize)constrainedSize
{
	__block CGFloat height = 0;
	NSDictionary * fontsAndStrings = @{
                                       UIFontTextStyleHeadline : title,
                                       UIFontTextStyleBody : desc
                                       };
	
	[fontsAndStrings enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
		NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:obj attributes:@{ NSFontAttributeName : [UIFont preferredFontForTextStyle:key] }];
		
		CGRect requiredFrame = [string boundingRectWithSize:constrainedSize options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) context:nil];
		
		height += requiredFrame.size.height;
	}];

	return height + 35;
}

@end
