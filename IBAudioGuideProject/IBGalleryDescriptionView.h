//
//  IBGalleryDescriptionView.h
//  IBExperiments
//
//  Created by Mobility 2014 on 04/06/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IBGalleryDescriptionView : UIView

@property (nonatomic,strong) NSString *descriptionHeader;
@property (nonatomic,strong) NSString *descriptionText;

- (void)refreshViewToWidth:(CGFloat)width andRespectiveHeight:(CGFloat)height;
                              
@end
