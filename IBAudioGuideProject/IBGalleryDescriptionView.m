//
//  IBGalleryDescriptionView.m
//  IBExperiments
//
//  Created by Mobility 2014 on 04/06/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBGalleryDescriptionView.h"

#import "IBGlobal.h"
#import "OBGradientView.h"
#import "HPGrowingTextView.h"

#define kTopEdgeInsetsValue 10
#define kHeaderMinHeight 30
#define kScreenWidth ([[UIScreen mainScreen] bounds].size.width)
#define kScreenHeight ([[UIScreen mainScreen] bounds].size.height)

@interface IBGalleryDescriptionView ()
{
    OBGradientView *gradientView;
    UILabel *labelHeaderView;
    HPGrowingTextView *growingDescriptionView;
    UITextView *textDescriptionView;
}

@end

@implementation IBGalleryDescriptionView

@synthesize descriptionText = _descriptionText;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
       
        gradientView = [[OBGradientView alloc] initWithFrame:self.bounds];
        [self addSubview:gradientView];

        NSArray *colors = [NSArray arrayWithObjects:[UIColor clearColor], [UIColor colorWithWhite:.0f alpha:.8f], nil];
        gradientView.colors = colors;
        
        labelHeaderView = [[UILabel alloc] initWithFrame:CGRectMake(9, kTopEdgeInsetsValue, self.frame.size.width-18, kHeaderMinHeight)];
        [gradientView addSubview:labelHeaderView];
        
        labelHeaderView.font = kAppFontSemibold(15);
        labelHeaderView.backgroundColor = [UIColor clearColor];
        labelHeaderView.textColor = [UIColor whiteColor];
        labelHeaderView.adjustsFontSizeToFitWidth = YES;
        
        growingDescriptionView = [[HPGrowingTextView alloc] initWithFrame:CGRectMake(0, kTopEdgeInsetsValue+kHeaderMinHeight, self.frame.size.width, self.frame.size.height-(kTopEdgeInsetsValue+kHeaderMinHeight))];
        [gradientView addSubview:growingDescriptionView];
        
        growingDescriptionView.editable = NO;
        growingDescriptionView.isScrollable = NO;
        growingDescriptionView.userInteractionEnabled = NO;
        growingDescriptionView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
        growingDescriptionView.minNumberOfLines = 1;
        growingDescriptionView.maxNumberOfLines = 30;
        growingDescriptionView.minHeight = kHeaderMinHeight;
        growingDescriptionView.maxHeight = kScreenHeight-(kHeaderMinHeight+kTopEdgeInsetsValue);
        growingDescriptionView.font = kAppFontRegular(15);
        growingDescriptionView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 0);
        growingDescriptionView.backgroundColor = [UIColor clearColor];
        growingDescriptionView.textColor = [UIColor whiteColor];
        
        textDescriptionView = [[UITextView alloc] initWithFrame:CGRectMake(0, kTopEdgeInsetsValue+kHeaderMinHeight, self.frame.size.width, self.frame.size.height-(kTopEdgeInsetsValue+kHeaderMinHeight))];
        [gradientView addSubview:textDescriptionView];
        
        textDescriptionView.font = kAppFontRegular(15);
        textDescriptionView.backgroundColor = [UIColor clearColor];
        textDescriptionView.textColor = [UIColor whiteColor];
        
        _descriptionHeader = @"some heading here";
        _descriptionText = @"some description here";
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    [super drawRect:rect];
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    if((orientation == UIDeviceOrientationPortrait) || (orientation == UIDeviceOrientationPortraitUpsideDown))  {
        
        [self refreshViewToWidth:kScreenWidth andRespectiveHeight:kScreenHeight];
    }
    else if((orientation == UIDeviceOrientationLandscapeLeft) || (orientation == UIDeviceOrientationLandscapeRight))    {
        if(IOS_NEWER_OR_EQUAL_TO(8.0))  {
            [self refreshViewToWidth:kScreenWidth andRespectiveHeight:kScreenHeight];
        }
        else    {
            [self refreshViewToWidth:kScreenHeight andRespectiveHeight:kScreenWidth];
        }
    }
}

- (void)setDescriptionText:(NSString *)descriptionText
{
    _descriptionText = descriptionText;
    [self setNeedsDisplay];
}

- (void)setDescriptionHeader:(NSString *)descriptionHeader
{
    _descriptionHeader = descriptionHeader;
    [self setNeedsDisplay];
}

- (void)refreshViewToWidth:(CGFloat)width andRespectiveHeight:(CGFloat)height
{
    if(self.frame.size.width != width)  {
        labelHeaderView.text = nil;
        growingDescriptionView.text = nil;

        CGRect headerFrame = labelHeaderView.frame;
        headerFrame.size.width = width-18;
        [labelHeaderView setFrame:headerFrame];
        
        CGRect descriptionFrame = growingDescriptionView.frame;
        descriptionFrame.size.width = width;
        [growingDescriptionView setFrame:descriptionFrame];
        
        CGRect descriptionLabelFrame = textDescriptionView.frame;
        descriptionLabelFrame.size.width = width;
        [textDescriptionView setFrame:descriptionLabelFrame];
    }
    
    labelHeaderView.text = _descriptionHeader;
    growingDescriptionView.text = _descriptionText;
    textDescriptionView.text = _descriptionText;
    
    int numLines = growingDescriptionView.internalTextView.contentSize.height/growingDescriptionView.internalTextView.font.leading;
    //NSLog(@"no of lines %d",numLines);
    
    if(IOS_OLDER_THAN(7.0)) {
        [textDescriptionView setHidden:NO];
        [growingDescriptionView setHidden:YES];
        
        CGRect descriptionLabelFrame = textDescriptionView.frame;
        descriptionLabelFrame.size.height = 22*numLines;
        [textDescriptionView setFrame:descriptionLabelFrame];
        
        CGFloat calculatedHeight = labelHeaderView.frame.size.height+textDescriptionView.frame.size.height+kTopEdgeInsetsValue;
        [super setFrame:CGRectMake(0, height-calculatedHeight, width, calculatedHeight)];
    }
    else    {
        [textDescriptionView setHidden:YES];
        [growingDescriptionView setHidden:NO];
        
        CGFloat calculatedHeight = labelHeaderView.frame.size.height+growingDescriptionView.frame.size.height+kTopEdgeInsetsValue;
        [super setFrame:CGRectMake(0, height-calculatedHeight, width, calculatedHeight)];
    }
    
    gradientView.frame = super.bounds;
    
    CGRect headerFrame = labelHeaderView.frame;
    headerFrame.origin.x = 9;
    headerFrame.origin.y = kTopEdgeInsetsValue;
    [labelHeaderView setFrame:headerFrame];
    
    CGRect descriptionFrame = growingDescriptionView.frame;
    descriptionFrame.origin.x = 0;
    descriptionFrame.origin.y = kTopEdgeInsetsValue+labelHeaderView.frame.size.height;
    [growingDescriptionView setFrame:descriptionFrame];
    
    CGRect descriptionLabelFrame = textDescriptionView.frame;
    descriptionLabelFrame.origin.x = 0;
    descriptionLabelFrame.origin.y = kTopEdgeInsetsValue+labelHeaderView.frame.size.height;
    [textDescriptionView setFrame:descriptionLabelFrame];
}

@end
