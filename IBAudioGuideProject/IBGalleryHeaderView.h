//
//  IBGalleryHeaderView.h
//  IBExperiments
//
//  Created by Mobility 2014 on 04/06/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IBGalleryHeaderViewDelegate
- (void)albumWillDismiss;
@end

@interface IBGalleryHeaderView : UIView

@property (assign) id <IBGalleryHeaderViewDelegate> delegate;

@property (nonatomic,strong) UIButton *buttonBack;
@property (nonatomic,strong) UILabel *labelTitle;
@property (nonatomic,strong) UILabel *labelDetails;

@end
