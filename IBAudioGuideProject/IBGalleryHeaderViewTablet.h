//
//  IBGalleryHeaderView.h
//  IBExperiments
//
//  Created by Mobility 2014 on 04/06/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IBGalleryHeaderViewDelegateTablet
- (void)albumWillDismiss;
@end

@interface IBGalleryHeaderViewTablet : UIView

@property (assign) id <IBGalleryHeaderViewDelegateTablet> delegate;

@property (nonatomic,strong) UIButton *buttonBack;
@property (nonatomic,strong) UILabel *labelTitle;
@property (nonatomic,strong) UILabel *labelDetails;

@end
