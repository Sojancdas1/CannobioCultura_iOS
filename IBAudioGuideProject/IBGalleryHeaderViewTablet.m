//
//  IBGalleryHeaderView.m
//  IBExperiments
//
//  Created by Mobility 2014 on 04/06/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBGalleryHeaderViewTablet.h"

#import "IBGlobal.h"
#import "UIButton+Extensions.h"

#define kTitleHeight 22
#define kDetailsHeight 15
#define kBottomSpaceValue 3
#define kLeadingSpaceValue 15
#define kTrailingSpaceValue 15
#define kTitleFont kAppFontSemibold(17)
#define kDetailsFont kAppFontSemibold(11)
#define RGBCOLOR_WITH_ALPHA(r, g, b, a) [UIColor colorWithRed:r/225.0f green:g/225.0f blue:b/225.0f alpha:a]
#define kBackgroundColor RGBCOLOR_WITH_ALPHA(0,0,0,0.7)
#define kScreenWidth ([[UIScreen mainScreen] bounds].size.width)
#define kScreenHeight ([[UIScreen mainScreen] bounds].size.height)

@interface IBGalleryHeaderViewTablet ()
{
    CGFloat kTopSpaceValue;
}

- (void)buttonTouched;

@end

@implementation IBGalleryHeaderViewTablet

@synthesize delegate;
@synthesize buttonBack;
@synthesize labelTitle;
@synthesize labelDetails;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        if(IOS_NEWER_OR_EQUAL_TO(7.0))  {
            kTopSpaceValue = 15;
        }
        else    {
            kTopSpaceValue = 5;
        }
        
        buttonBack = [[UIButton alloc] init];
        labelTitle = [[UILabel alloc] init];
        labelDetails = [[UILabel alloc] init];
        
        [self addSubview:buttonBack];
        [self addSubview:labelTitle];
        [self addSubview:labelDetails];
        
        [labelTitle setBackgroundColor:[UIColor clearColor]];
        [labelTitle setFont:kTitleFont];
        [labelTitle setTextColor:[UIColor whiteColor]];
        [labelTitle setTextAlignment:NSTextAlignmentCenter];
        [labelTitle setNumberOfLines:1];
        [labelTitle setAdjustsFontSizeToFitWidth:YES];
        [labelTitle setMinimumScaleFactor:0.5];
        
        [labelDetails setBackgroundColor:[UIColor clearColor]];
        [labelDetails setFont:kDetailsFont];
        [labelDetails setTextColor:[UIColor whiteColor]];
        [labelDetails setTextAlignment:NSTextAlignmentCenter];
        [labelDetails setNumberOfLines:1];
        [labelDetails setAdjustsFontSizeToFitWidth:YES];
        [labelDetails setMinimumScaleFactor:0.5];
        
        labelTitle.text = @"Some title here";
        labelDetails.text = @"Some details here";
        
        [buttonBack setBackgroundColor:[UIColor clearColor]];
        
//        [buttonBack setImage:[UIImage imageNamed:@"back-white.png"] forState:UIControlStateNormal];
        
        [buttonBack.titleLabel setFont:kAppFontSemibold(12)];
        [buttonBack setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [buttonBack setTitle:[[SharedAppDelegate langDictionary] valueForKey:@"LabelClose"] forState:UIControlStateNormal];
        // border radius
        [buttonBack.layer setCornerRadius:3.0f];
        
//        // border
//        [buttonBack.layer setBorderColor:[UIColor whiteColor].CGColor];
//        [buttonBack.layer setBorderWidth:1.5f];
        
        [buttonBack addTarget:self action:@selector(buttonTouched) forControlEvents:UIControlEventTouchDown];
        
        [buttonBack setHitTestEdgeInsets:UIEdgeInsetsMake(-15, -15, -15, -15)];
        
        [self setFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, kTitleHeight+kDetailsHeight+kTopSpaceValue+kBottomSpaceValue)];
        
        [self setBackgroundColor:kBackgroundColor];
        [self setUserInteractionEnabled:YES];
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    
    if(IOS_NEWER_OR_EQUAL_TO(7.0))  {
        kTopSpaceValue = 25;
    }
    else    {
        kTopSpaceValue = 10;
    }
    
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    if((orientation == UIDeviceOrientationPortrait) || (orientation == UIDeviceOrientationPortraitUpsideDown))  {
        [super setFrame:CGRectMake(0, 0, kScreenWidth, kTitleHeight+kDetailsHeight+kTopSpaceValue+kBottomSpaceValue)];
    }
    else    {
        if(IOS_NEWER_OR_EQUAL_TO(8.0))  {
            [super setFrame:CGRectMake(0, 0, kScreenWidth, kTitleHeight+kDetailsHeight+kTopSpaceValue+kBottomSpaceValue)];
        }
        else    {
            [super setFrame:CGRectMake(0, 0, kScreenHeight, kTitleHeight+kDetailsHeight+kTopSpaceValue+kBottomSpaceValue)];
        }
    }
    
    [labelTitle setFrame:CGRectMake(kLeadingSpaceValue+buttonBack.frame.size.width, kTopSpaceValue, super.frame.size.width-(kLeadingSpaceValue+buttonBack.frame.size.width)*2, kTitleHeight)];
    [labelDetails setFrame:CGRectMake(kLeadingSpaceValue+buttonBack.frame.size.width, kTopSpaceValue+labelTitle.frame.size.height, super.frame.size.width-(kLeadingSpaceValue+buttonBack.frame.size.width)*2, kDetailsHeight)];
    
    [buttonBack setFrame:CGRectMake(kLeadingSpaceValue, rect.size.height-31, 50, 25)];
}

- (void)buttonTouched
{
    [self.delegate albumWillDismiss];
}

@end
