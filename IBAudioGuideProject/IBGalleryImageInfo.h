//
//  IBGalleryImageInfo.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 26/05/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IBGalleryImageInfo : NSObject

@property (nonatomic, retain) NSString * photoPath;
@property (nonatomic, retain) NSString * photoTitle;
@property (nonatomic, retain) NSString * referenceNo;
@property (nonatomic, retain) NSString * photoDescription;

@end
