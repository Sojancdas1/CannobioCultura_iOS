//
//  IBGalleryImages.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 23/07/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBGalleryImageInfo.h"

@implementation IBGalleryImageInfo

@synthesize photoPath;
@synthesize photoTitle;
@synthesize referenceNo;
@synthesize photoDescription;

@end
