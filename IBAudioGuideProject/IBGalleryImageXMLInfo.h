//
//  IBGalleryImageInfo.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 26/05/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IBGalleryImageXMLInfo : NSObject

@property (nonatomic, retain) NSString *referenceNo;
@property (nonatomic, retain) NSString *imageName;
@property (nonatomic, retain) NSString *heading;
@property (nonatomic, retain) NSString *description;

@end
