//
//  IBGalleryImageInfo.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 26/05/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBGalleryImageXMLInfo.h"

@implementation IBGalleryImageXMLInfo

@synthesize referenceNo;
@synthesize imageName;
@synthesize heading;
@synthesize description;

@end
