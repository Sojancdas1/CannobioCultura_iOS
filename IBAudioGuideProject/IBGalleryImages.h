//
//  IBGalleryImages.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 23/07/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class IBMuseums;

@interface IBGalleryImages : NSManagedObject

@property (nonatomic, retain) NSString * photoDescription;
@property (nonatomic, retain) NSString * photoPath;
@property (nonatomic, retain) NSString * photoTitle;
@property (nonatomic, retain) NSString * referenceNo;
@property (nonatomic, retain) IBMuseums *imageGalleryToMuseum;

@end
