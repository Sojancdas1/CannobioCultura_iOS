//
//  IBGalleryImages.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 23/07/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBGalleryImages.h"
#import "IBMuseums.h"


@implementation IBGalleryImages

@dynamic photoDescription;
@dynamic photoPath;
@dynamic photoTitle;
@dynamic referenceNo;
@dynamic imageGalleryToMuseum;

@end
