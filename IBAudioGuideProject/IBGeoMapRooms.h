//
//  IBGeoMapRooms.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 23/07/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface IBGeoMapRooms : NSManagedObject

@property (nonatomic, retain) NSString * coordinates;
@property (nonatomic, retain) NSString * roomNo;
@property (nonatomic, retain) NSNumber * sectionNo;
@property (nonatomic, retain) NSString * circleCoordinates;

@end
