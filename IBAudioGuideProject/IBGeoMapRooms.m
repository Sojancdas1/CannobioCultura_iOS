//
//  IBGeoMapRooms.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 23/07/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBGeoMapRooms.h"


@implementation IBGeoMapRooms

@dynamic coordinates;
@dynamic roomNo;
@dynamic sectionNo;
@dynamic circleCoordinates;
@end
