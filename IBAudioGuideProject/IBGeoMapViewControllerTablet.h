//
//  IBGeoMapViewControllerTablet.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 02/09/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IBGeoMapViewControllerTabletDelegate
@required
- (void)wasOnDisplayController;
@end

@interface IBGeoMapViewControllerTablet : UIViewController

@property (nonatomic, assign) id <IBGeoMapViewControllerTabletDelegate> delegate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewBottomSpaceConstraint;

@end
