//
//  IBGeoMapViewControllerTablet.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 02/09/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBGeoMapViewControllerTablet.h"

#import "Base64.h"
#import "IBGlobal.h"
#import "IBZipDownloadButton.h"
#import "MBProgressHUD.h"
#import "ASIHTTPRequest.h"
#import "ZipArchive.h"
#import "MTImageMapView.h"
#import "IBAudioGuideTable.h"
#import "IBGuideDetailViewController.h"
#import "FPPopoverController.h"
#import "UIView+Toast.h"
#import "UIScrollView+ZoomToPoint.h"


@interface IBGeoMapViewControllerTablet ()<MTImageMapDelegate,UIScrollViewDelegate,IBAudioGuideTableDelegate>
{
    ASIHTTPRequest *asiRequest;
    long long receivedBytes,contentLength;
    
    MBProgressHUD *hud;
    IBAudioGuideTable *audioGuideTable;
    UIAlertView *alertView;
    IBAudioGuideInfo *_currentGuideInfo;
    CGPoint contentOffset;
    
    CAShapeLayer *polyLayer;
    CGRect imageInitialRect;
    
    UILabel * labelText ;
    UIView *_iBcircleView;
    NSInteger previousCircleViewTag;
    UIView *_backView;
}

@property (nonatomic,assign) Status status;
@property (nonatomic,readonly) NSString *key;
@property (nonatomic,strong) NSString *urlString;
@property (nonatomic,strong) NSString *destinationDir;
@property (nonatomic,strong) NSArray *roomIndices;
@property (nonatomic,strong) NSArray *roomCoordinates;
@property (nonatomic,strong) IBAudioGuideInfo *currentGuideInfo;

@property (weak, nonatomic) IBOutlet UIButton *buttonClose;

@property (weak, nonatomic) IBOutlet UIView *viewSectionSegment;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewTopSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *listButtonTopConstraint;
@property (weak, nonatomic) IBOutlet UIView *containerButtonView;
@property (strong, nonatomic) IBOutlet UIView *backView;

- (void)refreshView;
- (void)buttonBackTouched;
- (void)buttonListViewTouched;
- (void)selectedSegmentIndex:(UISegmentedControl *)paramSender;
- (void)loadMapOfIndex:(NSInteger)index;

- (IBAction)buttonMapListTouched:(id)sender;

@end

@implementation IBGeoMapViewControllerTablet

@synthesize key = _key;
@synthesize status = _status;
@synthesize urlString = _urlString;
@synthesize destinationDir = _destinationDir;
@synthesize roomIndices = _roomIndices;
@synthesize roomCoordinates = _roomCoordinates;
@synthesize delegate = _delegate;
@synthesize backView = _backView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.status = StatusDownloaded;
    self.segmentControl.segmentedControlStyle = UISegmentedControlStylePlain;
    self.segmentControl.tintColor = [UIColor grayColor];
    
    NSInteger count = [[SharedAppDelegate database] countNoOfMapSections];
    
    if(count==1)    {
        self.viewSectionSegment.hidden = YES;
        self.scrollViewTopSpaceConstraint.constant = 20;
        self.listButtonTopConstraint.constant = 30;
    }
    else    {
        self.viewSectionSegment.hidden = NO;
        self.scrollViewTopSpaceConstraint.constant = 68;
        self.listButtonTopConstraint.constant = 30;
    }
    
    self.scrollViewContainer.bounces = NO;
    self.scrollViewContainer.backgroundColor = [UIColor whiteColor];
    
    self.scrollViewContainer.delegate = self;
    
    audioGuideTable = [[IBAudioGuideTable alloc] initWithFrame:CGRectMake(0, 0, 520, 110)];
    audioGuideTable.delegate = self;
    audioGuideTable.layer.cornerRadius = 10.0f;
    
    _currentGuideInfo = nil;
    
    contentOffset = CGPointZero;
    
    polyLayer = nil;
    
    // border radius
    [self.containerButtonView.layer setCornerRadius:3.0f];
    
    //    // border
    //    [self.containerButtonView.layer setBorderColor:RGBCOLOR(104, 144, 152).CGColor];
    //    [self.containerButtonView.layer setBorderWidth:1.5f];
    
    //    // drop shadow
    //    [self.containerButtonView.layer setShadowColor:[UIColor blackColor].CGColor];
    //    [self.containerButtonView.layer setShadowOpacity:0.5];
    //    [self.containerButtonView.layer setShadowRadius:3.0];
    //    [self.containerButtonView.layer setShadowOffset:CGSizeMake(0.3, 0.3)];
    
    [[UIApplication sharedApplication] setStatusBarOrientation:[[SharedAppDelegate masterSplitController] interfaceOrientation] animated:YES];
    
    if (IOS_NEWER_OR_EQUAL_TO(7.0))    {
        [self setNeedsStatusBarAppearanceUpdate];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.segmentControl.alpha = 0;
    
    [self navigationController].navigationBarHidden = NO;
    [self.view setBackgroundColor:RGBCOLOR(200, 200, 200)];
    
    NSInteger locationNo = [SharedAppDelegate locationNo];
    
    NSString *museumName = nil;
    MUSEUMNAME_STR(locationNo, museumName);
    
    NSString *museumDir;
    DIRLOCATION_STR(locationNo,museumDir);
    
    _urlString = [NSString stringWithFormat:@"%@/%@/%@/%@",SYNC_URL,IBParentDirectory,museumDir,kMapZip];
    _destinationDir = [IBGlobal createDocumentsDirectoryByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@/",ENGLISH,museumDir]];
    
    NSString *downloadStatus = [[NSUserDefaults standardUserDefaults] valueForKey:self.key];
    
    [self.buttonClose setTitle:[[SharedAppDelegate langDictionary] valueForKey:@"LabelClose"] forState:UIControlStateNormal];
    [self.buttonClose setTitleColor:RGBCOLOR(104, 144, 152) forState:UIControlStateNormal];
    [self.buttonClose.titleLabel setFont:kAppFontSemibold(12)];
    
    if([downloadStatus intValue]==StatusDownloaded)   {
        self.status = StatusDownloaded;
    }
    else  {
        self.status = StatusDownloading;
        
        NSURL *url = [NSURL URLWithString:_urlString];
        asiRequest = [ASIHTTPRequest requestWithURL:url];
        [asiRequest setTimeOutSeconds:360];
        [asiRequest setDelegate:self];
        [asiRequest setDownloadProgressDelegate:self];
        [asiRequest setDownloadDestinationPath:[NSString stringWithFormat:@"%@%@",_destinationDir,[_urlString lastPathComponent]]];
        [asiRequest setTemporaryFileDownloadPath:[NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"loc%ld-%@.download",(long)[SharedAppDelegate locationNo],[_urlString lastPathComponent]]]];
        [asiRequest setAllowResumeForFileDownloads:YES];
        
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeDeterminate;
        hud.dimBackground = YES;
        
        [SharedAppDelegate lockInteractions];
        [asiRequest startAsynchronous];
        
        [self.view makeToast:[[SharedAppDelegate langDictionary] valueForKey:@"LabelMapLoad"] duration:3.f position:@"bottom"];
    }
    
    self.scrollViewContainer.alpha = 0.f;
    [self performSelector:@selector(refreshView) withObject:nil afterDelay:0.1];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated    {
    [super viewDidDisappear:animated];
    [SharedAppDelegate setGeoMapViewController:nil];
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (BOOL)shouldAutorotate    {
    return NO;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations   {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationMaskAll;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    self.scrollViewContainer.alpha = 0.f;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    
    [self performSelector:@selector(refreshView) withObject:nil afterDelay:0.1];
}

- (void)requestStarted:(ASIHTTPRequest *)request
{
    //NSLog(@"Request started!");
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:[NSString stringWithFormat:@"%d",self.status] forKey:self.key];
    [defaults synchronize];
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [hud setProgress:1.0f];
    
    ZipArchive *zipArchive = [[ZipArchive alloc] init];
    [zipArchive UnzipOpenFile:[NSString stringWithFormat:@"%@%@",_destinationDir,[_urlString lastPathComponent]]];
    if([zipArchive UnzipFileTo:_destinationDir overWrite:YES])  {
        [hud hide:YES afterDelay:1.f];
    }
    [zipArchive UnzipCloseFile];
    
    NSError *error = nil;
    [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@%@",_destinationDir,[_urlString lastPathComponent]] error:&error];
    
    //NSLog(@"Total length received %lld",receivedBytes);
    
    self.status = StatusDownloaded;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:[NSString stringWithFormat:@"%d",self.status] forKey:self.key];
    [defaults synchronize];
    
    [SharedAppDelegate unlockInteractions];
    [self performSelector:@selector(refreshView) withObject:nil afterDelay:0.1];
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    self.status = StatusNotDownloaded;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:[NSString stringWithFormat:@"%d",self.status] forKey:self.key];
    [defaults synchronize];
    
    [SharedAppDelegate unlockInteractions];
}

- (void)request:(ASIHTTPRequest *)request didReceiveBytes:(long long)bytes
{
    if(bytes>0) {
        receivedBytes = receivedBytes+bytes;
        float percentage = (float)receivedBytes/(float)contentLength;
        [hud setProgress:percentage];
    }
    
    //NSLog(@"bytes received %lld",bytes);
}

- (void)request:(ASIHTTPRequest *)request incrementDownloadSizeBy:(long long)newLength
{
    if(newLength>0) {
        contentLength = newLength;
    }
    
    //NSLog(@"Total length calculated %lld",newLength);
}

- (void)buttonBackTouched
{
    //NSLog(@"%d",self.status);
    if(self.status!=StatusDownloaded) return;
    
    UINavigationController *rootViewController = (UINavigationController *)[[[UIApplication sharedApplication] keyWindow] rootViewController];
    
    [rootViewController popToViewController:[rootViewController.viewControllers objectAtIndex:1] animated:YES];
    
    self.navigationController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self dismissViewControllerAnimated:YES completion:^{
        [SharedAppDelegate animatePlayerHidden:[SharedAppDelegate playerHidden]];
    }];
}

- (void)buttonListViewTouched   {
    //NSLog(@"%d",self.status);
    if(self.status!=StatusDownloaded) return;
    
    [self dismissViewControllerAnimated:YES completion:^{
        [SharedAppDelegate animatePlayerHidden:[SharedAppDelegate playerHidden]];
        //NSLog(@"Map controller dismissed successfully");
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
}

- (void)refreshView
{
    NSInteger locationNo = [SharedAppDelegate locationNo];
    
    NSString *museumName = nil;
    MUSEUMNAME_STR(locationNo, museumName);
    
    NSDictionary *langDictionary = [SharedAppDelegate langDictionary];
    
    NSInteger count = [[SharedAppDelegate database] countNoOfMapSections];
    
    for(UIView *view in self.scrollViewContainer.subviews)  {
        [view removeFromSuperview];
    }
    
    if(count==1)    {
        self.viewSectionSegment.hidden = YES;
        
        [self loadMapOfIndex:1];
    }
    else    {
        self.viewSectionSegment.hidden = NO;
        
        [self.segmentControl removeAllSegments];
        
        NSArray *array = [NSArray arrayWithObjects:[NSString stringWithFormat:@"%@ %@",[langDictionary valueForKey:@"LabelFirst"],[langDictionary valueForKey:@"LabelMap"]],[NSString stringWithFormat:@"%@ %@",[langDictionary valueForKey:@"LabelSecond"],[langDictionary valueForKey:@"LabelMap"]],[NSString stringWithFormat:@"%@ %@",[langDictionary valueForKey:@"LabelThird"],[langDictionary valueForKey:@"LabelMap"]],[NSString stringWithFormat:@"%@ %@",[langDictionary valueForKey:@"LabelFourth"],[langDictionary valueForKey:@"LabelMap"]],[NSString stringWithFormat:@"%@ %@",[langDictionary valueForKey:@"LabelFifth"],[langDictionary valueForKey:@"LabelMap"]],[NSString stringWithFormat:@"%@ %@",[langDictionary valueForKey:@"LabelSixth"],[langDictionary valueForKey:@"LabelMap"]],[NSString stringWithFormat:@"%@ %@",[langDictionary valueForKey:@"LabelSeventh"],[langDictionary valueForKey:@"LabelMap"]],[NSString stringWithFormat:@"%@ %@",[langDictionary valueForKey:@"LabelEighth"],[langDictionary valueForKey:@"LabelMap"]],[NSString stringWithFormat:@"%@ %@",[langDictionary valueForKey:@"LabelNinth"],[langDictionary valueForKey:@"LabelMap"]],[NSString stringWithFormat:@"%@ %@",[langDictionary valueForKey:@"LabelTenth"],[langDictionary valueForKey:@"LabelMap"]], nil];
        
        for(int i=0;i<count;i++)    {
            [self.segmentControl insertSegmentWithTitle:[array objectAtIndex:i] atIndex:i animated:NO];
        }
        
        [self.segmentControl setSelectedSegmentIndex:0];
        
        //attach target action for if the selection is changed by the user
        [self.segmentControl addTarget:self
                                action:@selector(selectedSegmentIndex:)
                      forControlEvents:UIControlEventValueChanged];
        
        [self loadMapOfIndex:1];
    }
    
    [UIView animateWithDuration:1.f animations:^{
        self.segmentControl.alpha = 1.f;
    }];
    
    //    if([SharedAppDelegate playerHidden] == NO)  {
    //        self.scrollViewBottomSpaceConstraint.constant = 65;
    //        [SharedAppDelegate animatePlayerHidden:NO];
    //    }
    
    NSString *downloadStatus = [[NSUserDefaults standardUserDefaults] valueForKey:self.key];
    
    if([downloadStatus intValue]==StatusDownloaded)   {
        self.status = StatusDownloaded;
    }
}

- (void)selectedSegmentIndex:(UISegmentedControl *)paramSender
{
    //get index position for the selected control
    NSInteger selectedIndex = [paramSender selectedSegmentIndex];
    //NSLog(@"%ld",(long)selectedIndex);
    
    for(UIView *view in self.scrollViewContainer.subviews)  {
        [view removeFromSuperview];
    }
    
    [self loadMapOfIndex:selectedIndex+1];
}

- (void)loadMapOfIndex:(NSInteger)index
{
    MTImageMapView *imageView;
    NSString *filePath = [NSString stringWithFormat:@"%@/%@/map%ld.png",_destinationDir,kMap,(long)index];
    if([[NSFileManager defaultManager] fileExistsAtPath:filePath])  {
        UIImage *image = [UIImage imageWithContentsOfFile:filePath];
        imageView = [[MTImageMapView alloc] initWithImage:image];
        imageView.delegate = self;
        imageInitialRect = CGRectMake(0, 0, image.size.width, image.size.height);
        imageView.frame = imageInitialRect;
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        ////
        
        _backView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
        
        
        //
        [self.scrollViewContainer addSubview:_backView];
        [_backView addSubview:imageView];
        
        
        CGFloat scaleFactor = imageView.frame.size.height/imageView.frame.size.height;
        
        for (UIView *view in [imageView subviews])
        {
            [view removeFromSuperview];
        }
        
        
        //        NSString *ParentGuideReferenceNUmber =[_audioGuideInfo.referenceNo  stringByTrimmingCharactersInSet:[NSCharacterSet lowercaseLetterCharacterSet]];
        //
        //        NSInteger referencenumber = [ParentGuideReferenceNUmber integerValue];
        NSInteger sessoionIndex;
        if (self.segmentControl.selectedSegmentIndex == 0) {
            sessoionIndex = 1;
        }
        else
        {
            sessoionIndex = 2;
        }
        
        
        
        NSArray *arrayOfGeoMapRooms = [[SharedAppDelegate database]fetchGeoMapRoomsBySessionIndex:sessoionIndex];
        NSMutableArray *currentRcircleCoordinates = [[NSMutableArray alloc]init];
        //    CGFloat scaleFactor = imageHeight/mapImage.size.height;
        
        for(IBGeoMapRooms *mapRoom in arrayOfGeoMapRooms)
        {
            
            NSArray *arrayCircleCoordintes = [mapRoom.circleCoordinates componentsSeparatedByString:@","];
            
            //NSLog(@"%@",mapRoom.roomNo);
            //NSLog(@"%@",arrayCircleCoordintes);
            
            for(NSString *coordinateString in arrayCircleCoordintes)
            {
                [currentRcircleCoordinates addObject:[NSNumber numberWithDouble:([coordinateString doubleValue]*scaleFactor)]];
            }
            
            
            _iBcircleView = [[UIView alloc]initWithFrame:CGRectMake([[currentRcircleCoordinates objectAtIndex:0] doubleValue] - 20, [[currentRcircleCoordinates objectAtIndex:1]doubleValue ] - 20 , 50, 50)];
            
            labelText = [[UILabel alloc]init];
            if ([mapRoom.roomNo integerValue] <= 9)
            {
                labelText.frame =CGRectMake(18, 8, 50, 30);
            }
            else if ([mapRoom.roomNo integerValue] == 35)
            {
                labelText.frame =CGRectMake(10, 5, 50, 30);
            }
            
            else
            {
                labelText.frame =CGRectMake(13, 8, 50, 30);
            }
            
            _iBcircleView.tag = [mapRoom.roomNo integerValue] + 200;
//            labelText.text = [NSString stringWithFormat:@"%@",mapRoom.roomNo];
            labelText.text = [NSString stringWithFormat:@"%d",[mapRoom.roomNo intValue]];
            labelText.font = kAppFontRegular(24);
            labelText.textColor = [UIColor whiteColor];
            [_iBcircleView addSubview:labelText];
            
            _iBcircleView.layer.cornerRadius = 25;
            _iBcircleView.layer.masksToBounds = YES;
            
            _iBcircleView.backgroundColor = RGBCOLOR(116, 141, 149);
            
            
            //[_iBcircleView setNeedsDisplay];
            
            //NSLog(@"%@",NSStringFromCGRect(_iBcircleView.frame));
            // [_iBcircleView drawRect:_iBcircleView.frame number:referencenumber];
            [imageView addSubview:_iBcircleView];
            [currentRcircleCoordinates removeAllObjects];
            
        }
        
        
        
        
        self.scrollViewContainer.contentSize = imageView.bounds.size;
        
        self.scrollViewContainer.minimumZoomScale = 0.1f;
        self.scrollViewContainer.zoomScale = 0.5f;
        self.scrollViewContainer.maximumZoomScale = 0.9f;
        //    self.scrollViewContainer.zoomScale = [SharedAppDelegate zoomScale];
        
        [self centerScrollViewContents];
        
        [self.scrollViewContainer setContentOffset:contentOffset animated:YES];
        
        NSMutableArray *arrayRooms = [NSMutableArray array];
        NSMutableArray *arrayIndexes = [NSMutableArray array];
        NSArray *arrayRoomObjects = [[SharedAppDelegate database] fetchGeoMapRoomsBySessionIndex:index];
        for(IBGeoMapRooms *geoMapRoomInfo in arrayRoomObjects)   {
            [arrayIndexes addObject:geoMapRoomInfo.roomNo];
            [arrayRooms addObject:geoMapRoomInfo.coordinates];
        }
        
        _roomIndices = [NSArray arrayWithArray:arrayIndexes];
        _roomCoordinates = [NSArray arrayWithArray:arrayRooms];
        
        [imageView
         setMapping:_roomCoordinates
         doneBlock:^(MTImageMapView *imageMapView) {
             //NSLog(@"Areas are all mapped");
         }];
        
        self.scrollViewContainer.alpha =0.f;
        [UIView animateWithDuration:1.0 animations:^{
            self.scrollViewContainer.alpha = 1.f;
        }];
        
        [self.scrollViewContainer setScrollsToTop:YES];
    }
}

- (IBAction)buttonMapListTouched:(id)sender {
    [self dismissViewControllerAnimated:NO completion:^{
        //NSLog(@"Dismissed mapview");
    }];
}

-(void)imageMapView:(MTImageMapView *)inImageMapView
   didSelectMapArea:(NSUInteger)inIndexSelected
{
    [audioGuideTable removeFromSuperview];
    [audioGuideTable setRoomNo:[_roomIndices objectAtIndex:inIndexSelected]];
    //NSLog(@"%@",audioGuideTable.roomNo);
    
    //    if (labelText.tag == [audioGuideTable.roomNo integerValue]) {
    //        labelText.backgroundColor = [UIColor clearColor];
    //    }
    //    else
    //    {
    //       labelText.backgroundColor = [UIColor clearColor];
    //
    //    }
    
    if([audioGuideTable.arrayAudioGuide count]>0)   {
        
        CGRect rect = audioGuideTable.frame;
        if([audioGuideTable.arrayAudioGuide count]>3)   {
            rect.size.height = 410;
        }
        else    {
            rect.size.height = ([audioGuideTable.arrayAudioGuide count]*110)+30;
        }
        
        NSString *stringPolygonCoordinates = [_roomCoordinates objectAtIndex:inIndexSelected];
        NSArray *arrayPolygonCoordinates = [stringPolygonCoordinates componentsSeparatedByString:@","];
        
        NSMutableArray *xIndeces = [NSMutableArray array];
        NSMutableArray *yIndices = [NSMutableArray array];
        
        
//        if ((self.segmentControl.selectedSegmentIndex == 1 && (inIndexSelected == 6 | inIndexSelected == 5)) | (inIndexSelected == 19 | inIndexSelected == 18)  )
//        {
//            NSString *stringPolygonCoordinatesSecond;
//            
//            if(inIndexSelected == 19 | inIndexSelected == 6)
//            {
//                stringPolygonCoordinatesSecond =[_roomCoordinates objectAtIndex:inIndexSelected - 1];
//            }
//            else
//            {
//                stringPolygonCoordinatesSecond =[_roomCoordinates objectAtIndex:inIndexSelected + 1];
//            }
//            
//            NSArray *      arrayPolygonCoordinatesSecond = [stringPolygonCoordinatesSecond componentsSeparatedByString:@","];
//            NSArray *combinedTwoPlogonArray = [NSArray arrayWithObjects:arrayPolygonCoordinates,arrayPolygonCoordinatesSecond, nil];
//            [self drawHighlightedLayerUsingCoordinatesSecond:combinedTwoPlogonArray];
//            
//            
//        }
//        else
//        {
            [self drawHighlightedLayerUsingCoordinates:arrayPolygonCoordinates];
//        }
        
        int i = 0;
        for(NSString *coordString in arrayPolygonCoordinates)   {
            if (i % 2)  {
                [yIndices addObject:[NSNumber numberWithInteger:[coordString integerValue]]];
            }
            else    {
                [xIndeces addObject:[NSNumber numberWithInteger:[coordString integerValue]]];
            }
            i++;
        }
        
        CGFloat minX = [[xIndeces valueForKeyPath:@"@min.self"] doubleValue];
        CGFloat maxX = [[xIndeces valueForKeyPath:@"@max.self"] doubleValue];
        CGFloat minY = [[yIndices valueForKeyPath:@"@min.self"] doubleValue];
        CGFloat maxY = [[yIndices valueForKeyPath:@"@max.self"] doubleValue];
        
        if (minX <= 529 && minX >= 487 | minX == 380) {
            minX = minX - 230;
        }
        if (maxX <= 630 && maxX >= 565 | maxX == 656)
        {
            maxX = maxX  - 230;
        }
        if (minX == 410)
        {
            minX = minX - 230;
        }
        
        
        CGPoint topCentreTagPoint = CGPointMake((minX+(maxX-minX)/2)-(rect.size.width/2), minY-rect.size.height-10);
        CGPoint leftUpTagPoint = CGPointMake(minX-rect.size.width-10, maxY-rect.size.height);
        CGPoint rightUpTagPoint = CGPointMake(maxX+10, maxY-rect.size.height);
        CGPoint bottomCentreTagPoint = CGPointMake((minX+(maxX-minX)/2)-(rect.size.width/2), maxY+10);
        CGPoint leftDownTagPoint = CGPointMake(minX-rect.size.width-10, minY);
        CGPoint rightDownTagPoint = CGPointMake(maxX+10, minY);
        
        NSArray *prefferedTagPoints = [NSArray arrayWithObjects:[NSValue valueWithCGPoint:topCentreTagPoint],[NSValue valueWithCGPoint:leftUpTagPoint],[NSValue valueWithCGPoint:rightUpTagPoint],[NSValue valueWithCGPoint:bottomCentreTagPoint],[NSValue valueWithCGPoint:leftDownTagPoint],[NSValue valueWithCGPoint:rightDownTagPoint], nil];
        
        MTImageMapView *imageView = (MTImageMapView *)[self.backView.subviews objectAtIndex:0];
        
        int j = 0;
        for(NSValue *tagOriginValue in prefferedTagPoints)  {
            CGPoint prefferedOrigin = [tagOriginValue CGPointValue];
            rect.origin = prefferedOrigin;
            audioGuideTable.frame = rect;
            
            if(CGRectContainsRect(imageInitialRect, audioGuideTable.frame))  {
                break;
            }
            j++;
        }
        
        if(j==0)    {
            [audioGuideTable setTagDirection:IBTagDirectionDown];
        }
        else if(j==1) {
            [audioGuideTable setTagDirection:IBTagDirectionTopRight];
        }
        else if (j==2)  {
            [audioGuideTable setTagDirection:IBTagDirectionBottomLeft];
        }
        else if (j==3)  {
            [audioGuideTable setTagDirection:IBTagDirectionUp];
        }
        else if (j==4)  {
            [audioGuideTable setTagDirection:IBTagDirectionBottomRight];
        }
        else if (j==5)  {
            [audioGuideTable setTagDirection:IBTagDirectionTopLeft];
        }
        
        
        
        [imageView insertSubview:audioGuideTable atIndex:0];
        [imageView insertSubview:audioGuideTable aboveSubview:imageView];
        
        // [self drawHighlightedLayerUsingCoordinates:arrayPolygonCoordinates];
        
        //        [self.scrollViewContainer zoomToPoint:leftUpTagPoint withScale:[SharedAppDelegate zoomScale] animated:YES];
    }
}
- (void)drawHighlightedLayerUsingCoordinatesSecond:(NSArray *)combinedArray
{
    if(polyLayer) { [polyLayer removeFromSuperlayer]; }
    
    MTImageMapView *imageView = (MTImageMapView*)[_backView.subviews objectAtIndex:0];
    
    polyLayer = [CAShapeLayer layer];
    
    [polyLayer setBounds:CGRectMake(0.0f, 0.0f, [imageView bounds].size.width, [imageView bounds].size.height)];
    
    [polyLayer setPosition:CGPointMake([imageView bounds].size.width/2.0f, [imageView bounds].size.height/2.0f)];
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    for(NSArray *coordinates in combinedArray)
    {
        
        [path moveToPoint:CGPointMake([[coordinates objectAtIndex:0] integerValue], [[coordinates objectAtIndex:1] integerValue])];
        
        NSInteger firstPoint = 0, secondPoint = 0;
        
        for (NSInteger i = 2; i < [coordinates count]; i++)
        {
            if (i%2 == 0)
            {
                firstPoint = [[coordinates objectAtIndex:i] integerValue];
            }
            else
            {
                secondPoint = [[coordinates objectAtIndex:i] integerValue];
                [path addLineToPoint:CGPointMake(firstPoint, secondPoint)];
            }
        }
        
        [path closePath];
        
        [polyLayer setPath:[path CGPath]];
        
        //  [polyLayer setStrokeColor:[RGBCOLOR(246.f, 151.f, 158.f) CGColor]];
        [polyLayer setFillColor:[RGBCOLOR_WITH_ALPHA(201, 51, 60, 1) CGColor]];
        [polyLayer setLineWidth:3.0f];
        
        [imageView removeFromSuperview];
        [[_backView layer] addSublayer:polyLayer];
        [_backView addSubview:imageView];
    }
    
    
    
    for (UIView *view in [imageView subviews])
    {
        if (view.tag == previousCircleViewTag + 200 && (previousCircleViewTag + 200 != [audioGuideTable.roomNo integerValue] + 200))
        {
            //   view.hidden = NO;
            view.backgroundColor = RGBCOLOR(116, 141, 149);
            
        }
        else if (view.tag == [audioGuideTable.roomNo integerValue] + 200)
        {
            if (view.tag == 235)
            {
                view.backgroundColor =[UIColor clearColor];
                
            }
            else
            {
                //   view.hidden = YES;
                [view viewWithTag:[audioGuideTable.roomNo integerValue] + 200].backgroundColor = [UIColor clearColor];
            }
            
        }
    }
    previousCircleViewTag = [audioGuideTable.roomNo integerValue] ;
    
    
}

- (void)imageMapViewTouchedOnIvalidArea {
    [audioGuideTable removeFromSuperview];
}

- (void)drawHighlightedLayerUsingCoordinates:(NSArray *)coordinates
{
    // [_iBcircleView viewWithTag:[audioGuideTable.roomNo integerValue]].backgroundColor = [UIColor clearColor];
    
    
    
    
    if(polyLayer) { [polyLayer removeFromSuperlayer]; }
    
    MTImageMapView *imageView = (MTImageMapView*)[self.backView.subviews objectAtIndex:0];
    
    
    polyLayer = [CAShapeLayer layer];
    
    [polyLayer setBounds:CGRectMake(0.0f, 0.0f, [imageView bounds].size.width, [imageView bounds].size.height)];
    
    [polyLayer setPosition:CGPointMake([imageView bounds].size.width/2.0f, [imageView bounds].size.height/2.0f)];
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    [path moveToPoint:CGPointMake([[coordinates objectAtIndex:0] integerValue], [[coordinates objectAtIndex:1] integerValue])];
    
    NSInteger firstPoint = 0, secondPoint = 0;
    
    for (NSInteger i = 2; i < [coordinates count]; i++)
    {
        if (i%2 == 0)
        {
            firstPoint = [[coordinates objectAtIndex:i] integerValue];
        }
        else
        {
            secondPoint = [[coordinates objectAtIndex:i] integerValue];
            [path addLineToPoint:CGPointMake(firstPoint, secondPoint)];
        }
    }
    
    [path closePath];
    
    [polyLayer setPath:[path CGPath]];
    
//    [polyLayer setStrokeColor:[RGBCOLOR(201, 51, 60) CGColor]];
    [polyLayer setFillColor:[RGBCOLOR_WITH_ALPHA(201, 51, 60, 1.0) CGColor]];
    //   [polyLayer setFillColor:[RGBCOLOR(201, 51, 60) CGColor]];
    [polyLayer setLineWidth:3.0f];
    
    //NSLog(@"%@",audioGuideTable.roomNo);
    
    
    for (UIView *view in [imageView subviews])
    {
        if (view.tag == previousCircleViewTag + 200 && (previousCircleViewTag + 200 != [audioGuideTable.roomNo integerValue] + 200))
        {
            
            view.backgroundColor = RGBCOLOR(116, 141, 149);
            
        }
        else if (view.tag == [audioGuideTable.roomNo integerValue] + 200)
        {
            
            //   view.hidden = YES;
            [view viewWithTag:[audioGuideTable.roomNo integerValue] + 200].backgroundColor = [UIColor clearColor];
            
            
        }
    }
    previousCircleViewTag = [audioGuideTable.roomNo integerValue] ;
    
    [imageView removeFromSuperview];
    [[_backView layer] addSublayer:polyLayer];
    [_backView addSubview:imageView];
}

//-(UIView *) circleDraywer: (CGPoint) point
//{
//    UIView *roundView = [[UIView alloc]initWithFrame:CGRectMake((point.x - 10), (point.y - 15), 30, 30)];
//    roundView.layer.cornerRadius = 15;
//    roundView.layer.masksToBounds = YES;
//    roundView.backgroundColor = [UIColor yellowColor];
//    return roundView;
//}
//
//-(UILabel *)labelDrawyer:(CGPoint)point
//{
//    UILabel *roundLabel = [[UILabel alloc]initWithFrame:CGRectMake((point.x - 10), (point.y - 15), 20, 30)];
//    roundLabel.layer.cornerRadius = 5;
//
//    roundLabel.text = @"9";
//    roundLabel.font = kAppFontBold(24);
//    roundLabel.backgroundColor = [UIColor greenColor];
//    roundLabel.textColor = RGBCOLOR(111, 134, 142);
//    return roundLabel;
//
//
//}

- (void)centerScrollViewContents {
    CGSize boundsSize = self.scrollViewContainer.bounds.size;
    CGRect contentsFrame = [(MTImageMapView*)[self.scrollViewContainer.subviews objectAtIndex:0] frame];
    
    if (contentsFrame.size.width < boundsSize.width) {
        contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
    } else {
        contentsFrame.origin.x = 0.0f;
    }
    
    if (contentsFrame.size.height < boundsSize.height) {
        contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
    } else {
        contentsFrame.origin.y = 0.0f;
    }
    
    [(MTImageMapView*)[self.scrollViewContainer.subviews objectAtIndex:0] setFrame:contentsFrame];
}

- (UIView*)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    // Return the view that we want to zoom
    return (MTImageMapView*)[self.scrollViewContainer.subviews objectAtIndex:0];
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    // The scroll view has zoomed, so we need to re-center the contents
    
    
    [SharedAppDelegate setZoomScale:self.scrollViewContainer.zoomScale];
    [self centerScrollViewContents];
}

-(void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view
{
    
}

#pragma mark -
#pragma IBAudioGuideTableDelegate methods

- (void)audioGuideSelected:(IBAudioGuideInfo *)guideInfo
{
    _currentGuideInfo = guideInfo;
    
    [audioGuideTable removeFromSuperview];
    
    [alertView dismissWithClickedButtonIndex:0 animated:NO];
    [SharedAppDelegate setZoomScale:self.scrollViewContainer.zoomScale];
    contentOffset = self.scrollViewContainer.contentOffset;
    
    IBGuideDetailViewControllerTablet *guideDetailController = [SharedAppDelegate guideDetailViewController];
    
    NSArray *arrayGuideGallery = [[SharedAppDelegate database] fetchGuideGalleryByAudioReferenceIndex:guideInfo.referenceNo];
    
    NSMutableArray *arrayImagePaths = [NSMutableArray array];
    for(IBGuideGallery *guideGalleryInfo in arrayGuideGallery)  {
        [arrayImagePaths addObject:guideGalleryInfo.guideGalleryFilePath];
    }
    //NSLog(@"%@",arrayImagePaths);
    NSInteger locationNo = [SharedAppDelegate locationNo];
    NSString *tempCoverPath = [[SharedAppDelegate database] loadCoverPhotoOfCurrespondWithLocationNo:locationNo andReferenceNo:guideInfo.referenceNo];
    guideDetailController.arrayImagePathsGallery = [NSArray arrayWithArray:arrayImagePaths];
    guideDetailController.arrayImagePaths = [[NSArray alloc]initWithObjects:tempCoverPath, nil];
    
    [SharedAppDelegate setSelectedGuideTitleForGallery:guideInfo.guideTitle ] ;
    
    guideDetailController.audioGuideInfo = guideInfo;
    
    if([[SharedAppDelegate childSplitViewController] showThirdContainer]!=YES)  {
        [[SharedAppDelegate childSplitViewController] setShowThirdContainer:YES];
    }
    
    guideDetailController.view.alpha = 0;
    [self dismissViewControllerAnimated:YES completion:^{
        [guideDetailController refreshView];
        guideDetailController.view.alpha = 1;
    }];
}

#pragma mark -

- (NSString *)key
{
    NSString *keyString = [[[_urlString stringByReplacingOccurrencesOfString:SYNC_URL withString:@""] base64EncodedString] uppercaseString];
    
    NSMutableString *newString = [NSMutableString string];
    for (int i = 0; i < [keyString length]; i++)
    {
        int ascii = [keyString characterAtIndex:i];
        if (((ascii>64) && (ascii<91)) || ((ascii>47) && (ascii<58)))
        {
            [newString appendFormat:@"%c",ascii];
        }
    }
    
    return [NSString stringWithString:newString];
}

@end
