//
//  IBGetInfoViewController.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 19/05/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBGetInfoViewController.h"

#import "IBGlobal.h"

@interface IBGetInfoViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UILabel *labelTitle;
}

@property (weak, nonatomic) IBOutlet UILabel *labelVersion;
@property (weak, nonatomic) IBOutlet UITableView *tableViewInfos;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *versionViewTopSpaceConstraint;
@property (strong, nonatomic) NSArray *arrayInfos;

- (void)refreshView;
- (void)buttonBackTouched;

@end

@implementation IBGetInfoViewController

@synthesize arrayInfos;


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (IOS_OLDER_THAN(6.0))    {
        [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:NO];
    }
    
    if (IOS_NEWER_OR_EQUAL_TO(7.0))    {
        self.extendedLayoutIncludesOpaqueBars=NO;
        self.automaticallyAdjustsScrollViewInsets=NO;
    }
    else    {
        self.versionViewTopSpaceConstraint.constant = 14;
    }
    
    self.tableViewInfos.delegate = self;
    self.tableViewInfos.dataSource = self;
    self.tableViewInfos.bounces = NO;
    self.tableViewInfos.scrollEnabled = NO;
    self.tableViewInfos.allowsSelection = YES;
    
    UIButton *buttonBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonBack setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [buttonBack setFrame:CGRectMake(0, 0, 12, 21)];
    [buttonBack addTarget:self action:@selector(buttonBackTouched) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem =
    [[UIBarButtonItem alloc] initWithCustomView:buttonBack];
    
    labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    labelTitle.backgroundColor = [UIColor clearColor];
    labelTitle.textColor = [UIColor blackColor];
    labelTitle.font = kAppFontSemibold(15);
    labelTitle.text = @"Your Title Header Add Here";
    labelTitle.textAlignment = NSTextAlignmentCenter;
    [labelTitle sizeToFit];
    
    self.navigationItem.titleView = labelTitle;
    
    UIView *viewNext = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 12, 21)];
    UIBarButtonItem *buttonRight = [[UIBarButtonItem alloc] initWithCustomView:viewNext];
    self.navigationItem.rightBarButtonItem = buttonRight;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    labelTitle.text = @"";
    [self navigationController].navigationBarHidden = NO;
    [self.view setBackgroundColor:RGBCOLOR(200, 200, 200)];
    
    [self refreshView];
}

- (BOOL)shouldAutorotate    {
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations    {
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    UIViewController *controller = [segue destinationViewController];
    if ([segue.identifier isEqualToString:@"segueToTerms"])
    {
        //segue to terms
        [controller.view setTag:1];
    }
    else    {
        //segue to credits
        [controller.view setTag:2];
    }
}

#pragma mark - Table view data source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrayInfos count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{ 
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = kAppFontSemibold(16);
    }
    
    cell.textLabel.text = [self.arrayInfos objectAtIndex:indexPath.row];
    cell.textLabel.textColor = [UIColor blackColor];
    
    return cell;
}

#pragma mark -
#pragma mark - Table view delegates

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:@"segueToCredits" sender:self];
    
 //   if(indexPath.row==0)    {
   //     [self performSegueWithIdentifier:@"segueToTerms" sender:self];
  //  }
  //  else    {
  //      [self performSegueWithIdentifier:@"segueToCredits" sender:self];
  //  }
}

#pragma mark -

- (void)refreshView
{
    NSDictionary *langDictionary = [SharedAppDelegate langDictionary];
    
    labelTitle.text = [langDictionary valueForKey:@"LabelInfos"];
    self.labelVersion.text = [langDictionary valueForKey:@"LabelVersion"];
    
    self.arrayInfos = [NSArray arrayWithObjects:[langDictionary valueForKey:@"LabelCredits"], nil];
    
    [self.tableViewInfos reloadData];
}

- (void)buttonBackTouched
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
