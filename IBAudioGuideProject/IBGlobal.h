//
//  IBGlobal.h
//  IBAudioGuideProject
//
//  Created by Jojin Johnson on 22/04/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

#import "IBAppDelegate.h"

#import "IBGalleryImages.h"
#import "IBLanguages+CoreDataClass.h"
#import "IBGuideGallery.h"
#import "IBMuseums.h"
#import "IBAudioGuide.h"
#import "IBGeoMapRooms.h"
#import "IBRoomAudioGuides.h"

#import "IBMuseumRoomInfo.h"
#import "IBGalleryImageXMLInfo.h"
#import "IBAudioGuideXMLInfo.h"
#import "IBAudioGuideInfo.h"

#import "MBProgressHUD.h"

extern NSString *const IBNotificationTriggerDownload;
extern NSString *const SYNC_URL;
extern BOOL const iSInAppPurchaseEnabled;

// Application Properties
#define ORGANIZATION_NAME     @"Gruppo Zenit"
#define ORGANIZATION_DOMAIN   @"www.gruppozenit.com"
#define AUTHOR_NAME           @"E-Team Informatica India Pvt. Ltd."
#define APPLICATION_NAME      @"IBAudioGuide"
#define APPLICATION_NAME_LONG @"Isole Borromee Audio Guide"
#define APPLICATION_VERSION   @"1.0.0"
#define APPLICATION_YEARS     @"2014-2015"
#define APPLICATION_DOMAIN    @""

#define IBParentDirectory @"borromee-mobile"

#define ISOLA_BELLA @"isole-bella"
#define ISOLA_MADRE @"isole-madre"
#define ROCCA_ANGERA @"rocca-angera"

#define ENGLISH @"en"
#define DEUTSCH @"de"
#define FRENCH @"fr"
#define ITALIAN @"it"

#define DIRLOCATION_STR(tag,dir) {if(tag==1){dir=ISOLA_BELLA;}else if(tag==2){dir=ISOLA_MADRE;}else{dir=ROCCA_ANGERA;}}

#define DIRLANGUAGE_STR(index,dir) {if (index==0){dir=ENGLISH;} else if(index==1){dir=ITALIAN;} else if(index==2){dir=FRENCH;} else {dir=DEUTSCH;} }

#define MUSEUMNAME_STR(locationNo,museumName) {if(locationNo==1){museumName=[[SharedAppDelegate langDictionary] valueForKey:@"LabelBella"];}else if(locationNo==2){museumName=[[SharedAppDelegate langDictionary] valueForKey:@"LabelMadre"];}else{museumName=[[SharedAppDelegate langDictionary] valueForKey:@"LabelAngera"];}}

#define kMap @"map"
#define kGallery @"photo-gallery"
#define kAudioGuide @"audio-guides"
#define kGuidesGallery @"guides-gallery"

#define kMapXml [NSString stringWithFormat:@"%@.xml",kMap]
#define kMapZip [NSString stringWithFormat:@"%@.zip",kMap]
#define kGalleryXml [NSString stringWithFormat:@"%@.xml",kGallery]
#define kGalleryZip [NSString stringWithFormat:@"%@.zip",kGallery]
#define kAudioGuideXml [NSString stringWithFormat:@"%@.xml",kAudioGuide]
#define kAudioGuideZip [NSString stringWithFormat:@"%@.zip",kAudioGuide]
#define kGuidesGalleryZip [NSString stringWithFormat:@"%@.zip",kGuidesGallery]

#define kThumbExtension @"jpg"
#define kImageExtension @"jpg"
#define kAudioExtension @"mp3"
// Application Shared defenitions
#define SharedAppDelegate (IBAppDelegate *)[[UIApplication sharedApplication] delegate]

#define RGBCOLOR(r, g, b) [UIColor colorWithRed:r/225.0f green:g/225.0f blue:b/225.0f alpha:1]
#define RGBCOLOR_WITH_ALPHA(r, g, b, a) [UIColor colorWithRed:r/225.0f green:g/225.0f blue:b/225.0f alpha:a]

#define DEVICE_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define DEVICE_HEIGHT ([[UIScreen mainScreen] bounds].size.height)

#define IOS_VERSION [[[UIDevice currentDevice] systemVersion] floatValue]
#define IOS_OLDER_THAN(v) ([[[UIDevice currentDevice] systemVersion] floatValue] < v)
#define IOS_NEWER_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] floatValue ] >= v)

#define kHighlightedColor RGBCOLOR(104,144,152)

#define kAppFontRegular(s) [UIFont fontWithName:@"OpenSans" size:s]
#define kAppFontBold(s) [UIFont fontWithName:@"OpenSans-Bold" size:s]
#define kAppFontLight(s) [UIFont fontWithName:@"OpenSans-Light" size:s]
#define kAppFontItalic(s) [UIFont fontWithName:@"OpenSans-Italic" size:s]
#define kAppFontSemibold(s) [UIFont fontWithName:@"OpenSans-Semibold" size:s]
#define kAppFontExtrabold(s) [UIFont fontWithName:@"OpenSans-Extrabold" size:s]
#define kAppFontBoldItalic(s) [UIFont fontWithName:@"OpenSans-BoldItalic" size:s]
#define kAppFontLightItalic(s) [UIFont fontWithName:@"OpenSansLight-Italic" size:s]
#define kAppFontSemiboldItalic(s) [UIFont fontWithName:@"OpenSans-SemiboldItalic" size:s]
#define kAppFontExtraboldItalic(s) [UIFont fontWithName:@"OpenSans-ExtraboldItalic" size:s]

#define kPGLeastPrize [SharedAppDelegate pgLeastPrizeString]
#define kPGComboPrize [SharedAppDelegate pgComboPrizeString]
#define kPGDefaultPrize [SharedAppDelegate pgDefaultPrizeString]

#define kFirstRestoreKey @"FirstRestoreState"
#define kComboPurchaseKey @"ComboPurchaseState"

#define IS_IPHONE (!IS_IPAD)
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPhone)

typedef enum {
    iphone_standard,
    iphone_taller,
    unknown_device
}device_type;

typedef enum {
    first_restore,
    Once_restored
}app_restore_state;

@interface IBGlobal : NSObject

+ (BOOL)isValidEmailAddress:(NSString *)inputText;
+ (NSString *)createDocumentsDirectoryByAppendingPathComponent:(NSString *)pathComponent;
+ (NSString *)copyResorceFile:(NSString *)resourceFilename withExtention:(NSString *)extention ToDocumentsDirectoryByAppendingPathComponent:(NSString *)pathComponent andDestinationFileName:(NSString *)destinatoionFilename;
+ (CGFloat)textViewHeightForAttributedText:(NSAttributedString *)text andWidth:(CGFloat)width;
+ (CGSize)text:(NSString *)text sizeWithFont:(UIFont *)font constrainedToSize:(CGSize)size;
+ (CGFloat)heightFromString:(NSString*)text withFont:(UIFont*)font constraintToWidth:(CGFloat)width;
+ (NSString *)styledAppHtmlWithString:(NSString *)html andTextSize:(int)pointSize;
+ (NSString *) stringByStrippingHTML:(NSString *)html;
+ (NSAttributedString *)attributedStringWithHTML:(NSString *)html;
+ (device_type)device;
+ (NSArray *)loadFilesAtDirectory:(NSString *)dirPath;
+ (NSString *)keyForUrlString:(NSString *)urlString;
+ (UIImage *)imageWithColor:(UIColor *)color;
+(UIColor*)colorWithHexString:(NSString*)hex;
@end
