//
//  IBGlobal.m
//  IBAudioGuideProject
//
//  Created by Jojin Johnson on 22/04/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBGlobal.h"
#import "Base64.h"

NSString *const IBNotificationTriggerDownload = @"__NotificationTriggerDownload__";


@implementation IBGlobal

+ (BOOL)isValidEmailAddress:(NSString *)inputText {
    //
    NSString *emailRegex = @"[A-Z0-9a-z][A-Z0-9a-z._%+-]*@[A-Za-z0-9][A-Za-z0-9.-]*\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    NSRange aRange;
    //
    if([emailTest evaluateWithObject:inputText]) {
        //
        aRange = [inputText rangeOfString:@"." options:NSBackwardsSearch range:NSMakeRange(0, [inputText length])];
        NSInteger indexOfDot = aRange.location;
        //
        if(aRange.location != NSNotFound) {
            //
            NSString *topLevelDomain = [inputText substringFromIndex:indexOfDot];
            topLevelDomain = [topLevelDomain lowercaseString];
            //
            NSSet *TLD = [NSSet setWithObjects:@".aero", @".asia", @".biz", @".cat", @".com", @".coop", @".edu", @".gov", @".info", @".int", @".jobs", @".mil", @".mobi", @".museum", @".name", @".net", @".org", @".pro", @".tel", @".travel", @".ac", @".ad", @".ae", @".af", @".ag", @".ai", @".al", @".am", @".an", @".ao", @".aq", @".ar", @".as", @".at", @".au", @".aw", @".ax", @".az", @".ba", @".bb", @".bd", @".be", @".bf", @".bg", @".bh", @".bi", @".bj", @".bm", @".bn", @".bo", @".br", @".bs", @".bt", @".bv", @".bw", @".by", @".bz", @".ca", @".cc", @".cd", @".cf", @".cg", @".ch", @".ci", @".ck", @".cl", @".cm", @".cn", @".co", @".cr", @".cu", @".cv", @".cx", @".cy", @".cz", @".de", @".dj", @".dk", @".dm", @".do", @".dz", @".ec", @".ee", @".eg", @".er", @".es", @".et", @".eu", @".fi", @".fj", @".fk", @".fm", @".fo", @".fr", @".ga", @".gb", @".gd", @".ge", @".gf", @".gg", @".gh", @".gi", @".gl", @".gm", @".gn", @".gp", @".gq", @".gr", @".gs", @".gt", @".gu", @".gw", @".gy", @".hk", @".hm", @".hn", @".hr", @".ht", @".hu", @".id", @".ie", @" No", @".il", @".im", @".in", @".io", @".iq", @".ir", @".is", @".it", @".je", @".jm", @".jo", @".jp", @".ke", @".kg", @".kh", @".ki", @".km", @".kn", @".kp", @".kr", @".kw", @".ky", @".kz", @".la", @".lb", @".lc", @".li", @".lk", @".lr", @".ls", @".lt", @".lu", @".lv", @".ly", @".ma", @".mc", @".md", @".me", @".mg", @".mh", @".mk", @".ml", @".mm", @".mn", @".mo", @".mp", @".mq", @".mr", @".ms", @".mt", @".mu", @".mv", @".mw", @".mx", @".my", @".mz", @".na", @".nc", @".ne", @".nf", @".ng", @".ni", @".nl", @".no", @".np", @".nr", @".nu", @".nz", @".om", @".pa", @".pe", @".pf", @".pg", @".ph", @".pk", @".pl", @".pm", @".pn", @".pr", @".ps", @".pt", @".pw", @".py", @".qa", @".re", @".ro", @".rs", @".ru", @".rw", @".sa", @".sb", @".sc", @".sd", @".se", @".sg", @".sh", @".si", @".sj", @".sk", @".sl", @".sm", @".sn", @".so", @".sr", @".st", @".su", @".sv", @".sy", @".sz", @".tc", @".td", @".tf", @".tg", @".th", @".tj", @".tk", @".tl", @".tm", @".tn", @".to", @".tp", @".tr", @".tt", @".tv", @".tw", @".tz", @".ua", @".ug", @".uk", @".us", @".uy", @".uz", @".va", @".vc", @".ve", @".vg", @".vi", @".vn", @".vu", @".wf", @".ws", @".ye", @".yt", @".za", @".zm", @".zw", @".mob", nil];
            //
            if(topLevelDomain != nil && ([TLD containsObject:topLevelDomain])) return TRUE;
        }
    }
    //
    return FALSE;
}

+ (NSString *)createDocumentsDirectoryByAppendingPathComponent:(NSString *)pathComponent
{
    NSArray *paths =
    NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                        NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    if(!pathComponent) {
        return documentsDirectory;
    }
    
    NSString *actualDirectory = [documentsDirectory stringByAppendingPathComponent:pathComponent];
    
    NSFileManager *fileManager= [NSFileManager defaultManager];
    
    BOOL isDir=YES;
    
    if(![fileManager fileExistsAtPath:actualDirectory isDirectory:&isDir])
    {
        if(![fileManager createDirectoryAtPath:actualDirectory withIntermediateDirectories:YES attributes:nil error:nil])    {
            NSAssert(false, @"CreateDocumentsDirectoryByAppendingPathComponent : %@ failed!!!",pathComponent);
        }
    }

    return actualDirectory;
}

+ (NSString *)copyResorceFile:(NSString *)resourceFilename withExtention:(NSString *)extention ToDocumentsDirectoryByAppendingPathComponent:(NSString *)pathComponent andDestinationFileName:(NSString *)destinatoionFilename
{
    NSString *destinationDirectory = [IBGlobal createDocumentsDirectoryByAppendingPathComponent:pathComponent];
    NSString *destinationPath = [destinationDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", destinatoionFilename, extention]];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:destinationPath])   {
        [[NSFileManager defaultManager] removeItemAtPath:destinationPath error:nil];
    }
    
    NSError *nError;
    if(![[NSFileManager defaultManager] copyItemAtPath:[[NSBundle mainBundle] pathForResource:resourceFilename ofType:extention] toPath:destinationPath error:&nError])    {
        NSAssert(false, @"copyResorceFile: %@ withExtention: %@ ToDestinationPath: %@ failed!!!",resourceFilename,extention,destinationPath);
    }
    
    return destinationPath;
}

+ (CGFloat)textViewHeightForAttributedText:(NSAttributedString *)text andWidth:(CGFloat)width
{
    UITextView *textView = [[UITextView alloc] init];
    [textView setAttributedText:text];
    CGSize size = [textView sizeThatFits:CGSizeMake(width, FLT_MAX)];
    textView = nil;
    return size.height;
}

+ (CGSize)text:(NSString *)text sizeWithFont:(UIFont *)font constrainedToSize:(CGSize)size
{
    if ([text respondsToSelector:@selector(boundingRectWithSize:options:attributes:context:)])   {
        CGRect frame = [text boundingRectWithSize:size
                                          options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                       attributes:@{NSFontAttributeName:font}
                                          context:nil];
        return frame.size;
    }
    else
    {
        return [text sizeWithFont:font constrainedToSize:size];
    }
}

+ (CGFloat)heightFromString:(NSString*)text withFont:(UIFont*)font constraintToWidth:(CGFloat)width
{
    CGRect rect;
    
    float iosVersion = [[[UIDevice currentDevice] systemVersion] floatValue];
    if (iosVersion >= 7.0) {
        rect = [text boundingRectWithSize:CGSizeMake(width, 1000) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:font} context:nil];
    }
    else {
        CGSize size = [text sizeWithFont:font constrainedToSize:CGSizeMake(width, 1000) lineBreakMode:NSLineBreakByWordWrapping];
        rect = CGRectMake(0, 0, size.width, size.height);
    }
    //NSLog(@"%@: W: %.f, H: %.f", self, rect.size.width, rect.size.height);
    return rect.size.height;
}

+ (NSString *)styledAppHtmlWithString:(NSString *)html andTextSize:(int)pointSize {
    NSString *style = [NSString stringWithFormat:@"<meta charset=\"UTF-8\"><style> body { font-family: 'OpenSans'; font-size: %dpx; font-color: '#6E6E6E'; } b {font-family: 'OpenSans-Bold'; font-size: %dpx; font-color: 'black'; } i {font-family: 'OpenSans-Italic'; font-size: %dpx; font-color: 'black'; }</style>",pointSize,pointSize,pointSize];
    
    return [NSString stringWithFormat:@"%@%@", style, html];
}

+ (NSString *)stringByStrippingHTML:(NSString *)html {
    NSRange r;
    NSString *s = html;
    while ((r = [s rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location != NSNotFound)
        s = [s stringByReplacingCharactersInRange:r withString:@""];
    return s;
}

+ (NSAttributedString *)attributedStringWithHTML:(NSString *)html
{
    NSMutableAttributedString *mutableAttributedString;
    
    if(IOS_NEWER_OR_EQUAL_TO(7.0))  {
        //
        NSDictionary *options = @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType };
        NSMutableDictionary *attributes = [[NSMutableDictionary alloc] init];
        [attributes setObject:kAppFontRegular(15) forKey:NSFontAttributeName];
        [attributes setObject:RGBCOLOR(110, 110, 110) forKey:NSForegroundColorAttributeName];
    
        mutableAttributedString = [[NSMutableAttributedString alloc] initWithData:[html dataUsingEncoding:NSUTF8StringEncoding] options:options documentAttributes:&attributes error:NULL];
    } else {
        //
        NSRange range = [html rangeOfString:@"}</style>" options:NSBackwardsSearch];
        
        NSString *escapedString;
        if(range.location!=NSNotFound)  {
            escapedString = [IBGlobal stringByStrippingHTML:[html substringFromIndex:range.location+9]];
        }
        else    {
            escapedString = [IBGlobal stringByStrippingHTML:html];
        }
        
        mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:escapedString];
        [mutableAttributedString addAttribute:NSFontAttributeName value:kAppFontRegular(15) range:NSMakeRange(0, [escapedString length])];
        [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:RGBCOLOR(110, 110, 110) range:NSMakeRange(0, [escapedString length])];
    }

    return [[NSAttributedString alloc] initWithAttributedString:mutableAttributedString];
}

+ (device_type)device {
    //
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        //
        CGSize result = [[UIScreen mainScreen] bounds].size;
        //
        if(result.height == 480) return iphone_standard;
        else if(result.height == 568) return iphone_taller;
        else return unknown_device;
    } else {
        return unknown_device;
    }
}

+ (NSArray *)loadFilesAtDirectory:(NSString *)dirPath
{
    NSError *error = nil;
    NSArray *filesArray = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dirPath error:&error];
    if(error != nil) {
        //NSLog(@"Error in reading files: %@", [error localizedDescription]);
        return nil;
    }
    
    return filesArray;
}

+ (NSString *)keyForUrlString:(NSString *)urlString
{
    NSString *keyString = [[[urlString stringByReplacingOccurrencesOfString:SYNC_URL withString:@""] base64EncodedString] uppercaseString];
    NSMutableString *newString = [NSMutableString string];
    //
    for (int i = 0; i < [keyString length]; i++) {
        //
        int ascii = [keyString characterAtIndex:i];
        if (((ascii>64) && (ascii<91)) || ((ascii>47) && (ascii<58))) [newString appendFormat:@"%c",ascii];
    }
    //
    return [NSString stringWithString:newString];
}

+ (UIImage *)imageWithColor:(UIColor *)color {
    //
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    //
    return image;
}

+(UIColor*)colorWithHexString:(NSString *)hex {
    //
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    unsigned int r, g, b;
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f) green:((float) g / 255.0f) blue:((float) b / 255.0f) alpha:1.0f];
}
@end
