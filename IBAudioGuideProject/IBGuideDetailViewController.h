//
//  IBGuideDetailViewController.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 17/06/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IBAudioGuide.h"

@interface IBGuideDetailViewController : UIViewController
{
    NSArray *_arrayImagePaths;
    IBAudioGuide *_audioGuide;
}
@property(nonatomic, strong)NSArray *arrayImagePathsGallery;

@property (nonatomic, strong) NSArray *arrayImagePaths;
@property (nonatomic, strong) IBAudioGuide *audioGuide;

@end
