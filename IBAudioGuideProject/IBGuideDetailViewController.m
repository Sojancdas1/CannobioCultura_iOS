//
//  IBGuideDetailViewController.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 17/06/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBGuideDetailViewController.h"

#import "IBGlobal.h"
#import "IBGuideGalleryViewController.h"
#import "IBGeoMapViewController.h"
#import "HPGrowingTextView.h"

#import "Base64.h"
#import "IBZipDownloadButton.h"
#import "MBProgressHUD.h"
#import "ASIHTTPRequest.h"
#import "ZipArchive.h"

#import "IBAudioGuideInfo.h"
#import "IBPolygonDrawer.h"
#import "UIView+Toast.h"

@interface IBGuideDetailViewController ()<UIScrollViewDelegate>
{
    //parent scroller
    UIScrollView *_scrollViewMaster;
    
    //Types of guide gallery preview
    UIView *_guideGalleryContainer;
    
    UIScrollView *_pagedScrollView;
    UIPageControl *_pageControl;
    UIButton *_guideGalleryButton;
    
    //Types of play button
    UIView *_playButtonContainer;
    
    UIView *_playButtonBackground;
    UIImageView *_playButtonImageView;
    UILabel *_playButtonLabel;
    UIButton *_playButtonOnAction;
    
    //Types of DescriptionView
    UIScrollView *_audioDetailsContainer;
    
    UILabel *_descriptionTitleLabel;
    UILabel *_descriptionSubTitleLabel;
    UITextView *_descriptionDetailsTextView;
    UIView *_descriptionDetailsTextViewMask;
    UIView *_mapTitleContainer;
    UILabel *_mapTitleLabel;
    IBPolygonDrawer *_polygonDrawer;
    UIImageView *_guideMapImageView;
    
    BOOL isLoaded;
    MBProgressHUD *hud;
    UIView *audioDescriptionMask;
    UILabel *labelTitle, *labelSubTitle;
    ASIHTTPRequest *asiRequest;
    long long receivedBytes,contentLength;
    CGRect mainScreenSize;
    BOOL pageControllAnimating;
    BOOL isPlayerHidden;
    UIView *backView;
    NSString *parentGuideTitle;
    
}

//downloader key
@property (nonatomic,assign) Status status;
@property (nonatomic,readonly) NSString *key;
@property (nonatomic,strong) NSString *urlString;
@property (nonatomic,strong) NSString *destinationDir;

//parent scroller
@property (strong, nonatomic) UIScrollView *scrollViewMaster;

//properties of paged images for audio guide gallery preview
@property (strong, nonatomic) UIView *guideGalleryContainer;
@property (strong, nonatomic) UIScrollView *pagedScrollView;
@property (strong, nonatomic) UIPageControl *pageControl;
@property (strong, nonatomic) UIButton *guideGalleryButton;

//Properties of play button view
@property (strong, nonatomic) UIView *playButtonContainer;
@property (strong, nonatomic) UIView *playButtonBackground;
@property (strong, nonatomic) UIImageView *playButtonImageView;
@property (strong, nonatomic) UILabel *playButtonLabel;
@property (strong, nonatomic) UIButton *playButtonOnAction;

//Properties of audio details view
@property (strong, nonatomic) UIScrollView *audioDetailsContainer;
@property (strong, nonatomic) UILabel *descriptionTitleLabel;
@property (strong, nonatomic) UILabel *descriptionSubTitleLabel;
@property (strong, nonatomic) UITextView *descriptionDetailsTextView;
@property (strong, nonatomic) UIView *descriptionDetailsTextViewMask;
@property (strong, nonatomic) UIView *mapTitleContainer;
@property (strong, nonatomic) UILabel *mapTitleLabel;
@property (strong, nonatomic) IBPolygonDrawer *polygonDrawer;
@property (strong, nonatomic) UIImageView *guideMapImageView;
@property (strong, nonatomic) UIView *backView;


- (void)refreshView;
- (void)buttonBackTouched;
- (void)buttonGuideGalleryTouched;
- (void)playButtonOnActionTouched;
-(void)handleLongPress:(UITapGestureRecognizer *)gesture;

@end

@implementation IBGuideDetailViewController

//downloader key
@synthesize key = _key;
@synthesize status = _status;
@synthesize urlString = _urlString;
@synthesize destinationDir = _destinationDir;

//public properties
@synthesize audioGuide = _audioGuide;
@synthesize arrayImagePaths = _arrayImagePaths;

//parent scroller
@synthesize scrollViewMaster = _scrollViewMaster;

//systhesize objects for paged images of audio guide gallery preview
@synthesize guideGalleryContainer = _guideGalleryContainer;
@synthesize pagedScrollView = _pagedScrollView;
@synthesize pageControl = _pageControl;
@synthesize guideGalleryButton = _guideGalleryButton;

//systhesize of play button
@synthesize playButtonContainer = _playButtonContainer;
@synthesize playButtonBackground = _playButtonBackground;
@synthesize playButtonImageView = _playButtonImageView;
@synthesize playButtonLabel = _playButtonLabel;
@synthesize playButtonOnAction = _playButtonOnAction;

//systhesize of audio details
@synthesize audioDetailsContainer = _audioDetailsContainer;
@synthesize descriptionTitleLabel = _descriptionTitleLabel;
@synthesize descriptionSubTitleLabel = _descriptionSubTitleLabel;
@synthesize descriptionDetailsTextView = _descriptionDetailsTextView;
@synthesize descriptionDetailsTextViewMask = _descriptionDetailsTextViewMask;
@synthesize mapTitleContainer = _mapTitleContainer;
@synthesize mapTitleLabel = _mapTitleLabel;
@synthesize polygonDrawer = _polygonDrawer;
@synthesize guideMapImageView = _guideMapImageView;
@synthesize arrayImagePathsGallery = _arrayImagePathsGallery;
@synthesize backView = _backView;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        isLoaded = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (IOS_OLDER_THAN(6.0))    {
        [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:NO];
    }
    
    if (IOS_NEWER_OR_EQUAL_TO(7.0))    {
        self.extendedLayoutIncludesOpaqueBars=NO;
        self.automaticallyAdjustsScrollViewInsets=NO;
    }
    
    //parent scroller
     mainScreenSize = [UIScreen mainScreen].bounds;
    _scrollViewMaster = [[UIScrollView alloc] initWithFrame:mainScreenSize];
    _scrollViewMaster.tag = 0;
    _scrollViewMaster.scrollEnabled = YES;
    _scrollViewMaster.showsHorizontalScrollIndicator = NO;
    _scrollViewMaster.showsVerticalScrollIndicator = NO;
    _scrollViewMaster.bounces = NO;
    _scrollViewMaster.delegate = self;
    _scrollViewMaster.backgroundColor = [UIColor whiteColor];
    
    //initializing objects for paged images of audio guide gallery preview
    _guideGalleryContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mainScreenSize.size.width, 170)];
    _guideGalleryContainer.backgroundColor = [UIColor blackColor];
    
    _pagedScrollView = [[UIScrollView alloc] initWithFrame:_guideGalleryContainer.bounds];
    _pagedScrollView.tag = 1;
    _pagedScrollView.backgroundColor = [UIColor clearColor];
    _pagedScrollView.pagingEnabled = YES;
    _pagedScrollView.delegate = self;
    [_guideGalleryContainer addSubview:_pagedScrollView];
    
    _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, 132, self.view.frame.size.width, 30)];
    _pageControl.backgroundColor = [UIColor clearColor];
    _pageControl.enabled = NO;
    [_guideGalleryContainer addSubview:_pageControl];
    
    _guideGalleryButton = [[UIButton alloc] initWithFrame:CGRectMake(mainScreenSize.size.width - 50, 122.5, 33, 34)];
    _guideGalleryButton.backgroundColor = [UIColor clearColor];
    [_guideGalleryButton setImage:[UIImage imageNamed:@"fullscreen.png"] forState:UIControlStateNormal];
    [[_guideGalleryButton imageView] setContentMode: UIViewContentModeScaleAspectFill];
    [[_guideGalleryButton layer] setMasksToBounds:YES];
    [[_guideGalleryButton imageView] setClipsToBounds:YES];
    [_guideGalleryButton.layer setCornerRadius:5];
    [_guideGalleryButton addTarget:self action:@selector(buttonGuideGalleryTouched) forControlEvents:UIControlEventTouchUpInside];
    
    
    UITapGestureRecognizer *longPressGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    [longPressGesture setNumberOfTouchesRequired:1];
    //    [longPressGesture setNumberOfTapsRequired:1];
    [_guideGalleryContainer addGestureRecognizer:longPressGesture];
    
    //imageview container added to scrollview parent
    [_scrollViewMaster addSubview:_guideGalleryContainer];
    
    //initializing objects for play button
    _playButtonContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 170, mainScreenSize.size.width, 60)];
    _playButtonContainer.backgroundColor = [IBGlobal colorWithHexString:@"eeeeee"];
    
    _playButtonBackground = [[UIView alloc] initWithFrame:CGRectMake(35, 10, mainScreenSize.size.width - 70, 40)];
    //_playButtonBackground.layer.backgroundColor = RGBCOLOR(104, 144, 152).CGColor;
    _playButtonBackground.backgroundColor = [IBGlobal colorWithHexString:@"699098"];
    _playButtonBackground.layer.cornerRadius = 5.f;
    [_playButtonContainer addSubview:_playButtonBackground];
    
    _playButtonLabel = [[UILabel alloc] initWithFrame:CGRectMake((_playButtonBackground.frame.size.width/2)-125, 0, 250, 40)];
    _playButtonLabel.font = kAppFontBold(18);
    _playButtonLabel.textColor = [UIColor whiteColor];
    _playButtonLabel.textAlignment = NSTextAlignmentCenter;
    [_playButtonBackground addSubview:_playButtonLabel];
    
    _playButtonImageView = [[UIImageView alloc] initWithFrame:CGRectMake(40, 13, 12, 14)];
    _playButtonImageView.backgroundColor = [UIColor clearColor];
    _playButtonImageView.image = [UIImage imageNamed:@"playbutton.png"];
    [_playButtonBackground addSubview:_playButtonImageView];
    
    _playButtonOnAction = [[UIButton alloc] initWithFrame:CGRectMake(20, 9, 280, 41)];
    [_playButtonOnAction addTarget:self action:@selector(playButtonOnActionTouched) forControlEvents:UIControlEventTouchUpInside];
    [_playButtonContainer addSubview:_playButtonOnAction];
    
    //playbutton container added to scrollview parent
    [_scrollViewMaster addSubview:_playButtonContainer];
    
    //initializing objects for details view
    _audioDetailsContainer = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 230,  mainScreenSize.size.width, 274)];
    _audioDetailsContainer.tag = 2;
    _audioDetailsContainer.backgroundColor = RGBCOLOR(218, 218, 218);
    _audioDetailsContainer.scrollEnabled = NO;
    _audioDetailsContainer.showsHorizontalScrollIndicator = NO;
    _audioDetailsContainer.showsVerticalScrollIndicator = NO;
    _audioDetailsContainer.bounces = NO;
    _audioDetailsContainer.delegate = self;
    
    _descriptionTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 20, 290, 30)];
    
    
    _descriptionTitleLabel.backgroundColor = [UIColor clearColor];
    _descriptionTitleLabel.font = kAppFontSemibold(23);
    [_audioDetailsContainer addSubview:_descriptionTitleLabel];
    
    _descriptionSubTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, _descriptionTitleLabel.frame.origin.y + _descriptionTitleLabel.frame.size.height, 290, 21)];
    _descriptionSubTitleLabel.backgroundColor = [UIColor clearColor];
    _descriptionSubTitleLabel.font = kAppFontRegular(13);
    [_audioDetailsContainer addSubview:_descriptionSubTitleLabel];
    
    _descriptionDetailsTextView = [[UITextView alloc] initWithFrame:CGRectMake(10, 311, mainScreenSize.size.width - 20, 300)];
    _descriptionDetailsTextView.backgroundColor = [UIColor clearColor];
    _descriptionDetailsTextView.editable = NO;
    _descriptionDetailsTextView.userInteractionEnabled = YES;
    _descriptionDetailsTextView.font = kAppFontRegular(15);
    _descriptionDetailsTextView.textColor = RGBCOLOR(110, 110, 110);
    
    _descriptionDetailsTextViewMask = [[UIView alloc] initWithFrame:_descriptionDetailsTextView.bounds];
    _descriptionDetailsTextViewMask.backgroundColor = [UIColor clearColor];
    [_descriptionDetailsTextView addSubview:_descriptionDetailsTextViewMask];
    [_audioDetailsContainer addSubview:_descriptionDetailsTextView];
    
    _mapTitleContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 157, self.view.frame.size.width, 20)];
    _mapTitleContainer.backgroundColor = RGBCOLOR(205, 205, 205);
    [_audioDetailsContainer addSubview:_mapTitleContainer];
    
    _mapTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 290, 20)];
    _mapTitleLabel.backgroundColor = [UIColor clearColor];
    _mapTitleLabel.font = kAppFontBold(11);
    _mapTitleLabel.textColor = RGBCOLOR(104, 144, 152);
    [_mapTitleContainer addSubview:_mapTitleLabel];
    
    _guideMapImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 178, mainScreenSize.size.width, 96)];
    _guideMapImageView.backgroundColor = [UIColor clearColor];
   
    
    _backView = [[UIView alloc]initWithFrame:CGRectMake(0, 178, mainScreenSize.size.width, 96)];
    _backView.backgroundColor = [UIColor whiteColor];
    [_audioDetailsContainer addSubview:_backView];
    [_audioDetailsContainer addSubview:_guideMapImageView];
    
    _polygonDrawer = [[IBPolygonDrawer alloc] initWithFrame:_guideMapImageView.bounds];
    _polygonDrawer.backgroundColor = [UIColor clearColor];
    [_backView addSubview:_polygonDrawer];
    
    //audio details container added to scrollview parent
    [_scrollViewMaster addSubview:_audioDetailsContainer];
    
    //Audio player set to top always while scrolling
    [_scrollViewMaster bringSubviewToFront:_playButtonContainer];
    
    [self.view addSubview:_scrollViewMaster];
    
    isLoaded = YES;
    
    //navigation Titles and buttons
    UIButton *buttonBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonBack setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [buttonBack setFrame:CGRectMake(0, 0, 12, 21)];
    [buttonBack addTarget:self action:@selector(buttonBackTouched) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem =
    [[UIBarButtonItem alloc] initWithCustomView:buttonBack];
    
    labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    labelTitle.backgroundColor = [UIColor clearColor];
    labelTitle.textColor = [UIColor blackColor];
    labelTitle.font = kAppFontSemibold(17);
    labelTitle.text = @"Your Title here";
    labelTitle.textAlignment = NSTextAlignmentCenter;
    [labelTitle sizeToFit];
    
    labelSubTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 22, 0, 0)];
    labelSubTitle.backgroundColor = [UIColor clearColor];
    labelSubTitle.textColor = [UIColor blackColor];
    labelSubTitle.font = kAppFontSemibold(11);
    labelSubTitle.text = @"Your subtitle here";
    labelSubTitle.textAlignment = NSTextAlignmentCenter;
    [labelSubTitle sizeToFit];
    
    UIView *twoLineTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAX(labelSubTitle.frame.size.width, labelTitle.frame.size.width), 30)];
    [twoLineTitleView addSubview:labelTitle];
    [twoLineTitleView addSubview:labelSubTitle];
    
    float widthDiff = labelSubTitle.frame.size.width - labelTitle.frame.size.width;
    
    CGSize maximumLabelSize = CGSizeMake(self.view.frame.size.width - 70,30);
    
    //NSLog(@"%@",_audioGuide.guideTitle);
    CGSize expectedLabelSize = [_audioGuide.guideTitle sizeWithFont:labelTitle.font constrainedToSize:maximumLabelSize
                                                      lineBreakMode:labelTitle.lineBreakMode];
    
    CGRect newFrame = labelTitle.frame;
    newFrame.size.width = expectedLabelSize.width;
 //   newFrame.size.height = expectedLabelSize.height;
    labelTitle.numberOfLines = 5;
    labelTitle.frame = newFrame;
    
    if (widthDiff > 0)
    {
        CGRect frame = labelTitle.frame;
        frame.origin.x = widthDiff / 2;
        labelTitle.frame = CGRectIntegral(frame);
           }
    else
    {
        CGRect frame = labelSubTitle.frame;
        frame.origin.x = fabsf(widthDiff) / 2;
        labelSubTitle.frame = CGRectIntegral(frame);
        [labelTitle setCenter:twoLineTitleView.center];

    }
    
    self.navigationItem.titleView = twoLineTitleView;
    
//    UIView *viewNext = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 65, 30)];
//    UIBarButtonItem *buttonRight = [[UIBarButtonItem alloc] initWithCustomView:viewNext];
//    self.navigationItem.rightBarButtonItem = buttonRight;
    
    isLoaded = YES;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView.tag==0)  {
        if(scrollView.contentOffset.y>=170) {
            _playButtonContainer.frame = CGRectMake(0, scrollView.contentOffset.y, self.view.frame.size.width, 60);
            [_playButtonContainer.layer setShadowColor:[UIColor grayColor].CGColor];
            [_playButtonContainer.layer setShadowOpacity:0.3];
            [_playButtonContainer.layer setShadowRadius:8.0];
            [_playButtonContainer.layer setShadowOffset:CGSizeMake(5, 5)];
        }
        else
        {
            _playButtonContainer.frame = CGRectMake(0, 170, self.view.frame.size.width, 60);
            [_playButtonContainer.layer setShadowColor:[UIColor whiteColor].CGColor];
            [_playButtonContainer.layer setShadowOpacity:0.0];
            [_playButtonContainer.layer setShadowRadius:0.0];
            [_playButtonContainer.layer setShadowOffset:CGSizeMake(0, 0)];
        }
    }
    else if(scrollView.tag==1)   {
        CGFloat pageWidth = self.pagedScrollView.frame.size.width;
        NSInteger page = (NSInteger)floor((self.pagedScrollView.contentOffset.x * 2.0f + pageWidth) / (pageWidth * 2.0f));
        
        if((pageControllAnimating==NO) && (self.pageControl.alpha==0.f)) {
            pageControllAnimating = YES;
            [UIView animateWithDuration:1.f animations:^{
                self.pageControl.alpha = 1.f;
            }completion:^(BOOL finished) {
                pageControllAnimating = NO;
            }];
        }

        // Update the page control
        self.pageControl.currentPage = page;
    }
}

-(void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_updateButton:) name:@"PlayPauseButtonTapped" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(_audioPlayerDidFinishPlaying:) name:@"audioPlayerDidFinishPlaying" object:nil];
    
    [self navigationController].navigationBarHidden = NO;
    
    [self.view setBackgroundColor:RGBCOLOR(200, 200, 200)];
    
    NSInteger locationNo = [SharedAppDelegate locationNo];
    NSString *museumDir;
    DIRLOCATION_STR(locationNo,museumDir);
    
    _urlString = [NSString stringWithFormat:@"%@/%@/%@/%@",SYNC_URL,IBParentDirectory,museumDir,kMapZip];
    _destinationDir = [IBGlobal createDocumentsDirectoryByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@/",ENGLISH,museumDir]];
    
    NSString *downloadStatus = [[NSUserDefaults standardUserDefaults] valueForKey:self.key];
    
    labelTitle.text = @"";
    labelSubTitle.text = @"";
    
    _guideGalleryContainer.alpha = 0.f;
    _playButtonContainer.alpha = 0.f;
    _audioDetailsContainer.alpha = 0.f;
    
    if([downloadStatus intValue]==StatusDownloaded)   {
        self.status = StatusDownloaded;
        
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        
        [self performSelector:@selector(refreshView) withObject:nil afterDelay:0.1];
    }
    else  {
        self.status = StatusDownloading;
        
        NSURL *url = [NSURL URLWithString:_urlString];
        asiRequest = [ASIHTTPRequest requestWithURL:url];
        [asiRequest setTimeOutSeconds:360];
        [asiRequest setDelegate:self];
        [asiRequest setDownloadProgressDelegate:self];
        [asiRequest setDownloadDestinationPath:[NSString stringWithFormat:@"%@%@",_destinationDir,[_urlString lastPathComponent]]];
        [asiRequest setTemporaryFileDownloadPath:[NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"loc%ld-%@.download",(long)[SharedAppDelegate locationNo],[_urlString lastPathComponent]]]];
        [asiRequest setAllowResumeForFileDownloads:YES];
        
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeDeterminate;
        hud.dimBackground = YES;
        
        [asiRequest startAsynchronous];
        
        [self.view makeToast:[[SharedAppDelegate langDictionary] valueForKey:@"LabelMapLoad"] duration:3.f position:@"bottom"];
    }
}

-(void)_audioPlayerDidFinishPlaying:(NSNotification *)notification {
    [self _pause];
}

-(void)_pause {
    // 
    NSDictionary *langDictionary = [SharedAppDelegate langDictionary];
    //
    _playButtonOnAction.selected = NO;
    _playButtonImageView.image = [UIImage imageNamed:@"playbutton.png"];
    _playButtonLabel.text = [langDictionary valueForKey:@"LabelPlayAudio"];
}

- (void)requestStarted:(ASIHTTPRequest *)request
{
    //NSLog(@"Request started!");
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:[NSString stringWithFormat:@"%d",self.status] forKey:self.key];
    [defaults synchronize];
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [hud setProgress:1.0f];
    
    ZipArchive *zipArchive = [[ZipArchive alloc] init];
    [zipArchive UnzipOpenFile:[NSString stringWithFormat:@"%@%@",_destinationDir,[_urlString lastPathComponent]]];
    if([zipArchive UnzipFileTo:_destinationDir overWrite:YES])  {
        [hud hide:YES];
        
        hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        
        [self performSelector:@selector(refreshView) withObject:nil afterDelay:0.1];
    }
    [zipArchive UnzipCloseFile];
    
    NSError *error = nil;
    [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@%@",_destinationDir,[_urlString lastPathComponent]] error:&error];
    
    //NSLog(@"Total length received %lld",receivedBytes);
    
    self.status = StatusDownloaded;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:[NSString stringWithFormat:@"%d",self.status] forKey:self.key];
    [defaults synchronize];
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    self.status = StatusNotDownloaded;
    
    [hud hide:YES afterDelay:1.f];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:[NSString stringWithFormat:@"%d",self.status] forKey:self.key];
    [defaults synchronize];
}

- (void)request:(ASIHTTPRequest *)request didReceiveBytes:(long long)bytes
{
    if(bytes>0) {
        receivedBytes = receivedBytes+bytes;
        float percentage = (float)receivedBytes/(float)contentLength;
        [hud setProgress:percentage];
    }
    
    //NSLog(@"bytes received %lld",bytes);
}

- (void)request:(ASIHTTPRequest *)request incrementDownloadSizeBy:(long long)newLength
{
    if(newLength>0) {
        contentLength = newLength;
    }
    
    //NSLog(@"Total length calculated %lld",newLength);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate
{
    return YES;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    UIViewController *controller = [segue destinationViewController];
    if ([segue.identifier isEqualToString:@"segueToGuideGallery"])
    {
        IBGuideGalleryViewController *guideDetailController = (IBGuideGalleryViewController *)controller;
        guideDetailController.arrayImagePaths = _arrayImagePathsGallery;
    }
}
-(void)setArrayImagePathsGallery:(NSArray *)arrayImagePathsGallery
{
    _arrayImagePathsGallery = [[NSArray alloc]initWithArray:arrayImagePathsGallery];
}
- (void)setArrayImagePaths:(NSArray *)arrayImagePaths
{
    _arrayImagePaths = [[NSArray alloc] initWithArray:arrayImagePaths];
}

- (void)buttonBackTouched
{
    
    _arrayImagePaths = nil;
    _arrayImagePathsGallery = nil;

    //NSLog(@"%d",self.status);
    if(self.status!=StatusDownloaded) return;
    
    _arrayImagePaths = nil;
    
    for(UIImageView *imageView in self.pagedScrollView.subviews)    {
        [imageView removeFromSuperview];
    }
//    
//    isPlayerHidden = [SharedAppDelegate playerHidden];
//    if(!isPlayerHidden)    {
//        [SharedAppDelegate animatePlayerHidden:YES];
//        isPlayerHidden = YES;
//    }
//    [[SharedAppDelegate playerView]setPlaying:NO];
//    [SharedAppDelegate pauseAudioPlayer];
    
 
    
    self.pageControl.currentPage = 0;
    self.pageControl.numberOfPages = 0;
    
    self.pagedScrollView.delegate = nil;
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)handleLongPress:(UITapGestureRecognizer *)gesture {
    switch(gesture.state){
        case UIGestureRecognizerStateBegan:
            //NSLog(@"State Began");
            break;
        case UIGestureRecognizerStateChanged:
            //NSLog(@"State changed");
            break;
        case UIGestureRecognizerStateEnded:
            [self buttonGuideGalleryTouched];
            //NSLog(@"State End");
            break;
        default:
            break;
    }
}

- (void)buttonGuideGalleryTouched
{
    [self performSegueWithIdentifier:@"segueToGuideGallery" sender:self];
}

-(void)_updateButton:(id)sender {
    //
    if (![[[SharedAppDelegate currentGuide] audioFilePath] isEqualToString:_audioGuide.audioFilePath]) { return ; }
    
    _playButtonOnAction.selected = !_playButtonOnAction.selected;
    
    NSDictionary *langDictionary = [SharedAppDelegate langDictionary];
    
    if (_playButtonOnAction.selected) {
        //
        _playButtonImageView.image = [UIImage imageNamed:@"pause.png"];
        _playButtonLabel.text = [langDictionary valueForKey:@"LabelPauseAudio"];
        
    } else {
        _playButtonImageView.image = [UIImage imageNamed:@"playbutton.png"];
        _playButtonLabel.text = [langDictionary valueForKey:@"LabelPlayAudio"];
    }
    //
    [SharedAppDelegate playerView].playing = _playButtonOnAction.selected;
}

- (void)playButtonOnActionTouched
{
    if ([[[SharedAppDelegate currentGuide] audioFilePath] isEqualToString:_audioGuide.audioFilePath]) {
        //
        [self _updateButton:nil];
        if ([SharedAppDelegate avAudioPlayer]) {
            
            if (_playButtonOnAction.selected) {
                //
                [[SharedAppDelegate avAudioPlayer] play];
                
            } else {
                [[SharedAppDelegate avAudioPlayer] pause];
            }
        }
        //
        return;
    }
    
    _playButtonOnAction.selected = !_playButtonOnAction.selected;
    
    NSDictionary *langDictionary = [SharedAppDelegate langDictionary];
    
    if (_playButtonOnAction.selected) {
        //
        _playButtonImageView.image = [UIImage imageNamed:@"pause.png"];
        _playButtonLabel.text = [langDictionary valueForKey:@"LabelPauseAudio"];
        
    } else {
        _playButtonImageView.image = [UIImage imageNamed:@"playbutton.png"];
        _playButtonLabel.text = [langDictionary valueForKey:@"LabelPlayAudio"];
    }
    
    NSInteger locationNo = [SharedAppDelegate locationNo];
    NSString *museumName = nil;
    MUSEUMNAME_STR(locationNo, museumName);
    
    NSString *referNumber = _audioGuide.referenceNo;
    NSString * tempRef = [referNumber stringByReplacingOccurrencesOfString:@"0" withString:@""]; 
    
    NSString *detailText;
    NSMutableAttributedString *mutableAttributedString;
    NSMutableAttributedString *mainAttributeString;
    if (![SharedAppDelegate isSubRoom])
    {
        
        mainAttributeString = [[NSMutableAttributedString alloc]initWithString:[[SharedAppDelegate langDictionary] valueForKey:@"LabelRoomTag"]];
        NSString *tempRefAttribute  = [NSString stringWithFormat:@" %@",tempRef];
        mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:tempRefAttribute];
        [mutableAttributedString addAttribute:NSFontAttributeName value:kAppFontBold(11) range:NSMakeRange(0, [tempRefAttribute length])];
        [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:RGBCOLOR(255, 255, 255) range:NSMakeRange(0, [tempRefAttribute length])];
        [mainAttributeString appendAttributedString:mutableAttributedString];
        
        
    } else {
        
        mainAttributeString = [[NSMutableAttributedString alloc]initWithString:[[SharedAppDelegate langDictionary] valueForKey:@"LabelRoomTag"]];
        detailText = [NSString stringWithFormat:@"  %@ (%@)",tempRef,parentGuideTitle];
        mutableAttributedString = [[NSMutableAttributedString alloc] initWithString:detailText];
        [mutableAttributedString addAttribute:NSFontAttributeName value:kAppFontBold(11) range:NSMakeRange(0, [detailText length])];
        [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:RGBCOLOR(255, 255, 255) range:NSMakeRange(0, [detailText length])];
        [mainAttributeString appendAttributedString:mutableAttributedString];
        
    }
    
    [SharedAppDelegate playerView].detailLabel.attributedText = mainAttributeString;
    [SharedAppDelegate playerView].locationLabel.text = [museumName uppercaseString];
    [SharedAppDelegate playerView].titleLabel.text = _audioGuide.guideTitle;
    [SharedAppDelegate playerView].thumbnailImageView.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[IBGlobal createDocumentsDirectoryByAppendingPathComponent:nil],_audioGuide.thumbnailPath]];
    [SharedAppDelegate playerView].playing = YES;
    [SharedAppDelegate playerView].isAppeared = YES;
    [SharedAppDelegate playButtonTriggered];
    mainAttributeString = nil;
    
    CGFloat contentHeight;
    
    if([IBGlobal device]==iphone_taller)  {
        if(IOS_OLDER_THAN(8.0)) {
            contentHeight = 100+_descriptionDetailsTextView.frame.size.height+_guideMapImageView.frame.size.height+64;
        } else {
            contentHeight = 100+_descriptionDetailsTextView.frame.size.height+_guideMapImageView.frame.size.height;
        }
    } else {
        if(IOS_OLDER_THAN(8.0)) {
            contentHeight = 100+_descriptionDetailsTextView.frame.size.height+_guideMapImageView.frame.size.height+88+64;
        } else {
            contentHeight = 100+_descriptionDetailsTextView.frame.size.height+_guideMapImageView.frame.size.height+88;
        }
    }
    
    if([[SharedAppDelegate playerView] playing])    {
        _audioDetailsContainer.contentSize = CGSizeMake(_scrollViewMaster.frame.size.width, contentHeight+45+20);
    } else {
        _audioDetailsContainer.contentSize = CGSizeMake(_scrollViewMaster.frame.size.width, contentHeight+20 );
    }
    
    CGRect destRect = _audioDetailsContainer.frame;
    destRect.size.height = _audioDetailsContainer.contentSize.height + 20 ;
    
    _audioDetailsContainer.frame = destRect;
    //_audioDetailsContainer.frame = CGRectMake(0, 0, self.view.frame.size.width, _audioDetailsContainer.frame.size.height);
    
    _scrollViewMaster.contentSize = CGSizeMake(_scrollViewMaster.frame.size.width, _audioDetailsContainer.frame.origin.y+_audioDetailsContainer.frame.size.height);
    
    [SharedAppDelegate animatePlayerHidden:NO];
    [[SharedAppDelegate playerView] refreshView];
    
    IBAudioGuideInfo *currentGuide = [[IBAudioGuideInfo alloc] init];
    currentGuide.guideDescription = _audioGuide.guideDescription;
    currentGuide.audioFilePath = _audioGuide.audioFilePath;
    currentGuide.guideTitle = _audioGuide.guideTitle;
    currentGuide.referenceNo = _audioGuide.referenceNo;
    currentGuide.thumbnailPath = _audioGuide.thumbnailPath;
    
    [SharedAppDelegate setCurrentGuide:currentGuide];
}


- (void)setAudioGuide:(IBAudioGuide *)audioGuide
{
    _audioGuide = audioGuide;
    [self refreshView];
}

- (void)refreshView
{
    
    
    
    if(!isLoaded) return;
    
    if([_arrayImagePaths count]>0)  {
        for(UIImageView *imageView in self.pagedScrollView.subviews)    {
            [imageView removeFromSuperview];
        }
        
        self.pageControl.currentPage = 0;
        self.pageControl.numberOfPages = [self.arrayImagePaths count];
        
        CGFloat originX = 0.;
        CGRect rect = self.pagedScrollView.bounds;
        for(NSString *path in self.arrayImagePaths)  {
            rect.origin.x = originX;
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:rect];
            [imageView setContentMode:UIViewContentModeScaleAspectFill];
            [self.pagedScrollView addSubview:imageView];
            imageView.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@",path]];
            
            originX = originX+rect.size.width;
        }
        
        self.pagedScrollView.contentOffset = CGPointZero;
        self.pagedScrollView.contentSize = CGSizeMake(originX, rect.size.height);
        
        [UIView  animateWithDuration:1.0 animations:^{
            [_pageControl setAlpha:0.f];
        }];
        
        if([_arrayImagePaths count]>=1)  {
            [_guideGalleryContainer addSubview:_pageControl];
            [_guideGalleryContainer addSubview:_guideGalleryButton];
        }
        else    {
            [_pageControl removeFromSuperview];
            [_guideGalleryButton removeFromSuperview];
        }
    }
    else    {
        [_pageControl removeFromSuperview];
        [_guideGalleryButton removeFromSuperview];
    }
    
    NSInteger locationNo = [SharedAppDelegate locationNo];
    
    NSString *museumName = nil;
    MUSEUMNAME_STR(locationNo, museumName);
    
    labelSubTitle.text = museumName;
    labelTitle.text = _audioGuide.guideTitle;
//    CGRect tempTitleFrame =labelTitle.frame;
//    tempTitleFrame.size.width = tempTitleFrame.size.width + 50;
//    labelTitle.frame = tempTitleFrame;
    
    NSDictionary *langDictionary = [SharedAppDelegate langDictionary];
    
    _playButtonImageView.image = [UIImage imageNamed:@"playbutton.png"];
    _playButtonLabel.text = [langDictionary valueForKey:@"LabelPlayAudio"];
    _mapTitleLabel.text = [langDictionary valueForKey:@"LabelWhereIs"];
   
    _descriptionTitleLabel.text = _audioGuide.guideTitle;
    
    CGSize maximumLabelSize = CGSizeMake(self.view.frame.size.width - 20,100);
    
    CGSize expectedLabelSize = [_audioGuide.guideTitle sizeWithFont:_descriptionTitleLabel.font constrainedToSize:maximumLabelSize
                                                      lineBreakMode:_descriptionTitleLabel.lineBreakMode];
    
    CGRect newFrame = _descriptionTitleLabel.frame;
    newFrame.size.height = expectedLabelSize.height;
    _descriptionTitleLabel.frame = newFrame;
    _descriptionTitleLabel.numberOfLines = 10;
    //NSLog(@"%@",NSStringFromCGRect(newFrame));
    _descriptionSubTitleLabel.frame = CGRectMake(15, _descriptionTitleLabel.frame.origin.y + _descriptionTitleLabel.frame.size.height, self.view.frame.size.width - 30 , 21);
    
    NSArray *arrayMapRooms = [[SharedAppDelegate database] fetchRoomsByAudioGuideReferenceNo:_audioGuide.referenceNo];
   	NSString *ParentGuideReferenceNUmber =[_audioGuide.referenceNo  stringByTrimmingCharactersInSet:[NSCharacterSet lowercaseLetterCharacterSet]];
    NSArray *arrayParentGuide = [[SharedAppDelegate database] fetchAudioGuideByReferenceNo:ParentGuideReferenceNUmber ];
    if ([arrayParentGuide count] > 1)
    { NSString *tempPreviousDesc;
        for(NSArray *tempArray in arrayParentGuide)
        {
           
            if ([[tempArray valueForKey:@"guideDescription"] isEqualToString:tempPreviousDesc])
            {
                parentGuideTitle = [tempArray valueForKey:@"guideTitle"] ;
                
            }
            else if ([[tempArray valueForKey:@"guideDescription"] isEqualToString:@"<Desc>-1</Desc>"])
            {
                parentGuideTitle = [tempArray valueForKey:@"guideTitle"] ;
            }
            
            tempPreviousDesc = [tempArray valueForKey:@"guideDescription"];
        }
    }
    else
    {
        parentGuideTitle = [[arrayParentGuide objectAtIndex:0]valueForKey:@"guideTitle"] ;
    }
    
    
    //	//NSLog(@"%@",[[arrayParentGuide objectAtIndex:0]valueForKey:@"guideTitle"]);
    
    if (![SharedAppDelegate isSubRoom])
    {
        NSString *subTitle = [NSString stringWithFormat:@"%@ audio %@ ",[langDictionary valueForKey:@"LabelCode"],_audioGuide.referenceNo];
        NSRange codeRange = [subTitle rangeOfString:_audioGuide.referenceNo];
        NSRange positionRange = [subTitle rangeOfString:[NSString stringWithFormat:@"%@",parentGuideTitle]];
        NSMutableAttributedString *mutableAttributedString= [[NSMutableAttributedString alloc] initWithString:subTitle];
        [mutableAttributedString addAttribute:NSFontAttributeName value:kAppFontRegular(13) range:NSMakeRange(0, [subTitle length])];
        [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:RGBCOLOR(124, 124, 124) range:NSMakeRange(0, [subTitle length])];
        [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:RGBCOLOR(104, 144, 152) range:codeRange];
        [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:RGBCOLOR(104, 144, 152) range:positionRange];
        [mutableAttributedString addAttribute:NSFontAttributeName value:kAppFontSemibold(13) range:codeRange];
        [mutableAttributedString addAttribute:NSFontAttributeName value:kAppFontSemibold(13) range:positionRange];
        _descriptionSubTitleLabel.attributedText = mutableAttributedString;
        
    }
    else
    {
        NSString *subTitle = [NSString stringWithFormat:@"%@ audio %@ - %@: %@ ",[langDictionary valueForKey:@"LabelCode"],_audioGuide.referenceNo,[langDictionary valueForKey:@"LabelPosition"],parentGuideTitle];
        NSRange codeRange = [subTitle rangeOfString:_audioGuide.referenceNo];
        NSRange positionRange = [subTitle rangeOfString:[NSString stringWithFormat:@"%@",parentGuideTitle]];
        NSMutableAttributedString *mutableAttributedString= [[NSMutableAttributedString alloc] initWithString:subTitle];
        [mutableAttributedString addAttribute:NSFontAttributeName value:kAppFontRegular(13) range:NSMakeRange(0, [subTitle length])];
        [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:RGBCOLOR(124, 124, 124) range:NSMakeRange(0, [subTitle length])];
        [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:RGBCOLOR(104, 144, 152) range:codeRange];
        [mutableAttributedString addAttribute:NSForegroundColorAttributeName value:RGBCOLOR(104, 144, 152) range:positionRange];
        [mutableAttributedString addAttribute:NSFontAttributeName value:kAppFontSemibold(13) range:codeRange];
        [mutableAttributedString addAttribute:NSFontAttributeName value:kAppFontSemibold(13) range:positionRange];
        _descriptionSubTitleLabel.attributedText = mutableAttributedString;
        
    }
    

    
    CGRect destRect;
    
    NSString *audioDescription = _audioGuide.guideDescription;
    
    destRect = _descriptionDetailsTextView.frame;
    destRect.origin.y = _descriptionSubTitleLabel.frame.origin.y+_descriptionSubTitleLabel.frame.size.height+15;

    // Apply some inline CSS
    NSString *styledHtml = [IBGlobal styledAppHtmlWithString:audioDescription andTextSize:15];
    
    // Generate an attributed string from the HTML
    NSAttributedString *attributedText = [IBGlobal attributedStringWithHTML:styledHtml];
    
    _descriptionDetailsTextView.attributedText = attributedText;
    
    CGSize size = [_descriptionDetailsTextView sizeThatFits:CGSizeMake(_descriptionDetailsTextView.frame.size.width, FLT_MAX)];
    CGFloat expectedHeight = size.height+15;
    
    destRect.size.height = expectedHeight;
    _descriptionDetailsTextView.frame = destRect;
    
    _descriptionDetailsTextViewMask.frame = _descriptionDetailsTextView.bounds;
    [_descriptionDetailsTextView setContentOffset:CGPointZero animated:YES];
    
    destRect = _mapTitleContainer.frame;
    destRect.origin.y = _descriptionDetailsTextView.frame.origin.y+_descriptionDetailsTextView.frame.size.height ;
    _mapTitleContainer.frame = destRect;
    
    NSInteger count = [[SharedAppDelegate database] countNoOfMapSections];
    
    if(count>0)    {
        
        NSArray *arrayParentGuide = [[SharedAppDelegate database] fetchAudioGuideByReferenceNo:ParentGuideReferenceNUmber ];
        NSArray *tempreferenceNumber;
        tempreferenceNumber = [arrayParentGuide valueForKey:@"referenceNo"];
        int referenceNoInt = (int)[[tempreferenceNumber objectAtIndex:0] integerValue];
        
        NSString *mapName;
        
        NSInteger locationNo = [SharedAppDelegate locationNo];
        if (locationNo == 1) {
            if (referenceNoInt >= 30){
                mapName = @"map2.png";
            }else {
                mapName = @"map1.png";
            }
        }else if (locationNo == 2){ // ISOLO MADRE
            if ([ParentGuideReferenceNUmber containsString:@"P"]) {
                mapName = @"map1.png";
            } else {
                mapName = @"map2.png";
            }
        }else{
            if (referenceNoInt >= 30){
                mapName = @"map2.png";
            }else {
                mapName = @"map1.png";
            }
        }

        NSString *filePath = [NSString stringWithFormat:@"%@/%@/%@",_destinationDir,kMap,mapName];
        if([[NSFileManager defaultManager] fileExistsAtPath:filePath])  {
            UIImage *mapImage = [UIImage imageWithContentsOfFile:filePath];
            
            CGFloat imageHeight = (mapImage.size.height*_guideMapImageView.frame.size.width)/mapImage.size.width;
            _guideMapImageView.frame = CGRectMake(0, _mapTitleContainer.frame.origin.y+_mapTitleContainer.frame.size.height, _guideMapImageView.frame.size.width, imageHeight);
            
             _backView.frame = CGRectMake(0, _mapTitleContainer.frame.origin.y+_mapTitleContainer.frame.size.height, _guideMapImageView.frame.size.width, imageHeight);
            
            _guideMapImageView.image = mapImage;
//            UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.view.frame];
//            [self.view addSubview:imageView];
//            imageView.backgroundColor = [UIColor redColor];
//            imageView.image = mapImage;
            _polygonDrawer.frame = _backView.bounds;
            
            CGFloat scaleFactor = imageHeight/mapImage.size.height;
            
          //  NSMutableArray *mapRoomsCoordinates = [NSMutableArray array];
            
            NSString *ParentGuideReferenceNUmber =[_audioGuide.referenceNo  stringByTrimmingCharactersInSet:[NSCharacterSet lowercaseLetterCharacterSet]];
            
//            NSInteger referencenumber = [ParentGuideReferenceNUmber integerValue];
            NSInteger sessoionIndex;
            NSInteger locationNo = [SharedAppDelegate locationNo];
            if (locationNo == 1) {
                if (referenceNoInt >= 30) {
                    sessoionIndex = 2;
                }else{
                    sessoionIndex = 1;
                }
            }else if (locationNo == 2){ // ISOLO MADRE
                if ([ParentGuideReferenceNUmber containsString:@"P"]) {
                    sessoionIndex = 1;
                } else {
                    sessoionIndex = 2;
                }
            }else{
                if (referenceNoInt >= 30){
                    sessoionIndex = 2;
                }else {
                    sessoionIndex = 1;
                }
            }
            
            NSArray *arrayOfGeoMapRooms = [[SharedAppDelegate database]fetchGeoMapRoomsBySessionIndex:sessoionIndex];
            NSMutableArray *currentRcircleCoordinates = [[NSMutableArray alloc]init];
            //    CGFloat scaleFactor = imageHeight/mapImage.size.height;
            
            for (UIView *view in [_guideMapImageView subviews])
            {
                [view removeFromSuperview];
            }
            
            
            for(IBGeoMapRooms *mapRoom in arrayOfGeoMapRooms)
            {
                
                NSArray *arrayCircleCoordintes = [mapRoom.circleCoordinates componentsSeparatedByString:@","];
                
                //NSLog(@"%@",mapRoom.roomNo);
                
                for(NSString *coordinateString in arrayCircleCoordintes)
                {
                    [currentRcircleCoordinates addObject:[NSNumber numberWithDouble:([coordinateString doubleValue]*scaleFactor)]];
                }
                
                
               UIView * _iBcircleView = [[UIView alloc]initWithFrame:CGRectMake([[currentRcircleCoordinates objectAtIndex:0] doubleValue] - 15, [[currentRcircleCoordinates objectAtIndex:1]doubleValue ] - 15 , 30, 30)];
                
                
                UILabel * labelText = [[UILabel alloc]init];
                if ([mapRoom.roomNo integerValue] <= 9)
                {
                    labelText.frame =CGRectMake(12, 0, 50, 30);
                }
                else
                {
                    labelText.frame =CGRectMake(7, 0, 50, 30);
                }
                
                _iBcircleView.tag = [mapRoom.roomNo integerValue];
//                labelText.text = [NSString stringWithFormat:@"%@",mapRoom.roomNo];
                labelText.text = [NSString stringWithFormat:@"%d",[mapRoom.roomNo intValue]];
               labelText.font = kAppFontRegular(17);
                labelText.textColor = [UIColor whiteColor];
                [_iBcircleView addSubview:labelText];
                
                if ([mapRoom.roomNo integerValue] == referenceNoInt) {
                    
                    _iBcircleView.backgroundColor = [UIColor clearColor];
                }
                else
                {
                    _iBcircleView.backgroundColor = RGBCOLOR(116, 141, 149);
                }
                
                _iBcircleView.layer.cornerRadius = 15;
                _iBcircleView.layer.masksToBounds = YES;
                
                
                //[_iBcircleView setNeedsDisplay];
                
                //NSLog(@"%@",NSStringFromCGRect(_iBcircleView.frame));
                // [_iBcircleView drawRect:_iBcircleView.frame number:referencenumber];
                [_guideMapImageView addSubview:_iBcircleView];
                [currentRcircleCoordinates removeAllObjects];
            }
            
            NSString *referenceInt = _audioGuide.referenceNo;
            NSString  *tempRefence = [referenceInt stringByTrimmingCharactersInSet:[NSCharacterSet lowercaseLetterCharacterSet]];
            NSInteger roomNoInt = [tempRefence integerValue];
            
            
            
            for (UIView *view in [_guideMapImageView subviews])
            {
                //NSLog(@"%ld",(long)view.tag);
                if (view.tag == roomNoInt)
                {
                    view.backgroundColor = [UIColor clearColor];

                    
                }
                else
                {
                    view.backgroundColor = RGBCOLOR(105,144,153);
                }
                
            }
            

            
            NSMutableArray *mapRoomsCoordinates = [NSMutableArray array];
            
            for(IBGeoMapRooms *mapRoom in arrayMapRooms)    {
                
                NSMutableArray *currentRoomCoordinates = [NSMutableArray array];
                NSArray *arrayCoordinates = [mapRoom.coordinates componentsSeparatedByString:@","];
                
                for(NSString *coordinateString in arrayCoordinates) {
                    [currentRoomCoordinates addObject:[NSNumber numberWithDouble:([coordinateString doubleValue]*scaleFactor)]];
                }
                
                [mapRoomsCoordinates addObject:[NSArray arrayWithArray:currentRoomCoordinates]];
            }
            
            [_polygonDrawer setCoordinates:[NSArray arrayWithArray:mapRoomsCoordinates]];
        }
    }
    else    {
        _guideMapImageView.frame = CGRectMake(0, _mapTitleContainer.frame.origin.y+_mapTitleContainer.frame.size.height, _guideMapImageView.frame.size.width, _guideMapImageView.frame.size.height);
        _polygonDrawer.frame = _guideMapImageView.bounds;
         _backView.frame = CGRectMake(0, _mapTitleContainer.frame.origin.y+_mapTitleContainer.frame.size.height, _guideMapImageView.frame.size.width, _guideMapImageView.frame.size.height);
    }
    
    CGFloat contentHeight;
    
    if([IBGlobal device]==iphone_taller)  {
        if(IOS_OLDER_THAN(8.0)) {
            contentHeight = 100+_descriptionDetailsTextView.frame.size.height+_guideMapImageView.frame.size.height+64;
        } else {
            contentHeight = 100+_descriptionDetailsTextView.frame.size.height+_guideMapImageView.frame.size.height;
        }
    }
    else    {
        if(IOS_OLDER_THAN(8.0)) {
            contentHeight = 100+_descriptionDetailsTextView.frame.size.height+_guideMapImageView.frame.size.height+88+64;
        }
        else    {
            contentHeight = 100+_descriptionDetailsTextView.frame.size.height+_guideMapImageView.frame.size.height+88;
        }
    }
    
    destRect = _audioDetailsContainer.frame;
    
    if([SharedAppDelegate playerHidden] == NO)    {
        destRect.size.height = contentHeight+45;
    }
    else    {
         destRect.size.height = contentHeight;
    }
    
    _audioDetailsContainer.frame = destRect;
    
    if ( DEVICE_WIDTH == 320 && DEVICE_HEIGHT == 480)
    {
         _scrollViewMaster.contentSize = CGSizeMake(_scrollViewMaster.frame.size.width, _audioDetailsContainer.frame.origin.y + _audioDetailsContainer.frame.size.height + 10);
    }
    else if (DEVICE_WIDTH == 320 && DEVICE_HEIGHT == 568)
    {
        _scrollViewMaster.contentSize = CGSizeMake(_scrollViewMaster.frame.size.width, _audioDetailsContainer.frame.origin.y + _audioDetailsContainer.frame.size.height + 80);
    }
    else
    {
     _scrollViewMaster.contentSize = CGSizeMake(_scrollViewMaster.frame.size.width, _audioDetailsContainer.frame.origin.y + _audioDetailsContainer.frame.size.height );
    }
   
    
    NSString *downloadStatus = [[NSUserDefaults standardUserDefaults] valueForKey:self.key];
    
    if([downloadStatus intValue]==StatusDownloaded)   {
        self.status = StatusDownloaded;
    }
    
    [UIView animateWithDuration:1.0f animations:^{
        _guideGalleryContainer.alpha = 1.f;
        _playButtonContainer.alpha = 1.f;
        _audioDetailsContainer.alpha = 1.f;
    }];
    
    if([SharedAppDelegate playerHidden] == NO)  {
        [SharedAppDelegate animatePlayerHidden:NO];
    }
    
    [hud hide:YES afterDelay:1.f];
}

- (NSString *)key
{
    NSString *keyString = [[[_urlString stringByReplacingOccurrencesOfString:SYNC_URL withString:@""] base64EncodedString] uppercaseString];
    
    NSMutableString *newString = [NSMutableString string];
    for (int i = 0; i < [keyString length]; i++)
    {
        int ascii = [keyString characterAtIndex:i];
        if (((ascii>64) && (ascii<91)) || ((ascii>47) && (ascii<58)))
        {
            [newString appendFormat:@"%c",ascii];
        }
    }
    
    return [NSString stringWithString:newString];
}

@end
