//
//  IBGuideDetailViewControllerTablet.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 02/09/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IBAudioGuideInfo.h"

@protocol tableReloadDelegate <NSObject>

-(void)reloadTreeviewOnPLaying;

@end

@interface IBGuideDetailViewControllerTablet : UIViewController
{
    NSArray *_arrayImagePaths;
    IBAudioGuideInfo *_audioGuideInfo;
}
@property(nonatomic, strong)NSArray *arrayImagePathsGallery;

@property (nonatomic, strong) NSArray *arrayImagePaths;
@property (nonatomic, strong) IBAudioGuideInfo *audioGuideInfo;
@property (nonatomic, strong) id <tableReloadDelegate>delegateTableReload;
- (void)refreshView;

@end
