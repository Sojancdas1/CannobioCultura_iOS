//
//  IBGuideGallery.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 23/07/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class IBAudioGuide;

@interface IBGuideGallery : NSManagedObject

@property (nonatomic, retain) NSString * guideGalleryFilePath;
@property (nonatomic, retain) IBAudioGuide *guideGalleryToAudioGuide;

@end
