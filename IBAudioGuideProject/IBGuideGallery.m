//
//  IBGuideGallery.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 23/07/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBGuideGallery.h"
#import "IBAudioGuide.h"


@implementation IBGuideGallery

@dynamic guideGalleryFilePath;
@dynamic guideGalleryToAudioGuide;

@end
