//
//  IBGuideGalleryViewController.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 17/06/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IBGuideGalleryViewController : UIViewController
{
    NSArray *_arrayImagePaths;
}

@property (nonatomic, strong) NSArray *arrayImagePaths;

@end
