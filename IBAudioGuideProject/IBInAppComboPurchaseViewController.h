//
//  IBInAppComboPurchaseViewController.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 27/08/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IBInAppComboPurchaseViewControllerDelegate
@required
- (void)comboInAppPurchaseTriggered;
- (void)singleInAppPurchaseTriggered;
@end

@interface IBInAppComboPurchaseViewController : UIViewController
{
    NSString *_heading;
    NSString *_description;
}

@property (nonatomic, assign) id <IBInAppComboPurchaseViewControllerDelegate> delegate;

@property (strong, nonatomic) NSString *heading;
@property (strong, nonatomic) NSString *description;

@end
