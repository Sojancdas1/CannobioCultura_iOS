//
//  IBInAppComboPurchaseViewController.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 27/08/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBInAppComboPurchaseViewController.h"

#import "IBGlobal.h"
#import "IBInAppTableView.h"

@interface IBInAppComboPurchaseViewController ()<IBInAppTableViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *labelHeader;
@property (weak, nonatomic) IBOutlet UILabel *labelDescription;
@property (weak, nonatomic) IBOutlet UIView *descriptionContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descriptionContainerHeightConstraint;
@property (weak, nonatomic) IBOutlet IBInAppTableView *inAppPurchaseTable;

- (void)refreshView;

@end

@implementation IBInAppComboPurchaseViewController

@synthesize delegate = _delegate;


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.inAppPurchaseTable.delegate = self;
    self.inAppPurchaseTable.backgroundColor = [UIColor whiteColor];
    
    self.labelHeader.font = kAppFontSemibold(30);
    self.labelHeader.textColor = RGBCOLOR(104, 144, 152);
    self.labelHeader.backgroundColor = [UIColor whiteColor];
    self.labelHeader.numberOfLines = 0;
    
    [self.labelHeader sizeToFit];
    
    self.labelDescription.font = kAppFontRegular(15);
    self.labelDescription.textColor = RGBCOLOR(141, 141, 141);
    self.labelDescription.backgroundColor = [UIColor whiteColor];
    self.labelDescription.numberOfLines = 0;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self refreshView];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [SharedAppDelegate unlockInteractions];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark-
#pragma IBInAppTableViewDelegate methods

- (void)comboInAppPurchaseTriggered
{
    [_delegate comboInAppPurchaseTriggered];
}

- (void)singleInAppPurchaseTriggered
{
    [_delegate singleInAppPurchaseTriggered];
}

#pragma mark-

- (void)setHeading:(NSString *)heading
{
    _heading = heading;
}

- (void)setDescription:(NSString *)description
{
    _description = description;
}

- (void)refreshView
{
    self.labelHeader.text = self.heading;
    self.labelDescription.text = self.description;
    
    CGFloat height = [IBGlobal heightFromString:self.description withFont:self.labelDescription.font constraintToWidth:self.descriptionContainer.frame.size.width];
    self.descriptionContainerHeightConstraint.constant = height;
}

@end
