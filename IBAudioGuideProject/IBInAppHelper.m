//
//  RageIAPHelper.m
//  In App Rage
//
//  Created by Ray Wenderlich on 9/5/12.
//  Copyright (c) 2012 Razeware LLC. All rights reserved.
//

#import "IBInAppHelper.h"
#import "IBGlobal.h"

@implementation IBInAppHelper

+ (IBInAppHelper *)sharedInstance {
    static dispatch_once_t once;
    static IBInAppHelper * sharedInstance;
    dispatch_once(&once, ^{
        NSURL *url = [[NSBundle mainBundle] URLForResource:@"InAppProducts" withExtension:@"plist"];
        NSArray *arrayIdentifiers = [NSArray arrayWithContentsOfURL:url];
        NSSet * productIdentifiers = [NSSet setWithArray:arrayIdentifiers];
        sharedInstance = [[self alloc] initWithProductIdentifiers:productIdentifiers];
    });
    return sharedInstance;
}

+ (IBInAppHelper *)restoreInstance {
    static dispatch_once_t once;
    static IBInAppHelper * sharedInstance;
    dispatch_once(&once, ^{
        NSURL *url = [[NSBundle mainBundle] URLForResource:@"InAppProducts" withExtension:@"plist"];
        NSArray *arrayIdentifiers = [NSArray arrayWithContentsOfURL:url];
        NSSet * productIdentifiers = [NSSet setWithArray:arrayIdentifiers];
        sharedInstance = [[self alloc] initWithProductIdentifiers:productIdentifiers];
    });
    return sharedInstance;
}

@end
