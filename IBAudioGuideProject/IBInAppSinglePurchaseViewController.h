//
//  IBInAppSinglePurchaseViewController.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 27/08/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IBTypes.h"

@protocol IBInAppSinglePurchaseViewControllerDelegate
@required
- (void)singleInAppPurchaseTriggered;
- (void)singleInAppPurchaseTriggeredFor:(IBType)type;
@end

@interface IBInAppSinglePurchaseViewController : UIViewController
{
    NSString *_heading;
    NSString *_description;
}

@property (nonatomic, assign) id <IBInAppSinglePurchaseViewControllerDelegate> delegate;

@property (strong, nonatomic) NSString *heading;
@property (strong, nonatomic) NSString *description;

@end
