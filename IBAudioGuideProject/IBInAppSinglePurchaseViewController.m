//
//  IBInAppSinglePurchaseViewController.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 27/08/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBInAppSinglePurchaseViewController.h"

#import "IBGlobal.h"
#import "HUDManager.h"

@interface IBInAppSinglePurchaseViewController ()

@property (weak, nonatomic) IBOutlet UILabel *labelHeader;
@property (weak, nonatomic) IBOutlet UILabel *labelDescription;
@property (weak, nonatomic) IBOutlet UIView *descriptionContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descriptionContainerHeightConstraint;
@property (weak, nonatomic) IBOutlet UIButton *inAppPurchaseButton;

- (void)refreshView;
- (IBAction)buttonBuyTouched:(id)sender;

@end

@implementation IBInAppSinglePurchaseViewController

@synthesize inAppPurchaseButton = _inAppPurchaseButton;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.labelHeader.font = kAppFontSemibold(30);
    self.labelHeader.textColor = RGBCOLOR(104, 144, 152);
    self.labelHeader.backgroundColor = [UIColor whiteColor];
    self.labelHeader.numberOfLines = 0;
    
    [self.labelHeader sizeToFit];
    
    self.labelDescription.font = kAppFontRegular(15);
    self.labelDescription.textColor = RGBCOLOR(141, 141, 141);
    self.labelDescription.backgroundColor = [UIColor whiteColor];
    self.labelDescription.numberOfLines = 0;
    
    [_inAppPurchaseButton setTitleColor:RGBCOLOR(104, 144, 152) forState:UIControlStateNormal];
    [_inAppPurchaseButton.titleLabel setFont:kAppFontSemibold(15)];
    _inAppPurchaseButton.layer.backgroundColor = [UIColor whiteColor].CGColor;
    _inAppPurchaseButton.layer.borderColor = RGBCOLOR(104, 144, 152).CGColor;
    _inAppPurchaseButton.layer.borderWidth = 2.f;
    _inAppPurchaseButton.layer.cornerRadius = 5.f;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refreshView];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [SharedAppDelegate unlockInteractions];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)buttonBuyTouched:(id)sender {
    //
    if ([self _isInternetconnectionExists] && [self _canPurchase]) {
        //
        NSLog(@"singleInAppPurchaseTriggered = %@", _delegate);
        IBType type = [SharedAppDelegate locationNo];
        // 
        [_delegate singleInAppPurchaseTriggered];
        [_delegate singleInAppPurchaseTriggeredFor:type];
    } else {
        //
        NSDictionary *langDictionary = [SharedAppDelegate langDictionary];
        [self _showAlertForNoInternetConnectionWithLangDictionary:langDictionary];
    }
}

#pragma mark-

- (void)setHeading:(NSString *)heading
{
    _heading = heading;
}

- (void)setDescription:(NSString *)description
{
    _description = description;
}

-(BOOL)_isInternetconnectionExists {
    //
    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"https://www.wikipedia.org/"]];
    //
    return (data.length != 0);
}

-(BOOL)_canPurchase {
    //
    return  (kPGDefaultPrize != nil || kPGDefaultPrize.length > 0 || ![kPGDefaultPrize isEqualToString:@"-1"]);
}

- (void)refreshView {
    //
    [HUDManager addHUDWithLabel:nil];
    //
    NSDictionary *langDictionary = [SharedAppDelegate langDictionary];
    self.labelHeader.text = self.heading;
    //
    if ([self _isInternetconnectionExists] && [self _canPurchase]) {
        //
        IBType locationNo = [SharedAppDelegate locationNo];
        NSString *description = nil;
        //
        if (locationNo == IBTypeIsoleBella) {
            //
            description = [langDictionary valueForKey:@"purchaseDescription-Bella"];
        } else if (locationNo == IBTypeIsoleMadre) {
            //
            description = [langDictionary valueForKey:@"purchaseDescription-Madre"];
            
        } else if (locationNo == IBTypeRoccaDiAngera) {
            //
            NSLog(@"not implemented");
        }
        //
        if (description) {
            //
            [self _updateUIWithDescription:description];
        }
    } else {
        //
        [self _showAlertForNoInternetConnectionWithLangDictionary:langDictionary];
    }
    //
    [HUDManager hideHUDFromWindow];
}

-(void)_updateUIWithDescription:(NSString *)description {
    //
    self.labelDescription.text = description;
    CGFloat height = [IBGlobal heightFromString:self.description withFont:self.labelDescription.font constraintToWidth:self.descriptionContainer.frame.size.width];
    self.descriptionContainerHeightConstraint.constant = height;
    [self.inAppPurchaseButton setTitle:kPGDefaultPrize forState:UIControlStateNormal];
    self.inAppPurchaseButton.hidden = NO;
}

-(void)_showAlertForNoInternetConnectionWithLangDictionary:(NSDictionary *)langDictionary {
    //
    NSString *error = [langDictionary valueForKey:@"Error"];
    NSString *internetNotAvailable = [langDictionary valueForKey:@"CheckInternetConnection"];
    //
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:error message:internetNotAvailable delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
    //
    self.inAppPurchaseButton.hidden = YES;
    self.labelDescription.text = internetNotAvailable;
}

@end
