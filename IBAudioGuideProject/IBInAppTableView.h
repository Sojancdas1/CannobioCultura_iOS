//
//  IBInAppTableView.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 08/08/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IBInAppTableViewDelegate
@required
- (void)comboInAppPurchaseTriggered;
- (void)singleInAppPurchaseTriggered;
@end

@interface IBInAppTableView : UIView

@property (nonatomic, assign) id<IBInAppTableViewDelegate> delegate;

@end
