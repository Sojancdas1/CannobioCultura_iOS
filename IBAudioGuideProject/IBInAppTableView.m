//
//  IBInAppTableView.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 08/08/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBInAppTableView.h"

#import "IBGlobal.h"
#import "IBInAppTableViewCell.h"

@interface IBInAppTableView ()<UITableViewDelegate,UITableViewDataSource>
{
    UITableView *_tableView;
}

@property (nonatomic,strong) UITableView *tableView;

- (void)buttonComboInAppPurchaseTouched;
- (void)buttonSingleInAppPurchaseTouched;

@end

@implementation IBInAppTableView

@synthesize delegate = _delegate;
@synthesize tableView = _tableView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder {
    if (self = [super initWithCoder:coder]) {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    _tableView = [[UITableView alloc] initWithFrame:self.bounds style:UITableViewStylePlain];
    [self addSubview:_tableView];
    
    if(IOS_NEWER_OR_EQUAL_TO(7.0))  {
        [_tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    [_tableView setScrollEnabled:NO];
    
    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)    {
        return 60;
    }
    else    {
        return 50;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{ 
    static NSString *CellIdentifier = @"Cell";
    IBInAppTableViewCell *cell = (IBInAppTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[IBInAppTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    NSDictionary *langDictionary = [SharedAppDelegate langDictionary];
    
    NSString *audioGuideLabel = [[langDictionary valueForKey:@"LabelAudioGuide"] stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSString *locationName;
    
    if([SharedAppDelegate locationNo]==1)   {
        locationName = [langDictionary valueForKey:@"LabelBella"];
    }
    else if ([SharedAppDelegate locationNo]==2) {
        locationName = [langDictionary valueForKey:@"LabelMadre"];
    }
    else    {
        locationName = [langDictionary valueForKey:@"LabelAngera"];
    }
    
    if (indexPath.row == 0)    {
        cell.textLabel.font = kAppFontRegular(12);
        cell.textLabel.text = [NSString stringWithFormat:@"%@ %@, %@ %@,\n%@ %@,etc...",[langDictionary valueForKey:@"LabelBella"],audioGuideLabel,[langDictionary valueForKey:@"LabelMadre"],audioGuideLabel,[langDictionary valueForKey:@"LabelAngera"],audioGuideLabel];
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
        cell.textLabel.textColor = RGBCOLOR(141, 141, 141);
        cell.textLabel.numberOfLines = 4;
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setFrame:CGRectMake(0, 0, 75, 30)];
        [button.titleLabel setFont:kAppFontSemibold(15)];
        
        button.layer.borderWidth = 1.5f;
        button.layer.borderColor = RGBCOLOR(104, 144, 152).CGColor;
        button.layer.cornerRadius = 5.f;
        
        [button setTitle:kPGComboPrize forState:UIControlStateNormal];
        [button setTitleColor:RGBCOLOR(104, 144, 152) forState:UIControlStateNormal];
        
        [button addTarget:self action:@selector(buttonComboInAppPurchaseTouched) forControlEvents:UIControlEventTouchUpInside];
        
        cell.accessoryView = button;
    }
    else    {
        cell.textLabel.font = kAppFontRegular(12);
        cell.textLabel.text = [NSString stringWithFormat:@"%@ %@",locationName,audioGuideLabel];
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
        cell.textLabel.textColor = RGBCOLOR(141, 141, 141);
        cell.textLabel.numberOfLines = 1;
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setFrame:CGRectMake(0, 0, 75, 30)];
        [button.titleLabel setFont:kAppFontSemibold(15)];
        
        button.layer.borderWidth = 1.5f;
        button.layer.borderColor = RGBCOLOR(104, 144, 152).CGColor;
        button.layer.cornerRadius = 5.f;
        
        if([SharedAppDelegate locationNo]==3)   {
            [button setTitle:kPGLeastPrize forState:UIControlStateNormal];
        }
        else    {
            [button setTitle:kPGDefaultPrize forState:UIControlStateNormal];
        }
        
        [button setTitleColor:RGBCOLOR(104, 144, 152) forState:UIControlStateNormal];
        
        [button addTarget:self action:@selector(buttonSingleInAppPurchaseTouched) forControlEvents:UIControlEventTouchUpInside];
        
        cell.accessoryView = button;
    }
    
    return cell;
}

- (void)buttonComboInAppPurchaseTouched {
    [_delegate comboInAppPurchaseTriggered];
}

- (void)buttonSingleInAppPurchaseTouched    {
    [_delegate singleInAppPurchaseTriggered];
}

@end
