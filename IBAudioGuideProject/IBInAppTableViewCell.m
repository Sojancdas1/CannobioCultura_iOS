//
//  IBInAppTableViewCell.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 16/07/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBInAppTableViewCell.h"

@implementation IBInAppTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark -
#pragma mark accessors

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    CGRect accessoryRect = self.accessoryView.frame;
    accessoryRect.origin.x = self.frame.size.width-80.f;
    [self.accessoryView setFrame:accessoryRect];
    
    [self.contentView setFrame:CGRectMake(0.f, 0.f, self.frame.size.width-accessoryRect.size.width, self.frame.size.height)];
    [self.textLabel setFrame:CGRectMake(3.f, 0.f, self.contentView.frame.size.width-6.f, self.contentView.frame.size.height)];
}

#pragma mark -

@end
