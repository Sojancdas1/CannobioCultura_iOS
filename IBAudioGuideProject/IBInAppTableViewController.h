//
//  IBInAppTableViewController.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 15/07/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IBInAppTableViewControllerDelegate
@required
- (void)comboInAppPurchaseTriggered;
- (void)singleInAppPurchaseTriggered;
@end

@interface IBInAppTableViewController : UITableViewController

@property (nonatomic, assign) id<IBInAppTableViewControllerDelegate> delegate;

@end
