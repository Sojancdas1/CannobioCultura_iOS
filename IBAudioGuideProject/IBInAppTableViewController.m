//
//  IBInAppTableViewController.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 15/07/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBInAppTableViewController.h"

#import "IBGlobal.h"
#import "IBInAppTableViewCell.h"

@interface IBInAppTableViewController ()

- (void)buttonComboInAppPurchaseTouched;
- (void)buttonSingleInAppPurchaseTouched;

@end

@implementation IBInAppTableViewController

@synthesize delegate = _delegate;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(IOS_NEWER_OR_EQUAL_TO(7.0))  {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }

    [self.tableView setScrollEnabled:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)    {
        return 45;
    }
    else if (indexPath.row == 1)    {
        return 90;
    }
    else    {
        return 75;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{ 
    static NSString *CellIdentifier = @"Cell";
    IBInAppTableViewCell *cell = (IBInAppTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[IBInAppTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    NSDictionary *langDictionary = [SharedAppDelegate langDictionary];
    
    NSString *audioGuideLabel = [[langDictionary valueForKey:@"LabelAudioGuide"] stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSString *locationName;
    
    if([SharedAppDelegate locationNo]==1)   {
        locationName = [langDictionary valueForKey:@"LabelBella"];
    }
    else if ([SharedAppDelegate locationNo]==2) {
        locationName = [langDictionary valueForKey:@"LabelMadre"];
    }
    else    {
        locationName = [langDictionary valueForKey:@"LabelAngera"];
    }
    
    if(indexPath.row == 0)  {
        cell.textLabel.font = kAppFontRegular(12);
        cell.textLabel.text = [langDictionary valueForKey:@"LabelOfferMessage"];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.textColor = [UIColor blackColor];
        cell.textLabel.numberOfLines = 2;
        
        cell.accessoryView = nil;
    }
    else if (indexPath.row == 1)    {
        cell.textLabel.font = kAppFontRegular(11);
        cell.textLabel.text = [NSString stringWithFormat:@"%@ %@,\n%@ %@,\n%@ %@,\netc...",[langDictionary valueForKey:@"LabelBella"],audioGuideLabel,[langDictionary valueForKey:@"LabelMadre"],audioGuideLabel,[langDictionary valueForKey:@"LabelAngera"],audioGuideLabel];
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
        cell.textLabel.textColor = [UIColor blackColor];
        cell.textLabel.numberOfLines = 4;
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setFrame:CGRectMake(0, 0, 75, 30)];
        [button.titleLabel setFont:kAppFontSemibold(15)];
        
        button.layer.borderWidth = 1.f;
        button.layer.borderColor = [UIColor blackColor].CGColor;
        button.layer.cornerRadius = 5.f;
        
        [button setTitle:kPGComboPrize forState:UIControlStateNormal];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [button addTarget:self action:@selector(buttonComboInAppPurchaseTouched) forControlEvents:UIControlEventTouchUpInside];
        
        cell.accessoryView = button;
    }
    else    {
        cell.textLabel.font = kAppFontRegular(11);
        cell.textLabel.text = [NSString stringWithFormat:@"%@ %@",locationName,audioGuideLabel];
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
        cell.textLabel.textColor = [UIColor blackColor];
        cell.textLabel.numberOfLines = 1;
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setFrame:CGRectMake(0, 0, 75, 30)];
        [button.titleLabel setFont:kAppFontSemibold(15)];
        
        button.layer.borderWidth = 1.f;
        button.layer.borderColor = [UIColor blackColor].CGColor;
        button.layer.cornerRadius = 5.f;

        if([SharedAppDelegate locationNo]==3)   {
            [button setTitle:kPGLeastPrize forState:UIControlStateNormal];
        }
        else    {
            [button setTitle:kPGDefaultPrize forState:UIControlStateNormal];
        }
        
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [button addTarget:self action:@selector(buttonSingleInAppPurchaseTouched) forControlEvents:UIControlEventTouchUpInside];
        
        cell.accessoryView = button;
    }
    
    return cell;
}

- (void)buttonComboInAppPurchaseTouched {
    [_delegate comboInAppPurchaseTriggered];
}

- (void)buttonSingleInAppPurchaseTouched    {
    [_delegate singleInAppPurchaseTriggered];
    NSLog(@"buttonSingleInAppPurchaseTouched = %@", _delegate);
}

@end
