//
//  IBLanguageTableViewCell.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 09/05/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBTableViewCellBase.h"

@protocol IBLanguageTableViewCellDelegate
@required
//index should be added as tag while creating or reusing table cell
- (void)languageSelectedWithIndexTag:(NSInteger)indexTag;
@end

@interface IBLanguageTableViewCell : IBTableViewCellBase

@property (nonatomic, assign) id<IBLanguageTableViewCellDelegate> delegate;
@property (nonatomic, assign) BOOL checked;

@end
