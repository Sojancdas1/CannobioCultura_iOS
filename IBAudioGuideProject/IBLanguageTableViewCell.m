//
//  IBLanguageTableViewCell.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 09/05/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBLanguageTableViewCell.h"

#import "IBGlobal.h"

@interface IBLanguageTableViewCell ()

@property (strong, nonatomic) UIImageView *imageViewAccessory;

@end

@implementation IBLanguageTableViewCell

@synthesize checked = _checked;
@synthesize imageViewAccessory = _imageViewAccessory;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        _imageViewAccessory = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width-40.f, 5.55f, 25.f, 22.f)];
        [self addSubview:_imageViewAccessory];
        
        self.textLabel.font = kAppFontSemibold(16);
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:NO animated:NO];

    // Configure the view for the selected state
    [self.delegate languageSelectedWithIndexTag:self.tag];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:NO animated:NO];
    // hack to get the effect of unchecking "Show Selection on Touch" option of UITableView in IB which has no effect
}

- (void)setChecked:(BOOL)checked
{
    _checked = checked;
    
    if(checked) {
        self.textLabel.textColor = RGBCOLOR(104.f, 144.f, 152.f);
        [_imageViewAccessory setImage:[UIImage imageNamed:@"tick.png"]];
    }
    else    {
        self.textLabel.textColor = RGBCOLOR(0.f, 0.f, 0.f);
        [_imageViewAccessory setImage:nil];
    }
}

#pragma mark -
#pragma mark accessors

- (void) layoutSubviews
{
    [super layoutSubviews];
    
    [self.textLabel setFrame:CGRectMake(10.f, 5.55f, 247.f, 35.f)];
    [self.imageViewAccessory setFrame:CGRectMake(self.frame.size.width-40.f, 5.55f, 25.f, 22.f)];
}

#pragma mark -

@end
