//
//  IBLanguageViewController.m
//  IBAudioGuideProject
//
//  Created by Jojin Johnson on 21/04/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBLanguageViewController.h"

#import "IBGlobal.h"
#import "IBLanguageTableViewCell.h"

@interface IBLanguageViewController ()<UITableViewDelegate,UITableViewDataSource,IBLanguageTableViewCellDelegate>
{
    UILabel *labelTitle;
    UILabel *labelButton;
}

@property (strong, nonatomic) NSArray *arrayLanguages;
@property (strong, nonatomic) NSString *langString;
@property (assign, nonatomic) NSInteger langIndex;

@property (weak, nonatomic) IBOutlet UITableView *tableViewLanguages;
@property (weak, nonatomic) IBOutlet UILabel *labelWelcome;
@property (weak, nonatomic) IBOutlet UILabel *labelLanguageIntroduction;
@property (weak, nonatomic) IBOutlet UIView *viewLabelContainer;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageViewTopSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewBottomSpaceConstraint;

- (void)refreshView;
- (void)buttonDoneClicked;

@end

@implementation IBLanguageViewController

@synthesize arrayLanguages,langString,langIndex;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    arrayLanguages = [[SharedAppDelegate database] fetchLanguageIndexes];
    
    self.tableViewLanguages.delegate = self;
    self.tableViewLanguages.dataSource = self;
   // self.tableViewLanguages.bounces = NO;
    self.tableViewLanguages.allowsSelection = YES;
    
    if (IOS_OLDER_THAN(6.0)) {
        [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:NO];
    }

    if(IOS_NEWER_OR_EQUAL_TO(7.0)) {
        [_tableViewLanguages setSeparatorInset:UIEdgeInsetsZero];
        self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    } else {
        self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
        self.imageViewTopSpaceConstraint.constant = 0;
        self.tableViewBottomSpaceConstraint.constant = -1;
    }
    
    _tableViewLanguages.separatorColor = RGBCOLOR(198.f, 198.f, 198.f);
    
    self.langString = [[NSLocale preferredLanguages]objectAtIndex:0];
    
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"LanguageString"] == nil) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        [defaults setValue:self.langString forKey:@"LanguageString"];
        [defaults setBool:NO forKey:@"LanguagePreferred"];
        
        NSInteger index = 0;
        for(NSDictionary *dictionary in arrayLanguages) {
            if([[[[dictionary valueForKey:@"displayName"] substringToIndex:2] lowercaseString] isEqualToString:self.langString])    {
                break;
            }
            index++;
        }
        
        self.langIndex = index;
        [defaults setInteger:index forKey:@"LanguageIndex"];
        
        [defaults synchronize];
    }
    
    [self.navigationController.navigationBar setBarStyle:UIBarStyleDefault];
    
    labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    labelTitle.backgroundColor = [UIColor clearColor];
    labelTitle.textColor = [UIColor blackColor];
    labelTitle.font = kAppFontSemibold(15);
    labelTitle.text = @"Your Title Header Add Here";
    labelTitle.textAlignment = NSTextAlignmentCenter;
    [labelTitle sizeToFit];
    
    self.navigationItem.titleView = labelTitle;
    
    UIView *viewLogo = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 65, 30)];
    UIImageView *logoImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
    logoImage.image = [UIImage imageNamed:@"logo.png"];
    logoImage.contentMode = UIViewContentModeScaleAspectFit;
    [viewLogo addSubview:logoImage];
    self.navigationItem.leftBarButtonItem =
    [[UIBarButtonItem alloc] initWithCustomView:viewLogo];
    
    UIView *viewNext = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 65, 30)];
    
    labelButton = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 65, 30)];
    labelButton.backgroundColor = [UIColor clearColor];
    labelButton.textColor = RGBCOLOR(104.f, 144.f, 152.f);
    labelButton.font = kAppFontSemibold(15);
    labelButton.textAlignment = NSTextAlignmentRight;
    [viewNext addSubview:labelButton];

    UIButton *buttonDone = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 65, 30)];
    [buttonDone setBackgroundColor:[UIColor clearColor]];
    [buttonDone addTarget:self action:@selector(buttonDoneClicked)
      forControlEvents:UIControlEventTouchUpInside];
    [viewNext addSubview:buttonDone];
    
    UIBarButtonItem *buttonRight = [[UIBarButtonItem alloc] initWithCustomView:viewNext];
    
    self.navigationItem.rightBarButtonItem = buttonRight;
    self.navigationItem.titleView.frame = CGRectMake(0, 0, 245, 30);
    
    self.labelWelcome.font = kAppFontBold(15);
    self.labelLanguageIntroduction.font = kAppFontItalic(15);
    
    self.viewLabelContainer.backgroundColor = RGBCOLOR(207.0f, 210.0f, 211.0f);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    labelTitle.text = @"";
    
    [self navigationController].navigationBarHidden = NO;
    [self.tableViewLanguages reloadData];
}

- (BOOL)shouldAutorotate    {
    return YES;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - IBLanguageTableViewCellDelegate methods

- (void)languageSelectedWithIndexTag:(NSInteger)indexTag
{
    //NSLog(@"Language selected at index %ld",(long)indexTag);
}

#pragma mark - Table view data source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //
    
    return [arrayLanguages count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 46.1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{ 
    static NSString *CellIdentifier = @"Cell";
    IBLanguageTableViewCell *cell = (IBLanguageTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[IBLanguageTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.delegate = self;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    NSString *displayString = [[[self.arrayLanguages objectAtIndex:indexPath.row] valueForKey:@"displayName"] uppercaseString];
    
    if([[[displayString substringToIndex:2] lowercaseString] isEqualToString:langString]) {
        [cell setChecked:YES];
        [SharedAppDelegate setResourcePath:[NSString stringWithFormat:@"%@/%@",[IBGlobal createDocumentsDirectoryByAppendingPathComponent:nil],[[self.arrayLanguages objectAtIndex:indexPath.row] valueForKey:@"resourcePath"]]];
        [self refreshView];
    }
    else {
         [cell setChecked:NO];
    }
    
    cell.tag = indexPath.row;
    cell.textLabel.text = displayString;
    //
    NSLog(@"cell.textLabel.text = displayString; = %@", cell.textLabel.text = displayString);
    //
    return cell;
}

#pragma mark -
#pragma mark - Table view delegates

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    langIndex = indexPath.row;
    self.langString = [[[[self.arrayLanguages objectAtIndex:indexPath.row] valueForKey:@"displayName"] substringToIndex:2] lowercaseString];
    [self.tableViewLanguages reloadData];
}

#pragma mark -

- (void)refreshView
{
    NSDictionary *langDictionary = [SharedAppDelegate langDictionary];
    
    NSString *languageIntro = [langDictionary valueForKey:@"LabelLanguageIntroduction"];
    NSData *data = [languageIntro dataUsingEncoding:NSUTF8StringEncoding];
    NSString *convertedString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    convertedString = [convertedString stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"];
    
    [self.labelLanguageIntroduction setText:convertedString];
    
    labelButton.text = [langDictionary valueForKey:@"LabelDone"];
    labelTitle.text = [langDictionary valueForKey:@"LabelSelectLanguage"];
}

- (void)buttonDoneClicked
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:self.langString forKey:@"LanguageString"];
    [defaults setInteger:self.langIndex forKey:@"LanguageIndex"];
    [defaults setBool:YES forKey:@"LanguagePreferred"];
    [defaults synchronize];
    
    [SharedAppDelegate setLanguageIndex:self.langIndex];
    
    [[SharedAppDelegate database] setLanguageSelectedByDisplayName:[[self.arrayLanguages objectAtIndex:langIndex] valueForKey:@"displayName"]];
    [SharedAppDelegate setResourcePath:[NSString stringWithFormat:@"%@/%@",[IBGlobal createDocumentsDirectoryByAppendingPathComponent:nil],[[self.arrayLanguages objectAtIndex:self.langIndex] valueForKey:@"resourcePath"]]];
    
    [self dismissViewControllerAnimated:YES completion:^{
        //NSLog(@"View Dismissed successfully");
    }];
}

@end
