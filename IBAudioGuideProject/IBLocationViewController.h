//
//  IBLocationViewController.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 22/04/14.
//  Copyright (c) 2014 Jojin Johnson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IBLocationViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIImageView *backgroundImageView;
@property (nonatomic, weak) IBOutlet UIImageView *logoImageView;
@end
