//
//  IBLocationViewController.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 22/04/14.
//  Copyright (c) 2014 Jojin Johnson. All rights reserved.
//

#import "IBLocationViewController.h"

#import "IBGlobal.h"
#import "HUDManager.h"
//
#import "IBDownloadsViewController.h"

@interface IBLocationViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) NSArray *arrayLocations; 
@property (weak, nonatomic) IBOutlet UIButton *buttonSettingsTouched;
@property (weak, nonatomic) IBOutlet UITableView *tableViewLocations;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *settingsButtonTopSpaceConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoImageTopConstraint;

- (IBAction)buttonSettingsTouched:(id)sender;

@end

@implementation IBLocationViewController

@synthesize arrayLocations;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if (IOS_OLDER_THAN(6.0)) {
        [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:NO];
    }
    
    if(IOS_OLDER_THAN(7.0)) {
        self.settingsButtonTopSpaceConstraint.constant = 14;
        self.logoImageTopConstraint.constant = 54;
    }
    
    NSArray *array = [[SharedAppDelegate database] fetchLocationIndexes];
    
    self.arrayLocations = [NSArray arrayWithObjects:[array[0] valueForKey:@"museumName"],[array[1] valueForKey:@"museumName"],[array[2] valueForKey:@"museumName"], nil];
    self.tableViewLocations.delegate = self;
    self.tableViewLocations.dataSource = self;
    self.tableViewLocations.backgroundColor = [UIColor blackColor];
    [self.view setBackgroundColor:[UIColor blackColor]];
    self.tableViewLocations.bounces = NO;
    self.tableViewLocations.separatorColor = RGBCOLOR(149.f, 149.f, 149.f);
}

- (void)viewWillAppear:(BOOL)animated {
    //
    [super viewWillAppear:animated];
    [self navigationController].navigationBarHidden = YES;
    //
    if(![[[[NSUserDefaults standardUserDefaults] valueForKey:@"LanguagePreferred"] stringValue] isEqualToString:@"1"]) {
        //
        [self.view setAlpha:0.f];
        [self performSegueWithIdentifier:@"segueToLanguage" sender:self];
    } else {
        //
        [UIView animateWithDuration:1.f animations:^{
            [self.view setAlpha:1.f];
        }];
    }
    //
    [self.tableViewLocations reloadData];
}

- (BOOL)shouldAutorotate    {
    return YES;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations   {
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Navigation

//
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    //
    UIViewController *controller = [segue destinationViewController];
    if ([segue.identifier isEqualToString:@"segueFromIsolaBella"]) {
        //segue to Isola Madre
        IBDownloadsViewController *vc = (IBDownloadsViewController *)controller;
        vc.uiMode = IBDownloadsViewControllerUIModeIsolaBella;
        [SharedAppDelegate setLocationNo:1];
    } else  if ([segue.identifier isEqualToString:@"segueFromIsolaMadre"])  {
        //segue to Isola Madre
        IBDownloadsViewController *vc = (IBDownloadsViewController *)controller;
        vc.uiMode = IBDownloadsViewControllerUIModeIsolaMadre;
        [SharedAppDelegate setLocationNo:2];
    } else  if ([segue.identifier isEqualToString:@"segueFromRoccaAngera"])  {
        //segue to Rocca d'Angera
        IBDownloadsViewController *vc = (IBDownloadsViewController *)controller;
        vc.uiMode = IBDownloadsViewControllerUIModeRoccaAngera;
        [SharedAppDelegate setLocationNo:3];
    } else {
        //segue to settings
        [controller.view setTag:-1];
    }
}

- (IBAction)buttonSettingsTouched:(id)sender {
    //
    [self performSegueWithIdentifier:@"segueToSettings" sender:self];
}

#pragma mark - Table view data source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrayLocations count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    //
    if (cell == nil) {
        //
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor blackColor];
        cell.textLabel.textColor = RGBCOLOR(211.f, 211.f, 211.f);
        cell.textLabel.font = kAppFontSemibold(16);
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"navigation.png"]];
    }
    //
    cell.tag = indexPath.row;
    cell.textLabel.text = [arrayLocations objectAtIndex:indexPath.row];
    NSInteger locationNo = indexPath.row+1;
    NSString *museumName = nil;
    MUSEUMNAME_STR(locationNo, museumName);
    cell.textLabel.text = museumName;
    //
    return cell;
}

#pragma mark -
#pragma mark - Table view delegates

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //
    NSString *segueID = nil;
    //
    if(indexPath.row == 0) segueID = @"segueFromIsolaBella";
    else if (indexPath.row == 1) segueID = @"segueFromIsolaMadre";
    else if (indexPath.row == 2) segueID = @"segueFromRoccaAngera";
    //
    [self performSegueWithIdentifier:segueID sender:self];
}

#pragma mark -

@end
