//
//  IBLocationViewControllerIphone.m
//  IBAudioGuideProject
//
//  Created by T T Marshel Daniel on 14/03/16.
//  Copyright © 2016 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBLocationViewControllerIphone.h"
#import "UIView+InterPolation.h"
#import "IBGlobal.h"


@interface IBLocationViewControllerIphone ()

@end

@implementation IBLocationViewControllerIphone

-(void)viewDidLoad {
    //
    [super viewDidLoad];
    //
    [self _applyShadow];
}//

-(void)viewDidAppear:(BOOL)animated {
    //
    [super viewDidAppear:animated];
    // [SharedAppDelegate animatePlayerHidden:YES];
   // [self _addMotionEffect];
}

-(void)viewDidDisappear:(BOOL)animated {
    //
    [super viewDidDisappear:animated];
    //
   // [self.backgroundImageView removeMotionEffects];
}

-(void)_applyShadow {
    //
    self.logoImageView.layer.masksToBounds = NO;
    self.logoImageView.layer.shadowOffset = CGSizeMake(6, 8);
    self.logoImageView.layer.shadowRadius = 8;
    self.logoImageView.layer.shadowOpacity = 0.3;
}

-(void)_addMotionEffect {
    //
    UIInterpolatingMotionEffect *xEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    xEffect.minimumRelativeValue = @(-40);
    xEffect.maximumRelativeValue = @(40);
    //
    UIInterpolatingMotionEffect *shadowEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"layer.shadowOffset" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    shadowEffect.minimumRelativeValue = [NSValue valueWithCGSize:CGSizeMake(-10, 5)];
    shadowEffect.maximumRelativeValue = [NSValue valueWithCGSize:CGSizeMake(10, 5)];
    //
    UIInterpolatingMotionEffect *yEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    yEffect.minimumRelativeValue = @(-40);
    yEffect.maximumRelativeValue = @(40);
    //
    UIMotionEffectGroup *group = [UIMotionEffectGroup new];
    group.motionEffects = @[xEffect, shadowEffect, yEffect];
    
    [self.backgroundImageView addMotionEffect:group];
}

@end
