//
//  IBLocationViewController.h
//  IBAudioGuide_iPad
//
//  Created by Mobility 2014 on 01/08/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IBLocationViewControllerTabletDelegate
@required
- (void)triggerSettingsViewController;
- (void)triggerLocationWithIndex:(NSInteger)locationNo;
@end

@interface IBLocationViewControllerTablet : UIViewController

@property (nonatomic, assign) id<IBLocationViewControllerTabletDelegate> delegate;
@property (nonatomic, assign) BOOL lockInteractionWithLocation;

@property (weak, nonatomic) IBOutlet UIImageView *imageViewLocation1;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewLocation2;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewLocation3;

@property (strong, nonatomic) IBOutlet UIView *viewBackgroundIsolaBellaImage;
@property (strong, nonatomic) IBOutlet UIView *viewBackgoundISolaMadrid;
@property (strong, nonatomic) IBOutlet UIView *viewBackgroundRoccaAngera;

@property (strong, nonatomic) IBOutlet UIButton *buttonSettings;



@property (weak, nonatomic) IBOutlet UILabel *labelLocation1;
@property (weak, nonatomic) IBOutlet UILabel *labelLocation2;
@property (weak, nonatomic) IBOutlet UILabel *labelLocation3;

@property (weak, nonatomic) IBOutlet UILabel *labelSettings;

- (IBAction)tapGestureLocationTriggered:(id)sender;
- (IBAction)buttonSettingsTouched:(id)sender;
- (void)revertToPreviousIndex;

- (void)updateLanguage;

@end
