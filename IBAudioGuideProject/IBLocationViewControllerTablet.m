//
//  IBLocationViewController.m
//  IBAudioGuide_iPad
//
//  Created by Mobility 2014 on 01/08/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBLocationViewControllerTablet.h"
#import "UIButton+Extensions.h"
#import "IBGlobal.h"

#define kMuseumLabelFont kAppFontSemibold(9)
#define kSettingsLabelFont kAppFontSemibold(9)
#define kNormalTextColor RGBCOLOR(255, 255, 255)
#define kHighlightTextColor RGBCOLOR(104, 144, 152)

@interface IBLocationViewControllerTablet ()
{
    NSInteger currentIndexTag;
    NSInteger previousIndexTag;
}

- (void)updateViewWithtag:(NSInteger)index;

@end

@implementation IBLocationViewControllerTablet

@synthesize delegate = _delegate;
@synthesize lockInteractionWithLocation = _lockInteractionWithLocation,buttonSettings;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [buttonSettings setHitTestEdgeInsets:UIEdgeInsetsMake(-40, -40, -40, -40)];
    currentIndexTag = -1;
    previousIndexTag = -1;
    _lockInteractionWithLocation = NO;
    
    self.labelLocation1.font = kMuseumLabelFont;
    self.labelLocation2.font = kMuseumLabelFont;
    self.labelLocation3.font = kMuseumLabelFont;
    
    self.labelSettings.font = kSettingsLabelFont;
    
    [self updateLanguage];
    
    self.labelLocation1.textColor = kNormalTextColor;
    self.labelLocation2.textColor = kNormalTextColor;
    self.labelLocation3.textColor = kNormalTextColor;
    
    self.labelSettings.textColor = kNormalTextColor;
}


- (IBAction)tapGestureLocationTriggered:(id)sender {
    if(self.lockInteractionWithLocation==YES)  return;
    
    UITapGestureRecognizer *gesture = (UITapGestureRecognizer *)sender;
    if(gesture.view.tag != currentIndexTag) {
        //Tag added in storyboard page for all 3 location specific views in order 1 (Bella),2 (Madre),3 (Angera)
        previousIndexTag = currentIndexTag;
        currentIndexTag = gesture.view.tag;
        _lockInteractionWithLocation = YES;
        [self updateViewWithtag:gesture.view.tag];
        
        if([[SharedAppDelegate childSplitViewController] showThirdContainer])   {
            [[SharedAppDelegate childSplitViewController] setShowThirdContainer:NO];
        }
    }
}

- (IBAction)buttonSettingsTouched:(id)sender {
    //
    [_delegate triggerSettingsViewController]; 
}

- (void)updateViewWithtag:(NSInteger)index  {
    //
    if (index == 1) {
        //
        self.labelLocation1.textColor = kHighlightTextColor;
        self.labelLocation2.textColor = kNormalTextColor;
        self.labelLocation3.textColor = kNormalTextColor;
        self.imageViewLocation1.image = [UIImage imageNamed:@"bellaNew-icon-ipad.png"];
        self.imageViewLocation2.image = [UIImage imageNamed:@"madreNew-icon-ipad.png"];
        self.imageViewLocation3.image = [UIImage imageNamed:@"angeraNew-icon-ipad.png"];
		self.viewBackgoundISolaMadrid.backgroundColor = [UIColor clearColor];
		self.viewBackgroundRoccaAngera.backgroundColor = [UIColor clearColor];
        self.viewBackgroundIsolaBellaImage.backgroundColor = [IBGlobal colorWithHexString:@"699098"];
    } else if (index == 2) {
        //
        self.labelLocation1.textColor = kNormalTextColor;
        self.labelLocation2.textColor = kHighlightTextColor;
        self.labelLocation3.textColor = kNormalTextColor;
        self.imageViewLocation1.image = [UIImage imageNamed:@"bellaNew-icon-ipad.png"];
        self.imageViewLocation2.image = [UIImage imageNamed:@"madreNew-icon-ipad.png"];
        self.imageViewLocation3.image = [UIImage imageNamed:@"angeraNew-icon-ipad.png"];
        self.viewBackgoundISolaMadrid.backgroundColor = [IBGlobal colorWithHexString:@"699098"];
		self.viewBackgroundRoccaAngera.backgroundColor = [UIColor clearColor];
        self.viewBackgroundIsolaBellaImage.backgroundColor = [UIColor clearColor];
    } else {
        //
        self.labelLocation1.textColor = kNormalTextColor;
        self.labelLocation2.textColor = kNormalTextColor;
        self.labelLocation3.textColor = kHighlightTextColor; 
        self.imageViewLocation1.image = [UIImage imageNamed:@"bellaNew-icon-ipad.png"];
        self.imageViewLocation2.image = [UIImage imageNamed:@"madreNew-icon-ipad.png"];
        self.imageViewLocation3.image = [UIImage imageNamed:@"angeraNew-icon-ipad.png"];
		self.viewBackgroundIsolaBellaImage.backgroundColor = [UIColor clearColor];
		self.viewBackgoundISolaMadrid.backgroundColor = [UIColor clearColor];
        self.viewBackgroundRoccaAngera.backgroundColor = [IBGlobal colorWithHexString:@"699098"];
    }
    
    [SharedAppDelegate setLocationNo:index];
    [_delegate triggerLocationWithIndex:index];
    //
  //  NSLog(@"_delegate = %@", _delegate);
}

- (void)revertToPreviousIndex {
    //
    if(previousIndexTag == 1) {
        //
        self.labelLocation1.textColor = kHighlightTextColor;
        self.labelLocation2.textColor = kNormalTextColor;
        self.labelLocation3.textColor = kNormalTextColor;
		self.imageViewLocation1.image = [UIImage imageNamed:@"bellaNew-icon-ipad.png"];
		self.imageViewLocation2.image = [UIImage imageNamed:@"madreNew-icon-ipad.png"];
		self.imageViewLocation3.image = [UIImage imageNamed:@"angeraNew-icon-ipad.png"];
        currentIndexTag = 1;
        //
    } else if(previousIndexTag == 2) {
        //
        self.labelLocation1.textColor = kNormalTextColor;
        self.labelLocation2.textColor = kHighlightTextColor;
        self.labelLocation3.textColor = kNormalTextColor;
		self.imageViewLocation1.image = [UIImage imageNamed:@"bellaNew-icon-ipad.png"];
		self.imageViewLocation2.image = [UIImage imageNamed:@"madreNew-icon-ipad.png"];
		self.imageViewLocation3.image = [UIImage imageNamed:@"angeraNew-icon-ipad.png"];
        currentIndexTag = 2;
        //
    } else {
        //
        self.labelLocation1.textColor = kNormalTextColor;
        self.labelLocation2.textColor = kNormalTextColor;
        self.labelLocation3.textColor = kHighlightTextColor;
		self.imageViewLocation1.image = [UIImage imageNamed:@"bellaNew-icon-ipad.png"];
		self.imageViewLocation2.image = [UIImage imageNamed:@"madreNew-icon-ipad.png"];
		self.imageViewLocation3.image = [UIImage imageNamed:@"angeraNew-icon-ipad.png"]; 
        currentIndexTag = 3;
    }
    //
    [SharedAppDelegate setLocationNo:previousIndexTag];
}

- (void)updateLanguage
{
    NSString *museumName = nil;
    
    MUSEUMNAME_STR(1, museumName);
    self.labelLocation1.text = [museumName uppercaseString];
    
    MUSEUMNAME_STR(2, museumName);
    self.labelLocation2.text = [museumName uppercaseString];
    
    MUSEUMNAME_STR(3, museumName);
    self.labelLocation3.text = [museumName uppercaseString];
    
    self.labelSettings.text = [[SharedAppDelegate langDictionary] valueForKey:@"LabelSettings"];
}

@end
