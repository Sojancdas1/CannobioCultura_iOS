//
//  IBMasterSplitViewController.m
//  IBAudioGuideProject
//
//  Created by Hackintosh #01 on 06/08/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBMasterSplitViewController.h"
#import "IBGlobal.h"
#import "IBLocationViewControllerTablet.h"
#import "IBDetailViewController.h"
#import "IBCatogoryListViewControllerTablet.h"

#define kMasterViewWidth 95.f
#define kSplitGap 0.f

@interface IBMasterSplitViewController ()<IBLocationViewControllerTabletDelegate,IBDetailViewControllerDelegate>

- (void)triggerLanguageSelectionViewController;

@end

@implementation IBMasterSplitViewController

- (void)viewWillAppear:(BOOL)animated   {
    [super viewWillAppear:animated];
    
    if (IOS_OLDER_THAN(6.0))    {
        [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeLeft animated:NO];
    }
}

- (void)viewDidAppear:(BOOL)animated    {
    [super viewDidAppear:animated];
    
    if(![[[[NSUserDefaults standardUserDefaults] valueForKey:@"LanguagePreferred"] stringValue] isEqualToString:@"1"]) {
        [self performSelector:@selector(triggerLanguageSelectionViewController) withObject:nil afterDelay:0.f];
    }
}

#pragma mark - Navigation


-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

-(UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleBlackOpaque;
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    self.view.backgroundColor = [UIColor blackColor];
    
    [SharedAppDelegate setMasterSplitController:self];
    IBLocationViewControllerTablet *locationViewController = [self.viewControllers objectAtIndex:0];
    [SharedAppDelegate setLocationViewController:locationViewController];
    UINavigationController *navigationController = [self.viewControllers objectAtIndex:1];
    [SharedAppDelegate setCurrentNavigation:navigationController];
    IBDetailViewController *detailViewController = navigationController.viewControllers[0];
    
    if (navigationController.view.frame.origin.x > 0.0)
    {
        if(IOS_OLDER_THAN(8.0)) {
            locationViewController.view.frame = CGRectMake(0,0, kMasterViewWidth, self.view.bounds.size.height);
            navigationController.view.frame = CGRectMake(kMasterViewWidth + kSplitGap, 0, self.view.bounds.size.width - kMasterViewWidth - kSplitGap, self.view.bounds.size.height);
            
            [locationViewController.view setNeedsLayout];
            [navigationController.view setNeedsLayout];
        }
        else    {
            self.preferredPrimaryColumnWidthFraction = 0.09;
        }
    }
    
    locationViewController.delegate = self;
    detailViewController.delegate = self;
}

- (void)triggerLanguageSelectionViewController {
    //
    NSLog(@"segueToLanguage");
    [self performSegueWithIdentifier:@"segueToLanguage" sender:self];
}

#pragma IBLocationViewControllerTabletDelegate methods

- (void)triggerSettingsViewController {
    //
    NSLog(@"segueToSettings");
    [self performSegueWithIdentifier:@"segueToSettings" sender:self];
}

- (void)triggerLocationWithIndex:(NSInteger)locationNo  {
    //
    NSLog(@"triggerLocationWithIndex %ld", (long)locationNo);
    IBLocationViewControllerTablet *locationViewController = [self.viewControllers objectAtIndex:0];
    [locationViewController setLockInteractionWithLocation:NO];
    
    UINavigationController *navigationController = [self.viewControllers objectAtIndex:1];
    [SharedAppDelegate setCurrentNavigation:navigationController];
    
    if([navigationController.viewControllers count] > 1)  {
        [[SharedAppDelegate categoryListController] refreshView]; 
    } else {
        IBDetailViewController *detailViewController = navigationController.viewControllers[0];
        [detailViewController performSegueWithIdentifier:@"segueToSplitter" sender:detailViewController];
    }
}

#pragma -
#pragma IBDetailViewControllerDelegate methods

- (void)downloadOnProgressSignal
{
    IBLocationViewControllerTablet *masterViewController = [self.viewControllers objectAtIndex:0];
    [masterViewController revertToPreviousIndex];
    
    double delayInSeconds = 0.1f;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [masterViewController setLockInteractionWithLocation:NO];
    });
}

- (void)detailsLoadedSuccess
{
    IBLocationViewControllerTablet *masterViewController = [self.viewControllers objectAtIndex:0];
    
    double delayInSeconds = 0.1f;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [masterViewController setLockInteractionWithLocation:NO];
    });
}

- (void)downloadingStated
{
    IBLocationViewControllerTablet *masterViewController = [self.viewControllers objectAtIndex:0];
    [masterViewController setLockInteractionWithLocation:YES];
}

- (void)downloadingStopped
{
    IBLocationViewControllerTablet *masterViewController = [self.viewControllers objectAtIndex:0];
    
    double delayInSeconds = 0.1f;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [masterViewController setLockInteractionWithLocation:NO];
    });
}

#pragma -

@end
