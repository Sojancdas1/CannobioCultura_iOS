//
//  IBMuseumRoomInfo.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 21/05/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBMuseumRoomInfo.h"

@implementation IBMuseumRoomInfo

@synthesize coordinates;
@synthesize roomNo;
@synthesize sectionNo;
@synthesize circleCoordinates;

@end
