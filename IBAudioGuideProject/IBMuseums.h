//
//  IBMuseums.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 23/07/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class IBAudioGuide, IBGalleryImages;

@interface IBMuseums : NSManagedObject

@property (nonatomic, retain) NSNumber * guidePurchased;
@property (nonatomic, retain) NSString * museumDescription;
@property (nonatomic, retain) NSString * museumName;
@property (nonatomic, retain) NSSet *museumToAudioGuide;
@property (nonatomic, retain) NSSet *museumToImageGallery;
@end

@interface IBMuseums (CoreDataGeneratedAccessors)

- (void)addMuseumToAudioGuideObject:(IBAudioGuide *)value;
- (void)removeMuseumToAudioGuideObject:(IBAudioGuide *)value;
- (void)addMuseumToAudioGuide:(NSSet *)values;
- (void)removeMuseumToAudioGuide:(NSSet *)values;

- (void)addMuseumToImageGalleryObject:(IBGalleryImages *)value;
- (void)removeMuseumToImageGalleryObject:(IBGalleryImages *)value;
- (void)addMuseumToImageGallery:(NSSet *)values;
- (void)removeMuseumToImageGallery:(NSSet *)values;

@end
