//
//  IBMuseums.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 23/07/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBMuseums.h"
#import "IBAudioGuide.h"
#import "IBGalleryImages.h"


@implementation IBMuseums

@dynamic guidePurchased;
@dynamic museumDescription;
@dynamic museumName;
@dynamic museumToAudioGuide;
@dynamic museumToImageGallery;

@end
