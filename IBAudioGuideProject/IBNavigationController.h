//
//  IBNavigationController.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 30/04/14.
//  Copyright (c) 2014 Jojin Johnson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IBNavigationController : UINavigationController

@end
