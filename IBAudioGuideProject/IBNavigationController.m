//
//  IBNavigationController.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 30/04/14.
//  Copyright (c) 2014 Jojin Johnson. All rights reserved.
//

#import "IBNavigationController.h"

#import "IBGlobal.h"
#import "IBPhotoGalleryViewController.h"
#import "IBGuideGalleryViewController.h"

@interface IBNavigationController ()

@end

@implementation IBNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    int interfaceOrientation = 0;
    
    if (self.viewControllers.count > 0)
    {
        for (id viewController in self.viewControllers)
        {
            if ([viewController isKindOfClass:([IBPhotoGalleryViewController class])] || [viewController isKindOfClass:([IBGuideGalleryViewController class])])
            {
                interfaceOrientation = UIInterfaceOrientationMaskAll;
            }
            else
            {
                interfaceOrientation = UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
            }
        }
    }
    
    return interfaceOrientation;
}

- (UIViewController *)childViewControllerForStatusBarStyle {
    return self.topViewController;
}

@end
