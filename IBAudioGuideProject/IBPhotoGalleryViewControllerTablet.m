//
//  IBPhotoGalleryViewControllerTablet.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 05/08/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBPhotoGalleryViewControllerTablet.h"

#import "IBGlobal.h"
#import "HUDManager.h"

#import "IBGalleryHeaderViewTablet.h"
#import "IBGalleryDescriptionView.h"

#import "UIImage+Resize.h"
#import "UIScreen+SSToolkitAdditions.h"

#define safeModulo(x,y) ((y + x % y) % y)

@interface IBPhotoGalleryViewControllerTablet ()<UIActionSheetDelegate,UIScrollViewDelegate,UIGestureRecognizerDelegate>
{
    BOOL fullView;
    IBGalleryHeaderViewTablet *galleryHeaderView;
    IBGalleryDescriptionView *galleryDescriptionView;
    
    NSArray *_arrayGalleryImages;
}

@property (nonatomic, assign) NSInteger previousIndex;

@property (nonatomic, strong)  UIImageView *imageViewPrev;
@property (nonatomic, strong)  UIImageView *imageViewCentre;
@property (nonatomic, strong)  UIImageView *imageViewNext;

@property (nonatomic, strong) NSArray *arrayGalleryImages;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewGalleryMaster;

- (void)refreshView;
- (void)buttonBackTouched;
- (void)loadFullView;
- (void)hideInfoBar:(BOOL)status;
- (void)handleSigleTap:(UIGestureRecognizer *)gestureRecognizer;
- (void)handleDoubleTap:(UITapGestureRecognizer *)gestureRecognizer;
- (void)arrangeGalleryRespectToOrientation;

@end

@implementation IBPhotoGalleryViewControllerTablet

@synthesize imageViewPrev,imageViewCentre,imageViewNext;

@synthesize previousIndex = _previousIndex;
@synthesize currentIndex = _currentIndex;
@synthesize arrayGalleryImages = _arrayGalleryImages;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        fullView = YES;
        _arrayGalleryImages = nil;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (IOS_OLDER_THAN(6.0))    {
        [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:NO];
    }
    
    if (IOS_NEWER_OR_EQUAL_TO(7.0))    {
        [self setNeedsStatusBarAppearanceUpdate];
    }
    
    galleryHeaderView = [[IBGalleryHeaderViewTablet alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 60)];
    [self.view addSubview:galleryHeaderView];
    
    [galleryHeaderView.buttonBack addTarget:self action:@selector(buttonBackTouched) forControlEvents:UIControlEventTouchUpInside];
    
    galleryDescriptionView = [[IBGalleryDescriptionView alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height/2, self.view.frame.size.width, self.view.frame.size.height/2)];
    [self.view addSubview:galleryDescriptionView];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSigleTap:)];
    [singleTap setNumberOfTapsRequired:1];
    [self.view addGestureRecognizer:singleTap];
    
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap:)];
    doubleTap.numberOfTapsRequired = 2;
    doubleTap.delegate = self;
    [self.scrollViewGalleryMaster addGestureRecognizer:doubleTap];
    
    [singleTap requireGestureRecognizerToFail:doubleTap];
    
    self.scrollViewGalleryMaster.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated   {
    [super viewWillAppear:animated];
    
    [[SharedAppDelegate playerView] setHidden:YES];
    
    [self navigationController].navigationBarHidden = YES;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [galleryHeaderView setAlpha:0.f];
    [galleryDescriptionView setAlpha:0.f];
    
    [self.scrollViewGalleryMaster setAlpha:0.f];
    
    [HUDManager addHUDWithLabel:nil dimBackground:YES];
    
    [self performSelector:@selector(refreshView) withObject:nil afterDelay:0.1f];
}

- (void)viewDidAppear:(BOOL)animated    {
    [super viewDidAppear:animated];
    
    [galleryHeaderView setNeedsDisplay];
    [galleryDescriptionView setNeedsDisplay];
    
    [UIView animateWithDuration:0.1f animations:^{
        [self.scrollViewGalleryMaster setAlpha:1.f];
    }];
    
    [UIView animateWithDuration:1.f animations:^{
        [galleryHeaderView setAlpha:1.f];
        [galleryDescriptionView setAlpha:1.f];
    }];
}

- (void)viewWillDisappear:(BOOL)animated    {
    [super viewWillDisappear:animated];
    
    [[SharedAppDelegate playerView] setHidden:NO];
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (BOOL)shouldAutorotate    {
    return NO;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations   {
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationMaskAll;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    [galleryHeaderView setAlpha:0.f];
    [galleryDescriptionView setAlpha:0.f];
    [[SharedAppDelegate playerView] setAlpha:0.f];
    
    [self.scrollViewGalleryMaster setAlpha:0.f];
    
    _previousIndex = [self currentIndex];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    
    [galleryHeaderView setNeedsDisplay];
    [galleryDescriptionView setNeedsDisplay];
    
    [self arrangeGalleryRespectToOrientation];
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self
                                             selector:@selector(loadFullView)
                                               object:nil];
    
    [self setCurrentIndex:_previousIndex];
    
    [UIView animateWithDuration:0.1f animations:^{
        [self.scrollViewGalleryMaster setAlpha:1.f];
    }];
    
    [UIView animateWithDuration:1.f animations:^{
        [galleryHeaderView setAlpha:1.f];
        [galleryDescriptionView setAlpha:1.f];
        [[SharedAppDelegate playerView] setAlpha:1.f];
    } completion:^(BOOL finished) {
        [self performSelector:@selector(loadFullView) withObject:nil afterDelay:3.f];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (void)buttonBackTouched
{
    [self dismissViewControllerAnimated:YES completion:^{
        //NSLog(@"dismissed");
    }];
}

- (void)loadFullView
{
    [self hideInfoBar:YES];
}

- (void)hideInfoBar:(BOOL)status
{
    static BOOL isAnimating = NO;
    
    if(!isAnimating)    {
        isAnimating = YES;
        
        CGRect frameHeaderView = galleryHeaderView.frame;
        CGRect frameDescriptionView = galleryDescriptionView.frame;
        
        if(status == YES)    {
            frameHeaderView.origin.y = -frameHeaderView.size.height-20;
            frameDescriptionView.origin.y = self.scrollViewGalleryMaster.frame.size.height+frameDescriptionView.size.height;
        }
        else    {
            frameHeaderView.origin.y = 0;
            frameDescriptionView.origin.y = self.scrollViewGalleryMaster.frame.size.height-frameDescriptionView.size.height;
        }
        
        if(status != YES)  {
            galleryHeaderView.alpha = 1.f;
            galleryDescriptionView.alpha = 1.f;
            
            if(fullView)    {
                galleryDescriptionView.frame = CGRectMake(frameDescriptionView.origin.x, self.scrollViewGalleryMaster.frame.size.height+frameDescriptionView.size.height, frameDescriptionView.size.width, frameDescriptionView.size.height);
            }
        }
        
        [UIView animateWithDuration:1
                         animations:^{
                             galleryHeaderView.frame = frameHeaderView;
                             galleryDescriptionView.frame = frameDescriptionView;
                         } completion:^(BOOL finished) {
                             isAnimating = NO;
                             fullView = status;
                             
                             if(status == YES)  {
                                 galleryHeaderView.alpha = 0.f;
                                 galleryDescriptionView.alpha = 0.f;
                             }
                         }];
    }
}

- (void)refreshView
{
    NSInteger locationNo = [SharedAppDelegate locationNo];
    NSUInteger languageIndex = [SharedAppDelegate languageIndex];
    
    NSString *museumDir,*languageDir;
    
    DIRLOCATION_STR(locationNo,museumDir);
    DIRLANGUAGE_STR(languageIndex, languageDir);
    
    if(_arrayGalleryImages) { _arrayGalleryImages = nil; }
    _arrayGalleryImages = [[SharedAppDelegate database] loadPhotoGalleryWithLocationNo:locationNo andLanguageIndex:languageIndex];
    
    NSString *museumName = nil;
    MUSEUMNAME_STR(locationNo, museumName);
    
    NSDictionary *langDictionary = [SharedAppDelegate langDictionary];
    
    galleryHeaderView.labelTitle.text = museumName;
    galleryHeaderView.labelDetails.text = [[langDictionary valueForKey:@"LabelPhotoGallery"] stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
    
    [self arrangeGalleryRespectToOrientation];
    
     //NSLog(@"ibphotogalleryview Hud");
    [HUDManager hideHUDFromWindowAfterDelay:0.2f];
    [self performSelector:@selector(loadFullView) withObject:nil afterDelay:3.f];
}

- (void)arrangeGalleryRespectToOrientation
{
    if(UIInterfaceOrientationIsLandscape([self interfaceOrientation]))  {
        self.scrollViewGalleryMaster.contentSize = CGSizeMake(CGRectGetWidth(self.scrollViewGalleryMaster.frame)*[_arrayGalleryImages count], CGRectGetHeight(self.scrollViewGalleryMaster.frame));
    }
    else    {
        self.scrollViewGalleryMaster.contentSize = CGSizeMake(CGRectGetWidth(self.scrollViewGalleryMaster.frame)*[_arrayGalleryImages count], CGRectGetHeight(self.scrollViewGalleryMaster.frame));
    }
    
    CGRect rect = CGRectZero;
    
    rect.size = CGSizeMake(CGRectGetWidth(self.scrollViewGalleryMaster.frame), CGRectGetHeight(self.scrollViewGalleryMaster.frame));
    
    for(UIView *view in self.scrollViewGalleryMaster.subviews)  {
        [view removeFromSuperview];
    }
    
    // add prevView as first in line
    self.imageViewPrev = [[UIImageView alloc] initWithFrame:rect];
    
    UIScrollView *zoomScrollView = [[UIScrollView alloc] initWithFrame:rect];
    [self.scrollViewGalleryMaster addSubview:zoomScrollView];
    
    zoomScrollView.delegate = self;
    [zoomScrollView addSubview:self.imageViewPrev];
    zoomScrollView.minimumZoomScale = 1.0;
    zoomScrollView.maximumZoomScale = 2.5;
    self.imageViewPrev.frame = zoomScrollView.bounds;
    
    // add currentView in the middle (center)
    rect.origin.x += CGRectGetWidth(self.scrollViewGalleryMaster.frame);
    
    self.imageViewCentre = [[UIImageView alloc] initWithFrame:rect];
    //    [self.scrollViewGalleryMaster addSubview:self.centerImgView];
    
    zoomScrollView = [[UIScrollView alloc] initWithFrame:rect];
    zoomScrollView.delegate = self;
    zoomScrollView.minimumZoomScale = 1.0;
    zoomScrollView.maximumZoomScale = 2.5;
    [self.scrollViewGalleryMaster addSubview:zoomScrollView];
    
    [zoomScrollView addSubview:self.imageViewCentre];
    self.imageViewCentre.frame = zoomScrollView.bounds;
    
    // add nextView as third view
    rect.origin.x += CGRectGetWidth(self.scrollViewGalleryMaster.frame);
    
    self.imageViewNext = [[UIImageView alloc] initWithFrame:rect];
    //    [self.scrollViewGalleryMaster addSubview:self.nextImgView];
    
    zoomScrollView = [[UIScrollView alloc] initWithFrame:rect];
    [self.scrollViewGalleryMaster addSubview:zoomScrollView];
    zoomScrollView.delegate = self;
    zoomScrollView.minimumZoomScale = 1.0;
    zoomScrollView.maximumZoomScale = 2.5;
    
    [zoomScrollView addSubview:self.imageViewNext];
    self.imageViewNext.frame = zoomScrollView.bounds;
    
    // center the scrollview to show the middle view only
    [self.scrollViewGalleryMaster setContentOffset:CGPointMake(CGRectGetWidth(self.scrollViewGalleryMaster.frame), 0)  animated:NO];
    self.scrollViewGalleryMaster.userInteractionEnabled=YES;
    self.scrollViewGalleryMaster.pagingEnabled = YES;
    self.scrollViewGalleryMaster.delegate = self;
    
    self.imageViewPrev.contentMode = UIViewContentModeScaleAspectFit;
    self.imageViewCentre.contentMode = UIViewContentModeScaleAspectFit;
    self.imageViewNext.contentMode = UIViewContentModeScaleAspectFit;
    
    self.currentIndex = _currentIndex;
}

- (void)handleSigleTap:(UIGestureRecognizer *)gestureRecognizer
{
    if (fullView) {
        [self hideInfoBar:NO];
        fullView = NO;
    }
    else    {
        [self hideInfoBar:YES];
        fullView = YES;
    }
}

#pragma mark - color button actions-
#pragma mark -page controller action-

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView;  {
    //incase we are zooming the center image view parent
    if (self.imageViewCentre.superview == scrollView){
        return self.imageViewCentre;
    }
    
    return nil;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    UIView *subView = [scrollView.subviews objectAtIndex:0];
    
    CGFloat offsetX = (scrollView.bounds.size.width > scrollView.contentSize.width)?
    (scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5 : 0.0;
    
    CGFloat offsetY = (scrollView.bounds.size.height > scrollView.contentSize.height)?
    (scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5 : 0.0;
    
    subView.center = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX,
                                 scrollView.contentSize.height * 0.5 + offsetY);
}

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    //    CGFloat pageWidth = sender.frame.size.width;
    //    pageNumber_ = floor((sender.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView;{
    CGFloat pageWidth = scrollView.frame.size.width;
    _previousIndex = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    scrollView.scrollEnabled = NO;
    double delayInSeconds = 0.3;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        scrollView.scrollEnabled = YES;
    });
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)sender {
    CGFloat pageWidth = sender.frame.size.width;
    int page = floor((sender.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    
    //incase we are still in same page, ignore the swipe action
    if(_previousIndex == page) return;
    
    if(sender.contentOffset.x >= sender.frame.size.width) {
        //swipe left, go to next image
        [self setRelativeIndex:1];
        
        // center the scrollview to the center UIImageView
        [self.scrollViewGalleryMaster setContentOffset:CGPointMake(CGRectGetWidth(self.scrollViewGalleryMaster.frame), 0)  animated:NO];
	}
	else if(sender.contentOffset.x < sender.frame.size.width) {
        //swipe right, go to previous image
        [self setRelativeIndex:-1];
        
        // center the scrollview to the center UIImageView
        [self.scrollViewGalleryMaster setContentOffset:CGPointMake(CGRectGetWidth(self.scrollViewGalleryMaster.frame), 0)  animated:NO];
	}
    
    UIScrollView *scrollView = (UIScrollView *)self.imageViewCentre.superview;
    scrollView.zoomScale = 1.0;
}

- (void)handleDoubleTap:(UITapGestureRecognizer *)gestureRecognizer {
    //
    UIScrollView *scrollView = (UIScrollView *)self.imageViewCentre.superview;
    float scale = scrollView.zoomScale;
    scale += 1.0;
    if(scale > 2.0) scale = 1.0;
    [scrollView setZoomScale:scale animated:YES];
}

#pragma mark - image loading-

-(UIImage *)imageAtIndex:(NSInteger)inImageIndex {
    //
    inImageIndex = safeModulo(inImageIndex, [self totalImages]);
    //
    IBGalleryImages *info = [self.arrayGalleryImages objectAtIndex:inImageIndex];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@",[IBGlobal createDocumentsDirectoryByAppendingPathComponent:nil],info.photoPath];
    //
	UIImage *image = nil;
    //
    if (nil == image) {
        //
		NSString *imagePath = filePath;
        //
		if (imagePath) {
            //
			if([imagePath isAbsolutePath]) image = [UIImage imageWithContentsOfFile:imagePath];
            else image = [UIImage imageNamed:imagePath];
            //
            if (nil == image) {
                //
                image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imagePath]]];
            }
        }
    }
    
    if([[UIScreen mainScreen] isRetinaDisplay]) {
        //
        return image;
    } else {
        //
        UIImage *resized = [image resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:image.size interpolationQuality:kCGInterpolationMedium];
        
        return resized;
    }
}

#pragma mark -

- (NSInteger)totalImages {
    return [_arrayGalleryImages count];
}

- (NSInteger)currentIndex {
    return safeModulo(_currentIndex, [self totalImages]);
}

- (void)setCurrentIndex:(NSInteger)inIndex {
    //
    _currentIndex = inIndex;
    
    if([_arrayGalleryImages count] > 0){
        self.imageViewPrev.image   = [self imageAtIndex:[self relativeIndex:-1]];
        self.imageViewCentre.image = [self imageAtIndex:[self relativeIndex: 0]];
        self.imageViewNext.image   = [self imageAtIndex:[self relativeIndex: 1]];
        
        IBGalleryImages *info = [self.arrayGalleryImages objectAtIndex:[self relativeIndex:0]];
        
        galleryDescriptionView.descriptionHeader = @"";//[info photoTitle];
        galleryDescriptionView.descriptionText = [info photoDescription];
    }
}

- (NSInteger)relativeIndex:(NSInteger)inIndex {
    return safeModulo(([self currentIndex] + inIndex), [self totalImages]);
}

- (void)setRelativeIndex:(NSInteger)inIndex {
    [self setCurrentIndex:self.currentIndex + inIndex];
}

@end
