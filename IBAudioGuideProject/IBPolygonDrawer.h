//
//  IBPolygonDrawer.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 02/07/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IBPolygonDrawer : UIView
{
    NSArray *_coordinates;
}

//should be no with double
@property (nonatomic, strong) NSArray *coordinates;

@end
