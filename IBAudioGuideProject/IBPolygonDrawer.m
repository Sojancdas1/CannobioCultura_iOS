//
//  IBPolygonDrawer.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 02/07/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBPolygonDrawer.h"

#import "IBGlobal.h"

@implementation IBPolygonDrawer

@synthesize coordinates = _coordinates;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _coordinates = nil;
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
//    for(NSArray *arrayCoordinates in _coordinates)   {
//        
//        CGContextRef context = UIGraphicsGetCurrentContext();
//        
//        CGMutablePathRef a_path = CGPathCreateMutable();
//        CGContextBeginPath(context);
//        
//        CGContextSetLineWidth(context, 3);
//        CGContextSetLineCap(context, kCGLineCapRound);
//        
//        int j=0;
//        for(int i = 0; i<[arrayCoordinates count]/2; i++)  {
//            if(i==0)    {
//                CGContextMoveToPoint(context, [arrayCoordinates[j] doubleValue], [arrayCoordinates[j+1] doubleValue]);
//            }
//            else    {
//                CGContextAddLineToPoint(context, [arrayCoordinates[j] doubleValue], [arrayCoordinates[j+1] doubleValue]);
//            }
//            j = j+2;
//        }
//        
//        CGContextClosePath(context);
//        
//        CGContextAddPath(context, a_path);
//        
//        // Fill and stroke the path
//        CGContextSetStrokeColorWithColor(context, [RGBCOLOR(246.f, 151.f, 158.f) CGColor]);
//        CGContextSetFillColorWithColor(context, [RGBCOLOR_WITH_ALPHA(246.f, 151.f, 158.f, 0.84f) CGColor]);
//        
//        CGContextSaveGState( context );
//        CGContextFillPath(context);
//        CGContextRestoreGState( context );
//        
//        CGContextSaveGState( context );
//        CGContextStrokePath(context);
//        CGContextRestoreGState( context );
//
//        CGPathRelease(a_path);
//    }
    
    for(NSArray *arrayCoordinates in _coordinates)   {
        
        UIBezierPath * path = [UIBezierPath bezierPath];
        [path setLineWidth:3];
        
        int j=0;
        for(int i = 0; i<[arrayCoordinates count]/2; i++)  {
            if(i==0)    {
                [path moveToPoint:CGPointMake([arrayCoordinates[j] doubleValue], [arrayCoordinates[j+1] doubleValue])];
            } else {
                [path addLineToPoint:CGPointMake([arrayCoordinates[j] doubleValue], [arrayCoordinates[j+1] doubleValue])];
            }
            j = j+2;
        }
        
        [path closePath];
        
        path.lineCapStyle = kCGLineCapRound;
        [RGBCOLOR(207.f, 51.f, 60.f) setFill]; 
        [path fill];
    }
}

- (void)setCoordinates:(NSArray *)coordinates
{
    _coordinates = coordinates;
    [self setNeedsDisplay];
}

@end
