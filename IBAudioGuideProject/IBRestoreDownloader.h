//
//  IBComboDownloader.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 23/09/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IBRestoreDownloader : UIView {
    //
    UIButton *_button;
    UIProgressView *_progressView;
    UILabel *_labelHeader,*_labelDownloadStatus;
    UILabel *_labelNowDownloading, *_labelPendingToDownload;
}

@property (nonatomic,strong,readonly) UIButton *button;
@property (nonatomic,strong,readonly) UILabel *labelHeader;
@property (nonatomic,strong,readonly) UILabel *labelDownloadStatus;
@property (nonatomic,strong,readonly) UIProgressView *progressView;
@property (nonatomic,strong,readonly) UILabel *labelNowDownloading;
@property (nonatomic,strong,readonly) UILabel *labelPendingToDownload;

@end
