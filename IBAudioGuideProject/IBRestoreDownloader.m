//
//  IBComboDownloader.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 23/09/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBRestoreDownloader.h"

#import "IBGlobal.h"

static inline CGFloat degreesToRadians(CGFloat degrees) { return degrees/(180.0 * M_PI); }

@interface IBRestoreDownloader () {
    //
    UIView *_viewBackground;
    UIView *_viewButtonBorderLine;
}

@end

@implementation IBRestoreDownloader

@synthesize button = _button;
@synthesize labelHeader = _labelHeader;
@synthesize progressView = _progressView;
@synthesize labelDownloadStatus = _labelDownloadStatus;
@synthesize labelNowDownloading = _labelNowDownloading;
@synthesize labelPendingToDownload = _labelPendingToDownload;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        super.frame = CGRectMake(0, 0, 300, 175);
        self.backgroundColor = [UIColor clearColor];
        NSDictionary *languageDictionary = [SharedAppDelegate langDictionary];
        _viewBackground = [[UIView alloc] initWithFrame:CGRectMake(15, 15, 270, 145)];
        _viewBackground.backgroundColor = RGBCOLOR_WITH_ALPHA(230, 230, 230, 0.9);
        _viewBackground.layer.cornerRadius = 10.f;
        _viewBackground.layer.shadowColor = [UIColor blackColor].CGColor;
        _viewBackground.layer.shadowOpacity = 0.3;
        _viewBackground.layer.shadowRadius = 3.f;
        _viewBackground.layer.shadowOffset = CGSizeMake(1, -1);
        [self addSubview:_viewBackground];
        
        _labelHeader = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, 240, 25)];
        _labelHeader.textAlignment = NSTextAlignmentCenter;
        _labelHeader.font = kAppFontBold(18);
        _labelHeader.textColor = [UIColor blackColor];
        _labelHeader.text = [[SharedAppDelegate langDictionary] valueForKey:@"LabelDownloading"];
        [_viewBackground addSubview:_labelHeader];
        
        _progressView = [[UIProgressView alloc] initWithFrame:CGRectMake(15,50,240,10)];
        _progressView.tintColor = RGBCOLOR(64, 118, 255);
        _progressView.trackTintColor = RGBCOLOR(187, 189, 191);
        _progressView.progress = 0.5;
        [_viewBackground addSubview:_progressView];
        
        _labelDownloadStatus = [[UILabel alloc] initWithFrame:CGRectMake(15, 55, 240, 25)];
        _labelDownloadStatus.textAlignment = NSTextAlignmentCenter;
        _labelDownloadStatus.font = kAppFontSemibold(15);
        _labelDownloadStatus.textColor = [UIColor blackColor];
        _labelDownloadStatus.text = @"34,5MB/150MB";
        [_viewBackground addSubview:_labelDownloadStatus];
        
        _labelNowDownloading = [[UILabel alloc] initWithFrame:CGRectMake(15, 80, 118, 18)];
        _labelNowDownloading.textAlignment = NSTextAlignmentRight;
        _labelNowDownloading.font = kAppFontSemibold(11);
        _labelNowDownloading.textColor = RGBCOLOR(128, 128, 128);
        _labelNowDownloading.text = [languageDictionary valueForKey:@"Now: Isole Borromee"];//@"(Now: Isole Borromee";
        _labelNowDownloading.lineBreakMode = NSLineBreakByTruncatingMiddle;
        [_viewBackground addSubview:_labelNowDownloading];
        
     /*   UILabel *labelComma = [[UILabel alloc] initWithFrame:CGRectMake(133, 80, 4, 18)];
        labelComma.textAlignment = NSTextAlignmentCenter;
        labelComma.font = kAppFontSemibold(11);
        labelComma.textColor = RGBCOLOR(128, 128, 128);
        labelComma.text = @",";
        [_viewBackground addSubview:labelComma];
        */
        
        _labelPendingToDownload = [[UILabel alloc] initWithFrame:CGRectMake(137, 80, 118, 18)];
        _labelPendingToDownload.textAlignment = NSTextAlignmentLeft;
        _labelPendingToDownload.font = kAppFontSemibold(11);
        _labelPendingToDownload.textColor = RGBCOLOR(128, 128, 128);
        _labelPendingToDownload.text = @"Pending: 2 locations)";
        _labelPendingToDownload.lineBreakMode = NSLineBreakByTruncatingMiddle;
        [_viewBackground addSubview:_labelPendingToDownload];
        
        _viewButtonBorderLine = [[UIView alloc] initWithFrame:CGRectMake(0, 100, 270, 1)];
        _viewButtonBorderLine.backgroundColor = RGBCOLOR(208, 208, 208);
        [_viewBackground addSubview:_viewButtonBorderLine];
        
        _button = [[UIButton alloc] initWithFrame:CGRectMake(15, 105, 240, 35)];
        [_button setTitle:[[SharedAppDelegate langDictionary] valueForKey:@"LabelCancel"] forState:UIControlStateNormal];
        [_button setTitleColor:RGBCOLOR(64, 118, 255) forState:UIControlStateNormal];
//        _button.titleLabel.font = kAppFontSemibold(16);
        [_viewBackground addSubview:_button];
        //
        _labelPendingToDownload.alpha = 0.0;
        _labelPendingToDownload.hidden = YES;
        //
        [_labelNowDownloading sizeToFit];
        const CGPoint center = CGPointMake(_labelDownloadStatus.center.x, 90);
        //
        _labelNowDownloading.center = center;
        //
//        _labelHeader.backgroundColor = [UIColor redColor];
      //  _labelDownloadStatus.backgroundColor = [UIColor blueColor];
      //  _labelNowDownloading.backgroundColor = [UIColor grayColor];
//        _labelPendingToDownload.backgroundColor = [UIColor greenColor];
    }
    // 
    return self;
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
 
}
*/

@end
