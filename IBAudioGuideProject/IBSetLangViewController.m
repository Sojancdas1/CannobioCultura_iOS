//
//  IBSetLangViewController.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 19/05/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBSetLangViewController.h"

#import "IBGlobal.h"
#import "IBLanguageTableViewCell.h"

@interface IBSetLangViewController ()<UITableViewDelegate,UITableViewDataSource,IBLanguageTableViewCellDelegate>
{
    UILabel *labelTitle;
}

@property (strong, nonatomic) NSString *langString;
@property (assign, nonatomic) NSInteger langIndex;
@property (strong, nonatomic) NSArray *arrayLanguages;
@property (weak, nonatomic) IBOutlet UITableView *tableViewLanguages;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerViewTopSpaceConstraint;

- (void)refreshView;
- (void)buttonBackTouched;

@end

@implementation IBSetLangViewController

@synthesize langString,langIndex,arrayLanguages;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (IOS_OLDER_THAN(6.0))    {
        [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:NO];
    }
    
    if(IOS_NEWER_OR_EQUAL_TO(7.0))  {
        [_tableViewLanguages setSeparatorInset:UIEdgeInsetsZero];
        self.extendedLayoutIncludesOpaqueBars=NO;
        self.automaticallyAdjustsScrollViewInsets=NO;
        [self setNeedsStatusBarAppearanceUpdate];
    }
    else    {
        self.containerViewTopSpaceConstraint.constant = 14;
    }

    self.arrayLanguages = [[SharedAppDelegate database] fetchLanguageIndexes];
    self.langString = [[NSUserDefaults standardUserDefaults] valueForKey:@"LanguageString"];
    
    NSInteger index = 0;
    for(NSDictionary *dictionary in arrayLanguages) {
        if([[[[dictionary valueForKey:@"displayName"] substringToIndex:2] lowercaseString] isEqualToString:self.langString])    {
            break;
        }
        index++;
    }
    
    self.langIndex = index;
    
    self.tableViewLanguages.delegate = self;
    self.tableViewLanguages.dataSource = self;
    self.tableViewLanguages.bounces = NO;
    self.tableViewLanguages.scrollEnabled = NO;
    self.tableViewLanguages.allowsSelection = YES;
    
    UIButton *buttonBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonBack setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [buttonBack setFrame:CGRectMake(0, 0, 12, 21)];
    [buttonBack addTarget:self action:@selector(buttonBackTouched) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem =
    [[UIBarButtonItem alloc] initWithCustomView:buttonBack];
    
    labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    labelTitle.backgroundColor = [UIColor clearColor];
    labelTitle.textColor = [UIColor blackColor];
    labelTitle.font = kAppFontSemibold(15);
    labelTitle.text = @"Your Title Header Add Here";
    labelTitle.textAlignment = NSTextAlignmentCenter;
    [labelTitle sizeToFit];
    
    self.navigationItem.titleView = labelTitle;
    
    UIView *viewNext = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 12, 21)];
    UIBarButtonItem *buttonRight = [[UIBarButtonItem alloc] initWithCustomView:viewNext];
    self.navigationItem.rightBarButtonItem = buttonRight;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    labelTitle.text = @"";
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self navigationController].navigationBarHidden = NO;
    [self.view setBackgroundColor:RGBCOLOR(200, 200, 200)];
    
    [self refreshView];
}

- (BOOL)shouldAutorotate    {
    return YES;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations   {
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - IBLanguageTableViewCellDelegate methods

- (void)languageSelectedWithIndexTag:(NSInteger)indexTag
{
    //NSLog(@"Language selected at index %ld",(long)indexTag);
}

#pragma mark - Table view data source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"[arrayLanguages count] = %lu", (unsigned long)[arrayLanguages count]);
    return [arrayLanguages count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 46.1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{ 
    static NSString *CellIdentifier = @"Cell";
    IBLanguageTableViewCell *cell = (IBLanguageTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[IBLanguageTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.delegate = self;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    NSString *displayString = [[self.arrayLanguages objectAtIndex:indexPath.row] valueForKey:@"displayName"];
    
    if([[[displayString substringToIndex:2] lowercaseString] isEqualToString:langString]) {
        [cell setChecked:YES];
        [SharedAppDelegate setResourcePath:[NSString stringWithFormat:@"%@/%@",[IBGlobal createDocumentsDirectoryByAppendingPathComponent:nil],[[self.arrayLanguages objectAtIndex:indexPath.row] valueForKey:@"resourcePath"]]];
        [self refreshView];
    }
    else    {
        [cell setChecked:NO];
    }
    
    cell.tag = indexPath.row;
    cell.textLabel.text = displayString;
    
    return cell;
}

#pragma mark -
#pragma mark - Table view delegates

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    langIndex = indexPath.row;
    self.langString = [[[[self.arrayLanguages objectAtIndex:indexPath.row] valueForKey:@"displayName"] substringToIndex:2] lowercaseString];
    [self.tableViewLanguages reloadData];
}

#pragma mark -

- (void)refreshView
{
    NSDictionary *langDictionary = [SharedAppDelegate langDictionary];

    labelTitle.text = [langDictionary valueForKey:@"LabelLanguage"];
}

- (void)buttonBackTouched
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:self.langString forKey:@"LanguageString"];
    [defaults setInteger:self.langIndex forKey:@"LanguageIndex"];
    [defaults synchronize];
    
    if ([SharedAppDelegate languageIndex] != self.langIndex) {
        [SharedAppDelegate setLanguageIndex:self.langIndex];
        
        [[SharedAppDelegate database] setLanguageSelectedByDisplayName:[[self.arrayLanguages objectAtIndex:langIndex] valueForKey:@"displayName"]];
        [SharedAppDelegate setResourcePath:[NSString stringWithFormat:@"%@/%@",[IBGlobal createDocumentsDirectoryByAppendingPathComponent:nil],[[self.arrayLanguages objectAtIndex:self.langIndex] valueForKey:@"resourcePath"]]];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
