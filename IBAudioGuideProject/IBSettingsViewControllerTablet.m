//
//  IBSettingsViewController.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 12/05/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBSettingsViewControllerTablet.h"

#import "IBGlobal.h"
#import "ZipArchive.h"
#import "IBInAppHelper.h"
#import "HUDManager.h"
#import "ASIHTTPRequest.h"
#import "ASINetworkQueue.h"
#import "IBRestoreDownloader.h"
#import <StoreKit/StoreKit.h>

@interface IBSettingsViewControllerTablet ()<UITableViewDelegate,UITableViewDataSource>
{
    UILabel *labelTitle;
    UILabel *labelButton;
    ASINetworkQueue *networkQueue;
    BOOL failed,cancelled;
    IBRestoreDownloader *restoreDownloader;
    NSTimer *timerProgressUpdater;
    NSNumberFormatter *progressValueFormatter;
    
    NSMutableArray *arrayPendingDownloads;
    NSMutableArray *arrayDownloadedItems;
    NSMutableArray *arrayFailedDownloads;
    
    BOOL isValidRestoreComfirmedByUser;
    
    NSArray *_products;
}
@property (strong, nonatomic) NSArray *arraySettings;
@property (weak, nonatomic) IBOutlet UITableView *tabelViewSettings;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *settingsViewTopSpaceConstraint;

- (void)refreshView;
- (void)buttonBackTouched;
- (void)forceRestoreURLForLocationNo:(int)locationNo andLanguageIndex:(int)languageIndex;
- (void)cancelRestoreDownloader;
- (void)updateProgressLabel;
- (void)imageFetchComplete:(ASIHTTPRequest *)request;
- (void)imageFetchFailed:(ASIHTTPRequest *)request;
- (void)queueFinished:(ASINetworkQueue *)queue;
- (void)productsRestoreCompleted:(NSNotification *)notification;

@end

@implementation IBSettingsViewControllerTablet

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization 
    }
    return self;
}

- (void)viewDidLoad {
    //
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (IOS_OLDER_THAN(6.0)) [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:NO];
    
    if (IOS_NEWER_OR_EQUAL_TO(7.0)) {
        //
        self.extendedLayoutIncludesOpaqueBars=NO;
        self.automaticallyAdjustsScrollViewInsets=NO;
        self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    } else {
        //
        self.settingsViewTopSpaceConstraint.constant = 14;
        self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    }
    
    UIView *viewBack = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 65, 30)];
    
    labelButton = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 65, 30)];
    labelButton.backgroundColor = [UIColor clearColor];
    labelButton.textColor = RGBCOLOR(104.f, 144.f, 152.f);
    labelButton.font = kAppFontSemibold(15);
    labelButton.textAlignment = NSTextAlignmentLeft;
    labelButton.enabled = NO;
    [viewBack addSubview:labelButton];
    
    UIButton *buttonDone = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 65, 30)];
    [buttonDone setBackgroundColor:[UIColor clearColor]];
    [buttonDone addTarget:self action:@selector(buttonBackTouched) forControlEvents:UIControlEventTouchUpInside];
    [viewBack addSubview:buttonDone];
    
   // UIBarButtonItem *buttonLeft = [[UIBarButtonItem alloc] initWithCustomView:viewBack];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:viewBack];
    
    labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    labelTitle.backgroundColor = [UIColor clearColor];
    labelTitle.textColor = [UIColor whiteColor];
    labelTitle.font = kAppFontSemibold(15);
    labelTitle.text = @"Your Title Header Add Here";
    labelTitle.textAlignment = NSTextAlignmentCenter;
    [labelTitle sizeToFit];
    
    self.navigationItem.titleView = labelTitle;
    
    UIView *viewNext = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 65, 30)];
    UIBarButtonItem *buttonRight = [[UIBarButtonItem alloc] initWithCustomView:viewNext];
    self.navigationItem.rightBarButtonItem = buttonRight;
    
    self.tabelViewSettings.delegate = self;
    self.tabelViewSettings.dataSource = self;
    self.tabelViewSettings.bounces = NO;
    self.tabelViewSettings.scrollEnabled = NO;
    self.tabelViewSettings.allowsSelection = YES;
    
    progressValueFormatter = [[NSNumberFormatter alloc] init];
    [progressValueFormatter setMinimumIntegerDigits:1];
    [progressValueFormatter setMinimumFractionDigits:1];
    [progressValueFormatter setMaximumFractionDigits:2];
    [progressValueFormatter setDecimalSeparator:@","];
    
    restoreDownloader = nil;
    arrayDownloadedItems = nil;
    arrayPendingDownloads = nil;
    arrayFailedDownloads = nil;
    _products = nil;
    
    [HUDManager addHUDWithLabel:nil];
    [[IBInAppHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        if (success) {
            _products = products;
        }
         //NSLog(@"ibsettingsView Hud");
        [HUDManager hideHUDFromWindowAfterDelay:0.2f];
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    labelTitle.text = @"";
    [self navigationController].navigationBarHidden = NO;
    [self.view setBackgroundColor:RGBCOLOR(200, 200, 200)];
    [self performSelector:@selector(refreshView) withObject:nil afterDelay:0.1f];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productsRestoreCompleted:) name:IAPHelperRestoreFinishedNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (BOOL)shouldAutorotate    {
    return NO;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations    {
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark-
#pragma IBInAppDelegate methods

- (void)productPurchased:(NSNotification *)notification {
    //
    NSString *productIdentifier = notification.object;
    //
    [_products enumerateObjectsUsingBlock:^(SKProduct * product, NSUInteger idx, BOOL *stop) {
        //
        if ([product.productIdentifier isEqualToString:productIdentifier]) {
            //
            NSInteger locationNo = [SharedAppDelegate locationNo];
            [[SharedAppDelegate database] updatePurchaseOfLocationNo:locationNo withStatus:YES];
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setBool:YES forKey:productIdentifier];
            [defaults synchronize];
            
            *stop = YES;
        }
    }];
}

//- (void)productPurchased:(NSNotification *)notification {
//    NSURL *url = [[NSBundle mainBundle] URLForResource:@"InAppProducts" withExtension:@"plist"];
//    NSArray *arrayIdentifiers = [NSArray arrayWithContentsOfURL:url];
//    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:nil ascending:YES];
//    NSArray *arraySortedIdentifiers = [arrayIdentifiers sortedArrayUsingDescriptors:@[sortDescriptor]];
//    
//    NSString * productIdentifier = notification.object;
//    [_products enumerateObjectsUsingBlock:^(SKProduct * product, NSUInteger idx, BOOL *stop) {
//        if ([product.productIdentifier isEqualToString:productIdentifier]) {
//            if([arraySortedIdentifiers[1] isEqualToString:productIdentifier]) {
//                [[SharedAppDelegate database] updatePurchaseOfLocationNo:1 withStatus:YES];
//            }
//            else if ([arraySortedIdentifiers[2] isEqualToString:productIdentifier])   {
//                [[SharedAppDelegate database] updatePurchaseOfLocationNo:2 withStatus:YES];
//            }
//            else if ([arraySortedIdentifiers[3] isEqualToString:productIdentifier])    {
//                [[SharedAppDelegate database] updatePurchaseOfLocationNo:3 withStatus:YES];
//            }
//            else    {
//                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//                [defaults setBool:YES forKey:kComboPurchaseKey];
//                [defaults synchronize];
//            }
//
//            *stop = YES;
//        }
//    }];
//}

- (void)productsRestoreCompleted:(NSNotification *)notification {
    //
    NSArray * productIdentifier = notification.object;
    
    if([productIdentifier count]>0)   {
        
        if(!isValidRestoreComfirmedByUser)   {
            
            //restore confirmation is valid because some of the products is already purchased
            isValidRestoreComfirmedByUser = YES;
            
            for(int i=1; i<=3; i++) {
                for(int j=0; j<4; j++)  {
                    [self forceRestoreURLForLocationNo:i andLanguageIndex:j];
                }
            }
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            if(IS_IPAD) {
                if([[SharedAppDelegate playerViewTablet] playing])  {
                    [[SharedAppDelegate playerViewTablet] setPlaying:NO];
                    [[SharedAppDelegate playerViewTablet].delegate playButtonTriggered];
                    [SharedAppDelegate animatePlayerHidden:YES];
                }
            }
            else    {
                if([[SharedAppDelegate playerView] playing])  {
                    [[SharedAppDelegate playerView] setPlaying:NO];
                    [[SharedAppDelegate playerView].delegate playButtonTriggered];
                    [SharedAppDelegate animatePlayerHidden:YES];
                }
            }
            
            if([[SharedAppDelegate childSplitViewController] showThirdContainer]==YES)   {
                [[SharedAppDelegate childSplitViewController] setShowThirdContainer:NO];
            }
            
            if([[SharedAppDelegate categoryListController] currentIndex]==1)    {
                [[SharedAppDelegate categoryListController] setCurrentIndex:0];
            }
            
            NSDictionary *langDictionary = [SharedAppDelegate langDictionary];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[langDictionary valueForKey:@"LabelRestoreAlert"]
                                                            message:[langDictionary valueForKey:@"LabelRestoreMessage"]
                                                           delegate:self
                                                  cancelButtonTitle:[langDictionary valueForKey:@"LabelCancel"]
                                                  otherButtonTitles:[langDictionary valueForKey:@"LabelOk"],nil];
            alert.tag = 2;
            [alert show];
        }
    }
}

#pragma mark-

- (void)buttonBackTouched
{
    [self dismissViewControllerAnimated:YES completion:^{
        //NSLog(@"Navigation dismissed");
        [[SharedAppDelegate locationViewController] updateLanguage];
        [[SharedAppDelegate categoryListController] updateLanguage];
        [[SharedAppDelegate directDownloadController] dismissViewControllerAnimated:YES completion:^{
            
        }];
        
        if([[SharedAppDelegate directDownloadController] status]==StatusDownloading)  {
            [[SharedAppDelegate categoryListController] setLocked:YES];
        }
    }];
}

- (void)refreshView
{
    NSDictionary *langDictionary = [SharedAppDelegate langDictionary];
    labelButton.text = [langDictionary valueForKey:@"LabelDone"];
    labelTitle.text = [langDictionary valueForKey:@"LabelSettings"];
    
    self.arraySettings = [NSArray arrayWithObjects:[langDictionary valueForKey:@"LabelLanguage"],[langDictionary valueForKey:@"LabelRestore"],[langDictionary valueForKey:@"LabelInfos"], nil];
    
    [self.tabelViewSettings reloadData];
}


#pragma mark - Table view data source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // 
    return [self.arraySettings count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)aTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{ 
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = kAppFontSemibold(16);
    }
    
    cell.textLabel.text = [self.arraySettings objectAtIndex:indexPath.row];;
    cell.textLabel.textColor = [UIColor blackColor];
    
    if(indexPath.row == 1)  {
        cell.accessoryType = UITableViewCellAccessoryNone;
    } else {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    return cell;
}

#pragma mark -
#pragma mark - Table view delegates

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //
    labelButton.enabled = YES;
    
    if(indexPath.row == 0) {
        //
        [self performSegueWithIdentifier:@"segueToLanguage" sender:self];
    } else if (indexPath.row == 1) {
        //
        isValidRestoreComfirmedByUser = NO;
        NSDictionary *langDictionary = [SharedAppDelegate langDictionary];
        NSString *title = [langDictionary valueForKey:@"LabelAttention"];
        NSString *message = [langDictionary valueForKey:@"AskRestore"];
        NSString *cancelButtonTitle = [langDictionary valueForKey:@"LabelCancel"];
        NSString *okButtonTitle = [langDictionary valueForKey:@"LabelOk"];
        //
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:cancelButtonTitle otherButtonTitles:okButtonTitle ,nil];
        alert.tag = 1;
        [alert show];
    } else {
        //
        [self performSegueWithIdentifier:@"segueToInfos" sender:self];
    }
}

#pragma mark -

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    //
    if(alertView.tag == 1)    {
        //
        if (buttonIndex == 1) {
            //
            [HUDManager addHUDWithLabel:nil dimBackground:YES];
            [[IBInAppHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
                //
                [[IBInAppHelper sharedInstance] restoreCompletedTransactions]; 
                [HUDManager hideHUDFromWindowAfterDelay:0.2f];
            }];
            //
#if TARGET_IPHONE_SIMULATOR
            [[SharedAppDelegate database] updatePurchaseOfLocationNo:1 withStatus:YES];
            [[SharedAppDelegate database] updatePurchaseOfLocationNo:2 withStatus:YES];
            [[SharedAppDelegate database] updatePurchaseOfLocationNo:3 withStatus:YES];
#endif
        }
    } else if (alertView.tag == 2) {
        // the user clicked OK
        if (buttonIndex == 1) {
            //
            if (!restoreDownloader) {
                //
                restoreDownloader = [[IBRestoreDownloader alloc] initWithFrame:CGRectMake(0, 0, 270, 145)];
                [restoreDownloader.button addTarget:self action:@selector(cancelRestoreDownloader) forControlEvents:UIControlEventTouchUpInside];
            } else {
                //
                [restoreDownloader setFrame:CGRectMake(0, 0, 270, 145)];
            }
            //
            restoreDownloader.labelHeader.text = [[SharedAppDelegate langDictionary] valueForKey:@"LabelDownloading"];
            [restoreDownloader.button setTitle:[[SharedAppDelegate langDictionary] valueForKey:@"LabelCancel"] forState:UIControlStateNormal];
            
            [SharedAppDelegate showAlertWithRestoreDownloader:restoreDownloader];
            
            if(!arrayPendingDownloads) arrayPendingDownloads = [NSMutableArray array];
            [arrayPendingDownloads removeAllObjects];
            
            if(!arrayDownloadedItems) arrayDownloadedItems = [NSMutableArray array];
            [arrayDownloadedItems removeAllObjects];
            
            if(!arrayFailedDownloads) arrayFailedDownloads = [NSMutableArray array];
            [arrayFailedDownloads removeAllObjects];
            
            if (!networkQueue) networkQueue = [ASINetworkQueue queue];
            //
            failed = NO;
            cancelled = NO;
            //
            [networkQueue reset];
            [networkQueue setMaxConcurrentOperationCount:1];
            [networkQueue setDownloadProgressDelegate:restoreDownloader.progressView];
            [networkQueue setRequestDidFinishSelector:@selector(imageFetchComplete:)];
            [networkQueue setRequestDidFailSelector:@selector(imageFetchFailed:)];
            [networkQueue setQueueDidFinishSelector:@selector(queueFinished:)];
            [networkQueue setShowAccurateProgress:YES];
            [networkQueue setDelegate:self];
    
            int count = 0;
            NSString *museumName = nil;
            
            if([[NSUserDefaults standardUserDefaults] boolForKey:kComboPurchaseKey]==YES)   {
                
                //Iterating through all three avalable locations
                for (int i=1; i<=3; i++) {
                    //
                    [self addDownloadForLocationNo:i];
                    MUSEUMNAME_STR(i,museumName);
                    [arrayPendingDownloads addObject:museumName];
                    count++;
                }
            } else {
                //Adding download for location 1
                if([[SharedAppDelegate database] isPurchasedForLocationNo:1]) {
                    //
                    [self addDownloadForLocationNo:1];
                    MUSEUMNAME_STR(1,museumName);
                    [arrayPendingDownloads addObject:museumName];
                    count++;
                }
                //Adding download for location 2
                if([[SharedAppDelegate database] isPurchasedForLocationNo:2]) {
                    //
                    [self addDownloadForLocationNo:2];
                    MUSEUMNAME_STR(2,museumName);
                    [arrayPendingDownloads addObject:museumName];
                    count++;
                } 
                //Adding download for location 3
                if([[SharedAppDelegate database] isPurchasedForLocationNo:3]) {
                    //
                    [self addDownloadForLocationNo:3];
                    MUSEUMNAME_STR(3,museumName);
                    [arrayPendingDownloads addObject:museumName];
                    count++;
                }
            }
            
            NSDictionary *langDic = [SharedAppDelegate langDictionary];
            
         /*   if([arrayPendingDownloads count] > 1) {
                //
                restoreDownloader.labelNowDownloading.text = [NSString stringWithFormat:@"(%@: %@",[langDic valueForKey:@"LabelNow"],[arrayPendingDownloads objectAtIndex:0]];
                restoreDownloader.labelPendingToDownload.text = [NSString stringWithFormat:@"%@: %u %@)",[langDic valueForKey:@"LabelPending"],[arrayPendingDownloads count]-1,[langDic valueForKey:@"LabelLocations"]];
            } else if([arrayPendingDownloads count]==1) {
                //
                restoreDownloader.labelNowDownloading.text = [NSString stringWithFormat:@"(%@: %@",[langDic valueForKey:@"LabelNow"],[arrayPendingDownloads objectAtIndex:0]];
                restoreDownloader.labelPendingToDownload.text = [NSString stringWithFormat:@"No %@ %@)",[[langDic valueForKey:@"LabelPending"] lowercaseString],[langDic valueForKey:@"LabelLocations"]];
            } else {
                //
                restoreDownloader.labelPendingToDownload.text = [NSString stringWithFormat:@"%@: %u %@)",[langDic valueForKey:@"LabelPending"],[arrayPendingDownloads count]-1,[langDic valueForKey:@"LabelLocations"]];
            } */
            //
            timerProgressUpdater = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(updateProgressLabel) userInfo:nil repeats:YES];
            [networkQueue go];
        }
    } else {
        NSLog(@"");
    }
    
    
    
}

- (void)cancelRestoreDownloader
{
    cancelled = YES;
    [networkQueue cancelAllOperations];
}

- (void)forceRestoreURLForLocationNo:(int)locationNo andLanguageIndex:(int)languageIndex
{
    NSString *museumDir,*languageDir;
    NSString *audioGuideURL,*guidesGalleryURLString,*keyString;
    
    DIRLOCATION_STR(locationNo,museumDir);
    DIRLANGUAGE_STR(languageIndex, languageDir);
    
    audioGuideURL = [NSString stringWithFormat:@"%@/%@/%@/%@/%@",SYNC_URL,IBParentDirectory,museumDir,languageDir,kAudioGuideZip];
    keyString = [IBGlobal keyForUrlString:audioGuideURL];
    
    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"0"] forKey:keyString];
    
    guidesGalleryURLString = [NSString stringWithFormat:@"%@/%@/%@/%@",SYNC_URL,IBParentDirectory,museumDir,kGuidesGalleryZip];
    keyString = [IBGlobal keyForUrlString:guidesGalleryURLString];
    
    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"0"] forKey:keyString];
}

- (void)addDownloadForLocationNo:(int)locationNo_
{
    int locationNo = 0;
    NSString *museumDir,*languageDir;
    NSString *audioGuideURLString,*guidesGalleryURLString;
    NSString *absoluteDestinationDir,*absoluteDestinationPath,*defaultDestinationDir,*defaultDestinationPath;
    NSInteger languageIndex = [SharedAppDelegate languageIndex];
    
    DIRLOCATION_STR(locationNo,museumDir);
    DIRLANGUAGE_STR(languageIndex, languageDir);
    
    audioGuideURLString = [NSString stringWithFormat:@"%@/%@/%@/%@/%@",SYNC_URL,IBParentDirectory,museumDir,languageDir,kAudioGuideZip];
    absoluteDestinationDir = [IBGlobal createDocumentsDirectoryByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",languageDir,museumDir]];
    absoluteDestinationPath = [NSString stringWithFormat:@"%@/%@",absoluteDestinationDir,kAudioGuideZip];
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:audioGuideURLString]];
    [request setTimeOutSeconds:360];
    [request setDownloadDestinationPath:absoluteDestinationPath];
    [request setTemporaryFileDownloadPath:[NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"loc%d_%@.download",locationNo,kAudioGuideZip]]];
    [request setAllowResumeForFileDownloads:YES];
    [request setUserInfo:[NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"audioGuidesLocation%d",locationNo] forKey:@"name"]];
    [networkQueue addOperation:request];
    
    guidesGalleryURLString = [NSString stringWithFormat:@"%@/%@/%@/%@",SYNC_URL,IBParentDirectory,museumDir,kGuidesGalleryZip];
    defaultDestinationDir = [IBGlobal createDocumentsDirectoryByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",ENGLISH,museumDir]];
    defaultDestinationPath = [NSString stringWithFormat:@"%@/%@",defaultDestinationDir,kGuidesGalleryZip];
    
    request = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:guidesGalleryURLString]];
    [request setTimeOutSeconds:360];
    [request setDownloadDestinationPath:defaultDestinationPath];
    [request setTemporaryFileDownloadPath:[NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"loc%d-%@.download",locationNo,kGuidesGalleryZip]]];
    [request setAllowResumeForFileDownloads:YES];
    [request setUserInfo:[NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"guidesGalleryLocation%d",locationNo] forKey:@"name"]];
    [networkQueue addOperation:request];
}

- (void)updateProgressLabel {
    //NSLog(@"%llu : %llu",[networkQueue bytesDownloadedSoFar],[networkQueue totalBytesToDownload]);
    [progressValueFormatter setMinimumFractionDigits:2];
    NSString *mbDownloaded = [progressValueFormatter stringFromNumber:[NSNumber numberWithDouble:[networkQueue bytesDownloadedSoFar]/(1024.f*1024.f)]];
    [progressValueFormatter setMinimumFractionDigits:0];
    NSString *mbToDownload = [progressValueFormatter stringFromNumber:[NSNumber numberWithDouble:[networkQueue totalBytesToDownload]/(1024.f*1024.f)]];
    restoreDownloader.labelDownloadStatus.text = [NSString stringWithFormat:@"%@MB/%@MB",mbDownloaded,mbToDownload];
}

- (void)imageFetchComplete:(ASIHTTPRequest *)request {
    //
    NSLog(@"imageFetchComplete 1");
    NSString *museumName = nil;
    NSString *museumDir,*languageDir;
    NSString *urlString,*destinationDir;
    
    NSDictionary *userInfo = [request userInfo];
    NSString *name = [userInfo valueForKey:@"name"];
    NSUInteger languageIndex = [SharedAppDelegate languageIndex];
    
    DIRLANGUAGE_STR(languageIndex, languageDir);
    
    [arrayDownloadedItems addObject:name];
   
    if([name rangeOfString:@"1"].location != NSNotFound)
    {
        DIRLOCATION_STR(1,museumDir);
        if([name isEqualToString:@"audioGuidesLocation1"]) {
            urlString = [NSString stringWithFormat:@"%@/%@/%@/%@/%@",SYNC_URL,IBParentDirectory,museumDir,languageDir,kAudioGuideZip];
            destinationDir = [IBGlobal createDocumentsDirectoryByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",languageDir,museumDir]];
        } else {
            urlString = [NSString stringWithFormat:@"%@/%@/%@/%@",SYNC_URL,IBParentDirectory,museumDir,kGuidesGalleryZip];
            destinationDir = [IBGlobal createDocumentsDirectoryByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",ENGLISH,museumDir]];
        }
        
        if([arrayDownloadedItems containsObject:@"audioGuidesLocation1"] && [arrayDownloadedItems containsObject:@"guidesGalleryLocation1"])    {
            MUSEUMNAME_STR(1,museumName);
            [arrayPendingDownloads removeObject:museumName];
        }
    } else if ([name rangeOfString:@"2"].location != NSNotFound) {
        DIRLOCATION_STR(2,museumDir);
        
        if([name isEqualToString:@"audioGuidesLocation2"])  {
            urlString = [NSString stringWithFormat:@"%@/%@/%@/%@/%@",SYNC_URL,IBParentDirectory,museumDir,languageDir,kAudioGuideZip];
            destinationDir = [IBGlobal createDocumentsDirectoryByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",languageDir,museumDir]];
        } else {
            urlString = [NSString stringWithFormat:@"%@/%@/%@/%@",SYNC_URL,IBParentDirectory,museumDir,kGuidesGalleryZip];
            destinationDir = [IBGlobal createDocumentsDirectoryByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",ENGLISH,museumDir]];
        }
        
        if([arrayDownloadedItems containsObject:@"audioGuidesLocation2"] && [arrayDownloadedItems containsObject:@"guidesGalleryLocation2"])    {
            MUSEUMNAME_STR(2,museumName);
            [arrayPendingDownloads removeObject:museumName];
        }
    }
    else if ([name rangeOfString:@"3"].location!=NSNotFound)
    {
        DIRLOCATION_STR(3,museumDir);
        
        if([name isEqualToString:@"audioGuidesLocation3"])  {
            urlString = [NSString stringWithFormat:@"%@/%@/%@/%@/%@",SYNC_URL,IBParentDirectory,museumDir,languageDir,kAudioGuideZip];
            destinationDir = [IBGlobal createDocumentsDirectoryByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",languageDir,museumDir]];
        }
        else    {
            urlString = [NSString stringWithFormat:@"%@/%@/%@/%@",SYNC_URL,IBParentDirectory,museumDir,kGuidesGalleryZip];
            destinationDir = [IBGlobal createDocumentsDirectoryByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",ENGLISH,museumDir]];
        }
        
        if([arrayDownloadedItems containsObject:@"audioGuidesLocation3"] && [arrayDownloadedItems containsObject:@"guidesGalleryLocation3"])    {
            MUSEUMNAME_STR(3,museumName);
            [arrayPendingDownloads removeObject:museumName];
        }
    }
    
    NSDictionary *langDic = [SharedAppDelegate langDictionary];
    
   /* if([arrayPendingDownloads count]>1) {
        restoreDownloader.labelNowDownloading.text = [NSString stringWithFormat:@"(%@: %@",[langDic valueForKey:@"LabelNow"],[arrayPendingDownloads objectAtIndex:0]];
        restoreDownloader.labelPendingToDownload.text = [NSString stringWithFormat:@"%@: %u %@)",[langDic valueForKey:@"LabelPending"],[arrayPendingDownloads count]-1,[langDic valueForKey:@"LabelLocations"]];
    }
    else if([arrayPendingDownloads count]==1) {
        restoreDownloader.labelNowDownloading.text = [NSString stringWithFormat:@"(%@: %@",[langDic valueForKey:@"LabelNow"],[arrayPendingDownloads objectAtIndex:0]];
        restoreDownloader.labelPendingToDownload.text = [NSString stringWithFormat:@"No %@ %@)",[[langDic valueForKey:@"LabelPending"] lowercaseString],[langDic valueForKey:@"LabelLocations"]];
    }
    else    {
        restoreDownloader.labelPendingToDownload.text = [NSString stringWithFormat:@"No %@ %@)",[[langDic valueForKey:@"LabelPending"] lowercaseString],[langDic valueForKey:@"LabelLocations"]];
    } */

    ZipArchive *zipArchive = [[ZipArchive alloc] init];
    [zipArchive UnzipOpenFile:[NSString stringWithFormat:@"%@/%@",destinationDir,[urlString lastPathComponent]]];
    if([zipArchive UnzipFileTo:destinationDir overWrite:YES])
    {
        NSError *error = nil;
        [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@/%@",destinationDir,[urlString lastPathComponent]] error:&error];
        
        if(!error)  {
            //NSLog(@"Zip delete failed");
        }
        
        //NSLog(@"unzipped successfully");
    }
    [zipArchive UnzipCloseFile];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:[NSString stringWithFormat:@"%d",StatusDownloaded] forKey:[IBGlobal keyForUrlString:urlString]];
    [defaults synchronize];
    
    //NSLog(@"%@",[request userInfo]);
}

- (void)imageFetchFailed:(ASIHTTPRequest *)request
{
    NSLog(@"imageFetchComplete 2");
    failed = YES;
    
    NSDictionary *userInfo = [request userInfo];
    NSString *name = [userInfo valueForKey:@"name"];
    NSString *museumName = nil;
    
    [arrayFailedDownloads addObject:name];
    
    if([name rangeOfString:@"1"].location!=NSNotFound)  {
        if([arrayFailedDownloads containsObject:@"audioGuidesLocation1"] && [arrayFailedDownloads containsObject:@"guidesGalleryLocation1"])    {
            MUSEUMNAME_STR(1,museumName);
            [arrayPendingDownloads removeObject:museumName];
        }
    }
    else if ([name rangeOfString:@"2"].location!=NSNotFound)    {
        if([arrayFailedDownloads containsObject:@"audioGuidesLocation2"] && [arrayFailedDownloads containsObject:@"guidesGalleryLocation2"])    {
            MUSEUMNAME_STR(2,museumName);
            [arrayPendingDownloads removeObject:museumName];
        }
    }
    else if ([name rangeOfString:@"3"].location!=NSNotFound)    {
        if([arrayFailedDownloads containsObject:@"audioGuidesLocation3"] && [arrayFailedDownloads containsObject:@"guidesGalleryLocation3"])    {
            MUSEUMNAME_STR(3,museumName);
            [arrayPendingDownloads removeObject:museumName];
        }
    }
    
    NSDictionary *langDic = [SharedAppDelegate langDictionary];
    
  /*  if([arrayPendingDownloads count]>1) {
        //
        restoreDownloader.labelNowDownloading.text = [NSString stringWithFormat:@"(%@: %@",[langDic valueForKey:@"LabelNow"],[arrayPendingDownloads objectAtIndex:0]];
        restoreDownloader.labelPendingToDownload.text = [NSString stringWithFormat:@"%@: %u %@)",[langDic valueForKey:@"LabelPending"],[arrayPendingDownloads count]-1,[langDic valueForKey:@"LabelLocations"]];
    } else if([arrayPendingDownloads count]==1) {
        //
        restoreDownloader.labelNowDownloading.text = [NSString stringWithFormat:@"(%@: %@",[langDic valueForKey:@"LabelNow"],[arrayPendingDownloads objectAtIndex:0]];
        restoreDownloader.labelPendingToDownload.text = [NSString stringWithFormat:@"No %@ %@)",[[langDic valueForKey:@"LabelPending"] lowercaseString],[langDic valueForKey:@"LabelLocations"]];
    } else {
        //
        restoreDownloader.labelPendingToDownload.text = [NSString stringWithFormat:@"No %@ %@)",[[langDic valueForKey:@"LabelPending"] lowercaseString],[langDic valueForKey:@"LabelLocations"]];
    } */
    // NSLog(@"%@",[request userInfo]);
}

- (void)queueFinished:(ASINetworkQueue *)queue {
    //
    NSLog(@"imageFetchComplete 3");
    [timerProgressUpdater invalidate];
    [restoreDownloader removeFromSuperview];
    //
    [SharedAppDelegate hideAlertRestoreDownloader];
    //
    if(([arrayFailedDownloads count] > 0) && !cancelled) {
        //
        id dictionary = [SharedAppDelegate langDictionary];
        NSString *title = [dictionary valueForKey:@"LabelAttention"];
        NSString *message = [dictionary valueForKey:@"LabelRestoreFailure"];
        NSString *otherButton = [dictionary valueForKey:@"LabelOk"];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:otherButton, nil];
        alert.tag = 2;
        [alert show];
    }
    //
    [arrayPendingDownloads removeAllObjects];
    [arrayDownloadedItems removeAllObjects];
    [arrayFailedDownloads removeAllObjects];
    //
    [IBInAppHelper setPurchaseRestored:YES for:[SharedAppDelegate locationNo]];
    dispatch_async(dispatch_get_main_queue(), ^{
        //
        [[NSNotificationCenter defaultCenter] postNotificationName:IBNotificationTriggerDownload object:nil];
    });
}

@end
