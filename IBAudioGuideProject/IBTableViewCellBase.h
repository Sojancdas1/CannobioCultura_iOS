//
//  IBTableViewCellBase.h
//  IBAudioGuideProject
//
//  Created by T T Marshel Daniel on 01/03/16.
//  Copyright © 2016 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IBTableViewCellBase : UITableViewCell

@end
