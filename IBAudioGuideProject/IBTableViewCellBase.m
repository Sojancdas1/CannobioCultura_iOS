//
//  IBTableViewCellBase.m
//  IBAudioGuideProject
//
//  Created by T T Marshel Daniel on 01/03/16.
//  Copyright © 2016 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBTableViewCellBase.h"

@implementation IBTableViewCellBase

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
} 

@end
