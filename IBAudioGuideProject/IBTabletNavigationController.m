//
//  IBTabletNavigationController.m
//  IBAudioGuideProject
//
//  Created by Hackintosh #01 on 05/08/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBTabletNavigationController.h"

#import "IBGlobal.h"
#import "IBPhotoGalleryViewControllerTablet.h"
#import "IBGuideGalleryViewControllerTablet.h"

@interface IBTabletNavigationController ()

@end

@implementation IBTabletNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//- (BOOL)shouldAutorotate
//{
//    return YES;
//}

//- (NSUInteger)supportedInterfaceOrientations
//{
//    int interfaceOrientation = 0;
//    
//    if (self.viewControllers.count > 0)
//    {
//        for (id viewController in self.viewControllers)
//        {
//            if ([viewController isKindOfClass:([IBPhotoGalleryViewControllerTablet class])] || [viewController isKindOfClass:([IBGuideGalleryViewControllerTablet class])])
//            {
//                interfaceOrientation = UIInterfaceOrientationMaskAll;
//            }
//            else
//            {
//                interfaceOrientation = UIInterfaceOrientationMaskLandscape;
//            }
//        }
//    }
//    
//    return interfaceOrientation;
//}
//
//- (UIViewController *)childViewControllerForStatusBarStyle {
//    return self.topViewController;
//}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

@end
