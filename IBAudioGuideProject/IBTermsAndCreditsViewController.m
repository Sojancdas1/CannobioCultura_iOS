//
//  IBTermsAndCreditsViewController.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 19/05/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBTermsAndCreditsViewController.h"

#import "IBGlobal.h"

@interface IBTermsAndCreditsViewController ()
{
    UILabel *labelTitle;
}

@property (weak, nonatomic) IBOutlet UITextView *textViewContents;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewTopSpaceConstrait;

- (void)buttonBackTouched;
- (void)refreshViewWithTag:(NSInteger)tag;

@end

@implementation IBTermsAndCreditsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (IOS_OLDER_THAN(6.0))    {
        [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait animated:NO];
    }
    
    if (IOS_NEWER_OR_EQUAL_TO(7.0))    {
        self.extendedLayoutIncludesOpaqueBars=NO;
        self.automaticallyAdjustsScrollViewInsets=NO;
    }
    else    {
        self.textViewTopSpaceConstrait.constant = 0;
    }
    
    UIButton *buttonBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonBack setBackgroundImage:[UIImage imageNamed:@"back.png"] forState:UIControlStateNormal];
    [buttonBack setFrame:CGRectMake(0, 0, 12, 21)];
    [buttonBack addTarget:self action:@selector(buttonBackTouched) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.leftBarButtonItem =
    [[UIBarButtonItem alloc] initWithCustomView:buttonBack];
    
    labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    labelTitle.backgroundColor = [UIColor clearColor];
    labelTitle.textColor = [UIColor blackColor];
    labelTitle.font = kAppFontSemibold(15);
    labelTitle.text = @"Your Title Header Add Here";
    labelTitle.textAlignment = NSTextAlignmentCenter;
    [labelTitle sizeToFit];
    
    self.navigationItem.titleView = labelTitle;
    
    UIView *viewNext = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 12, 21)];
    UIBarButtonItem *buttonRight = [[UIBarButtonItem alloc] initWithCustomView:viewNext];
    self.navigationItem.rightBarButtonItem = buttonRight;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    labelTitle.text = @"";
    [self navigationController].navigationBarHidden = NO;
    [self.view setBackgroundColor:RGBCOLOR(200, 200, 200)];
    [self refreshViewWithTag:self.view.tag];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotate    {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations    {
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)buttonBackTouched
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)refreshViewWithTag:(NSInteger)tag
{
    NSDictionary *langDictionary = [SharedAppDelegate langDictionary];
    
    if(tag==1)  {
        labelTitle.text = [langDictionary valueForKey:@"LabelTerms"];
    }
    else    {
        labelTitle.text = [langDictionary valueForKey:@"LabelCredits"];
    }
}

@end
