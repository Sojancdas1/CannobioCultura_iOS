//
//  IBTypes.h
//  IBAudioGuideProject
//
//  Created by E-Team Mac on 7/11/17.
//  Copyright © 2017 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#ifndef IBTypes_h
#define IBTypes_h


typedef enum : NSUInteger {
    IBTypeIsoleBella = 1,
    IBTypeIsoleMadre,
    IBTypeRoccaDiAngera,
} IBType;


typedef enum : NSUInteger {
    DownLoadingStatusNone,
    DownLoadingStatusStarted,
    DownLoadingStatusDownloading,
    DownLoadingStatusFailed,
    DownLoadingStatusCompleted
} DownLoadingStatus;

#endif /* IBTypes_h */
