//
//  IBXMLParser.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 21/05/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IBXMLParser : NSObject

- (NSDictionary *)loadLocationXMLWithLocationNo:(NSInteger)locationNo andLanguageIndex:(NSInteger)languageIndex forceDownload:(BOOL)needDownload;
- (NSArray *)loadPhotoGalleryXMLWithLocationNo:(NSInteger)locationNo andLanguageIndex:(NSInteger)languageIndex forceDownload:(BOOL)needDownload;
- (NSArray *)loadAudioGuideXMLWithLocationNo:(NSInteger)locationNo andLanguageIndex:(NSInteger)languageIndex forceDownload:(BOOL)needDownload;

@end
