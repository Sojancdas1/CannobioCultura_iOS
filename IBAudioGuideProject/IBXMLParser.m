//
//  IBXMLParser.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 21/05/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBXMLParser.h"

#import "IBGlobal.h"
#import "GDataXMLNode.h"
#import "ASIHTTPRequest.h"

@interface IBXMLParser ()

- (NSArray *)loadAudioGuideXMLFromURLPath:(NSString *)urlPath toDestinationPath:(NSString *)destination forceDownload:(BOOL)needDownload;

@end

@implementation IBXMLParser

- (NSDictionary *)loadLocationXMLWithLocationNo:(NSInteger)locationNo andLanguageIndex:(NSInteger)languageIndex forceDownload:(BOOL)needDownload
{
    NSString *urlPath,*museumDir,*languageDir;
    
    DIRLOCATION_STR(locationNo,museumDir);
    DIRLANGUAGE_STR(languageIndex,languageDir);
    
    urlPath = [NSString stringWithFormat:@"%@/%@/%@/%@",SYNC_URL,IBParentDirectory,museumDir,kMapXml];
    NSString *destinationDir = [IBGlobal createDocumentsDirectoryByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",languageDir,museumDir]];
    NSString *destinationPath = [destinationDir stringByAppendingPathComponent:kMapXml];
    
    BOOL fileExist = [[NSFileManager defaultManager] fileExistsAtPath:destinationPath];
   
    if(needDownload || !fileExist)    {
        NSURL *url = [NSURL URLWithString:urlPath];
        ASIHTTPRequest *asiRequest = [ASIHTTPRequest requestWithURL:url];
        [asiRequest setTimeOutSeconds:360];
        [asiRequest setDownloadDestinationPath:destinationPath];
    
        [asiRequest startSynchronous];
    }
    
    NSData *xmlData = [[NSMutableData alloc] initWithContentsOfFile:destinationPath];
    NSString* parseString = [[NSString alloc] initWithData:xmlData encoding:NSUTF8StringEncoding];
    
    NSError *error = nil;
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:parseString options:0 error:&error];

    if (doc == nil)
    {
        //NSLog(@"Parsing Map xml failed!!!");
        return nil;
    }
    else    {
        GDataXMLElement *root = [doc rootElement];
        
        NSInteger roomIndex = 1;
        NSInteger sectionIndex = 1;
        
        NSUInteger childCount = [root childCount];
        NSMutableDictionary *detailsDictionary = [NSMutableDictionary dictionary];
        NSMutableArray *sectionArray = [NSMutableArray array];
        
        if(childCount>0)
        {
            for(int i=0; i<childCount; i++)
            {
                if([[[root childAtIndex:i] name] isEqualToString:@"Desc"])
                {
                    GDataXMLNode *node=[root childAtIndex:i];
                    
                    NSUInteger nodeChildCount= [node childCount];
                    
                    if (nodeChildCount>0)
                    {
                        NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
                        for(int j=0; j<nodeChildCount; j++)
                        {
                            NSString *value = [[node childAtIndex:j] XMLString];

                            [dictionary setObject:value forKey:[[node childAtIndex:j] name]];
                        }
                        
                        [detailsDictionary setObject:[NSDictionary dictionaryWithDictionary:dictionary] forKey:@"Desc"];
                    }
                }
                else    {
                    GDataXMLNode *node=[root childAtIndex:i];
                    
                    NSUInteger nodeChildCount= [node childCount];
                    
                    if (nodeChildCount>0)
                    {
                        NSMutableArray *array = [NSMutableArray array];
                        for(int j=0; j<nodeChildCount; j++)
                        {
                            GDataXMLNode *subNode=[node childAtIndex:j];
                            
                            NSUInteger subNodeChildCount= [subNode childCount];
                            
                            if (subNodeChildCount>0)
                            {
                                IBMuseumRoomInfo *info = [[IBMuseumRoomInfo alloc] init];
                                for(int k=0; k<subNodeChildCount; k++)
                                {
                                    NSString *key = [[subNode childAtIndex:k] name];
                                    NSString *value = [[subNode childAtIndex:k] stringValue];
                                    
                                    if([key isEqualToString:@"No"])   {
                                        [info setRoomNo:value];
                                    }
                                    else if ([key isEqualToString:@"Coords"])  {
                                        [info setCoordinates:value];
                                    }
                                    else if ([key isEqualToString:@"circle"])
                                    {
                                        [info setCircleCoordinates:value];
                                    }
                                    else    {
                                        continue;
                                    }
                                }
                                
                                [info setSectionNo:[NSNumber numberWithInteger:sectionIndex]];
                                
                                [array addObject:info];
                                
                                roomIndex++;
                            }
                        }
                        
                        NSString *index = [[((GDataXMLElement *)[root childAtIndex:i]) attributeForName:@"index"] stringValue];
                        NSDictionary *sectionDictionary = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:index,[NSArray arrayWithArray:array], nil] forKeys:[NSArray arrayWithObjects:@"index",@"rooms",nil]];
                        [sectionArray addObject:sectionDictionary];
                    }
                    
                    sectionIndex++;
                }
            }
        }
        
        [detailsDictionary setObject:[NSArray arrayWithArray:sectionArray] forKey:@"Section"];
        
        return [NSDictionary dictionaryWithDictionary:detailsDictionary];
    }
}

- (NSArray *)loadPhotoGalleryXMLWithLocationNo:(NSInteger)locationNo andLanguageIndex:(NSInteger)languageIndex forceDownload:(BOOL)needDownload
{
    NSString *urlPath,*museumDir,*languageDir;
    
    DIRLOCATION_STR(locationNo,museumDir);
    DIRLANGUAGE_STR(languageIndex,languageDir);
    
    urlPath = [NSString stringWithFormat:@"%@/%@/%@/%@/%@",SYNC_URL,IBParentDirectory,museumDir,languageDir,kGalleryXml];
    NSString *destinationDir = [IBGlobal createDocumentsDirectoryByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",languageDir,museumDir]];
    NSString *destinationPath = [destinationDir stringByAppendingPathComponent:kGalleryXml];
    
    BOOL fileExist = [[NSFileManager defaultManager] fileExistsAtPath:destinationPath];
    
    if(needDownload || !fileExist)    {
        NSURL *url = [NSURL URLWithString:urlPath];
        ASIHTTPRequest *asiRequest = [ASIHTTPRequest requestWithURL:url];
        [asiRequest setTimeOutSeconds:360];
        [asiRequest setDownloadDestinationPath:destinationPath];
        
        [asiRequest startSynchronous];
    }
    
    NSData *xmlData = [[NSMutableData alloc] initWithContentsOfFile:destinationPath];
    NSString* parseString = [[NSString alloc] initWithData:xmlData encoding:NSUTF8StringEncoding];
    
    NSError *error = nil;
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:parseString options:0 error:&error];
    
    if (doc == nil)
    {
        //NSLog(@"Parsing photo gallery xml failed or nil!!!");
        return nil;
    }
    else
    {
        GDataXMLElement *root = [doc rootElement];
        
        NSUInteger childCount = [root childCount];
        
        NSMutableArray *photoArray = [NSMutableArray array];
        
        if(childCount > 0) {
            //
            for(int i=0; i<childCount; i++) {
                //
                if([[[root childAtIndex:i] name] isEqualToString:@"Photo"]) {
                    //
                    GDataXMLNode *node=[root childAtIndex:i];
                    NSUInteger nodeChildCount= [node childCount];
                    
                    if (nodeChildCount > 0) {
                        //
                        IBGalleryImageXMLInfo *info = [[IBGalleryImageXMLInfo alloc] init];
                        //
                        for(int j=0; j<nodeChildCount; j++) {
                            //
                            NSString *key = [[node childAtIndex:j] name];
                            NSString *value = [[node childAtIndex:j] stringValue];
                            //
                            if([key isEqualToString:@"No"]) {
                                //
                                [info setReferenceNo:value];
                                [info setImageName:[NSString stringWithFormat:@"%@.%@",value,kImageExtension]];
                            } else if ([key isEqualToString:@"HD"]) {
                                //
                                [info setHeading:value];
                            } else if ([key isEqualToString:@"Desc"]) {
                                //
                                [info setDescription:value];
                            } else {
                                continue;
                            }
                        }
                        
                        [photoArray addObject:info];
                    }
                }
            }
        }
        //
        return photoArray;
    }
}

- (NSArray *)loadAudioGuideXMLWithLocationNo:(NSInteger)locationNo andLanguageIndex:(NSInteger)languageIndex forceDownload:(BOOL)needDownload
{
    NSString *urlPath,*museumDir,*languageDir;
    
    DIRLOCATION_STR(locationNo,museumDir);
    DIRLANGUAGE_STR(languageIndex,languageDir);
    
    urlPath = [NSString stringWithFormat:@"%@/%@/%@/%@/%@",SYNC_URL,IBParentDirectory,museumDir,languageDir,kAudioGuideXml];
    NSString *destinationDir = [IBGlobal createDocumentsDirectoryByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",languageDir,museumDir]];
    NSString *destinationPath = [destinationDir stringByAppendingPathComponent:kAudioGuideXml];
    
    return [self loadAudioGuideXMLFromURLPath:urlPath toDestinationPath:destinationPath forceDownload:needDownload];
}

- (NSArray *)loadAudioGuideXMLFromURLPath:(NSString *)urlPath toDestinationPath:(NSString *)destination forceDownload:(BOOL)needDownload
{
    BOOL fileExist = [[NSFileManager defaultManager] fileExistsAtPath:destination];
    
    if(needDownload || !fileExist)    {
        NSURL *url = [NSURL URLWithString:urlPath];
        ASIHTTPRequest *asiRequest = [ASIHTTPRequest requestWithURL:url];
        [asiRequest setTimeOutSeconds:360];
        [asiRequest setDownloadDestinationPath:destination];
        
        [asiRequest startSynchronous];
    }
    
    NSData *xmlData = [[NSMutableData alloc] initWithContentsOfFile:destination];
    NSString* parseString = [[NSString alloc] initWithData:xmlData encoding:NSUTF8StringEncoding];
    
    NSError *error = nil;
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithXMLString:parseString options:0 error:&error];
    
    if (doc == nil)
    {
        //NSLog(@"Parsing audio guide xml failed or nil!!!");
        return nil;
    }
    else
    {
        GDataXMLElement *root = [doc rootElement];
        
        NSUInteger childCount = [root childCount];
        
        NSMutableArray *guideArray = [NSMutableArray array];
        
        if(childCount>0)
        {
            for(int i=0; i<childCount; i++)
            {
                if([[[root childAtIndex:i] name] isEqualToString:@"Guide"])
                {
                    GDataXMLNode *node=[root childAtIndex:i];
                    
                    NSUInteger nodeChildCount= [node childCount];
                    
                    if (nodeChildCount>0)
                    {
                        IBAudioGuideXMLInfo *info = [[IBAudioGuideXMLInfo alloc] init];
                        for(int j=0; j<nodeChildCount; j++)
                        {
                            NSString *key = [[node childAtIndex:j] name];
                            NSString *value = [[node childAtIndex:j] stringValue];
                            
                            if([key isEqualToString:@"No"])   {
                                [info setReferenceNo:value];
                            }
                            else if ([key isEqualToString:@"Title"])  {
                                [info setGuideTitle:value];
                            }
                            else if ([key isEqualToString:@"Desc"])    {
                                value = [[node childAtIndex:j] XMLString];
                                [info setGuideDescription:value];
                            }
                            else    {
                                continue;
                            }
                        }
                        
                        [guideArray addObject:info];
                    }
                }
            }
        }
        
        return guideArray;
    }
}

@end
