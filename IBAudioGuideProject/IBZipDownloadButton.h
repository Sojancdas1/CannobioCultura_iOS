//
//  IBDownloadButton.h
//  IBExperiments
//
//  Created by Mobility 2014 on 26/05/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    StatusNone,
    StatusInAppPurchase,
    StatusNotDownloaded,
    StatusDownloading,
    StatusDownloaded,
    StatusFailed
}Status;

//typedef enum {
//    DownloadTypeNone,
//    InAppPurchase,
//    PromotionalGift,
//    DirectDownload,
//}DownloadType;

typedef enum {
    AlignLeft,
    AlignRight,
    AlignCentre
}Alignment;

@class IBZipDownloadButton;

@protocol IBZipDownloadButtonDelegate <NSObject>
@required
- (void)inAppActionRequired;
- (void)zipDownloadedForItem:(IBZipDownloadButton *)zipDownloadButton;
- (void)actionRequiredOnZipDownloadButton:(IBZipDownloadButton *)zipDownloadButton;
@end

@interface IBZipDownloadButton : UIControl
{
    id <IBZipDownloadButtonDelegate> delegate;
}

@property (nonatomic, assign) id <IBZipDownloadButtonDelegate> delegate;

@property (nonatomic,assign) Status status;
@property (nonatomic,assign) Alignment alignment;
@property (nonatomic,assign) BOOL selection;
@property (nonatomic,strong) NSString *urlString;
@property (nonatomic,strong) NSString *destinationDir;
@property (nonatomic,strong) NSString *promoLocation;
@property (nonatomic,strong) NSString *promoCode;
@property (nonatomic,readonly) NSString *key;
@property (nonatomic,strong,readwrite) UIColor *normalColor;
@property (nonatomic,strong,readwrite) UIColor *highlightedColor;

- (void)forceDownload;
//- (void)setButtonLabel:(NSString *)labelText;
- (NSString *)buttonLabel;

- (void)startCustomProgress;
- (void)setCustomProgress:(CGFloat)value;
- (void)endCustomProgress;
@end
