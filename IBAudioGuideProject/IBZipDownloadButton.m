//
//  IBDownloadButton.m
//  IBExperiments
//
//  Created by Mobility 2014 on 26/05/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBZipDownloadButton.h"

#import "IBCircularProgressView.h"
#import "IBActivityIndicator.h"

#import "ASIHTTPRequest.h"
#import "ZipArchive.h"

#import "Base64.h"
#import "IBGlobal.h"

#import "NSString+SSToolkitAdditions.h"

//Username: IBM
//Password: 29052014pass

@interface IBZipDownloadButton ()<ASIHTTPRequestDelegate,ASIProgressDelegate,UIAlertViewDelegate>
{
    CGPoint progressCentre;
    ASIHTTPRequest *asiRequest;
    long long receivedBytes,contentLength;
}

@property (nonatomic,assign) BOOL isDownloading; 
@property (nonatomic,strong) IBCircularProgressView *progressView;
@property (nonatomic,strong) IBActivityIndicator *activityIdicator;

- (void)updateView;
- (void)startDownload;
- (void)downloadButtonTouched;
- (void)pauseOrResumeDownloading;
- (void)actionRequiredOnTarget;
- (void)alertPromocodeNeed;

@end

@implementation IBZipDownloadButton

@synthesize delegate = _delegate;

@synthesize key = _key;
@synthesize status = _status;
@synthesize alignment = _alignment;
@synthesize selection = _selection;
@synthesize progressView = _progressView;
@synthesize activityIdicator = _activityIdicator;
@synthesize isDownloading = _isDownloading;
@synthesize urlString = _urlString;
@synthesize destinationDir = _destinationDir;
@synthesize promoLocation = _promoLocation;
@synthesize promoCode = _promoCode;
@synthesize normalColor = _normalColor;
@synthesize highlightedColor = _highlightedColor;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _progressView = [[IBCircularProgressView alloc] init];
        _progressView.frame = CGRectMake(0, 0, 30, 30);
        
        [_progressView setTintColor:[UIColor blackColor]];
        
        [_progressView addTarget:self action:@selector(pauseOrResumeDownloading) forControlEvents:UIControlEventTouchUpInside];
        
        _activityIdicator = [[IBActivityIndicator alloc] init];
        _activityIdicator.frame= CGRectMake(0, 0, 21, 21);
        
        _activityIdicator.hidesWhenStopped = YES;
        _activityIdicator.indicationStyle = IBActivityIndicationStyleGradient;
        _activityIdicator.indicatorViewStyle = IBActivityIndicatorViewStyleGray;
        
        self.alignment = AlignRight;
        self.isDownloading = NO;
        
        _normalColor = [UIColor blackColor];
        _highlightedColor = [UIColor blueColor];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)forceDownload
{
    if(_isDownloading) return;

    if(_status == StatusDownloaded)   {
        [self removeTarget:self action:@selector(actionRequiredOnTarget) forControlEvents:UIControlEventTouchUpInside];
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:[NSString stringWithFormat:@"%d",StatusNotDownloaded] forKey:self.key];
    [defaults synchronize];
    
    self.status = StatusNotDownloaded;
    
    [_progressView setProgress:0];
    contentLength = 0;
    receivedBytes = 0;
}

- (void)setStatus:(Status)status
{
    _status = status;
    
    for(UIView *view in self.subviews) [view removeFromSuperview];
    //
    if(status == StatusInAppPurchase) {
        //
        [self setPromoCode:kPGDefaultPrize];
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, (self.frame.size.height/2)-15, self.frame.size.width, 30)];
        [button setShowsTouchWhenHighlighted:NO];
        [button setAdjustsImageWhenDisabled:NO];
        [button setBackgroundColor:[UIColor clearColor]];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [button setTitle:kPGDefaultPrize forState:UIControlStateNormal];
        [button.titleLabel setFont:kAppFontSemibold(12)];
        [button.layer setBorderColor:[UIColor grayColor].CGColor];
        [button.layer setBorderWidth:2.0f];
        [button.layer setCornerRadius:3.0f];
        [button addTarget:self action:@selector(downloadButtonTouched) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        
    } else if(status == StatusNotDownloaded) {
        //
        [self setPromoCode:@"Download"];
        
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, (self.frame.size.height/2)-15, self.frame.size.width, 30)];
        [button setShowsTouchWhenHighlighted:NO];
        [button setAdjustsImageWhenDisabled:NO];
        [button setBackgroundColor:[UIColor clearColor]];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [button setTitle:@"Download" forState:UIControlStateNormal];
        [button.titleLabel setFont:kAppFontSemibold(12)];
        [button.layer setBorderColor:[UIColor grayColor].CGColor];
        [button.layer setBorderWidth:2.0f];
        [button.layer setCornerRadius:3.0f];
        [button addTarget:self action:@selector(downloadButtonTouched) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
        
    } else if (status == StatusDownloading) {
        //
        [self addSubview:_activityIdicator];
        self.isDownloading = YES;
        [self updateView];
        [_activityIdicator startAnimating];
        
    } else if (status == StatusDownloaded) {
        //
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"navigation.png"]];
        imageView.frame = CGRectMake(self.frame.size.width-12, (self.frame.size.height/2)-10.5, 12, 21);
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.backgroundColor = [UIColor clearColor];
        [self addSubview:imageView]; 
       [self addTarget:self action:@selector(actionRequiredOnTarget) forControlEvents:UIControlEventTouchUpInside];
    }
    //
    [self updateView];
}

- (void)startCustomProgress {
    //
    if(_status == StatusDownloaded) {
        [self removeTarget:self action:@selector(actionRequiredOnTarget) forControlEvents:UIControlEventTouchUpInside];
    }
    _status = StatusDownloading;
    
    for(UIView *view in self.subviews)  {
        [view removeFromSuperview];
    }
    
    [self addSubview:_activityIdicator];
    [_activityIdicator startAnimating];
    
    [self setUserInteractionEnabled:NO];
}

- (void)setCustomProgress:(CGFloat)value
{
    if(value==0.f) {
        [_activityIdicator stopAnimating];
        
        for(UIView *view in self.subviews)  {
            [view removeFromSuperview];
        }
        
        [self addSubview:_progressView];
        
        _progressView.progress = 0.f;
    }
    else if(value<1.f)    {
        _progressView.progress = value;
    }
}

- (void)endCustomProgress   {
    //
    _progressView.progress = 1.f;
    
    for(UIView *view in self.subviews)  {
        [view removeFromSuperview];
    }
    
    self.status = StatusDownloaded;
    [self setUserInteractionEnabled:YES];
}

- (void)actionRequiredOnTarget
{
    [_delegate actionRequiredOnZipDownloadButton:self]; 
}

- (void)downloadButtonTouched {
    //
    [self removeTarget:self action:@selector(actionRequiredOnTarget) forControlEvents:UIControlEventTouchUpInside];
    
    if (self.status == StatusInAppPurchase) {
        [_delegate inAppActionRequired];
    } else if (self.status == StatusNotDownloaded) {
        [self startDownload];
    }
    
    self.selection = YES;
    [self actionRequiredOnTarget];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{//Place code here
        [self addTarget:self action:@selector(actionRequiredOnTarget) forControlEvents:UIControlEventTouchUpInside];
    });
}

//- (void)downloadButtonTouched {
//    //
//    [self removeTarget:self action:@selector(actionRequiredOnTarget) forControlEvents:UIControlEventTouchUpInside];
//    
//    if(self.type == InAppPurchase) {
//        //
//        if(![[self buttonLabel] isEqualToString:@"Download"]) {
//            [_delegate inAppActionRequired];
//        } else {
//            [self startDownload];
//        }
//    } else if(self.type == PromotionalGift)   {
//        [self alertPromocodeNeed];
//    } else {
//        [self startDownload];
//    }
//    
//    self.selection = YES;
//    [_delegate actionRequiredOnZipDownloadButton:self];
//    
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{//Place code here
//        [self addTarget:self action:@selector(actionRequiredOnTarget) forControlEvents:UIControlEventTouchUpInside];
//    });
//}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)    {
        NSString *promoCodeEntered = [[alertView textFieldAtIndex:0].text stringByTrimmingLeadingAndTrailingWhitespaceAndNewlineCharacters];
        if([promoCodeEntered isEqualToString:self.promoCode]) {
            [self startDownload];
        }
        else    {
            [self alertPromocodeNeed];
        }
    }
}

- (void)setButtonLabel:(NSString *)labelText
{
    for(UIButton *button in self.subviews)  {
        if([button isKindOfClass:[UIButton class]])   {
            [button setTitle:labelText forState:UIControlStateNormal];
        }
    }
}

- (NSString *)buttonLabel
{
    UIButton *downloadButton;
    for(UIButton *button in self.subviews)  {
        downloadButton = button;
        break;
    }
    
    return downloadButton.titleLabel.text;
}
           
- (void)alertPromocodeNeed  {
    NSDictionary *langDictionary = [SharedAppDelegate langDictionary];
    
    NSString *message = [[langDictionary valueForKey:@"LabelPromotionalMessage"] stringByReplacingOccurrencesOfString:@"%@" withString:self.promoLocation];
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:[langDictionary valueForKey:@"LabelPromotionalGift"] message:message delegate:self cancelButtonTitle:[langDictionary valueForKey:@"LabelCancel"] otherButtonTitles:[langDictionary valueForKey:@"LabelOk"],nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [alert textFieldAtIndex:0].placeholder = [langDictionary valueForKey:@"LabelPromotionalPlaceholder"];
    [alert show];
}

- (void)setAlignment:(Alignment)alignment
{
    _alignment = alignment;
    
    CGFloat width = self.frame.size.width;
    CGFloat height = self.frame.size.height;
    
    if(alignment == AlignLeft)  {
        progressCentre = CGPointMake(height/2, height/2);
    }
    else if (alignment == AlignRight)   {
        progressCentre = CGPointMake(width-(height/2), height/2);
    }
    else    {
        progressCentre = CGPointMake(width/2, height/2);
    }

    if(_progressView)    {
        _progressView.center = progressCentre;
    }
    
    if(_activityIdicator)    {
        _activityIdicator.center = progressCentre;
    }
}

- (void)pauseOrResumeDownloading
{
    if(self.isDownloading)   {
        self.isDownloading = NO;
        [asiRequest cancel];
        receivedBytes = 0;
        contentLength = 0;
    }
    else    {
        self.isDownloading = YES;
        [asiRequest startAsynchronous];
    }
}

- (void)startDownload
{
    self.status = StatusDownloading;
    
    NSURL *url = [NSURL URLWithString:_urlString];
    asiRequest = [ASIHTTPRequest requestWithURL:url];
    [asiRequest setTimeOutSeconds:360];
    [asiRequest setDelegate:self];
    [asiRequest setDownloadProgressDelegate:self];
    [asiRequest setDownloadDestinationPath:[NSString stringWithFormat:@"%@%@",_destinationDir,[_urlString lastPathComponent]]];
    [asiRequest setTemporaryFileDownloadPath:[NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"loc%ld-%@.download",(long)[SharedAppDelegate locationNo],[_urlString lastPathComponent]]]];
    [asiRequest setAllowResumeForFileDownloads:YES];
    
    [asiRequest startAsynchronous];
}

- (void)requestStarted:(ASIHTTPRequest *)request
{
    //NSLog(@"Request started!");
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:[NSString stringWithFormat:@"%d",self.status] forKey:self.key];
    // 
    [defaults synchronize];
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [_progressView setProgress:1.0f animated:YES];
    
    [self addSubview:_activityIdicator];
    [_activityIdicator startAnimating];
    
    ZipArchive *zipArchive = [[ZipArchive alloc] init];
    [zipArchive UnzipOpenFile:[NSString stringWithFormat:@"%@%@",_destinationDir,[_urlString lastPathComponent]]];
    if([zipArchive UnzipFileTo:_destinationDir overWrite:YES])  {
        [_activityIdicator stopAnimating];
        [_activityIdicator removeFromSuperview];
        
        self.status = StatusDownloaded;
    }
    [zipArchive UnzipCloseFile];
    
    NSError *error = nil;
    [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@%@",_destinationDir,[_urlString lastPathComponent]] error:&error];
    
    self.isDownloading = NO;
    //NSLog(@"Total length received %lld",receivedBytes);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:[NSString stringWithFormat:@"%d",self.status] forKey:self.key];
    [defaults synchronize];
    
    if(self.status == StatusDownloaded)   {
        [_delegate zipDownloadedForItem:self];
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    self.status = StatusNotDownloaded;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setValue:[NSString stringWithFormat:@"%d",self.status] forKey:self.key];
    [defaults synchronize];
}

- (void)request:(ASIHTTPRequest *)request didReceiveBytes:(long long)bytes
{
    if(bytes>0) {
        receivedBytes = receivedBytes+bytes;
        float percentage = (float)receivedBytes/(float)contentLength;
        [_progressView setProgress:percentage animated:YES];
    }
    
    //NSLog(@"bytes received %lld",bytes);
}

- (void)request:(ASIHTTPRequest *)request incrementDownloadSizeBy:(long long)newLength
{
    if(newLength>0) {
        [_activityIdicator stopAnimating];
        
        for(UIView *view in self.subviews)  {
            [view removeFromSuperview];
        }
        
        [self addSubview:_progressView];
        
        _progressView.progress = 0;
        contentLength = newLength;
    }
    
    //NSLog(@"Total length calculated %lld",newLength);
}

- (void)setUrlString:(NSString *)urlString
{
    _urlString = urlString;
    
 //   NSString *downloadStatus = [[NSUserDefaults standardUserDefaults] valueForKey:self.key];
    
 //   if([downloadStatus intValue] == StatusDownloaded)   {
 //       self.status = StatusDownloaded;
 //       [_delegate zipDownloadedForItem:self];
 //   }
}

- (void)setDestinationDir:(NSString *)destinationDir
{
    BOOL isDir = YES;
    if (![[NSFileManager defaultManager] fileExistsAtPath:destinationDir isDirectory:&isDir])  {
        [[NSFileManager defaultManager] createDirectoryAtPath:destinationDir withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    if(![destinationDir hasSuffix:@"/"])   {
        _destinationDir = [NSString stringWithFormat:@"%@/",destinationDir];
    }
    else    {
        _destinationDir = [NSString stringWithString:destinationDir];
    }
}

- (void)updateView
{
    if (self.selection==NO) {
        if(self.status == StatusNotDownloaded)   {
            for(UIButton *button in self.subviews)    {
                [button setTitleColor:_normalColor forState:UIControlStateNormal];
                [button setBackgroundColor:[UIColor clearColor]];
                [button.layer setBorderColor:_normalColor.CGColor];
            }
        }
        else if(self.status == StatusDownloaded)   {
            for(UIImageView *imageView in self.subviews)    {
                if([imageView isKindOfClass:[UIImageView class]])   {
                    [imageView setImage:[UIImage imageNamed:@"navigation.png"]];
                }
            }
        }
        else  if(self.status == StatusDownloading) {
            [_progressView setTintColor:_normalColor];
            [_progressView setBackgroundColor:[UIColor clearColor]];
        }
    }
    else    {
        if(self.status == StatusNotDownloaded)   {
            for(UIButton *button in self.subviews)    {
                [button setTitleColor:_highlightedColor forState:UIControlStateNormal];
                [button setBackgroundColor:[UIColor clearColor]];
                [button.layer setBorderColor:_highlightedColor.CGColor];
            }
        }
        else if(self.status == StatusDownloaded)   {
            for(UIImageView *imageView in self.subviews)    {
                if([imageView isKindOfClass:[UIImageView class]])   {
                    [imageView setImage:[UIImage imageNamed:@"navigate-highlighted.png"]];
                }
            }
        }
        else if(self.status == StatusDownloading) {
             [_progressView setTintColor:_normalColor];
            [_progressView setBackgroundColor:[UIColor clearColor]];
        }
    }
}

- (void)setNormalColor:(UIColor *)normalColor
{
    _normalColor = normalColor;
    [self updateView];
}

- (void)setHighlightedColor:(UIColor *)highlightedColor
{
    _highlightedColor = highlightedColor;
    [self updateView];
}

- (void)setSelection:(BOOL)selection
{
    _selection = selection;
    [self updateView];
}

- (NSString *)key
{
    NSString *keyString = [[[_urlString stringByReplacingOccurrencesOfString:SYNC_URL withString:@""] base64EncodedString] uppercaseString];
    
    NSMutableString *newString = [NSMutableString string];
    for (int i = 0; i < [keyString length]; i++)
    {
        int ascii = [keyString characterAtIndex:i];
        if (((ascii>64) && (ascii<91)) || ((ascii>47) && (ascii<58)))
        {
            [newString appendFormat:@"%c",ascii];
        }
    }
    
    return [NSString stringWithString:newString];
}

@end
