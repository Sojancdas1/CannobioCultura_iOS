//
//  IBZipDownloadItem.h
//  IBExperiments
//
//  Created by Mobility 2014 on 03/06/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IBZipDownloadButton.h"
#import "HPGrowingTextView.h"

@class IBZipDownloadItemView;

@protocol IBZipDownloadItemViewDelegate <NSObject>
@required
- (void)inAppActionRequired;
- (void)zipDownloadedForItem:(IBZipDownloadItemView *)zipDownloadItemView;
- (void)actionRequiredOnZipDownloadItemView:(IBZipDownloadItemView *)zipDownloadItemView;
@end

@interface IBZipDownloadItemView : UIControl
{
    UIImageView *_imageView;
    IBZipDownloadButton *_downloadButton;
    
    id <IBZipDownloadItemViewDelegate> delegate;
}

@property (nonatomic, assign) id <IBZipDownloadItemViewDelegate> delegate;

@property (nonatomic,assign) BOOL currentSelection;
@property (nonatomic,strong,readwrite) NSString *text;
@property (nonatomic,strong,readwrite) UIColor *normalColor;
@property (nonatomic,strong,readwrite) UIColor *highlightedColor;
@property (nonatomic,strong,readwrite) UIColor *normalBackgoundColor;
@property (nonatomic,strong,readwrite) UIColor *highlightedBackgoundColor;
@property (nonatomic,strong,readwrite) UIImage *normalImage;
@property (nonatomic,strong,readwrite) UIImage *highlightedImage;
@property (nonatomic,strong,readonly) IBZipDownloadButton *downloadButton;

@end
