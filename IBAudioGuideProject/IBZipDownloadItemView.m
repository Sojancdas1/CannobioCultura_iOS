//
//  IBZipDownloadItem.m
//  IBExperiments
//
//  Created by Mobility 2014 on 03/06/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBZipDownloadItemView.h"

#import "IBGlobal.h"

#define kAccessoryHeight 30
#define kAccessoryWidth 75

@interface IBZipDownloadItemView ()<IBZipDownloadButtonDelegate>
{
    HPGrowingTextView *_textView;
}

@property (nonatomic,strong,readonly) UIImageView *imageView;
@property (nonatomic,strong,readonly) HPGrowingTextView *textView;

- (void)updateView;
- (void)actionRequiredOnTarget;

@end

@implementation IBZipDownloadItemView

@synthesize delegate = _delegate;

@synthesize textView = _textView;
@synthesize imageView = _imageView;
@synthesize downloadButton = _downloadButton;
@synthesize highlighted = _highlighted;
@synthesize normalColor = _normalColor;
@synthesize highlightedBackgoundColor = _highlightedBackgoundColor;
@synthesize normalBackgoundColor = _normalBackgoundColor;
@synthesize highlightedImage = _highlightedImage;
@synthesize normalImage = _normalImage;
@synthesize currentSelection = _currentSelection;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code
        _textView = [[HPGrowingTextView alloc] init];
        _imageView = [[UIImageView alloc] init];
        _downloadButton = [[IBZipDownloadButton alloc] init];
        
        _downloadButton.delegate = self;
        
        _textView.isScrollable = NO;
        _textView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
        _textView.minNumberOfLines = 1;
        _textView.maxNumberOfLines = 30;
        _textView.font = kAppFontSemibold(15);
        _textView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 0);
        _textView.backgroundColor = [UIColor whiteColor];
        
        [self addSubview:_textView];
        [self addSubview:_imageView];
        [self addSubview:_downloadButton];
        
        self.clipsToBounds = YES;
        self.currentSelection = NO;
        
        _normalColor = [UIColor blackColor];
        _highlightedColor = [UIColor blueColor];
        _normalBackgoundColor = [UIColor grayColor];
        _highlightedBackgoundColor = [UIColor whiteColor];
        
        self.backgroundColor = _normalColor;
        
        [self addTarget:self action:@selector(actionRequiredOnTarget) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        CGFloat width = frame.size.width;
        CGFloat height = frame.size.height;
        
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(15, (height/2)-(kAccessoryHeight/2), kAccessoryHeight, kAccessoryHeight)];
        _downloadButton = [[IBZipDownloadButton alloc] initWithFrame:CGRectMake(width-kAccessoryWidth-15, (height/2)-(kAccessoryHeight/2), kAccessoryWidth, kAccessoryHeight)];
        _textView = [[HPGrowingTextView alloc] initWithFrame:CGRectMake(kAccessoryHeight+13, 3, width-(kAccessoryHeight+kAccessoryWidth+26), height-6)];
        
        _downloadButton.delegate = self;
        _imageView.contentMode = UIViewContentModeScaleAspectFit;
        
        _textView.editable = NO;
        _textView.isScrollable = NO;
        _textView.userInteractionEnabled = NO;
        _textView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
        _textView.minNumberOfLines = 1;
        _textView.maxNumberOfLines = 30;
        _textView.font = kAppFontSemibold(15);
        _textView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 0);
        _textView.backgroundColor = [UIColor whiteColor];
        
        [self addSubview:_textView];
        [self addSubview:_imageView];
        [self addSubview:_downloadButton];
        
        self.clipsToBounds = YES;
        self.currentSelection = NO;
        
        _normalColor = [UIColor blackColor];
        _highlightedColor = [UIColor blueColor];
        _normalBackgoundColor = [UIColor grayColor];
        _highlightedBackgoundColor = [UIColor whiteColor];
        
        self.backgroundColor = _normalColor;
        
        [self addTarget:self action:@selector(actionRequiredOnTarget) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    
    CGFloat width = rect.size.width;
    CGFloat height = rect.size.height;
    
    _imageView.frame = CGRectMake(15, (height/2)-(kAccessoryHeight/2), kAccessoryHeight, kAccessoryHeight);
    _downloadButton.frame = CGRectMake(width-kAccessoryWidth-15, (height/2)-(kAccessoryHeight/2), kAccessoryWidth, kAccessoryHeight);
    _textView.frame = CGRectMake(kAccessoryHeight+13, 3, width-(kAccessoryHeight+kAccessoryWidth+26), height-6);
}

- (void)setText:(NSString *)text
{
    [_textView setText:text];
    
    if(_textView.frame.size.height>kAccessoryHeight)    {
        [super setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width,_textView.frame.size.height+6)];
    }
    else    {
        [super setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width,kAccessoryHeight+6)];
    }
    
    [self setNeedsDisplay];
}

- (NSString *)text
{
    return [_textView text];
}

- (void)actionRequiredOnTarget
{
    [_delegate performSelector:@selector(actionRequiredOnZipDownloadItemView:) withObject:self];
    self.currentSelection = YES;
}

- (void)inAppActionRequired
{
    if (_downloadButton.status == StatusInAppPurchase) {
        [_delegate performSelector:@selector(inAppActionRequired)];
    }
//    if(![[_downloadButton buttonLabel] isEqualToString:@"Download"]) {
//        [_delegate performSelector:@selector(inAppActionRequired)];
//    }
}

- (void)zipDownloadedForItem:(IBZipDownloadButton *)zipDownloadButton
{
    [_delegate performSelector:@selector(zipDownloadedForItem:) withObject:self];
}

- (void)actionRequiredOnZipDownloadButton:(IBZipDownloadButton *)zipDownloadButton
{
    [_delegate performSelector:@selector(actionRequiredOnZipDownloadItemView:) withObject:self];
    self.currentSelection = YES;
}

- (void)setNormalColor:(UIColor *)normalColor
{
    _normalColor = normalColor;
    _downloadButton.normalColor = normalColor;
    [self updateView];
}

- (void)setHighlightedColor:(UIColor *)highlightedColor
{
    _highlightedColor = highlightedColor;
    _downloadButton.highlightedColor = highlightedColor;
    [self updateView];
}

- (void)setNormalBackgoundColor:(UIColor *)normalBackgoundColor
{
    _normalBackgoundColor = normalBackgoundColor;
    [self updateView];
}

- (void)setHighlightedBackgoundColor:(UIColor *)highlightedBackgoundColor
{
    _highlightedBackgoundColor = highlightedBackgoundColor;
    [self updateView];
}

- (void)setNormalImage:(UIImage *)normalImage
{
    _normalImage = normalImage;
    [self updateView];
}

- (void)setHighlightedImage:(UIImage *)highlightedImage
{
    _highlightedImage = highlightedImage;
    [self updateView];
}

- (void)updateView
{
    if(self.currentSelection)   {
        self.textView.backgroundColor = _highlightedBackgoundColor;
        self.backgroundColor = _highlightedBackgoundColor;
    }
    else    {
        self.textView.backgroundColor = _normalBackgoundColor;
        self.backgroundColor = _normalBackgoundColor;
    }
    
    [UIView animateWithDuration:1.f animations:^{
        if(self.currentSelection) {
            self.imageView.image = _highlightedImage;
            self.textView.textColor = _highlightedColor;
            
            [self.downloadButton setSelection:YES];
        }
        else    {
            self.imageView.image = _normalImage;
            self.textView.textColor = _normalColor;
            
            [self.downloadButton setSelection:NO];
        }
    }];
}

- (void)setCurrentSelection:(BOOL)currentSelection
{
    _currentSelection = currentSelection;
    [self updateView];
}

@end
