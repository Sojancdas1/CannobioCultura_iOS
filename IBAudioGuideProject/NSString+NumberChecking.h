//
//  NSString+NumberChecking.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 21/06/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NumberChecking)

- (BOOL)isNumber;

@end
