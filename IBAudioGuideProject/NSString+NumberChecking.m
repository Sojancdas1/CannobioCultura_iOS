//
//  NSString+NumberChecking.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 21/06/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "NSString+NumberChecking.h"

@implementation NSString (NumberChecking)

- (BOOL)isNumber {
    if([self rangeOfCharacterFromSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]].location == NSNotFound) {
        return YES;
    }else {
        return NO;
    }
}

@end
