
//The MIT License (MIT)
//
//Copyright (c) 2014 Rafał Augustyniak
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of
//this software and associated documentation files (the "Software"), to deal in
//the Software without restriction, including without limitation the rights to
//use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//the Software, and to permit persons to whom the Software is furnished to do so,
//subject to the following conditions:
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
//FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
//IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#import "IBTableViewCellBase.h"

@interface RATableViewCell : IBTableViewCellBase

@property (weak, nonatomic) IBOutlet UILabel *labelDescription;
@property (weak, nonatomic) IBOutlet UILabel *customTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *labelSubArrayCount;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewAddition;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (strong, nonatomic) IBOutlet UILabel *labelGuideName;
@property (nonatomic)  BOOL isExpanded;
@property (nonatomic)  BOOL isSelectedCell;
@property (nonatomic)  BOOL isCellDequed;

@property (nonatomic, copy) void (^additionButtonTapAction)(id sender);
@property (nonatomic) BOOL additionButtonHidden;
//

- (void)setAdditionButtonHidden:(BOOL)additionButtonHidden cellInfo:(RATableViewCell*)cell;

- (void)setupWithTitle:(NSString *)title detailText:(NSString *)detailText imagePath:(NSString *)thumbnailPath descriptionText:(NSMutableAttributedString *)descText secondDescription:(NSMutableAttributedString *)secondDescriptionText level:(NSInteger)level additionButtonHidden:(BOOL)additionButtonHidden;



- (void)setAdditionButtonHidden:(BOOL)additionButtonHidden animated:(BOOL)animated;
- (void)selectedCellInRows;
- (void)UnselectedCell:(BOOL)isInnerArray;

@end
