
//The MIT License (MIT)
//
//Copyright (c) 2014 Rafał Augustyniak
//
//Permission is hereby granted, free of charge, to any person obtaining a copy of
//this software and associated documentation files (the "Software"), to deal in
//the Software without restriction, including without limitation the rights to
//use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//the Software, and to permit persons to whom the Software is furnished to do so,
//subject to the following conditions:
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
//FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
//IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#import "RATableViewCell.h"
#import "IBGlobal.h"
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface RATableViewCell ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLeadingSpaceToDescriptionLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLabelDescriptionWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintLabelGuideNameWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintSeperatorDistance;

@end

@implementation RATableViewCell
@synthesize isExpanded,isSelectedCell;
- (void)awakeFromNib
{
  [super awakeFromNib];
    self.isCellDequed = NO;
	isExpanded = NO;
    isSelectedCell = NO;
    self.labelSubArrayCount.hidden = YES;
    
    
       // self.imageView.frame.origin.x = self.imageView.frame.origin.x - 10;
  
  self.selectedBackgroundView = [UIView new];
  self.selectedBackgroundView.backgroundColor = [UIColor clearColor];
    self.labelGuideName.frame = CGRectMake(self.labelDescription.frame.origin.x + self.labelDescription.frame.size.width + 2, self.labelDescription.frame.origin.y , 280, 15);
    self.labelGuideName.font = [UIFont fontWithName:self.labelDescription.font.fontName size:13];
 //   CGRect tempTitleLabelFrame = self.customTitleLabel.frame;
    
    
}

- (void)prepareForReuse
{
  [super prepareForReuse];
  
//  self.additionButtonHidden = NO;
}


- (void)setupWithTitle:(NSString *)title detailText:(NSString *)detailText imagePath:(NSString *)thumbnailPath descriptionText:(NSMutableAttributedString *)descText secondDescription:(NSMutableAttributedString *)secondDescriptionText level:(NSInteger)level additionButtonHidden:(BOOL)additionButtonHidden {
    //
  self.customTitleLabel.text = title;
  self.labelGuideName.attributedText = nil;
//
    NSString *countString = [detailText stringByReplacingOccurrencesOfString:@"Number of children " withString:@""];
    NSString *tempDescription = [secondDescriptionText string];
    NSString *tempCountString = [NSString stringWithFormat:@" %@",countString];
    NSMutableAttributedString *countMutableAttributedString = [[NSMutableAttributedString alloc] initWithString:tempCountString];
    [countMutableAttributedString addAttribute:NSFontAttributeName value:kAppFontBold(11) range:NSMakeRange(0, [tempCountString length])];
    [countMutableAttributedString addAttribute:NSForegroundColorAttributeName value:RGBCOLOR(124, 124, 124) range:NSMakeRange(0, [tempCountString length])];
    NSMutableAttributedString *combinedMutableAttributeString = [[NSMutableAttributedString alloc]initWithAttributedString:secondDescriptionText];
    
    if ( ![countString isEqualToString:@"0"]&&[tempDescription isEqualToString:@"Nr.audio"])
    {
        self.labelSubArrayCount.hidden = NO;
        self.imageViewAddition.hidden = NO;
        self.labelSubArrayCount.text = countString;
         self.labelDescription.attributedText = descText;
		
		if (isExpanded)  {
			self.imageViewAddition.image = [UIImage imageNamed:@"Expanded.png"];
         //   additionButtonHidden = NO;
			//additionButtonHidden = YES;
		} else {
        self.imageViewAddition.image = [UIImage imageNamed:@"NotExpanded.png"];
           //   additionButtonHidden = YES;
		}
        //
        [combinedMutableAttributeString appendAttributedString:countMutableAttributedString];
        self.constraintLabelDescriptionWidth.constant = 0;
        self.constraintLeadingSpaceToDescriptionLabel.constant = 98;
        
         self.constraintLabelGuideNameWidth.constant = 280;
         self.constraintSeperatorDistance.constant = 7;
        
    //    CGRect frameCode1 = self.labelDescription.frame;
        CGRect frameCode1 = CGRectMake(103, 46, 70, 15);
        self.labelDescription.frame = frameCode1;
        
        CGRect frameCode = self.labelDescription.frame;
        frameCode.size.width = 0;
        self.labelDescription.frame = frameCode;
        
     //   CGRect frameGuideCode = self.labelGuideName.frame;
        CGRect frameGuideCode = CGRectMake(103, 46, 280, 15);
        self.labelGuideName.frame = frameGuideCode;
        
        //NSLog(@"guideName:%@",self.labelGuideName.text);
        //NSLog(@"Description:%@",self.labelDescription.text);
        
        
        self.labelGuideName.attributedText = combinedMutableAttributeString;


    }
    else if ([countString isEqualToString:@"0"]&&[tempDescription isEqualToString:@"Nr.audio"] )
    {
        self.labelSubArrayCount.text = @"no subRooms";
        self.labelSubArrayCount.hidden = YES;
        self.imageViewAddition.hidden = NO;
        self.imageViewAddition.image = [UIImage imageNamed:@"navigate-normal.png"];
        
       self.labelDescription.attributedText = descText;
        
        
        self.constraintLabelDescriptionWidth.constant = 60;
         self.constraintLeadingSpaceToDescriptionLabel.constant =  103 ;
        self.constraintLabelGuideNameWidth.constant = 0;
        self.constraintSeperatorDistance.constant = 2;

        
        //CGRect frameCode = self.labelDescription.frame;
        CGRect frameCode = CGRectMake(103, 46, 70, 15);
        self.labelDescription.frame = frameCode;
        
        
      //  CGRect frameGuideCode = self.labelGuideName.frame;
       CGRect frameGuideCode = CGRectMake(0, 0, 0, 0);
        frameGuideCode.origin.x = self.labelDescription.frame.origin.x +self.labelDescription.frame.size.width ;
        self.labelGuideName.frame = frameGuideCode;
        
        NSString *  tempStringTemp = @" 1";
        NSAttributedString *countMutableAttributedString1 = [[NSAttributedString alloc] initWithString:tempStringTemp];
        NSMutableAttributedString *combinedMutableAttributeString1 = [[NSMutableAttributedString alloc]initWithAttributedString:secondDescriptionText];
        [combinedMutableAttributeString1 appendAttributedString:countMutableAttributedString1];
        self.labelGuideName.attributedText = combinedMutableAttributeString1;

    }
    else
    {
       // CGRect frameCode1 = self.labelDescription.frame;
        CGRect frameCode1 = CGRectMake(103, 46, 60, 15);
        self.labelDescription.frame = frameCode1;
        
        //CGRect frameGuideCode = self.labelGuideName.frame;
        CGRect frameGuideCode = CGRectMake(103, 46, 280, 15);
        frameGuideCode.origin.x = self.labelDescription.frame.origin.x + self.labelDescription.frame.size.width  ;
        self.labelGuideName.frame = frameGuideCode;

          self.constraintSeperatorDistance.constant = 2;
        
        self.constraintLabelDescriptionWidth.constant = 60;
        self.constraintLeadingSpaceToDescriptionLabel.constant =  103 ;
         self.constraintLabelGuideNameWidth.constant = 280;
        
        
         self.labelSubArrayCount.text = @"no subRooms";
        self.labelSubArrayCount.hidden = YES;
        self.labelGuideName.attributedText = secondDescriptionText;
         self.imageViewAddition.image = [UIImage imageNamed:@"navigate-normal.png"];
         self.labelDescription.attributedText = descText;
        
    }
    countMutableAttributedString = nil;
    
     //NSLog(@"%@",[NSString stringWithFormat:@"%@/%@",[IBGlobal createDocumentsDirectoryByAppendingPathComponent:nil],thumbnailPath]);
     self.imageView.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[IBGlobal createDocumentsDirectoryByAppendingPathComponent:nil],thumbnailPath]];
    
    
    if (isSelectedCell)
    {
        self.labelGuideName.textColor = [UIColor grayColor];
        self.customTitleLabel.textColor = [UIColor whiteColor];
        self.labelDescription.textColor = [UIColor grayColor];
       self.backgroundColor = RGBCOLOR(117, 142, 150);
      //  self.imageViewAddition.image = [UIImage imageNamed:@"navigate-me-highlighted.png"];
    }
    else
    {
        self.labelGuideName.textColor = [UIColor grayColor];
        self.customTitleLabel.textColor = [UIColor blackColor];
        self.labelDescription.textColor = [UIColor grayColor];
        self.backgroundColor = [UIColor clearColor];
     //   self.imageViewAddition.image = [UIImage imageNamed:@"navigate-normal.png"];
    }
  if (level == 0) {
      self.backgroundColor = [UIColor whiteColor];// RGBCOLOR(218, 222, 223);
  } else if (level == 1) {
     self.backgroundColor = [IBGlobal colorWithHexString:@"dadedf"];
  } else if (level >= 2) {
    self.backgroundColor = UIColorFromRGB(0xE0F8D8);
  }
    //
   // NSArray *imageViewConstrains = [self _constrainsWithImageView:self.imageView andParentView:self.contentView];
   // [self.contentView addConstraints:imageViewConstrains];
}

-(UIImageView *)imageView {
    return _iconImageView;
}


//-(NSArray *)_constrainsWithImageView:(UIImageView *)imageView andParentView:(UIView *)parentView {
//    //
//    imageView.translatesAutoresizingMaskIntoConstraints = NO;
//    //
//    NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:parentView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:5.0];
//    //
//    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:parentView attribute:NSLayoutAttributeTop multiplier:1.0 constant:5.0];
//    //
//    NSLayoutConstraint *width = [NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:parentView attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:25.0];
//    //
//    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:imageView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:parentView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:5.0];
//    //
//    return @[left, top, width, bottom];
//}

#pragma mark - Properties

- (void)setAdditionButtonHidden:(BOOL)additionButtonHidden cellInfo:(RATableViewCell *)cell
{
    if (additionButtonHidden)
    {
        self.imageViewAddition.image = [UIImage imageNamed:@"Expanded.png"];
    }
    else 
    {
        self.imageViewAddition.image = [UIImage imageNamed:@"NotExpanded.png"];
    }
}

- (void)setAdditionButtonHidden:(BOOL)additionButtonHidden animated:(BOOL)animated
{
  _additionButtonHidden = additionButtonHidden;
  [UIView animateWithDuration:animated ? 0.2 : 0 animations:^{
   // self.additionButton.hidden = additionButtonHidden;
  }];
}
-(void)selectedCellInRows
{
    //self.backgroundColor = RGBCOLOR(105, 144, 152);
    self.backgroundColor = [IBGlobal colorWithHexString:@"699098"];
    self.customTitleLabel.textColor = [UIColor whiteColor];
     self.labelDescription.textColor = [UIColor whiteColor];
    self.labelGuideName.textColor = [UIColor whiteColor];
    self.imageViewAddition.image = [UIImage imageNamed:@"navigate-me-highlighted.png"];
    
    
}

-(void)UnselectedCell:(BOOL)isInnerArray
{
    if (isInnerArray)
    {
        self.backgroundColor = [UIColor whiteColor];
    }
    else
    {
        self.backgroundColor = [IBGlobal colorWithHexString:@"dadedf"];
// blue color
   }
  
        
   
    self.customTitleLabel.textColor = [UIColor blackColor];
    self.labelDescription.textColor = [UIColor grayColor];
    self.labelGuideName.textColor = [UIColor grayColor];
     self.imageViewAddition.image = [UIImage imageNamed:@"navigate-normal.png"];

}
#pragma mark - Actions

- (IBAction)additionButtonTapped:(id)sender
{
  if (self.additionButtonTapAction) {
    self.additionButtonTapAction(sender);
  }
}

@end
