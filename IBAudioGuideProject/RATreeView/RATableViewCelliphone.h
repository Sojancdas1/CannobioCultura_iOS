//
//  RATableViewCelliphone.h
//  IBAudioGuideProject
//
//  Created by Sreeshaj Kp on 08/12/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBTableViewCellBase.h"

@interface RATableViewCelliphone : IBTableViewCellBase

@property (weak, nonatomic) IBOutlet UILabel *labelDescription;
@property (weak, nonatomic) IBOutlet UILabel *customTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *labelSubArrayCount;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewAddition;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (strong, nonatomic) IBOutlet UILabel *labelGuideName;
@property (nonatomic)  BOOL isExpanded;
@property (nonatomic)  BOOL isSelectedCell;
@property (nonatomic)  BOOL isCellDequed;

@property (nonatomic, copy) void (^additionButtonTapAction)(id sender);
@property (nonatomic) BOOL additionButtonHidden;
- (void)setAdditionButtonHidden:(BOOL)additionButtonHidden cellInfo:(RATableViewCelliphone*)cell;

- (void)setupWithTitle:(NSString *)title detailText:(NSString *)detailText imagePath:(NSString *)thumbnailPath descriptionText:(NSMutableAttributedString *)descText secondDescription:(NSMutableAttributedString *)secondDescriptionText level:(NSInteger)level additionButtonHidden:(BOOL)additionButtonHidden;



- (void)setAdditionButtonHidden:(BOOL)additionButtonHidden animated:(BOOL)animated;
- (void)selectedCellInRows;
- (void)UnselectedCell:(BOOL)isInnerArray;

@end
