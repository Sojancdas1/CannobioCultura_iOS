//
//  RATableViewCelliphone.m
//  IBAudioGuideProject
//
//  Created by Sreeshaj Kp on 08/12/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "RATableViewCelliphone.h"

#import "IBGlobal.h"
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface RATableViewCelliphone ()


@end

@implementation RATableViewCelliphone
@synthesize isExpanded,isSelectedCell,isCellDequed;
- (void)awakeFromNib
{
    [super awakeFromNib];
    isCellDequed = NO;
    isExpanded = NO;
    isSelectedCell = NO;
    self.labelSubArrayCount.hidden = YES;
    
    
    // self.imageView.frame.origin.x = self.imageView.frame.origin.x - 10;
    
    self.selectedBackgroundView = [UIView new];
    self.selectedBackgroundView.backgroundColor = [UIColor clearColor];
    self.labelGuideName.frame = CGRectMake(self.labelDescription.frame.origin.x + self.labelDescription.frame.size.width , self.labelDescription.frame.origin.y , 200, 15);
    self.labelGuideName.font = [UIFont fontWithName:self.labelDescription.font.fontName size:11];
    
    
   
     
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    //  self.additionButtonHidden = NO;
}

-(UIImageView *)imageView {
    return _iconImageView;
}


- (void)setupWithTitle:(NSString *)title detailText:(NSString *)detailText imagePath:(NSString *)thumbnailPath descriptionText:(NSMutableAttributedString *)descText secondDescription:(NSMutableAttributedString *)secondDescriptionText level:(NSInteger)level additionButtonHidden:(BOOL)additionButtonHidden {
    //
    self.customTitleLabel.text = title;
    self.labelDescription.attributedText = descText;
    self.labelGuideName.attributedText = nil;
    
    NSString *countString = [detailText stringByReplacingOccurrencesOfString:@"Number of children " withString:@""];
    NSString *tempDescription = [secondDescriptionText string];
    NSString *tempCountString = [NSString stringWithFormat:@" %@",countString];
    NSAttributedString *countMutableAttributedString = [[NSAttributedString alloc] initWithString:tempCountString];
    NSMutableAttributedString *combinedMutableAttributeString = [[NSMutableAttributedString alloc]initWithAttributedString:secondDescriptionText];
    
    if ( ![countString isEqualToString:@"0"]&&[tempDescription isEqualToString:@"Nr.audio"]) {
        //
        self.labelSubArrayCount.hidden = NO;
        self.imageViewAddition.hidden = NO;
        self.labelSubArrayCount.text = countString;
        //
        if (isExpanded) self.imageViewAddition.image = [UIImage imageNamed:@"Expanded.png"];
        else self.imageViewAddition.image = [UIImage imageNamed:@"NotExpanded.png"];
        //
        CGRect frameCode = self.labelDescription.frame;
        frameCode.size.width = 0;
        self.labelDescription.frame = frameCode;
        
        CGRect frameGuideCode = self.labelGuideName.frame;
        frameGuideCode.origin.x = self.labelDescription.frame.origin.x;
        self.labelGuideName.frame = frameGuideCode;
        
        [combinedMutableAttributeString appendAttributedString:countMutableAttributedString];
        self.labelGuideName.attributedText = combinedMutableAttributeString;
        
        
    }
    else if ([countString isEqualToString:@"0"] && [tempDescription isEqualToString:@"Nr.audio"])
    {
        self.labelSubArrayCount.hidden = YES;
         self.labelSubArrayCount.text = @"no subRooms";
        self.imageViewAddition.hidden = NO;
        self.imageViewAddition.image = [UIImage imageNamed:@"navigate-normal.png"];
        
        NSString *  tempStringTemp = @" 1";
        NSAttributedString *countMutableAttributedString1 = [[NSAttributedString alloc] initWithString:tempStringTemp];
        
        NSMutableAttributedString *combinedMutableAttributeString1 = [[NSMutableAttributedString alloc]initWithAttributedString:secondDescriptionText];
        [combinedMutableAttributeString1 appendAttributedString:countMutableAttributedString1];
        self.labelGuideName.attributedText = combinedMutableAttributeString1;
        
    }
    else
    {
        self.labelSubArrayCount.hidden = YES;
         self.labelSubArrayCount.text = @"no subRooms";
        self.labelGuideName.attributedText = secondDescriptionText;
        self.imageViewAddition.image = [UIImage imageNamed:@"navigate-normal.png"];
        
    }
    countMutableAttributedString = nil;
    
    self.imageView.image = [UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@",[IBGlobal createDocumentsDirectoryByAppendingPathComponent:nil],thumbnailPath]];
    
    
    if (isSelectedCell)
    {
        self.labelGuideName.textColor = [UIColor grayColor];
        self.customTitleLabel.textColor = [UIColor whiteColor];
        self.labelDescription.textColor = [UIColor grayColor];
       self.backgroundColor = [IBGlobal colorWithHexString:@"699098"];
        //  self.imageViewAddition.image = [UIImage imageNamed:@"navigate-me-highlighted.png"];
    }
    else
    {
        self.labelGuideName.textColor = [UIColor grayColor];
        self.customTitleLabel.textColor = [UIColor blackColor];
        self.labelDescription.textColor = [UIColor grayColor];
        self.backgroundColor = [UIColor clearColor];
        //   self.imageViewAddition.image = [UIImage imageNamed:@"navigate-normal.png"];
    }
    
    if (level == 0) self.backgroundColor = [UIColor whiteColor];
    else if (level == 1) self.backgroundColor = [IBGlobal colorWithHexString:@"dadedf"];
    else if (level >= 2) self.backgroundColor = [UIColor whiteColor];
    //
    NSLog(@"title = %@", title);
    NSLog(@"descText = %@", descText);
    NSLog(@"thumbnailPath = %@", thumbnailPath);
    NSLog(@"self.imageView.image = %@", self.imageView.image);
}


#pragma mark - Properties

- (void)setAdditionButtonHidden:(BOOL)additionButtonHidden cellInfo:(RATableViewCelliphone *)cell
{
    if (additionButtonHidden)
    {
        self.imageViewAddition.image = [UIImage imageNamed:@"Expanded.png"];
    }
    else
    {
        self.imageViewAddition.image = [UIImage imageNamed:@"NotExpanded.png"];
    }
}

- (void)setAdditionButtonHidden:(BOOL)additionButtonHidden animated:(BOOL)animated
{
    _additionButtonHidden = additionButtonHidden;
    [UIView animateWithDuration:animated ? 0.2 : 0 animations:^{
        // self.additionButton.hidden = additionButtonHidden;
    }];
}
-(void)selectedCellInRows
{
    self.backgroundColor = [IBGlobal colorWithHexString:@"699098"];
    self.customTitleLabel.textColor = [UIColor whiteColor];
    self.labelDescription.textColor = [UIColor whiteColor];
    self.labelGuideName.textColor = [UIColor whiteColor];
    self.imageViewAddition.image = [UIImage imageNamed:@"navigate-me-highlighted.png"];
    
    
}

-(void)UnselectedCell:(BOOL)isInnerArray
{
    if (isInnerArray)
    {
        self.backgroundColor =[UIColor whiteColor];
    }
    else
    {
         self.backgroundColor = [IBGlobal colorWithHexString:@"dadedf"];// blue color
    }
    
    
    
    self.customTitleLabel.textColor = [UIColor blackColor];
    self.labelDescription.textColor = [UIColor grayColor];
    self.labelGuideName.textColor = [UIColor grayColor];
    
    
    self.imageViewAddition.image = [UIImage imageNamed:@"navigate-normal.png"];
    
}
#pragma mark - Actions

- (IBAction)additionButtonTapped:(id)sender
{
    if (self.additionButtonTapAction) {
        self.additionButtonTapAction(sender);
    }
}

@end
