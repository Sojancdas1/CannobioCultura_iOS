//
//  UIButton+Extensions.h
//  Famiglia Boroli
//
//  Created by bijinhkarim on 29/10/13.
//  Copyright (c) 2013 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Extensions)
@property(nonatomic, assign) UIEdgeInsets hitTestEdgeInsets;
@end
