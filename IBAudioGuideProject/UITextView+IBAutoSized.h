//
//  UITextView+IBAutoSized.h
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 21/05/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextView (IBAutoSized)

- (void)makeHeightForAttributedText:(NSAttributedString *)text;

@end
