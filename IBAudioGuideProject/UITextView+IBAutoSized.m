//
//  UITextView+IBAutoSized.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 21/05/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "UITextView+IBAutoSized.h"

@implementation UITextView (IBAutoSized)

- (void)makeHeightForAttributedText:(NSAttributedString *)text
{
    UITextView *textView = [[UITextView alloc] init];
    [textView setAttributedText:text];
    CGSize destSize = [textView sizeThatFits:CGSizeMake(self.frame.size.width, FLT_MAX)];
    CGRect frameRect = self.frame;
    frameRect.size.height  = destSize.height;
    self.frame = frameRect;
    textView = nil;
}

@end
