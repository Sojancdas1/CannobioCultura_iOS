//
//  UIView+InterPolation.h
//  IBAudioGuideProject
//
//  Created by T T Marshel Daniel on 14/03/16.
//  Copyright © 2016 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (InterPolation)

-(void)addNaturalOnTopEffectWithMaximumRelativeValue:(CGFloat)maximumRealtiveValue;
-(void)addNaturalBelowEffectWithMaximumRelativeValue:(CGFloat)maximumRealtiveValue;
- (void)removeMotionEffects;
@end
