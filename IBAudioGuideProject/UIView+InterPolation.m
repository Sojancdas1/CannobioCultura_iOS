//
//  UIView+InterPolation.m
//  IBAudioGuideProject
//
//  Created by T T Marshel Daniel on 14/03/16.
//  Copyright © 2016 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "UIView+InterPolation.h"

@implementation UIView (InterPolation)

-(void)addNaturalOnTopEffectWithMaximumRelativeValue:(CGFloat)maximumRealtiveValue {
    //
    UIInterpolatingMotionEffect* motionEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    motionEffect.minimumRelativeValue = @(maximumRealtiveValue);
    motionEffect.maximumRelativeValue = @(-maximumRealtiveValue);
    [self addMotionEffect:motionEffect];
    motionEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    motionEffect.minimumRelativeValue = @(maximumRealtiveValue);
    motionEffect.maximumRelativeValue = @(-maximumRealtiveValue);
    [self addMotionEffect:motionEffect];
}

-(void)addNaturalBelowEffectWithMaximumRelativeValue:(CGFloat)maximumRealtiveValue {
    //
    UIInterpolatingMotionEffect* motionEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    motionEffect.minimumRelativeValue = @(-maximumRealtiveValue);
    motionEffect.maximumRelativeValue = @(maximumRealtiveValue);
    [self addMotionEffect:motionEffect];
    motionEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    motionEffect.minimumRelativeValue = @(-maximumRealtiveValue);
    motionEffect.maximumRelativeValue = @(maximumRealtiveValue);
    [self addMotionEffect:motionEffect];
}

- (void)removeMotionEffects {
    //
    NSArray *motionEffects = [NSArray arrayWithArray:self.motionEffects];
    [motionEffects enumerateObjectsUsingBlock:^(UIMotionEffect *motionEffect, NSUInteger idx, BOOL *stop) {
        [self removeMotionEffect:motionEffect];
    }]; 
}


@end
