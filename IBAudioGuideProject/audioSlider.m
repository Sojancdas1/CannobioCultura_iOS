//
//  audioSlider.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 16/01/15.
//  Copyright (c) 2015 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "audioSlider.h"
#import "IBAppDelegate.h"
#import "IBGlobal.h"

@implementation audioSlider
{
    MPMusicPlayerController *iPod;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        
        
        
        
        //
        //        [[NSNotificationCenter defaultCenter]
        //         addObserver:self
        //         selector:@selector(volumeChanged:)
        //         name:@"AVSystemController_SystemVolumeDidChangeNotification"
        //         object:nil];
        
        
        
        UISlider *sliderDemo = [[UISlider alloc] initWithFrame:CGRectMake(-70, 82, 180, 40)];
        CGAffineTransform trans = CGAffineTransformMakeRotation(4.71);
        sliderDemo.transform = trans;
        [sliderDemo addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];
        [sliderDemo setTintColor:[UIColor whiteColor]];
        iPod = [MPMusicPlayerController iPodMusicPlayer];
        float volumeLevel = iPod.volume;
        //  //NSLog(@"%f",volumeLevel);
        
        
        // 最大值
        sliderDemo.minimumValue = 0;
        sliderDemo.maximumValue = 1;
        //  //NSLog(@"%f",[SharedAppDelegate avAudioPlayer].volume);
        sliderDemo.value = volumeLevel;
        
        [self addSubview:sliderDemo];
    }
    return self;
}

- (IBAction)sliderValueChanged:(UISlider *)sliderTemp
{
    
    //  [SharedAppDelegate avAudioPlayer].volume = sliderTemp.value;
    iPod.volume = sliderTemp.value;
    
}

//- (void)volumeChanged:(NSNotification *)notification
//{
//    float volume =
//    [[[notification userInfo]
//      objectForKey:@"AVSystemController_AudioVolumeNotificationParameter"]
//     floatValue];
//
//    // Do stuff with volume
//}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
