//
//  main.m
//  IBAudioGuideProject
//
//  Created by Mobility 2014 on 07/05/14.
//  Copyright (c) 2014 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IBAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IBAppDelegate class]));
    }
}