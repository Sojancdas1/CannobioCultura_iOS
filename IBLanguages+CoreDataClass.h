//
//  IBLanguages+CoreDataClass.h
//  IBAudioGuideProject
//
//  Created by E-Team Mac on 3/20/17.
//  Copyright © 2017 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface IBLanguages : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "IBLanguages+CoreDataProperties.h"
