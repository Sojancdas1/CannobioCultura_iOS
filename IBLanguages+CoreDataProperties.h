//
//  IBLanguages+CoreDataProperties.h
//  IBAudioGuideProject
//
//  Created by E-Team Mac on 3/20/17.
//  Copyright © 2017 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBLanguages+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface IBLanguages (CoreDataProperties)

+ (NSFetchRequest<IBLanguages *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *displayName;
@property (nullable, nonatomic, copy) NSString *resourcePath;
@property (nullable, nonatomic, copy) NSNumber *selected;
@property (nullable, nonatomic, copy) NSNumber *order;

@end

NS_ASSUME_NONNULL_END
