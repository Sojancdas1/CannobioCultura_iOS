//
//  IBLanguages+CoreDataProperties.m
//  IBAudioGuideProject
//
//  Created by E-Team Mac on 3/20/17.
//  Copyright © 2017 E-Team Informatica India Pvt. Ltd. All rights reserved.
//

#import "IBLanguages+CoreDataProperties.h"

@implementation IBLanguages (CoreDataProperties)

+ (NSFetchRequest<IBLanguages *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"IBLanguages"];
}

@dynamic displayName;
@dynamic resourcePath;
@dynamic selected;
@dynamic order;

@end
